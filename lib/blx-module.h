/*
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_MODULE_H
#define BLX_MODULE_H

#include <glib-object.h>

G_BEGIN_DECLS

#define BLX_TYPE_MODULE		(blx_module_get_type ())
#define BLX_MODULE(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), BLX_TYPE_MODULE, BlxModule))
#define BLX_MODULE_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_MODULE, BlxModuleClass))
#define BLX_IS_MODULE(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_MODULE))
#define BLX_IS_MODULE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), BLX_TYPE_MODULE))
#define BLX_MODULE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), BLX_TYPE_MODULE, BlxModuleClass))

typedef struct _BlxModule	BlxModule;

GType		 blx_module_get_type	(void);

BlxModule	*blx_module_new	(const char *path,
					 gboolean resident);

const char	*blx_module_get_path	(BlxModule *module);

GObject		*blx_module_new_object	(BlxModule *module);

G_END_DECLS

#endif
