/*
 *  Copyright © 2000-2003 Marco Pesenti Gritti
 *  Copyright © 2010 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_PREFS_H
#define BLX_PREFS_H

G_BEGIN_DECLS

typedef enum
{
	BLX_PREFS_UI_TOOLBAR_STYLE_BOTH,
	BLX_PREFS_UI_TOOLBAR_STYLE_BOTH_HORIZ,
	BLX_PREFS_UI_TOOLBAR_STYLE_ICONS,
	BLX_PREFS_UI_TOOLBAR_STYLE_TEXT
} BlxPrefsUIToolbarStyle;

typedef enum
{
	BLX_PREFS_WEB_COOKIES_POLICY_ALWAYS,
	BLX_PREFS_WEB_COOKIES_POLICY_NO_THIRD_PARTY,
	BLX_PREFS_WEB_COOKIES_POLICY_NEVER
} BlxPrefsWebCookiesPolicy;

typedef enum
{
	BLX_PREFS_STATE_HISTORY_DATE_FILTER_LAST_HALF_HOUR,
	BLX_PREFS_STATE_HISTORY_DATE_FILTER_EVER,
	BLX_PREFS_STATE_HISTORY_DATE_FILTER_TODAY,
	BLX_PREFS_STATE_HISTORY_DATE_FILTER_LAST_TWO_DAYS,
	BLX_PREFS_STATE_HISTORY_DATE_FILTER_LAST_THREE_DAYS
} BlxPrefsStateHistoryDateFilter;

#define BLX_PREFS_UI_SCHEMA			"org.gnome.Blxbrowser.ui"
#define BLX_PREFS_UI_ALWAYS_SHOW_TABS_BAR	"always-show-tabs-bar"
#define BLX_PREFS_UI_SHOW_TOOLBARS		"show-toolbars"
#define BLX_PREFS_UI_SHOW_BOOKMARKS_BAR	"show-bookmarks-bar"
#define BLX_PREFS_UI_TOOLBAR_STYLE		"toolbar-style"
#define BLX_PREFS_UI_DOWNLOADS_HIDDEN		"downloads-hidden"

#define BLX_PREFS_STATE_SCHEMA			"org.gnome.Blxbrowser.state"
#define BLX_PREFS_STATE_SAVE_DIR		"save-dir"
#define BLX_PREFS_STATE_SAVE_IMAGE_DIR		"save-image-dir"
#define BLX_PREFS_STATE_OPEN_DIR		"open-dir"
#define BLX_PREFS_STATE_DOWNLOAD_DIR		"download-dir"
#define BLX_PREFS_STATE_UPLOAD_DIR		"upload-dir"
#define BLX_PREFS_STATE_RECENT_ENCODINGS	"recent-encodings"
#define BLX_PREFS_STATE_BOOKMARKS_VIEW_TITLE	"bookmarks-view-title"
#define BLX_PREFS_STATE_BOOKMARKS_VIEW_ADDRESS	"bookmarks-view-address"
#define BLX_PREFS_STATE_HISTORY_VIEW_TITLE	"history-view-title"
#define BLX_PREFS_STATE_HISTORY_VIEW_ADDRESS	"history-view-address"
#define BLX_PREFS_STATE_HISTORY_VIEW_DATE	"history-view-date"
#define BLX_PREFS_STATE_HISTORY_DATE_FILTER	"history-date-filter"

#define BLX_PREFS_WEB_SCHEMA			"org.gnome.Blxbrowser.web"
#define BLX_PREFS_WEB_FONT_MIN_SIZE		"min-font-size"
#define BLX_PREFS_WEB_LANGUAGE			"language"
#define BLX_PREFS_WEB_USE_OWN_FONTS		"use-own-fonts"
#define BLX_PREFS_WEB_USE_GNOME_FONTS		"use-gnome-fonts"
#define BLX_PREFS_WEB_SANS_SERIF_FONT		"sans-serif-font"
#define BLX_PREFS_WEB_SERIF_FONT		"serif-font"
#define BLX_PREFS_WEB_MONOSPACE_FONT		"monospace-font"
#define BLX_PREFS_WEB_USE_OWN_COLORS		"use-own-colors"
#define BLX_PREFS_WEB_ENABLE_USER_CSS		"enable-user-css"
#define BLX_PREFS_WEB_ENABLE_POPUPS		"enable-popups"
#define BLX_PREFS_WEB_ENABLE_PLUGINS		"enable-plugins"
#define BLX_PREFS_WEB_ENABLE_JAVASCRIPT	"enable-javascript"
#define BLX_PREFS_WEB_ENABLE_SPELL_CHECKING	"enable-spell-checking"
#define BLX_PREFS_WEB_COOKIES_POLICY		"cookies-policy"
#define BLX_PREFS_WEB_IMAGE_ANIMATION_MODE	"image-animation-mode"
#define BLX_PREFS_WEB_DEFAULT_ENCODING		"default-encoding"

#define BLX_PREFS_SCHEMA			"org.gnome.Blxbrowser"
#define BLX_PREFS_USER_AGENT			"user-agent"
#define BLX_PREFS_CACHE_SIZE			"cache-size"
#define BLX_PREFS_NEW_WINDOWS_IN_TABS		"new-windows-in-tabs"
#define BLX_PREFS_AUTO_DOWNLOADS		"automatic-downloads"
#define BLX_PREFS_WARN_ON_CLOSE_UNSUBMITTED_DATA	"warn-on-close-unsubmitted-data"
#define BLX_PREFS_MIDDLE_CLICK_OPENS_URL	"middle-click-opens-url"
#define BLX_PREFS_REMEMBER_PASSWORDS		"remember-passwords"
#define BLX_PREFS_KEYWORD_SEARCH_URL		"keyword-search-url"
#define BLX_PREFS_MANAGED_NETWORK		"managed-network"
#define BLX_PREFS_ENABLE_SMOOTH_SCROLLING	"enable-smooth-scrolling"
#define BLX_PREFS_ENABLE_CARET_BROWSING	"enable-caret-browsing"
#define BLX_PREFS_ENABLED_EXTENSIONS		"enabled-extensions"
#define BLX_PREFS_INTERNAL_VIEW_SOURCE		"internal-view-source"

#define BLX_PREFS_LOCKDOWN_SCHEMA		"org.gnome.Blxbrowser.lockdown"
#define BLX_PREFS_LOCKDOWN_FULLSCREEN		"disable-fullscreen"
#define BLX_PREFS_LOCKDOWN_ARBITRARY_URL	"disable-arbitrary-url"
#define BLX_PREFS_LOCKDOWN_BOOKMARK_EDITING	"disable-bookmark-editing"
#define BLX_PREFS_LOCKDOWN_HISTORY		"disable-history"
#define BLX_PREFS_LOCKDOWN_SAVE_TO_DISK	"disable-save-to-disk"
#define BLX_PREFS_LOCKDOWN_PRINTING		"disable-printing"
#define BLX_PREFS_LOCKDOWN_PRINT_SETUP		"disable-print-setup"
#define BLX_PREFS_LOCKDOWN_COMMAND_LINE	"disable-command-line"
#define BLX_PREFS_LOCKDOWN_QUIT		"disable-quit"
#define BLX_PREFS_LOCKDOWN_JAVASCRIPT_CHROME	"disable-javascript-chrome"
#define BLX_PREFS_LOCKDOWN_MENUBAR		"disable-menubar"

G_END_DECLS

#endif
