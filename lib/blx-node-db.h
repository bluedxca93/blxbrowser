/*
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_NODE_DB_H
#define BLX_NODE_DB_H

#include <glib-object.h>

G_BEGIN_DECLS

#define BLX_TYPE_NODE_DB	  (blx_node_db_get_type ())
#define BLX_NODE_DB(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_NODE_DB, BlxNodeDb))
#define BLX_NODE_DB_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_NODE_DB, BlxNodeDbClass))
#define BLX_IS_NODE_DB(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_NODE_DB))
#define BLX_IS_NODE_DB_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_NODE_DB))
#define BLX_NODE_DB_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_NODE_DB, BlxNodeDbClass))

typedef struct _BlxNodeDb BlxNodeDb;
typedef struct _BlxNodeDbPrivate BlxNodeDbPrivate;

struct _BlxNodeDb
{
	GObject parent;

	/*< private >*/
	BlxNodeDbPrivate *priv;
};

typedef struct
{
	GObjectClass parent;

} BlxNodeDbClass;

#include "blx-node.h"

GType         blx_node_db_get_type		(void);

BlxNodeDb   *blx_node_db_new			(const char *name);

gboolean      blx_node_db_load_from_file	(BlxNodeDb *db,
						 const char *xml_file,
						 const xmlChar *xml_root,
						 const xmlChar *xml_version);

int           blx_node_db_write_to_xml_safe	(BlxNodeDb *db,
						 const xmlChar *filename,
						 const xmlChar *root,
						 const xmlChar *version,
						 const xmlChar *comment,
						 BlxNode *node, ...);

const char   *blx_node_db_get_name		(BlxNodeDb *db);

gboolean      blx_node_db_is_immutable		(BlxNodeDb *db);

void	      blx_node_db_set_immutable	(BlxNodeDb *db,
						 gboolean immutable);

BlxNode     *blx_node_db_get_node_from_id	(BlxNodeDb *db,
						 guint id);

guint	      _blx_node_db_new_id		(BlxNodeDb *db);

void	      _blx_node_db_add_id		(BlxNodeDb *db,
						 guint id,
						 BlxNode *node);

void	      _blx_node_db_remove_id		(BlxNodeDb *db,
						 guint id);

G_END_DECLS

#endif /* __BLX_NODE_DB_H */
