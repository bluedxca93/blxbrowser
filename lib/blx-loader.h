/*
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_LOADER_H
#define BLX_LOADER_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define BLX_TYPE_LOADER		(blx_loader_get_type ())
#define BLX_LOADER(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), BLX_TYPE_LOADER, BlxLoader))
#define BLX_LOADER_IFACE(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_LOADER, BlxLoaderIface))
#define BLX_IS_LOADER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_LOADER))
#define BLX_IS_LOADER_IFACE(iface)	(G_TYPE_CHECK_CLASS_TYPE ((iface), BLX_TYPE_LOADER))
#define BLX_LOADER_GET_IFACE(inst)	(G_TYPE_INSTANCE_GET_INTERFACE ((inst), BLX_TYPE_LOADER, BlxLoaderIface))

typedef struct _BlxLoader		BlxLoader;
typedef struct _BlxLoaderIface	BlxLoaderIface;
	
struct _BlxLoaderIface
{
	GTypeInterface base_iface;

	/* Identifier */
	const char *type;

	/* Methods */
	GObject *    (* get_object)	(BlxLoader *loader,
					 GKeyFile *keyfile);
	void	     (* release_object)	(BlxLoader *loader,
					 GObject *object);
};

GType	    blx_loader_get_type	(void);

const char *blx_loader_type		(const BlxLoader *loader);

GObject    *blx_loader_get_object	(BlxLoader *loader,
					 GKeyFile *keyfile);

void	    blx_loader_release_object	(BlxLoader *loader,
					 GObject *object);

G_END_DECLS

#endif
