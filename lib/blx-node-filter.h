/*
 *  Copyright © 2002 Olivier Martin <omartin@ifrance.com>
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_NODE_FILTER_H
#define BLX_NODE_FILTER_H

#include <glib-object.h>

#include "blx-node.h"

G_BEGIN_DECLS

#define BLX_TYPE_NODE_FILTER         (blx_node_filter_get_type ())
#define BLX_NODE_FILTER(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_NODE_FILTER, BlxNodeFilter))
#define BLX_NODE_FILTER_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_NODE_FILTER, BlxNodeFilterClass))
#define BLX_IS_NODE_FILTER(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_NODE_FILTER))
#define BLX_IS_NODE_FILTER_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_NODE_FILTER))
#define BLX_NODE_FILTER_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_NODE_FILTER, BlxNodeFilterClass))

typedef struct _BlxNodeFilterPrivate BlxNodeFilterPrivate;

typedef struct
{
	GObject parent;

	/*< private >*/
	BlxNodeFilterPrivate *priv;
} BlxNodeFilter;

typedef struct
{
	GObjectClass parent;

	void (*changed) (BlxNodeFilter *filter);
} BlxNodeFilterClass;

typedef enum
{
	BLX_NODE_FILTER_EXPRESSION_ALWAYS_TRUE,           /* args: none */
	BLX_NODE_FILTER_EXPRESSION_NODE_EQUALS,           /* args: BlxNode *a, BlxNode *b */
	BLX_NODE_FILTER_EXPRESSION_EQUALS,                /* args: BlxNode *node */
	BLX_NODE_FILTER_EXPRESSION_HAS_PARENT,            /* args: BlxNode *parent */
	BLX_NODE_FILTER_EXPRESSION_HAS_CHILD,             /* args: BlxNode *child */
	BLX_NODE_FILTER_EXPRESSION_NODE_PROP_EQUALS,      /* args: int prop_id, BlxNode *node */
	BLX_NODE_FILTER_EXPRESSION_CHILD_PROP_EQUALS,     /* args: int prop_id, BlxNode *node */
	BLX_NODE_FILTER_EXPRESSION_STRING_PROP_CONTAINS,  /* args: int prop_id, const char *string */
	BLX_NODE_FILTER_EXPRESSION_STRING_PROP_EQUALS,    /* args: int prop_id, const char *string */
	BLX_NODE_FILTER_EXPRESSION_KEY_PROP_CONTAINS,     /* args: int prop_id, const char *string */
	BLX_NODE_FILTER_EXPRESSION_KEY_PROP_EQUALS,       /* args: int prop_id, const char *string */
	BLX_NODE_FILTER_EXPRESSION_INT_PROP_EQUALS,       /* args: int prop_id, int int */
	BLX_NODE_FILTER_EXPRESSION_INT_PROP_BIGGER_THAN,  /* args: int prop_id, int int */
	BLX_NODE_FILTER_EXPRESSION_INT_PROP_LESS_THAN     /* args: int prop_id, int int */
} BlxNodeFilterExpressionType;

typedef struct _BlxNodeFilterExpression BlxNodeFilterExpression;

/* The filter starts iterating over all expressions at level 0,
 * if one of them is TRUE it continues to level 1, etc.
 * If it still has TRUE when there are no more expressions at the
 * next level, the result is TRUE. Otherwise, it's FALSE.
 */

GType           blx_node_filter_get_type       (void);

BlxNodeFilter *blx_node_filter_new            (void);

void            blx_node_filter_add_expression (BlxNodeFilter *filter,
					         BlxNodeFilterExpression *expression,
					         int level);

void            blx_node_filter_empty          (BlxNodeFilter *filter);

void            blx_node_filter_done_changing  (BlxNodeFilter *filter);

gboolean        blx_node_filter_evaluate       (BlxNodeFilter *filter,
					         BlxNode *node);

BlxNodeFilterExpression *blx_node_filter_expression_new  (BlxNodeFilterExpressionType,
						            ...);
/* no need to free unless you didn't add the expression to a filter */
void                      blx_node_filter_expression_free (BlxNodeFilterExpression *expression);

G_END_DECLS

#endif /* BLX_NODE_FILTER_H */
