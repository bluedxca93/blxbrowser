/*
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include "blx-loader.h"

GType
blx_loader_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0))
	{
		const GTypeInfo our_info =
		{
			sizeof (BlxLoaderIface),
			NULL,
			NULL,
		};
	
		type = g_type_register_static (G_TYPE_INTERFACE,
					       "BlxLoader",
					       &our_info, 0);
	}

	return type;
}

const char *
blx_loader_type (const BlxLoader *loader)
{
	BlxLoaderIface *iface = BLX_LOADER_GET_IFACE (loader);
	return iface->type;
}

GObject *
blx_loader_get_object (BlxLoader *loader,
			GKeyFile *keyfile)
{
	BlxLoaderIface *iface = BLX_LOADER_GET_IFACE (loader);
	return iface->get_object (loader, keyfile);
}

void
blx_loader_release_object (BlxLoader *loader,
			     GObject *object)
{
	BlxLoaderIface *iface = BLX_LOADER_GET_IFACE (loader);
	iface->release_object (loader, object);
}
