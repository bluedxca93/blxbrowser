/*
 *  Copyright © 2001 Matthew Mueller
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *  Copyright © 2003 Marco Pesenti Gritti <mpeseng@tin.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_STATE_H
#define BLX_STATE_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef enum
{
	BLX_STATE_WINDOW_SAVE_NONE = 0,
	BLX_STATE_WINDOW_SAVE_SIZE = 1 << 0,
	BLX_STATE_WINDOW_SAVE_POSITION = 1 << 1
} BlxStateWindowFlags;

void  blx_state_add_window	(GtkWidget *window,
				 const char *name,
				 int default_width,
				 int default_heigth,
				 gboolean maximize,
				 BlxStateWindowFlags flags);

void  blx_state_add_paned	(GtkWidget *paned,
				 const char *name,
				 int default_width);

void  blx_state_add_expander	(GtkWidget *widget,
				 const char *name,
				 gboolean default_state);

void  blx_state_save		(void);

G_END_DECLS

#endif /* BLX_STATE_H */
