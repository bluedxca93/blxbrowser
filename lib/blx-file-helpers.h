/*
 *  Copyright © 2002 Jorn Baayen
 *  Copyright © 2003, 2004 Marco Pesenti Gritti
 *  Copyright © 2004, 2005, 2006 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_FILE_HELPERS_H
#define BLX_FILE_HELPERS_H

#include <glib.h>
#include <gio/gio.h>
#include <gtk/gtk.h>

/* From libgnome */
#define GNOME_DOT_GNOME ".gnome2"

extern GQuark blx_file_helpers_error_quark;
#define BLX_FILE_HELPERS_ERROR_QUARK	(blx_file_helpers_error_quark)

G_BEGIN_DECLS

typedef enum
{
	BLX_MIME_PERMISSION_SAFE	= 1,
	BLX_MIME_PERMISSION_UNSAFE	= 2,
	BLX_MIME_PERMISSION_UNKNOWN	= 3
} BlxMimePermission;

#define BLX_UUID_ENVVAR	"BLX_UNIQUE"

gboolean           blx_file_helpers_init        (const char  *profile_dir,
                                                  gboolean     private_profile,
                                                  gboolean     keep_temp_dir,
                                                  GError     **error);
const char *       blx_file                     (const char  *filename);
const char *       blx_dot_dir                  (void);
void               blx_file_helpers_shutdown    (void);
char	   *       blx_file_get_downloads_dir   (void);
char       *       blx_file_desktop_dir         (void);
const char *       blx_file_tmp_dir             (void);
char       *       blx_file_tmp_filename        (const char  *base,
                                                  const char  *extension);
gboolean           blx_ensure_dir_exists        (const char  *dir,
                                                  GError **);
GSList     *       blx_file_find                (const char  *path,
                                                  const char  *fname,
                                                  gint         maxdepth);
gboolean           blx_file_switch_temp_file    (GFile       *file_dest,
                                                  GFile       *file_temp);
void               blx_file_delete_on_exit      (GFile       *file);
void               blx_file_add_recent_item     (const char  *uri,
                                                  const char  *mime_type);
BlxMimePermission blx_file_check_mime          (const char  *mime_type);
gboolean           blx_file_launch_desktop_file (const char  *filename,
                                                  const char  *parameter,
                                                  guint32      user_time,
                                                  GtkWidget   *widget);
gboolean           blx_file_launch_application  (GAppInfo    *app,
                                                  GList       *files,
                                                  guint32      user_time,
                                                  GtkWidget   *widget);
gboolean           blx_file_launch_handler      (const char  *mime_type,
                                                  GFile       *file,
                                                  guint32      user_time);
gboolean           blx_file_browse_to           (GFile       *file,
                                                  guint32      user_time);
gboolean           blx_file_delete_dir_recursively (GFile *file,
                                                     GError      **error);
void               blx_file_delete_uri          (const char  *uri);
void               blx_file_load_accels (void);
void               blx_file_save_accels (void);

G_END_DECLS

#endif /* BLX_FILE_HELPERS_H */
