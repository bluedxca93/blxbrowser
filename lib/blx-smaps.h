/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 *  Copyright © 2011 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#ifndef BLX_SMAPS_H
#define BLX_SMAPS_H

#include <glib-object.h>

#define BLX_TYPE_SMAPS            (blx_smaps_get_type ())
#define BLX_SMAPS(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), BLX_TYPE_SMAPS, BlxSMaps))
#define BLX_SMAPS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_SMAPS, BlxSMapsClass))
#define BLX_IS_SMAPS(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), BLX_TYPE_SMAPS))
#define BLX_IS_SMAPS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), BLX_TYPE_SMAPS))
#define BLX_SMAPS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), BLX_TYPE_SMAPS, BlxSMapsClass))

typedef struct _BlxSMapsPrivate BlxSMapsPrivate;

typedef struct {
  GObject parent;

  BlxSMapsPrivate *priv;
} BlxSMaps;

typedef struct {
  GObjectClass parent;

} BlxSMapsClass;

GType       blx_smaps_get_type (void);
BlxSMaps * blx_smaps_new      (void);
char      * blx_smaps_to_html  (BlxSMaps *smaps);

#endif /* BLX_SMAPS_H */
