/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set sw=2 ts=2 sts=2 et: */
/*
 *  Copyright © 2010 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_SETTINGS_H
#define BLX_SETTINGS_H

#include <glib.h>
#include <gio/gio.h>

#include "blx-prefs.h"

#define BLX_SETTINGS_MAIN      blx_settings_get (BLX_PREFS_SCHEMA)
#define BLX_SETTINGS_UI        blx_settings_get (BLX_PREFS_UI_SCHEMA)
#define BLX_SETTINGS_WEB       blx_settings_get (BLX_PREFS_WEB_SCHEMA)
#define BLX_SETTINGS_LOCKDOWN  blx_settings_get (BLX_PREFS_LOCKDOWN_SCHEMA)
#define BLX_SETTINGS_STATE     blx_settings_get (BLX_PREFS_STATE_SCHEMA)

GSettings *blx_settings_get (const char *schema);

void blx_settings_shutdown (void);

#endif /* BLX_SETTINGS_H */
