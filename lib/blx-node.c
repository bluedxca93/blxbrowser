/* 
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <gdk/gdk.h>
#include <time.h>

#include "blx-node.h"

typedef struct
{
	BlxNode *node;
	int id;
	BlxNodeCallback callback;
	BlxNodeSignalType type;
	gpointer data;
	gboolean invalidated;
} BlxNodeSignalData;

typedef struct
{
	BlxNode *node;
	guint index;
} BlxNodeParent;

typedef struct
{
	BlxNode *node;
	guint property_id;
} BlxNodeChange;

struct _BlxNode
{
	int ref_count;

	guint id;

	GPtrArray *properties;

	GHashTable *parents;
	GPtrArray *children;

	GHashTable *signals;
	int signal_id;
	guint emissions;
	guint invalidated_signals;
	guint is_drag_source : 1;
	guint is_drag_dest : 1;

	BlxNodeDb *db;
};

typedef struct
{
	BlxNodeSignalType type;
	va_list valist;
} ENESCData;

static gboolean
int_equal (gconstpointer a,
	   gconstpointer b)
{
	return GPOINTER_TO_INT (a) == GPOINTER_TO_INT (b);
}

static guint
int_hash (gconstpointer a)
{
	return GPOINTER_TO_INT (a);
}

static void
callback (long id, BlxNodeSignalData *data, gpointer *dummy)
{
	ENESCData *user_data;
	va_list valist;

	if (data->invalidated) return;

	user_data = (ENESCData *) dummy;

	G_VA_COPY(valist, user_data->valist);

	if (data->type != user_data->type) return;

	switch (data->type)
	{
		case BLX_NODE_DESTROY:
		case BLX_NODE_RESTORED:
			data->callback (data->node, data->data);
		break;

		case BLX_NODE_CHANGED:
		{
			guint property_id;

			property_id = va_arg (valist, guint);
		
			data->callback (data->node, property_id, data->data);
		}
		break;

		case BLX_NODE_CHILD_ADDED:
		{
			BlxNode *node;

			node = va_arg (valist, BlxNode *);
		
			data->callback (data->node, node, data->data);
		}
		break;

		case BLX_NODE_CHILD_CHANGED:
		{
			BlxNode *node;
			guint property_id;

			node = va_arg (valist, BlxNode *);
			property_id = va_arg (valist, guint);
		
			data->callback (data->node, node, property_id, data->data);
		}
		break;

		case BLX_NODE_CHILD_REMOVED:
		{
			BlxNode *node;
			guint last_index;

			node = va_arg (valist, BlxNode *);
			last_index = va_arg (valist, guint);

			data->callback (data->node, node, last_index, data->data);
		}
		break;

		case BLX_NODE_CHILDREN_REORDERED:
			data->callback (data->node, va_arg (valist, int *), data->data);
		break;
	}

        va_end(valist);
}

static gboolean
remove_invalidated_signals (long id,
			    BlxNodeSignalData *data,
			    gpointer user_data)
{
	return data->invalidated;
}

static void
blx_node_emit_signal (BlxNode *node, BlxNodeSignalType type, ...)
{
	ENESCData data;

	++node->emissions;

	va_start (data.valist, type);

	data.type = type;

	g_hash_table_foreach (node->signals,
			      (GHFunc) callback,
			      &data);

	va_end (data.valist);

	if (G_UNLIKELY (--node->emissions == 0 && node->invalidated_signals))
	{
		int removed;

		removed = g_hash_table_foreach_remove
				(node->signals,
				 (GHRFunc) remove_invalidated_signals,
				 NULL);
		g_assert (removed == node->invalidated_signals);

		node->invalidated_signals = 0;
	}
}

static inline void
real_remove_child (BlxNode *node,
		   BlxNode *child,
		   gboolean remove_from_parent,
		   gboolean remove_from_child)
{
	BlxNodeParent *node_info;

	node_info = g_hash_table_lookup (child->parents,
			                 GINT_TO_POINTER (node->id));

	if (remove_from_parent) {
		guint i;
		guint old_index;

		old_index = node_info->index;

		g_ptr_array_remove_index (node->children,
					  node_info->index);

		/* correct indices on kids */
		for (i = node_info->index; i < node->children->len; i++) {
			BlxNode *borked_node;
			BlxNodeParent *borked_node_info;

			borked_node = g_ptr_array_index (node->children, i);


			borked_node_info = g_hash_table_lookup (borked_node->parents,
						                GINT_TO_POINTER (node->id));
			borked_node_info->index--;
		}

		blx_node_emit_signal (node, BLX_NODE_CHILD_REMOVED, child, old_index);
	}

	if (remove_from_child) {
		g_hash_table_remove (child->parents,
				     GINT_TO_POINTER (node->id));
	}
}

static void
remove_child (long id,
	      BlxNodeParent *node_info,
	      BlxNode *node)
{
	real_remove_child (node_info->node, node, TRUE, FALSE);
}

static void
signal_object_weak_notify (BlxNodeSignalData *signal_data,
                           GObject *where_the_object_was)
{
	signal_data->data = NULL;
	blx_node_signal_disconnect (signal_data->node, signal_data->id);
}

static void
destroy_signal_data (BlxNodeSignalData *signal_data)
{
	if (signal_data->data)
	{
		g_object_weak_unref (G_OBJECT (signal_data->data),
				     (GWeakNotify)signal_object_weak_notify,
				     signal_data);
	}
        
        g_slice_free (BlxNodeSignalData, signal_data);
}

static void
node_parent_free (BlxNodeParent *parent)
{
	g_slice_free (BlxNodeParent, parent);
}

static void
blx_node_destroy (BlxNode *node)
{
	guint i;

	blx_node_emit_signal (node, BLX_NODE_DESTROY);

        /* Remove from parents. */
	g_hash_table_foreach (node->parents,
			      (GHFunc) remove_child,
			      node);
	g_hash_table_destroy (node->parents);

        /* Remove children. */
	for (i = 0; i < node->children->len; i++) {
		BlxNode *child;

		child = g_ptr_array_index (node->children, i);

		real_remove_child (node, child, FALSE, TRUE);
	}
	g_ptr_array_free (node->children, TRUE);
        
        /* Remove signals. */
	g_hash_table_destroy (node->signals);

        /* Remove id. */
	_blx_node_db_remove_id (node->db, node->id);

        /* Remove properties. */
	for (i = 0; i < node->properties->len; i++) {
		GValue *val;

		val = g_ptr_array_index (node->properties, i);

		if (val != NULL) {
			g_value_unset (val);
			g_slice_free (GValue, val);
		}
	}
	g_ptr_array_free (node->properties, TRUE);

	g_slice_free (BlxNode, node);
}

BlxNode *
blx_node_new (BlxNodeDb *db)
{
	long id;

	g_return_val_if_fail (BLX_IS_NODE_DB (db), NULL);

	if (blx_node_db_is_immutable (db)) return NULL;

	id = _blx_node_db_new_id (db);

	return blx_node_new_with_id (db, id);
}

BlxNode *
blx_node_new_with_id (BlxNodeDb *db, guint reserved_id)
{
	BlxNode *node;

	g_return_val_if_fail (BLX_IS_NODE_DB (db), NULL);

	if (blx_node_db_is_immutable (db)) return NULL;

	node = g_slice_new0 (BlxNode);

	node->ref_count = 1;

	node->id = reserved_id;

	node->db = db;

	node->properties = g_ptr_array_new ();

	node->children = g_ptr_array_new ();

	node->parents = g_hash_table_new_full
          (int_hash, int_equal, NULL, (GDestroyNotify) node_parent_free);

	node->signals = g_hash_table_new_full
          (int_hash, int_equal, NULL,
           (GDestroyNotify)destroy_signal_data);

	node->signal_id = 0;
	node->emissions = 0;
	node->invalidated_signals = 0;
	node->is_drag_source = TRUE;
	node->is_drag_dest = TRUE;

	_blx_node_db_add_id (db, reserved_id, node);

	return node;
}

/**
 * blx_node_get_db:
 *
 * Return value: (transfer none):
 **/
BlxNodeDb *
blx_node_get_db (BlxNode *node)
{
	g_return_val_if_fail (BLX_IS_NODE (node), NULL);
	
	return node->db;
}

guint
blx_node_get_id (BlxNode *node)
{
	long ret;

	g_return_val_if_fail (BLX_IS_NODE (node), G_MAXUINT);

	ret = node->id;

	return ret;
}

void
blx_node_ref (BlxNode *node)
{
	g_return_if_fail (BLX_IS_NODE (node));

	node->ref_count++;
}

void
blx_node_unref (BlxNode *node)
{
	g_return_if_fail (BLX_IS_NODE (node));

	node->ref_count--;

	if (node->ref_count <= 0) {
		blx_node_destroy (node);
	}
}

static void
child_changed (guint id,
	       BlxNodeParent *node_info,
	       BlxNodeChange *change)
{
	blx_node_emit_signal (node_info->node, BLX_NODE_CHILD_CHANGED,
			       change->node, change->property_id);
}

static inline void
real_set_property (BlxNode *node,
		   guint property_id,
		   GValue *value)
{
	GValue *old;

	if (property_id >= node->properties->len) {
		g_ptr_array_set_size (node->properties, property_id + 1);
	}

	old = g_ptr_array_index (node->properties, property_id);
	if (old != NULL) {
		g_value_unset (old);
		g_slice_free (GValue, old);
	}

	g_ptr_array_index (node->properties, property_id) = value;
}

static inline void
blx_node_set_property_internal (BlxNode *node,
		        	 guint property_id,
		        	 GValue *value)
{
	BlxNodeChange change;

	real_set_property (node, property_id, value);

	change.node = node;
	change.property_id = property_id;
	g_hash_table_foreach (node->parents,
			      (GHFunc) child_changed,
			      &change);
    
	blx_node_emit_signal (node, BLX_NODE_CHANGED, property_id);

}

void
blx_node_set_property (BlxNode *node,
		        guint property_id,
		        const GValue *value)
{
	GValue *new;

	g_return_if_fail (BLX_IS_NODE (node));
	g_return_if_fail (value != NULL);

	if (blx_node_db_is_immutable (node->db)) return;

	new = g_slice_new0 (GValue);
	g_value_init (new, G_VALUE_TYPE (value));
	g_value_copy (value, new);

	blx_node_set_property_internal (node, property_id, new);
}

/**
 * blx_node_get_property:
 *
 * @value: (out):
 */
gboolean
blx_node_get_property (BlxNode *node,
		        guint property_id,
		        GValue *value)
{
	GValue *ret;

	g_return_val_if_fail (BLX_IS_NODE (node), FALSE);
	g_return_val_if_fail (value != NULL, FALSE);

	if (property_id >= node->properties->len) {
		return FALSE;
	}

	ret = g_ptr_array_index (node->properties, property_id);
	if (ret == NULL) {
		return FALSE;
	}

	g_value_init (value, G_VALUE_TYPE (ret));
	g_value_copy (ret, value);

	return TRUE;
}

void
blx_node_set_property_string (BlxNode *node,
			       guint property_id,
			       const char *value)
{
	GValue *new;

	g_return_if_fail (BLX_IS_NODE (node));

	if (blx_node_db_is_immutable (node->db)) return;

	new = g_slice_new0 (GValue);
	g_value_init (new, G_TYPE_STRING);
	g_value_set_string (new, value);

	blx_node_set_property_internal (node, property_id, new);
}

const char *
blx_node_get_property_string (BlxNode *node,
			       guint property_id)
{
	GValue *ret;
	const char *retval;

	g_return_val_if_fail (BLX_IS_NODE (node), NULL);

	if (property_id >= node->properties->len) {
		return NULL;
	}

	ret = g_ptr_array_index (node->properties, property_id);
	if (ret == NULL) {
		return NULL;
	}

	retval = g_value_get_string (ret);

	return retval;
}

void
blx_node_set_property_boolean (BlxNode *node,
			        guint property_id,
			        gboolean value)
{
	GValue *new;

	g_return_if_fail (BLX_IS_NODE (node));

	if (blx_node_db_is_immutable (node->db)) return;

	new = g_slice_new0 (GValue);
	g_value_init (new, G_TYPE_BOOLEAN);
	g_value_set_boolean (new, value);

	blx_node_set_property_internal (node, property_id, new);
}

gboolean
blx_node_get_property_boolean (BlxNode *node,
			        guint property_id)
{
	GValue *ret;
	gboolean retval;

	g_return_val_if_fail (BLX_IS_NODE (node), FALSE);

	if (property_id >= node->properties->len) {
		return FALSE;
	}

	ret = g_ptr_array_index (node->properties, property_id);
	if (ret == NULL) {
		return FALSE;
	}

	retval = g_value_get_boolean (ret);

	return retval;
}

void
blx_node_set_property_long (BlxNode *node,
			     guint property_id,
			     long value)
{
	GValue *new;

	g_return_if_fail (BLX_IS_NODE (node));

	if (blx_node_db_is_immutable (node->db)) return;

	new = g_slice_new0 (GValue);
	g_value_init (new, G_TYPE_LONG);
	g_value_set_long (new, value);

	blx_node_set_property_internal (node, property_id, new);
}

long
blx_node_get_property_long (BlxNode *node,
			     guint property_id)
{
	GValue *ret;
	long retval;

	g_return_val_if_fail (BLX_IS_NODE (node), -1);

	if (property_id >= node->properties->len) {
		return -1;
	}

	ret = g_ptr_array_index (node->properties, property_id);
	if (ret == NULL) {
		return -1;
	}

	retval = g_value_get_long (ret);

	return retval;
}

void
blx_node_set_property_int (BlxNode *node,
			    guint property_id,
			    int value)
{
	GValue *new;

	g_return_if_fail (BLX_IS_NODE (node));

	if (blx_node_db_is_immutable (node->db)) return;

	new = g_slice_new0 (GValue);
	g_value_init (new, G_TYPE_INT);
	g_value_set_int (new, value);

	blx_node_set_property_internal (node, property_id, new);
}

int
blx_node_get_property_int (BlxNode *node,
			    guint property_id)
{
	GValue *ret;
	int retval;

	g_return_val_if_fail (BLX_IS_NODE (node), -1);

	if (property_id >= node->properties->len) {
		return -1;
	}

	ret = g_ptr_array_index (node->properties, property_id);
	if (ret == NULL) {
		return -1;
	}

	retval = g_value_get_int (ret);

	return retval;
}

void
blx_node_set_property_double (BlxNode *node,
			       guint property_id,
			       double value)
{
	GValue *new;

	g_return_if_fail (BLX_IS_NODE (node));

	if (blx_node_db_is_immutable (node->db)) return;

	new = g_slice_new0 (GValue);
	g_value_init (new, G_TYPE_DOUBLE);
	g_value_set_double (new, value);

	blx_node_set_property_internal (node, property_id, new);
}

double
blx_node_get_property_double (BlxNode *node,
			       guint property_id)
{
	GValue *ret;
	double retval;

	g_return_val_if_fail (BLX_IS_NODE (node), -1);

	if (property_id >= node->properties->len) {
		return -1;
	}

	ret = g_ptr_array_index (node->properties, property_id);
	if (ret == NULL) {
		return -1;
	}

	retval = g_value_get_double (ret);

	return retval;
}

void
blx_node_set_property_float (BlxNode *node,
			      guint property_id,
			      float value)
{
	GValue *new;

	g_return_if_fail (BLX_IS_NODE (node));

	if (blx_node_db_is_immutable (node->db)) return;

	new = g_slice_new0 (GValue);
	g_value_init (new, G_TYPE_FLOAT);
	g_value_set_float (new, value);

	blx_node_set_property_internal (node, property_id, new);
}

float
blx_node_get_property_float (BlxNode *node,
			      guint property_id)
{
	GValue *ret;
	float retval;

	g_return_val_if_fail (BLX_IS_NODE (node), -1);

	if (property_id >= node->properties->len) {
		return -1;
	}

	ret = g_ptr_array_index (node->properties, property_id);
	if (ret == NULL) {
		return -1;
	}

	retval = g_value_get_float (ret);

	return retval;
}

/**
 * blx_node_get_property_node:
 *
 * Return value: (transfer none):
 **/
BlxNode *
blx_node_get_property_node (BlxNode *node,
			     guint property_id)
{
	GValue *ret;
	BlxNode *retval;

	g_return_val_if_fail (BLX_IS_NODE (node), NULL);
	g_return_val_if_fail (property_id >= 0, NULL);

	if (property_id >= node->properties->len) {
		return NULL;
	}

	ret = g_ptr_array_index (node->properties, property_id);
	if (ret == NULL) {
		return NULL;
	}

	retval = g_value_get_pointer (ret);

	return retval;
}

typedef struct
{
	xmlTextWriterPtr writer;
	int ret;
} ForEachData;

static void
write_parent (guint id,
	      BlxNodeParent *node_info,
	      ForEachData* data)
{
	xmlTextWriterPtr writer = data->writer;

	/* there already was an error, do nothing. this works around
	 * the fact that g_hash_table_foreach cannot be cancelled.
	 */
	if (data->ret < 0) return;

	data->ret = xmlTextWriterStartElement (writer, (const xmlChar *)"parent");
	if (data->ret < 0) return;

	data->ret = xmlTextWriterWriteFormatAttribute
			(writer, (const xmlChar *)"id", "%d", node_info->node->id);
	if (data->ret < 0) return;

	data->ret = xmlTextWriterEndElement (writer); /* parent */
	if (data->ret < 0) return;
}

static inline int
safe_write_string (xmlTextWriterPtr writer,
		   const xmlChar *string)
{
	int ret;
	xmlChar *copy, *p;

	if (!string)
		return 0;

	/* http://www.w3.org/TR/REC-xml/#sec-well-formed :
	   Character Range
	   [2]     Char       ::=          #x9 | #xA | #xD | [#x20-#xD7FF] |
	   [#xE000-#xFFFD] | [#x10000-#x10FFFF]
	   any Unicode character, excluding the surrogate blocks, FFFE, and FFFF.
	*/

	copy = xmlStrdup (string);
	for (p = copy; *p; p++)
	{
		xmlChar c = *p;
		if (G_UNLIKELY (c < 0x20 && c != 0xd && c != 0xa && c != 0x9)) {
			*p = 0x20;
		}
	}

	ret = xmlTextWriterWriteString (writer, copy);
	xmlFree (copy);

	return ret;
}

int
blx_node_write_to_xml(BlxNode *node,
		       xmlTextWriterPtr writer)
{
	xmlChar xml_buf[G_ASCII_DTOSTR_BUF_SIZE];
	guint i;
	int ret;
	ForEachData data;

	g_return_val_if_fail (BLX_IS_NODE (node), -1);
	g_return_val_if_fail (writer != NULL, -1);

	/* start writing the node */
	ret = xmlTextWriterStartElement (writer, (const xmlChar *)"node");
	if (ret < 0) goto out;

	/* write node id */
	ret = xmlTextWriterWriteFormatAttribute (writer, (const xmlChar *)"id", "%d", node->id);
	if (ret < 0) goto out;

	/* write node properties */
	for (i = 0; i < node->properties->len; i++)
	{
		GValue *value;

		value = g_ptr_array_index (node->properties, i);

		if (value == NULL) continue;
		if (G_VALUE_TYPE (value) == G_TYPE_STRING &&
		    g_value_get_string (value) == NULL) continue;

		ret = xmlTextWriterStartElement (writer, (const xmlChar *)"property");
		if (ret < 0) break;

		ret = xmlTextWriterWriteFormatAttribute (writer, (const xmlChar *)"id", "%d", i);
		if (ret < 0) break;

		ret = xmlTextWriterWriteAttribute
			(writer, (const xmlChar *)"value_type", 
			 (const xmlChar *)g_type_name (G_VALUE_TYPE (value)));
		if (ret < 0) break;

		switch (G_VALUE_TYPE (value))
		{
		case G_TYPE_STRING:
			ret = safe_write_string
				(writer, (const xmlChar *)g_value_get_string (value));
			break;
		case G_TYPE_BOOLEAN:
			ret = xmlTextWriterWriteFormatString
				(writer, "%d", g_value_get_boolean (value));
			break;
		case G_TYPE_INT:
			ret = xmlTextWriterWriteFormatString
				(writer, "%d", g_value_get_int (value));
			break;
		case G_TYPE_LONG:
			ret = xmlTextWriterWriteFormatString
				(writer, "%ld", g_value_get_long (value));
			break;
		case G_TYPE_FLOAT:
			g_ascii_dtostr ((gchar *)xml_buf, sizeof (xml_buf), 
					g_value_get_float (value));
			ret = xmlTextWriterWriteString (writer, xml_buf);
			break;
		case G_TYPE_DOUBLE:
			g_ascii_dtostr ((gchar *)xml_buf, sizeof (xml_buf),
					g_value_get_double (value));
			ret = xmlTextWriterWriteString (writer, xml_buf);
			break;
		default:
			g_assert_not_reached ();
			break;
		}
		if (ret < 0) break;
	
		ret = xmlTextWriterEndElement (writer); /* property */
		if (ret < 0) break;
	}
	if (ret < 0) goto out;

	/* now write parent node ids */
	data.writer = writer;
	data.ret = 0;

	g_hash_table_foreach (node->parents,
			      (GHFunc) write_parent,
			      &data);
	ret = data.ret;
	if (ret < 0) goto out;

	ret = xmlTextWriterEndElement (writer); /* node */
	if (ret < 0) goto out;

out:
	return ret >= 0 ? 0 : -1;
}

static inline void
real_add_child (BlxNode *node,
		BlxNode *child)
{
	BlxNodeParent *node_info;

	if (g_hash_table_lookup (child->parents,
				 GINT_TO_POINTER (node->id)) != NULL) {
		return;
	}

	g_ptr_array_add (node->children, child);

	node_info = g_slice_new0 (BlxNodeParent);
	node_info->node  = node;
	node_info->index = node->children->len - 1;

	g_hash_table_insert (child->parents,
			     GINT_TO_POINTER (node->id),
			     node_info);
}

BlxNode *
blx_node_new_from_xml (BlxNodeDb *db, xmlNodePtr xml_node)
{
	BlxNode *node;
	xmlNodePtr xml_child;
	xmlChar *xml;
	long id;

	g_return_val_if_fail (BLX_IS_NODE_DB (db), NULL);
	g_return_val_if_fail (xml_node != NULL, NULL);

	if (blx_node_db_is_immutable (db)) return NULL; 

	xml = xmlGetProp (xml_node, (const xmlChar *)"id");
	if (xml == NULL)
		return NULL;
	id = atol ((const char *)xml);
	xmlFree (xml);

	node = blx_node_new_with_id (db, id);

	for (xml_child = xml_node->children; xml_child != NULL; xml_child = xml_child->next) {
		if (strcmp ((const char *)xml_child->name, "parent") == 0) {
			BlxNode *parent;
			long parent_id;

			xml = xmlGetProp (xml_child, (const xmlChar *)"id");
			g_assert (xml != NULL);
			parent_id = atol ((const char *)xml);
			xmlFree (xml);

			parent = blx_node_db_get_node_from_id (db, parent_id);

			if (parent != NULL)
			{
				real_add_child (parent, node);

				blx_node_emit_signal (parent, BLX_NODE_CHILD_ADDED, node);
			}
		} else if (strcmp ((const char *)xml_child->name, "property") == 0) {
			GValue *value;
			xmlChar *xmlType, *xmlValue;
			int property_id;

			xml = xmlGetProp (xml_child, (const xmlChar *)"id");
			property_id = atoi ((const char *)xml);
			xmlFree (xml);

			xmlType = xmlGetProp (xml_child, (const xmlChar *)"value_type");
			xmlValue = xmlNodeGetContent (xml_child);

			value = g_slice_new0 (GValue);

			if (xmlStrEqual (xmlType, (const xmlChar *) "gchararray"))
			{
				g_value_init (value, G_TYPE_STRING);
				g_value_set_string (value, (const gchar *)xmlValue);
			}
			else if (xmlStrEqual (xmlType, (const xmlChar *) "gint"))
			{
				g_value_init (value, G_TYPE_INT);
				g_value_set_int (value, atoi ((const char *)xmlValue));
			}
			else if (xmlStrEqual (xmlType, (const xmlChar *) "gboolean"))
			{
				g_value_init (value, G_TYPE_BOOLEAN);
				g_value_set_boolean (value, atoi ((const char *)xmlValue));
			}
			else if (xmlStrEqual (xmlType, (const xmlChar *) "glong"))
			{
				g_value_init (value, G_TYPE_LONG);
				g_value_set_long (value, atol ((const char *)xmlValue));
			}
			else if (xmlStrEqual (xmlType, (const xmlChar *) "gfloat"))
			{
				g_value_init (value, G_TYPE_FLOAT);
				g_value_set_float (value, g_ascii_strtod ((const gchar *)xmlValue, NULL));
			}
			else if (xmlStrEqual (xmlType, (const xmlChar *) "gdouble"))
			{
				g_value_init (value, G_TYPE_DOUBLE);
				g_value_set_double (value, g_ascii_strtod ((const gchar *)xmlValue, NULL));
			}
			else if (xmlStrEqual (xmlType, (const xmlChar *) "gpointer"))
			{
				BlxNode *property_node;

				property_node = blx_node_db_get_node_from_id (db, atol ((const char *)xmlValue));

				g_value_set_pointer (value, property_node);
				break;
			}
			else
			{
				g_assert_not_reached ();
			}

			real_set_property (node, property_id, value);

			xmlFree (xmlValue);
			xmlFree (xmlType);
		}
	}

	blx_node_emit_signal (node, BLX_NODE_RESTORED);

	return node;
}

void
blx_node_add_child (BlxNode *node,
		     BlxNode *child)
{
	g_return_if_fail (BLX_IS_NODE (node));

	if (blx_node_db_is_immutable (node->db)) return;
	
	real_add_child (node, child);

	blx_node_emit_signal (node, BLX_NODE_CHILD_ADDED, child);
}

void
blx_node_remove_child (BlxNode *node,
		        BlxNode *child)
{
	g_return_if_fail (BLX_IS_NODE (node));

	if (blx_node_db_is_immutable (node->db)) return;

	real_remove_child (node, child, TRUE, TRUE);
}

gboolean
blx_node_has_child (BlxNode *node,
		     BlxNode *child)
{
	gboolean ret;

	g_return_val_if_fail (BLX_IS_NODE (node), FALSE);
	
	ret = (g_hash_table_lookup (child->parents,
				    GINT_TO_POINTER (node->id)) != NULL);

	return ret;
}

static int
blx_node_real_get_child_index (BlxNode *node,
			   BlxNode *child)
{
	BlxNodeParent *node_info;
	int ret;

	node_info = g_hash_table_lookup (child->parents,
					 GINT_TO_POINTER (node->id));

	if (node_info == NULL)
		return -1;

	ret = node_info->index;

	return ret;
}

/**
 * blx_node_sort_children:
 * @node: an #BlxNode
 * @compare_func: (scope call): function to compare children
 *
 * Sorts the children of @node using @compare_func.
 *
 **/
void
blx_node_sort_children (BlxNode *node,
			 GCompareFunc compare_func)
{
	GPtrArray *newkids;
	int i, *new_order;

	if (blx_node_db_is_immutable (node->db)) return;

	g_return_if_fail (BLX_IS_NODE (node));
	g_return_if_fail (compare_func != NULL);

	newkids = g_ptr_array_new ();
	g_ptr_array_set_size (newkids, node->children->len);

	/* dup the array */
	for (i = 0; i < node->children->len; i++)
	{
		g_ptr_array_index (newkids, i) = g_ptr_array_index (node->children, i);
	}

	g_ptr_array_sort (newkids, compare_func);

	new_order = g_new (int, newkids->len);
	memset (new_order, -1, sizeof (int) * newkids->len);

	for (i = 0; i < newkids->len; i++)
	{
		BlxNodeParent *node_info;
		BlxNode *child;

		child = g_ptr_array_index (newkids, i);
		new_order[blx_node_real_get_child_index (node, child)] = i;
		node_info = g_hash_table_lookup (child->parents,
					         GINT_TO_POINTER (node->id));
		node_info->index = i;
	}

	g_ptr_array_free (node->children, FALSE);
	node->children = newkids;

	blx_node_emit_signal (node, BLX_NODE_CHILDREN_REORDERED, new_order);

	g_free (new_order);
}

void
blx_node_reorder_children (BlxNode *node,
			    int *new_order)
{
	GPtrArray *newkids;
	int i;

	g_return_if_fail (BLX_IS_NODE (node));
	g_return_if_fail (new_order != NULL);

	if (blx_node_db_is_immutable (node->db)) return;

	newkids = g_ptr_array_new ();
	g_ptr_array_set_size (newkids, node->children->len);

	for (i = 0; i < node->children->len; i++) {
		BlxNode *child;
		BlxNodeParent *node_info;

		child = g_ptr_array_index (node->children, i);

		g_ptr_array_index (newkids, new_order[i]) = child;

		node_info = g_hash_table_lookup (child->parents,
					         GINT_TO_POINTER (node->id));
		node_info->index = new_order[i];
	}

	g_ptr_array_free (node->children, FALSE);
	node->children = newkids;

	blx_node_emit_signal (node, BLX_NODE_CHILDREN_REORDERED, new_order);
}

/**
 * blx_node_get_children:
 *
 * Return value: (array) (element-type BlxNode) (transfer none):
 **/
GPtrArray *
blx_node_get_children (BlxNode *node)
{
	g_return_val_if_fail (BLX_IS_NODE (node), NULL);

	return node->children;
}

int
blx_node_get_n_children (BlxNode *node)
{
	int ret;

	g_return_val_if_fail (BLX_IS_NODE (node), -1);

	ret = node->children->len;

	return ret;
}

/**
 * blx_node_get_nth_child:
 *
 * Return value: (transfer none):
 **/
BlxNode *
blx_node_get_nth_child (BlxNode *node,
		         guint n)
{
	BlxNode *ret;

	g_return_val_if_fail (BLX_IS_NODE (node), NULL);
	g_return_val_if_fail (n >= 0, NULL);

	if (n < node->children->len) {
		ret = g_ptr_array_index (node->children, n);
	} else {
		ret = NULL;
	}

	return ret;
}

static inline int
get_child_index_real (BlxNode *node,
		      BlxNode *child)
{
	BlxNodeParent *node_info;

	node_info = g_hash_table_lookup (child->parents,
					 GINT_TO_POINTER (node->id));

	if (node_info == NULL)
		return -1;

	return node_info->index;
}


int
blx_node_get_child_index (BlxNode *node,
			   BlxNode *child)
{
	int ret;

	g_return_val_if_fail (BLX_IS_NODE (node), -1);
	g_return_val_if_fail (BLX_IS_NODE (child), -1);

	ret = blx_node_real_get_child_index (node, child);

	return ret;
}

/**
 * blx_node_get_next_child:
 *
 * Return value: (transfer none):
 **/
BlxNode *
blx_node_get_next_child (BlxNode *node,
			  BlxNode *child)
{
	BlxNode *ret;
	guint idx;

	g_return_val_if_fail (BLX_IS_NODE (node), NULL);
	g_return_val_if_fail (BLX_IS_NODE (child), NULL);
	
	idx = get_child_index_real (node, child);

	if ((idx + 1) < node->children->len) {
		ret = g_ptr_array_index (node->children, idx + 1);
	} else {
		ret = NULL;
	}

	return ret;
}

/**
 * blx_node_get_previous_child:
 *
 * Return value: (transfer none):
 **/
BlxNode *
blx_node_get_previous_child (BlxNode *node,
			      BlxNode *child)
{
	BlxNode *ret;
	int idx;

	g_return_val_if_fail (BLX_IS_NODE (node), NULL);
	g_return_val_if_fail (BLX_IS_NODE (child), NULL);
	
	idx = get_child_index_real (node, child);

	if ((idx - 1) >= 0) {
		ret = g_ptr_array_index (node->children, idx - 1);
	} else {
		ret = NULL;
	}

	return ret;
}

/**
 * blx_node_signal_connect_object:
 * @node: an #BlxNode
 * @type: signal type
 * @callback: (scope notified): the callback to connect
 * @object: data to pass to @callback
 *
 * Connects a callback function to the @type signal of @node.
 *
 * Returns: an identifier for the connected signal
 **/
int
blx_node_signal_connect_object (BlxNode *node,
				 BlxNodeSignalType type,
				 BlxNodeCallback callback,
				 GObject *object)
{
	BlxNodeSignalData *signal_data;
	int ret;

	g_return_val_if_fail (BLX_IS_NODE (node), -1);
	/* FIXME: */
	g_return_val_if_fail (node->emissions == 0, -1);

	signal_data = g_slice_new0 (BlxNodeSignalData);
	signal_data->node = node;
	signal_data->id = node->signal_id;
	signal_data->callback = callback;
	signal_data->type = type;
	signal_data->data = object;

	g_hash_table_insert (node->signals,
			     GINT_TO_POINTER (node->signal_id),
			     signal_data);
	if (object)
	{
		g_object_weak_ref (object,
				   (GWeakNotify)signal_object_weak_notify,
				   signal_data);
	}

	ret = node->signal_id;
	node->signal_id++;

	return ret;
}

static gboolean
remove_matching_signal_data (gpointer key,
			     BlxNodeSignalData *signal_data,
			     BlxNodeSignalData *user_data)
{
	return (user_data->data == signal_data->data &&
		user_data->type == signal_data->type &&
		user_data->callback == signal_data->callback);           
}

static void
invalidate_matching_signal_data (gpointer key,
				 BlxNodeSignalData *signal_data,
				 BlxNodeSignalData *user_data)
{
	if (user_data->data == signal_data->data &&
	    user_data->type == signal_data->type &&
	    user_data->callback == signal_data->callback &&
	    !signal_data->invalidated)
	{
		signal_data->invalidated = TRUE;
		++signal_data->node->invalidated_signals;
	}
}

/**
 * blx_node_signal_disconnect_object:
 * @node: an #BlxNode
 * @type: signal type
 * @callback: (scope notified): the callback to disconnect
 * @object: data passed to @callback when it was connected
 *
 * Disconnects @callback from @type in @node. @callback is identified by the
 * @object previously passed in blx_node_signal_connect_object.
 *
 * Returns: the number of signal handlers removed
 **/
guint
blx_node_signal_disconnect_object (BlxNode *node,
                                    BlxNodeSignalType type,
                                    BlxNodeCallback callback,
                                    GObject *object)
{
        BlxNodeSignalData user_data;

	g_return_val_if_fail (BLX_IS_NODE (node), 0);

	user_data.callback = callback;
	user_data.type = type;
	user_data.data = object;

	if (G_LIKELY (node->emissions == 0))
	{
		return g_hash_table_foreach_remove (node->signals,
						    (GHRFunc) remove_matching_signal_data,
						    &user_data);
	}
	else
	{
		g_hash_table_foreach (node->signals,
				      (GHFunc) invalidate_matching_signal_data,
				      &user_data);
		return 0;
	}
}

void
blx_node_signal_disconnect (BlxNode *node,
			     int signal_id)
{
	g_return_if_fail (BLX_IS_NODE (node));
	g_return_if_fail (signal_id != -1);

	if (G_LIKELY (node->emissions == 0))
	{
		g_hash_table_remove (node->signals,
				     GINT_TO_POINTER (signal_id));
	}
	else
	{
		BlxNodeSignalData *data;

		data = g_hash_table_lookup (node->signals,
					    GINT_TO_POINTER (signal_id));
		g_return_if_fail (data != NULL);
		g_return_if_fail (!data->invalidated);

		data->invalidated = TRUE;
		node->invalidated_signals++;
	}
}

void
blx_node_set_is_drag_source (BlxNode *node,
			      gboolean allow)
{
	node->is_drag_source = allow != FALSE;
}

gboolean
blx_node_get_is_drag_source (BlxNode *node)
{
	return node->is_drag_source;
}

void
blx_node_set_is_drag_dest (BlxNode *node,
			    gboolean allow)
{
	node->is_drag_dest = allow != FALSE;
}

gboolean
blx_node_get_is_drag_dest (BlxNode *node)
{
	return node->is_drag_dest;
}

GType
blx_node_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0))
	{
		type = g_boxed_type_register_static ("BlxNode",
						     (GBoxedCopyFunc) blx_node_ref,
						     (GBoxedFreeFunc) blx_node_unref);
	}

	return type;
}
