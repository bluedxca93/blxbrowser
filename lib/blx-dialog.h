/*
 *  Copyright © 2000-2003 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_DIALOG_H
#define BLX_DIALOG_H

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_DIALOG		(blx_dialog_get_type ())
#define BLX_DIALOG(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_DIALOG, BlxDialog))
#define BLX_DIALOG_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_DIALOG, BlxDialogClass))
#define BLX_IS_DIALOG(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_DIALOG))
#define BLX_IS_DIALOG_CLASS(k)		(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_DIALOG))
#define BLX_DIALOG_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_DIALOG, BlxDialogClass))

typedef struct _BlxDialogClass		BlxDialogClass;
typedef struct _BlxDialog		BlxDialog;
typedef struct _BlxDialogPrivate	BlxDialogPrivate;
typedef struct _BlxDialogProperty	BlxDialogProperty;

struct _BlxDialogClass
{
	GObjectClass parent_class;

	/* Signals */

	void	(* changed)	(BlxDialog *dialog,
				 const GValue *value);

	/* Methods */
	void	(* construct)	(BlxDialog *dialog,
				 const char *file,
				 const char *name,
				 const char *domain);
	void	(* show)	(BlxDialog *dialog);
};

struct _BlxDialog
{
	GObject parent;

	/*< private >*/
	BlxDialogPrivate *priv;
};

GType		blx_dialog_get_type		(void);

BlxDialog     *blx_dialog_new			(void);

BlxDialog     *blx_dialog_new_with_parent	(GtkWidget *parent_window);

void		blx_dialog_construct		(BlxDialog *dialog,
						 const char *file,
						 const char *name,
						 const char *domain);

void		blx_dialog_set_size_group	(BlxDialog *dialog,
						 const char *first_id,
						 ...);

int		blx_dialog_run			(BlxDialog *dialog);

void		blx_dialog_show		(BlxDialog *dialog);

void		blx_dialog_hide		(BlxDialog *dialog);

void		blx_dialog_set_parent		(BlxDialog *dialog,
						 GtkWidget *parent);

GtkWidget      *blx_dialog_get_parent		(BlxDialog *dialog);

GtkWidget      *blx_dialog_get_control		(BlxDialog *dialog,
						 const char *property_id);

void		blx_dialog_get_controls	(BlxDialog *dialog,
						 const char *first_id,
						 ...);

G_END_DECLS

#endif
