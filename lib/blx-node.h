/*
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_NODE_H
#define BLX_NODE_H

#include <libxml/tree.h>
#include <libxml/xmlwriter.h>

G_BEGIN_DECLS

#define BLX_TYPE_NODE	(blx_node_get_type ())
#define BLX_IS_NODE(o)	(o != NULL)

typedef struct _BlxNode BlxNode;

typedef enum
{
	BLX_NODE_DESTROY,           /* BlxNode *node */
	BLX_NODE_RESTORED,          /* BlxNode *node */
	BLX_NODE_CHANGED,           /* BlxNode *node, guint property_id */
	BLX_NODE_CHILD_ADDED,       /* BlxNode *node, BlxNode *child */
	BLX_NODE_CHILD_CHANGED,     /* BlxNode *node, BlxNode *child, guint property_id */
	BLX_NODE_CHILD_REMOVED,     /* BlxNode *node, BlxNode *child, guint old_index */
	BLX_NODE_CHILDREN_REORDERED /* BlxNode *node, int *new_order */
} BlxNodeSignalType;

#include "blx-node-db.h"

typedef void (*BlxNodeCallback) (BlxNode *node, ...);
typedef gboolean (*BlxNodeFilterFunc) (BlxNode *, gpointer);

GType	    blx_node_get_type		    (void) G_GNUC_CONST;

BlxNode   *blx_node_new                   (BlxNodeDb *db);

BlxNode   *blx_node_new_with_id           (BlxNodeDb *db,
					     guint reserved_id);

BlxNodeDb *blx_node_get_db		    (BlxNode *node);

/* unique node ID */
guint       blx_node_get_id                (BlxNode *node);

/* refcounting */
void        blx_node_ref                   (BlxNode *node);
void        blx_node_unref                 (BlxNode *node);

/* signals */
int         blx_node_signal_connect_object (BlxNode *node,
					     BlxNodeSignalType type,
					     BlxNodeCallback callback,
					     GObject *object);

guint       blx_node_signal_disconnect_object (BlxNode *node,
					     BlxNodeSignalType type,
					     BlxNodeCallback callback,
					     GObject *object);

void        blx_node_signal_disconnect     (BlxNode *node,
					     int signal_id);

/* properties */
void        blx_node_set_property          (BlxNode *node,
				             guint property_id,
				             const GValue *value);
gboolean    blx_node_get_property          (BlxNode *node,
				             guint property_id,
				             GValue *value);

const char *blx_node_get_property_string   (BlxNode *node,
					     guint property_id);
void        blx_node_set_property_string   (BlxNode *node,
					     guint property_id,
					     const char *value);
gboolean    blx_node_get_property_boolean  (BlxNode *node,
					     guint property_id);
void        blx_node_set_property_boolean  (BlxNode *node,
					     guint property_id,
					     gboolean value);
long        blx_node_get_property_long     (BlxNode *node,
					     guint property_id);
void        blx_node_set_property_long     (BlxNode *node,
					     guint property_id,
					     long value);
int         blx_node_get_property_int      (BlxNode *node,
					     guint property_id);
void        blx_node_set_property_int      (BlxNode *node,
					     guint property_id,
					     int value);
double      blx_node_get_property_double   (BlxNode *node,
					     guint property_id);
void        blx_node_set_property_double   (BlxNode *node,
					     guint property_id,
					     double value);
float       blx_node_get_property_float    (BlxNode *node,
					     guint property_id);
void        blx_node_set_property_float    (BlxNode *node,
					     guint property_id,
					     float value);
BlxNode   *blx_node_get_property_node     (BlxNode *node,
					     guint property_id);

/* xml storage */
int           blx_node_write_to_xml	    (BlxNode *node,
					     xmlTextWriterPtr writer);
BlxNode     *blx_node_new_from_xml        (BlxNodeDb *db,
					     xmlNodePtr xml_node);

/* DAG structure */
void          blx_node_add_child           (BlxNode *node,
					     BlxNode *child);
void          blx_node_remove_child        (BlxNode *node,
					     BlxNode *child);
void	      blx_node_sort_children	    (BlxNode *node,
					     GCompareFunc compare_func);
gboolean      blx_node_has_child           (BlxNode *node,
					     BlxNode *child);
void	      blx_node_reorder_children    (BlxNode *node,
					     int *new_order);
GPtrArray    *blx_node_get_children        (BlxNode *node);
int           blx_node_get_n_children      (BlxNode *node);
BlxNode     *blx_node_get_nth_child       (BlxNode *node,
					     guint n);
int           blx_node_get_child_index     (BlxNode *node,
					     BlxNode *child);
BlxNode     *blx_node_get_next_child      (BlxNode *node,
					     BlxNode *child);
BlxNode     *blx_node_get_previous_child  (BlxNode *node,
					     BlxNode *child);
void	      blx_node_set_is_drag_source  (BlxNode *node,
					     gboolean allow);
gboolean      blx_node_get_is_drag_source  (BlxNode *node);
void	      blx_node_set_is_drag_dest    (BlxNode *node,
					     gboolean allow);
gboolean      blx_node_get_is_drag_dest    (BlxNode *node);

G_END_DECLS

#endif /* __BLX_NODE_H */
