/*
 *  Copyright © 2002  Ricardo Fernández Pascual
 *  Copyright © 2003, 2004  Marco Pesenti Gritti
 *  Copyright © 2003, 2004, 2005  Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_LOCATION_ENTRY_H
#define BLX_LOCATION_ENTRY_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_LOCATION_ENTRY		(blx_location_entry_get_type())
#define BLX_LOCATION_ENTRY(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), BLX_TYPE_LOCATION_ENTRY, BlxLocationEntry))
#define BLX_LOCATION_ENTRY_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), BLX_TYPE_LOCATION_ENTRY, BlxLocationEntryClass))
#define BLX_IS_LOCATION_ENTRY(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), BLX_TYPE_LOCATION_ENTRY))
#define BLX_IS_LOCATION_ENTRY_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), BLX_TYPE_LOCATION_ENTRY))
#define BLX_LOCATION_ENTRY_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), BLX_TYPE_LOCATION_ENTRY, BlxLocationEntryClass))

typedef struct _BlxLocationEntryClass		BlxLocationEntryClass;
typedef struct _BlxLocationEntry		BlxLocationEntry;
typedef struct _BlxLocationEntryPrivate	BlxLocationEntryPrivate;

struct _BlxLocationEntryClass
{
	GtkToolItemClass parent_class;

	/* Signals */
	void   (* user_changed)	(BlxLocationEntry *entry);
	void   (* lock_clicked)	(BlxLocationEntry *entry);
	/* for getting the drag data */
	char * (* get_location)	(BlxLocationEntry *entry);
	char * (* get_title)	(BlxLocationEntry *entry);
};

struct _BlxLocationEntry
{
	GtkToolItem parent_object;

	/*< private >*/
	BlxLocationEntryPrivate *priv;
};

GType		blx_location_entry_get_type		(void);

GtkWidget      *blx_location_entry_new			(void);

void		blx_location_entry_set_completion	(BlxLocationEntry *entry,
							 GtkTreeModel *model,
							 guint text_col,
							 guint action_col,
							 guint keywords_col,
							 guint relevance_col,
							 guint url_col,
							 guint extra_col,
							 guint favicon_col);

void		blx_location_entry_set_location	(BlxLocationEntry *entry,
							 const char *address);

void		blx_location_entry_set_match_func	(BlxLocationEntry *entry, 
							 GtkEntryCompletionMatchFunc match_func,
							 gpointer user_data,
							 GDestroyNotify notify);
					
const char     *blx_location_entry_get_location	(BlxLocationEntry *entry);

gboolean	blx_location_entry_get_can_undo	(BlxLocationEntry *entry);

gboolean	blx_location_entry_get_can_redo	(BlxLocationEntry *entry);

GSList         *blx_location_entry_get_search_terms	(BlxLocationEntry *entry);

gboolean	blx_location_entry_reset		(BlxLocationEntry *entry);

void		blx_location_entry_undo_reset		(BlxLocationEntry *entry);

void		blx_location_entry_activate		(BlxLocationEntry *entry);

GtkWidget      *blx_location_entry_get_entry		(BlxLocationEntry *entry);

void		blx_location_entry_set_favicon		(BlxLocationEntry *entry,
							 GdkPixbuf *pixbuf);

void		blx_location_entry_set_show_lock	(BlxLocationEntry *entry,
							 gboolean show_lock);

void		blx_location_entry_set_lock_stock	(BlxLocationEntry *entry,
							 const char *stock_id);

void		blx_location_entry_set_lock_tooltip	(BlxLocationEntry *entry,
							 const char *tooltip);

G_END_DECLS

#endif
