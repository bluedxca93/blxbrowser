/*  Copyright © 2002 Olivier Martin <omartin@ifrance.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_TREE_MODEL_SORT_H
#define BLX_TREE_MODEL_SORT_H

#include <glib-object.h>

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_TREE_MODEL_SORT         (blx_tree_model_sort_get_type ())
#define BLX_TREE_MODEL_SORT(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_TREE_MODEL_SORT, BlxTreeModelSort))
#define BLX_TREE_MODEL_SORT_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_TREE_MODEL_SORT, BlxTreeModelSortClass))
#define BLX_IS_TREE_MODEL_SORT(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_TREE_MODEL_SORT))
#define BLX_IS_TREE_MODEL_SORT_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_TREE_MODEL_SORT))
#define BLX_TREE_MODEL_SORT_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_TREE_MODEL_SORT, BlxTreeModelSortClass))

typedef struct _BlxTreeModelSort BlxTreeModelSort;
typedef struct _BlxTreeModelSortClass BlxTreeModelSortClass;
typedef struct _BlxTreeModelSortPrivate BlxTreeModelSortPrivate;

struct _BlxTreeModelSort
{
	GtkTreeModelSort parent;

	/*< private >*/
	BlxTreeModelSortPrivate *priv;
};

struct _BlxTreeModelSortClass
{
	GtkTreeModelSortClass parent_class;

	void (*node_from_iter) (BlxTreeModelSort *model, GtkTreeIter *iter, void **node);
};

GType		blx_tree_model_sort_get_type		      (void);

GtkTreeModel   *blx_tree_model_sort_new		      (GtkTreeModel *child_model);

void		blx_tree_model_sort_set_base_drag_column_id  (BlxTreeModelSort *ms,
							       int id);
void		blx_tree_model_sort_set_extra_drag_column_id (BlxTreeModelSort *ms,
							       int id);

G_END_DECLS

#endif /* BLX_TREE_MODEL_SORT_H */
