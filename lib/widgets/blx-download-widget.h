/* vim: set foldmethod=marker sw=2 ts=2 sts=2 et: */
/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 * blx-download.c
 * This file is part of Blxbrowser
 *
 * Copyright © 2011 - Igalia S.L.
 *
 * Blxbrowser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Blxbrowser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blxbrowser; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef _BLX_DOWNLOAD_WIDGET_H
#define _BLX_DOWNLOAD_WIDGET_H

#include <glib-object.h>
#include "blx-download.h"

G_BEGIN_DECLS

#define BLX_TYPE_DOWNLOAD_WIDGET              blx_download_widget_get_type()
#define BLX_DOWNLOAD_WIDGET(obj)              (G_TYPE_CHECK_INSTANCE_CAST ((obj), BLX_TYPE_DOWNLOAD_WIDGET, BlxDownloadWidget))
#define BLX_DOWNLOAD_WIDGET_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_DOWNLOAD_WIDGET, BlxDownloadWidgetClass))
#define BLX_IS_DOWNLOAD_WIDGET(obj)           (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_DOWNLOAD_WIDGET))
#define BLX_IS_DOWNLOAD_WIDGET_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), BLX_TYPE_DOWNLOAD_WIDGET))
#define BLX_DOWNLOAD_WIDGET_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), BLX_TYPE_DOWNLOAD_WIDGET, BlxDownloadWidgetClass))

typedef struct _BlxDownloadWidget BlxDownloadWidget;
typedef struct _BlxDownloadWidgetClass BlxDownloadWidgetClass;
typedef struct _BlxDownloadWidgetPrivate BlxDownloadWidgetPrivate;

struct _BlxDownloadWidget
{
  GtkBox parent;

  BlxDownloadWidgetPrivate *priv;
};

struct _BlxDownloadWidgetClass
{
  GtkBoxClass parent_class;
};

GType          blx_download_widget_get_type      (void) G_GNUC_CONST;

GtkWidget     *blx_download_widget_new           (BlxDownload *blx_download);

BlxDownload  *blx_download_widget_get_download  (BlxDownloadWidget *widget);

G_END_DECLS

#endif /* _BLX_DOWNLOAD_WIDGET_H */
