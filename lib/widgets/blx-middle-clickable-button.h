/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 *  Copyright © 2011 Alexandre Mazari
 *  Copyright © 2011 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */
 
#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef __BLX_MIDDLE_CLICKABLE_BUTTON_H__
#define __BLX_MIDDLE_CLICKABLE_BUTTON_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_MIDDLE_CLICKABLE_BUTTON             (blx_middle_clickable_button_get_type ())
#define BLX_MIDDLE_CLICKABLE_BUTTON(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj),BLX_TYPE_MIDDLE_CLICKABLE_BUTTN, BlxMiddleClickableButton))
#define BLX_MIDDLE_CLICKABLE_BUTTON_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_MIDDLE_CLICKABLE_BUTTN, BlxMiddleClickableButtonClass))
#define BLX_IS_MIDDLE_CLICKABLE_BUTTON(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_MIDDLE_CLICKABLE_BUTON))
#define BLX_IS_MIDDLE_CLICKABLE_BUTTON_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), BLX_TYPE_MIDDLE_CLICKABLE_BUTTN))
#define BLX_MIDDLE_CLICKABLE_BUTTON_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), BLX_TYPE_MIDDLE_CLICKABLE_BUTTN, BlxMiddleClickableButtonClass))

typedef struct _BlxMiddleClickableButton       BlxMiddleClickableButton;
typedef struct _BlxMiddleClickableButtonClass  BlxMiddleClickableButtonClass;

struct _BlxMiddleClickableButton {
  GtkButton parent;
};

struct _BlxMiddleClickableButtonClass {
  GtkButtonClass parent_class;
};

GType      blx_middle_clickable_button_get_type (void) G_GNUC_CONST;
GtkWidget *blx_middle_clickable_button_new      (void);

G_END_DECLS

#endif
