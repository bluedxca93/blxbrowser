/*
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_ZOOM_ACTION_H
#define BLX_ZOOM_ACTION_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_ZOOM_ACTION            (blx_zoom_action_get_type ())
#define BLX_ZOOM_ACTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), BLX_TYPE_ZOOM_ACTION, BlxZoomAction))
#define BLX_ZOOM_ACTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_ZOOM_ACTION, BlxZoomActionClass))
#define BLX_IS_ZOOM_ACTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_ZOOM_ACTION))
#define BLX_IS_ZOOM_ACTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), BLX_TYPE_ZOOM_ACTION))
#define BLX_ZOOM_ACTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), BLX_TYPE_ZOOM_ACTION, BlxZoomActionClass))

typedef struct _BlxZoomAction		BlxZoomAction;
typedef struct _BlxZoomActionClass	BlxZoomActionClass;
typedef struct _BlxZoomActionPrivate	BlxZoomActionPrivate;

struct _BlxZoomAction
{
	GtkAction parent;
	
	/*< private >*/
	BlxZoomActionPrivate *priv;
};

struct _BlxZoomActionClass
{
	GtkActionClass parent_class;

	void (* zoom_to_level)	(BlxZoomAction *action, float level);
};

GType	blx_zoom_action_get_type	(void);

void	blx_zoom_action_set_zoom_level	(BlxZoomAction *action, float zoom);

float	blx_zoom_action_get_zoom_level	(BlxZoomAction *action);

G_END_DECLS

#endif
