/*
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#ifndef __BLX_NODE_VIEW_H
#define __BLX_NODE_VIEW_H

#include <gtk/gtk.h>

#include "blx-tree-model-node.h"
#include "blx-node-filter.h"

G_BEGIN_DECLS

#define BLX_TYPE_NODE_VIEW         (blx_node_view_get_type ())
#define BLX_NODE_VIEW(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_NODE_VIEW, BlxNodeView))
#define BLX_NODE_VIEW_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_NODE_VIEW, BlxNodeViewClass))
#define BLX_IS_NODE_VIEW(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_NODE_VIEW))
#define BLX_IS_NODE_VIEW_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_NODE_VIEW))
#define BLX_NODE_VIEW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_NODE_VIEW, BlxNodeViewClass))

typedef struct _BlxNodeView		BlxNodeView;
typedef struct _BlxNodeViewClass	BlxNodeViewClass;
typedef struct _BlxNodeViewPrivate	BlxNodeViewPrivate;

struct _BlxNodeView
{
	GtkTreeView parent;

	/*< private >*/
	BlxNodeViewPrivate *priv;
};

struct _BlxNodeViewClass
{
	GtkTreeViewClass parent;

	void (*node_toggled)	    (BlxNodeView *view, BlxNode *node, gboolean checked);
	void (*node_activated)      (BlxNodeView *view, BlxNode *node);
	void (*node_selected)       (BlxNodeView *view, BlxNode *node);
	void (*node_dropped)        (BlxNodeView *view, BlxNode *node, GList *uris);
	void (*node_middle_clicked) (BlxNodeView *view, BlxNode *node);
};

typedef enum
{
	BLX_NODE_VIEW_ALL_PRIORITY,
	BLX_NODE_VIEW_SPECIAL_PRIORITY,
	BLX_NODE_VIEW_NORMAL_PRIORITY
} BlxNodeViewPriority;

typedef enum
{
	BLX_NODE_VIEW_SHOW_PRIORITY = 1 << 0,
	BLX_NODE_VIEW_SORTABLE = 1 << 1,
	BLX_NODE_VIEW_EDITABLE = 1 << 2,
	BLX_NODE_VIEW_SEARCHABLE = 1 << 3,
	BLX_NODE_VIEW_ELLIPSIZED = 1 << 4
} BlxNodeViewFlags;

GType      blx_node_view_get_type	      (void);

GtkWidget *blx_node_view_new                 (BlxNode *root,
					       BlxNodeFilter *filter);

void	   blx_node_view_add_toggle	      (BlxNodeView *view,
					       BlxTreeModelNodeValueFunc value_func,
					       gpointer data);

int	   blx_node_view_add_column	      (BlxNodeView *view,
					       const char  *title,
					       GType value_type,
					       guint prop_id,
					       BlxNodeViewFlags flags,
					       BlxTreeModelNodeValueFunc icon_func,
					       GtkTreeViewColumn **ret);

int	   blx_node_view_add_data_column     (BlxNodeView *view,
			                       GType value_type,
					       guint prop_id,
			                       BlxTreeModelNodeValueFunc func,
					       gpointer data);

void	   blx_node_view_set_sort            (BlxNodeView *view,
			                       GType value_type,
					       guint prop_id,
					       GtkSortType sort_type);

void	   blx_node_view_set_priority        (BlxNodeView *view,
					       BlxNodeViewPriority priority_prop_id);

void	   blx_node_view_remove              (BlxNodeView *view);

GList     *blx_node_view_get_selection       (BlxNodeView *view);

void	   blx_node_view_select_node         (BlxNodeView *view,
			                       BlxNode *node);

void	   blx_node_view_enable_drag_source  (BlxNodeView *view,
					       const GtkTargetEntry *types,
					       int n_types,
					       int base_drag_column_id,
					       int extra_drag_column_id);

void	   blx_node_view_enable_drag_dest    (BlxNodeView *view,
					       const GtkTargetEntry *types,
					       int n_types);

void	   blx_node_view_edit		      (BlxNodeView *view,
					       gboolean remove_if_cancelled);

gboolean   blx_node_view_is_target	      (BlxNodeView *view);

void	   blx_node_view_popup		      (BlxNodeView *view,
					       GtkWidget *menu);

G_END_DECLS

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#endif /* BLX_NODE_VIEW_H */
