/*
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#ifndef __BLX_TREE_MODEL_NODE_H
#define __BLX_TREE_MODEL_NODE_H

#include <gtk/gtk.h>

#include "blx-node.h"

G_BEGIN_DECLS

#define BLX_TYPE_TREE_MODEL_NODE         (blx_tree_model_node_get_type ())
#define BLX_TREE_MODEL_NODE(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_TREE_MODEL_NODE, BlxTreeModelNode))
#define BLX_TREE_MODEL_NODE_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_TREE_MODEL_NODE, BlxTreeModelNodeClass))
#define BLX_IS_TREE_MODEL_NODE(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_TREE_MODEL_NODE))
#define BLX_IS_TREE_MODEL_NODE_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_TREE_MODEL_NODE))
#define BLX_TREE_MODEL_NODE_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_TREE_MODEL_NODE, BlxTreeModelNodeClass))

typedef void (*BlxTreeModelNodeValueFunc) (BlxNode *node, GValue *value, gpointer user_data);

typedef struct _BlxTreeModelNode BlxTreeModelNode;
typedef struct _BlxTreeModelNodeClass BlxTreeModelNodeClass;
typedef struct _BlxTreeModelNodePrivate BlxTreeModelNodePrivate;

struct _BlxTreeModelNode
{
	GObject parent;

	/*< private >*/
	BlxTreeModelNodePrivate *priv;
};

struct _BlxTreeModelNodeClass
{
	GObjectClass parent;
};

GType              blx_tree_model_node_get_type         (void);

BlxTreeModelNode *blx_tree_model_node_new              (BlxNode *root);

int                blx_tree_model_node_add_prop_column  (BlxTreeModelNode *model,
						          GType value_type,
						          int prop_id);

int                blx_tree_model_node_add_func_column  (BlxTreeModelNode *model,
						          GType value_type,
						          BlxTreeModelNodeValueFunc func,
						          gpointer user_data);

BlxNode          *blx_tree_model_node_node_from_iter   (BlxTreeModelNode *model,
						          GtkTreeIter *iter);

void               blx_tree_model_node_iter_from_node   (BlxTreeModelNode *model,
						          BlxNode *node,
						          GtkTreeIter *iter);

G_END_DECLS

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#endif /* BLX_TREE_MODEL_NODE_H */
