/*
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_SEARCH_ENTRY_H
#define BLX_SEARCH_ENTRY_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_SEARCH_ENTRY		(blx_search_entry_get_type ())
#define BLX_SEARCH_ENTRY(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_SEARCH_ENTRY, BlxSearchEntry))
#define BLX_SEARCH_ENTRY_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_SEARCH_ENTRY, BlxSearchEntryClass))
#define BLX_IS_SEARCH_ENTRY(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_SEARCH_ENTRY))
#define BLX_IS_SEARCH_ENTRY_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_SEARCH_ENTRY))
#define BLX_SEARCH_ENTRY_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_SEARCH_ENTRY, BlxSearchEntryClass))

typedef struct _BlxSearchEntryClass	BlxSearchEntryClass;
typedef struct _BlxSearchEntry		BlxSearchEntry;
typedef struct _BlxSearchEntryPrivate	BlxSearchEntryPrivate;

struct _BlxSearchEntryClass
{
	GtkEntryClass parent;

	void (*search) (BlxSearchEntry *entry, const char *text);
};

struct _BlxSearchEntry
{
	GtkEntry parent;

	/*< private >*/
	BlxSearchEntryPrivate *priv;
};

GType            blx_search_entry_get_type (void);

GtkWidget       *blx_search_entry_new      (void);

void             blx_search_entry_clear    (BlxSearchEntry *entry);

G_END_DECLS

#endif /* __BLX_SEARCH_ENTRY_H */
