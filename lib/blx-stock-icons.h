/*
 *  Copyright © 2002 Jorn Baayen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_STOCK_ICONS_H
#define BLX_STOCK_ICONS_H

G_BEGIN_DECLS

#define BLX_STOCK_BLX            "web-browser"

/* Custom Blxbrowser named icons */
#define BLX_STOCK_POPUPS          "popup-hidden"
#define BLX_STOCK_HISTORY         "history-view"
#define BLX_STOCK_BOOKMARK        "bookmark-web"
#define BLX_STOCK_BOOKMARKS       "bookmark-view"
#define BLX_STOCK_ENTRY           "location-entry"
#define STOCK_LOCK_INSECURE        "lock-insecure"
#define STOCK_LOCK_SECURE          "lock-secure"
#define STOCK_LOCK_BROKEN          "lock-broken"

/* Named icons defined in fd.o Icon Naming Spec */
#define STOCK_NEW_TAB              "tab-new"
#define STOCK_NEW_WINDOW           "window-new"
#define STOCK_SEND_MAIL            "mail-forward"
#define STOCK_NEW_MAIL             "mail-message-new"
#define STOCK_ADD_BOOKMARK         "bookmark-new"
#define STOCK_PRINT_SETUP          "document-page-setup"
#define STOCK_DOWNLOAD             "emblem-downloads"

void blx_stock_icons_init (void);

G_END_DECLS

#endif /* BLX_STOCK_ICONS_H */
