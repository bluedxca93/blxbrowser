/*
 *  Copyright © 2003, 2004 Christian Persch
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_FILE_CHOOSER_H
#define BLX_FILE_CHOOSER_H

#include <glib-object.h>

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_FILE_CHOOSER		(blx_file_chooser_get_type ())
#define BLX_FILE_CHOOSER(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_FILE_CHOOSER, BlxFileChooser))
#define BLX_FILE_CHOOSER_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST ((k), BLX_TYPE_FILE_CHOOSER, BlxFileChooserClass))
#define BLX_IS_FILE_CHOOSER(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_FILE_CHOOSER))
#define BLX_IS_FILE_CHOOSER_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_FILE_CHOOSER))
#define BLX_FILE_CHOOSER_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_FILE_CHOOSER, BlxFileChooserClass))

typedef struct _BlxFileChooser		BlxFileChooser;
typedef struct _BlxFileChooserPrivate	BlxFileChooserPrivate;
typedef struct _BlxFileChooserClass	BlxFileChooserClass;

typedef enum
{
	BLX_FILE_FILTER_ALL_SUPPORTED,
	BLX_FILE_FILTER_WEBPAGES,
	BLX_FILE_FILTER_IMAGES,
	BLX_FILE_FILTER_ALL,
	BLX_FILE_FILTER_NONE,
	BLX_FILE_FILTER_LAST = BLX_FILE_FILTER_NONE
} BlxFileFilterDefault;

struct _BlxFileChooser
{
	GtkFileChooserDialog parent;

	/*< private >*/
	BlxFileChooserPrivate *priv;
};

struct _BlxFileChooserClass
{
	GtkFileChooserDialogClass parent_class;
};

GType		 blx_file_chooser_get_type		(void);

BlxFileChooser	*blx_file_chooser_new			(const char *title,
							 GtkWidget *parent,
							 GtkFileChooserAction action,
							 const char *persist_key,
							 BlxFileFilterDefault default_filter);

void		 blx_file_chooser_set_persist_key	(BlxFileChooser *dialog,
							 const char *key);

const char	*blx_file_chooser_get_persist_key	(BlxFileChooser *dialog);

GtkFileFilter	*blx_file_chooser_add_pattern_filter	(BlxFileChooser *dialog,
							 const char *title,
							 const char *first_pattern,
							 ...);

GtkFileFilter	*blx_file_chooser_add_mime_filter	(BlxFileChooser *dialog,
							 const char *title,
							 const char *first_mimetype,
							 ...);

G_END_DECLS

#endif
