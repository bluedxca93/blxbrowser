/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 * Copyright (C) 2011, Igalia S.L.
 */

#ifndef BLX_REQUEST_ABOUT_H
#define BLX_REQUEST_ABOUT_H 1

#define LIBSOUP_USE_UNSTABLE_REQUEST_API
#include <libsoup/soup-request.h>

#define BLX_TYPE_REQUEST_ABOUT            (blx_request_about_get_type ())
#define BLX_REQUEST_ABOUT(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), BLX_TYPE_REQUEST_ABOUT, BlxRequestAbout))
#define BLX_REQUEST_ABOUT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_REQUEST_ABOUT, BlxRequestAboutClass))
#define BLX_IS_REQUEST_ABOUT(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), BLX_TYPE_REQUEST_ABOUT))
#define BLX_IS_REQUEST_ABOUT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), BLX_TYPE_REQUEST_ABOUT))
#define BLX_REQUEST_ABOUT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), BLX_TYPE_REQUEST_ABOUT, BlxRequestAboutClass))

#define BLX_ABOUT_SCHEME "blx-about"
#define BLX_ABOUT_SCHEME_LEN 10

typedef struct _BlxRequestAboutPrivate BlxRequestAboutPrivate;

typedef struct {
  SoupRequest parent;

  BlxRequestAboutPrivate *priv;
} BlxRequestAbout;

typedef struct {
  SoupRequestClass parent;

} BlxRequestAboutClass;

GType blx_request_about_get_type (void);

#endif /* BLX_REQUEST_ABOUT_H */
