/*
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include "blx-permission-manager.h"
#include "blx-embed-type-builtins.h"
#include "blx-debug.h"

GType
blx_permission_info_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0))
	{
		type = g_boxed_type_register_static ("BlxPermissionInfo",
						     (GBoxedCopyFunc) blx_permission_info_copy,
						     (GBoxedFreeFunc) blx_permission_info_free);
	}

	return type;
}

/**
 * blx_permission_info_new:
 * @host: a host name
 * @type: an #BlxPermissionType
 * @permission: whether @host should be allowed to do what @type specifies
 *
 * Return value: the new #BlxPermissionInfo
 **/
BlxPermissionInfo *
blx_permission_info_new (const char *host,
			  const char *type,
			  BlxPermission permission)
{
	BlxPermissionInfo *info = g_slice_new0 (BlxPermissionInfo);

	info->host = g_strdup (host);
	info->qtype = g_quark_from_string (type);
	info->permission = permission;

	return info;
}

/**
 * blx_permission_info_copy:
 * @info: an #BlxPermissionInfo
 *
 * Return value: a copy of @info
 **/
BlxPermissionInfo *
blx_permission_info_copy (const BlxPermissionInfo *info)
{
	BlxPermissionInfo *copy = g_slice_new0 (BlxPermissionInfo);

	copy->host = g_strdup (info->host);
	copy->qtype = info->qtype;
	copy->permission = info->permission;

	return copy;
}

/**
 * blx_permission_info_free:
 * @info: an #BlxPermissionInfo
 *
 * Frees @info.
 **/
void
blx_permission_info_free (BlxPermissionInfo *info)
{
	if (info != NULL)
	{
		g_free (info->host);
		g_slice_free (BlxPermissionInfo, info);
	}
}

/* BlxPermissionManager */

static void blx_permission_manager_base_init (gpointer g_class);

GType
blx_permission_manager_get_type (void)
{
       static GType type = 0;

	if (G_UNLIKELY (type == 0))
	{
		const GTypeInfo our_info =
		{
		sizeof (BlxPermissionManagerIface),
		blx_permission_manager_base_init,
		NULL,
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE,
					       "BlxPermissionManager",
					       &our_info,
					       (GTypeFlags) 0);
	}

	return type;
}

static void
blx_permission_manager_base_init (gpointer g_class)
{
	static gboolean initialised = FALSE;

	if (initialised == FALSE)
	{
	/**
	 * BlxPermissionManager::permission-added
	 * @manager: the #BlxPermissionManager
	 * @info: a #BlxPermissionInfo
	 *
	 * The permission-added signal is emitted when a permission entry has
	 * been added.
	 */
	g_signal_new ("permission-added",
		      BLX_TYPE_PERMISSION_MANAGER,
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET (BlxPermissionManagerIface, added),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__BOXED,
		      G_TYPE_NONE,
		      1,
		      BLX_TYPE_PERMISSION_INFO | G_SIGNAL_TYPE_STATIC_SCOPE);

	/**
	 * BlxPermissionManager::permission-changed
	 * @manager: the #BlxPermissionManager
	 * @info: a #BlxPermissionInfo
	 *
	 * The permission-changed signal is emitted when a permission entry has
	 * been changed.
	 */
	g_signal_new ("permission-changed",
		      BLX_TYPE_PERMISSION_MANAGER,
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET (BlxPermissionManagerIface, changed),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__BOXED,
		      G_TYPE_NONE,
		      1,
		      BLX_TYPE_PERMISSION_INFO | G_SIGNAL_TYPE_STATIC_SCOPE);

	/**
	 * BlxPermissionManager::permission-deleted
	 * @manager: the #BlxPermissionManager
	 * @info: a #BlxPermissionInfo
	 *
	 * The permission-deleted signal is emitted when a permission entry has
	 * been deleted.
	 */
	g_signal_new ("permission-deleted",
		      BLX_TYPE_PERMISSION_MANAGER,
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET (BlxPermissionManagerIface, deleted),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__BOXED,
		      G_TYPE_NONE,
		      1,
		      BLX_TYPE_PERMISSION_INFO | G_SIGNAL_TYPE_STATIC_SCOPE);

	/**
	 * BlxPermissionManager::permissions-cleared
	 * @manager: the #BlxPermissionManager
	 *
	 * The permissions-cleared signal is emitted when the permissions
	 * database has been cleared.
	 */
	g_signal_new ("permissions-cleared",
		      BLX_TYPE_PERMISSION_MANAGER,
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET (BlxPermissionManagerIface, cleared),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__VOID,
		      G_TYPE_NONE,
		      0);

	initialised = TRUE;
	}
}

/**
 * blx_permission_manager_add_permission:
 * @manager: the #BlxPermissionManager
 * @host: a website URL
 * @type: a string to identify the type of the permission
 * @permission: either %BLX_PERMISSION_ALLOWED or %BLX_PERMISSION_DENIED
 * 
 * Adds the specified permission to the permissions database.
 **/
void
blx_permission_manager_add_permission (BlxPermissionManager *manager,
					const char *host,
					const char *type,
					BlxPermission permission)
{
	BlxPermissionManagerIface *iface = BLX_PERMISSION_MANAGER_GET_IFACE (manager);
	iface->add (manager, host, type, permission);
}

/**
 * blx_permission_manager_remove_permission:
 * @manager: the #BlxPermissionManager
 * @host: a website URL
 * @type: a string to identify the type of the permission
 * 
 * Removes the specified permission from the permissions database. This implies
 * that the browser should use defaults when next visiting the specified
 * @host's web pages.
 **/
void
blx_permission_manager_remove_permission (BlxPermissionManager *manager,
					   const char *host,
					   const char *type)
{
	BlxPermissionManagerIface *iface = BLX_PERMISSION_MANAGER_GET_IFACE (manager);
	iface->remove (manager, host, type);
}

/**
 * blx_permission_manager_clear_permission:
 * @manager: the #BlxPermissionManager
 * 
 * Clears the permissions database. This cannot be undone.
 **/
void
blx_permission_manager_clear_permissions (BlxPermissionManager *manager)
{
	BlxPermissionManagerIface *iface = BLX_PERMISSION_MANAGER_GET_IFACE (manager);
	iface->clear (manager);
}

/**
 * blx_permission_manager_test_permission:
 * @manager: the #BlxPermissionManager
 * @host: a website URL
 * @type: a string to identify the type of the permission
 * 
 * Retrieves an #BlxPermissionType from the permissions database. If there is
 * no entry for this @type and @host, it will return %BLX_PERMISSION_DEFAULT.
 * In that case, the caller may need to determine the appropriate default
 * behavior.
 *
 * Return value: the permission of type #BlxPermission
 **/
BlxPermission
blx_permission_manager_test_permission (BlxPermissionManager *manager,
					 const char *host,
					 const char *type)
{
	BlxPermissionManagerIface *iface = BLX_PERMISSION_MANAGER_GET_IFACE (manager);
	return iface->test (manager, host, type);
}

/**
 * blx_permission_manager_list_permissions:
 * @manager: the #BlxPermissionManager
 * @type: a string to identify the type of the permission
 * 
 * Lists all permission entries of type @type in the permissions database, each
 * as its own #BlxPermissionInfo. These entries must be freed using
 * blx_permission_info_free().
 * 
 * Return value: (transfer none): the list of permission database entries
 **/
GList *
blx_permission_manager_list_permissions (BlxPermissionManager *manager,
					 const char *type)
{
	BlxPermissionManagerIface *iface = BLX_PERMISSION_MANAGER_GET_IFACE (manager);
	return iface->list (manager, type);
}
