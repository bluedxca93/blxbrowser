/*
 *  Copyright © 2003, 2004 Christian Persch
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_ENCODINGS_H
#define BLX_ENCODINGS_H

#include <glib-object.h>
#include <glib.h>

#include "blx-node.h"

G_BEGIN_DECLS

#define BLX_TYPE_ENCODINGS		(blx_encodings_get_type ())
#define BLX_ENCODINGS(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_ENCODINGS, BlxEncodings))
#define BLX_ENCODINGS_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST ((k), BLX_TYPE_ENCODINGS, BlxEncodingsClass))
#define BLX_IS_ENCODINGS(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_ENCODINGS))
#define BLX_IS_ENCODINGS_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_ENCODINGS))
#define BLX_ENCODINGS_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_ENCODINGS, BlxEncodingsClass))

typedef struct _BlxEncodings		BlxEncodings;
typedef struct _BlxEncodingsPrivate	BlxEncodingsPrivate;
typedef struct _BlxEncodingsClass	BlxEncodingsClass;

typedef enum
{
	LG_NONE			= 0,
	LG_ARABIC		= 1 << 0,
	LG_BALTIC		= 1 << 1,
	LG_CAUCASIAN		= 1 << 2,
	LG_C_EUROPEAN		= 1 << 3,
	LG_CHINESE_TRAD		= 1 << 4,
	LG_CHINESE_SIMP		= 1 << 5,
	LG_CYRILLIC		= 1 << 6,
	LG_GREEK		= 1 << 7,
	LG_HEBREW		= 1 << 8,
	LG_INDIAN		= 1 << 9,
	LG_JAPANESE		= 1 << 10,
	LG_KOREAN		= 1 << 12,
	LG_NORDIC		= 1 << 13,
	LG_PERSIAN		= 1 << 14,
	LG_SE_EUROPEAN		= 1 << 15,
	LG_THAI			= 1 << 16,
	LG_TURKISH		= 1 << 17,
	LG_UKRAINIAN		= 1 << 18,
	LG_UNICODE		= 1 << 19,
	LG_VIETNAMESE		= 1 << 20,
	LG_WESTERN		= 1 << 21,
	LG_ALL			= 0x3fffff,
}
BlxLanguageGroup;

enum
{
	BLX_NODE_ENCODING_PROP_TITLE = 1,
	BLX_NODE_ENCODING_PROP_TITLE_ELIDED = 2,
	BLX_NODE_ENCODING_PROP_COLLATION_KEY = 3,
	BLX_NODE_ENCODING_PROP_ENCODING = 4,
	BLX_NODE_ENCODING_PROP_LANGUAGE_GROUPS = 5,
};

struct _BlxEncodings
{
	GObject parent;

	/*< private >*/
	BlxEncodingsPrivate *priv;
};

struct _BlxEncodingsClass
{
	GObjectClass parent_class;
};

GType		 blx_encodings_get_type        (void);

BlxEncodings	*blx_encodings_new             (void);

BlxNode	*blx_encodings_get_node	(BlxEncodings *encodings,
						 const char *code,
						 gboolean add_if_not_found);

GList		*blx_encodings_get_encodings	(BlxEncodings *encodings,
						 BlxLanguageGroup group_mask);

BlxNode	*blx_encodings_get_all		(BlxEncodings *encodings);

void		 blx_encodings_add_recent	(BlxEncodings *encodings,
						 const char *code);

GList		*blx_encodings_get_recent	(BlxEncodings *encodings);

G_END_DECLS

#endif
