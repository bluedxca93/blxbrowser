/*
 *  Copyright © 2000-2003 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_HISTORY_H
#define BLX_HISTORY_H

#include <glib-object.h>

#include "blx-node.h"

G_BEGIN_DECLS

#define BLX_TYPE_HISTORY		(blx_history_get_type ())
#define BLX_HISTORY(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_HISTORY, BlxHistory))
#define BLX_HISTORY_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_HISTORY, BlxHistoryClass))
#define BLX_IS_HISTORY(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_HISTORY))
#define BLX_IS_HISTORY_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_HISTORY))
#define BLX_HISTORY_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_HISTORY, BlxHistoryClass))

typedef struct _BlxHistoryClass	BlxHistoryClass;
typedef struct _BlxHistory		BlxHistory;
typedef struct _BlxHistoryPrivate	BlxHistoryPrivate;

enum
{
	BLX_NODE_PAGE_PROP_TITLE = 2,
	BLX_NODE_PAGE_PROP_LOCATION = 3,
	BLX_NODE_PAGE_PROP_VISITS = 4,
	BLX_NODE_PAGE_PROP_LAST_VISIT = 5,
	BLX_NODE_PAGE_PROP_FIRST_VISIT = 6,
	BLX_NODE_PAGE_PROP_HOST_ID = 7,
	BLX_NODE_PAGE_PROP_PRIORITY = 8,
	BLX_NODE_PAGE_PROP_ICON = 9,
	BLX_NODE_HOST_PROP_ZOOM = 10,
	BLX_NODE_PAGE_PROP_GECKO_FLAGS = 11,
	BLX_NODE_PAGE_PROP_EXTRA_FLAGS = 12
};

struct _BlxHistory
{
	GObject parent;

	/*< private >*/
	BlxHistoryPrivate *priv;
};

struct _BlxHistoryClass
{
	GObjectClass parent_class;

	/* Signals */
	gboolean (* add_page)	 (BlxHistory *history,
				  const char *url,
				  gboolean redirect,
				  gboolean toplevel);
	void	(* visited)	 (BlxHistory *history,
				  const char *url);
	void	(* cleared)	 (BlxHistory *history);
				 
	void	(* redirect)	 (BlxHistory *history,
				  const char *from_uri,
				  const char *to_uri);
	void	(* icon_updated) (BlxHistory *history,
				  const char *address,
				  const char *icon);
};

GType		blx_history_get_type		(void);

BlxHistory    *blx_history_new		(void);

BlxNode       *blx_history_get_hosts		(BlxHistory *gh);

BlxNode       *blx_history_get_host		(BlxHistory *gh,
						 const char *url);

BlxNode       *blx_history_get_pages		(BlxHistory *gh);

BlxNode       *blx_history_get_page		(BlxHistory *gh,
						 const char *url);

void		blx_history_add_page		(BlxHistory *gh,
						 const char *url,
						 gboolean redirect,
						 gboolean toplevel);

gboolean	blx_history_is_page_visited	(BlxHistory *gh,
						 const char *url);

int		blx_history_get_page_visits	(BlxHistory *gh,
						 const char *url);

void		blx_history_set_page_title	(BlxHistory *gh,
						 const char *url,
						 const char *title);

const char     *blx_history_get_last_page	(BlxHistory *gh);

void		blx_history_set_icon		(BlxHistory *gh,
						 const char *url,
						 const char *icon);
const char     *blx_history_get_icon		(BlxHistory *gh,
						 const char *url);

void		blx_history_clear		(BlxHistory *gh);

gboolean	blx_history_is_enabled		(BlxHistory *history);

void		blx_history_set_enabled	(BlxHistory *history,
						 gboolean enabled);

G_END_DECLS

#endif
