/*
 *  Copyright © 2002 Jorn Baayen
 *  Copyright © 2003-2004 Marco Pesenti Gritti
 *  Copyright © 2004, 2005 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_FAVICON_CACHE_H
#define BLX_FAVICON_CACHE_H

#include <glib-object.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

G_BEGIN_DECLS

#define BLX_TYPE_FAVICON_CACHE         (blx_favicon_cache_get_type ())
#define BLX_FAVICON_CACHE(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_FAVICON_CACHE, BlxFaviconCache))
#define BLX_FAVICON_CACHE_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST ((k), BLX_TYPE_FAVICON_CACHE, BlxFaviconCacheClass))
#define BLX_IS_FAVICON_CACHE(o)	(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_FAVICON_CACHE))
#define BLX_IS_FAVICON_CACHE_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_FAVICON_CACHE))
#define BLX_FAVICON_CACHE_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_FAVICON_CACHE, BlxFaviconCacheClass))

typedef struct _BlxFaviconCacheClass	BlxFaviconCacheClass;
typedef struct _BlxFaviconCache	BlxFaviconCache;
typedef struct _BlxFaviconCachePrivate	BlxFaviconCachePrivate;

struct _BlxFaviconCache
{
	GObject parent;

	/*< private >*/
	BlxFaviconCachePrivate *priv;
};

struct _BlxFaviconCacheClass
{
	GObjectClass parent_class;

	/* Signals */
	void (*changed)	(BlxFaviconCache *cache,
			 const char *url);
};

GType		 blx_favicon_cache_get_type	(void);

BlxFaviconCache *blx_favicon_cache_new	(void);

GdkPixbuf	 *blx_favicon_cache_get	(BlxFaviconCache *cache,
						 const char *url);

void		  blx_favicon_cache_clear	(BlxFaviconCache *cache);

G_END_DECLS

#endif /* BLX_FAVICON_CACHE_H */
