/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2; -*- */
/*  Copyright © 2008 Xan Lopez <xan@gnome.org>
 *  Copyright © 2009 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#ifndef __BLX_EMBED_PREFS_H__
#define __BLX_EMBED_PREFS_H__

#include "blx-embed.h"

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#define USER_STYLESHEET_FILENAME	"user-stylesheet.css"

G_BEGIN_DECLS

void blx_embed_prefs_init                  (void);
void blx_embed_prefs_shutdown              (void);
void blx_embed_prefs_add_embed             (BlxEmbed     *embed);
void blx_embed_prefs_set_cookie_jar_policy (SoupCookieJar *jar,
                                             const char    *gconf_policy);

G_END_DECLS

#endif /* __BLX_EMBED_PREFS_H__ */
