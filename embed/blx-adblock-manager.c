/*
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003 Christian Persch
 *  Copyright © 2005 Jean-François Rameau
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"
#include "blx-debug.h"

#include "blx-adblock-manager.h"
#include "blx-adblock.h"

struct _BlxAdBlockManagerPrivate
{
       	BlxAdBlock *blocker;
};

G_DEFINE_TYPE (BlxAdBlockManager, blx_adblock_manager, G_TYPE_OBJECT);

#define BLX_ADBLOCK_MANAGER_GET_PRIVATE(object) \
	(G_TYPE_INSTANCE_GET_PRIVATE ((object), \
         BLX_TYPE_ADBLOCK_MANAGER, BlxAdBlockManagerPrivate))


/**
 * blx_adblock_manager_set_blocker:
 * @shell: a #BlxAdBlockManager
 * @blocker: the new blocker or NULL
 *
 * Set a new ad blocker. If #blocker is %NULL,
 * ad blocking is toggled off.
 *
 **/
void
blx_adblock_manager_set_blocker (BlxAdBlockManager *self,
			  	  BlxAdBlock *blocker)
{
	self->priv->blocker = blocker;
}

/**
 * blx_adblock_manager_should_load:
 * @shell: a #BlxAdBlockManager
 * @url: the target url to be loaded or not
 * @AdUriCheckType: what check to be applied (image, script, ...)
 *
 * Check if an url is to be loaded or not 
 *
 * Return value: TRUE if the url is to be loaded
 **/
gboolean
blx_adblock_manager_should_load (BlxAdBlockManager *self,
				  BlxEmbed *embed,
	    	 	    	  const char *url,
	    	 	    	  AdUriCheckType check_type)
{
	if (self->priv->blocker != NULL)
	{
		return blx_adblock_should_load (self->priv->blocker,	
						 embed,
						 url,
						 check_type);
	}

	/* default: let's process any url */
	return TRUE;
}

static void
blx_adblock_manager_init (BlxAdBlockManager *self)
{
	LOG ("blx_adblock_manager_init");

	self->priv = BLX_ADBLOCK_MANAGER_GET_PRIVATE(self);
}

static void
blx_adblock_manager_class_init (BlxAdBlockManagerClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	g_signal_new ("rules_changed",
		      G_OBJECT_CLASS_TYPE (object_class),
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET (BlxAdBlockManagerClass, rules_changed),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__VOID,
		      G_TYPE_NONE,
		      0,
		      0);

	g_type_class_add_private (object_class, sizeof (BlxAdBlockManagerPrivate));
}

/**
 * blx_adblock_manager_edit_rule:
 * @shell: a #BlxAdBlockManager
 * @url: the target url on which the rule is based
 * @allowed: TRUE if the url has to be blocked.
 *
 * Ask to the blocker a new rule based on @url.
 *
 **/
void
blx_adblock_manager_edit_rule (BlxAdBlockManager *self,
	    	 	    	const char *url,
	    	 	    	gboolean allowed)
{
	if (self->priv->blocker != NULL)
	{
		blx_adblock_edit_rule (self->priv->blocker,	
					url,
					allowed);
	}
}

/**
 * blx_adblock_manager_has_blocker:
 * @shell: a #BlxAdBlockManager
 *
 * Check if Blxbrowser has currently an active blocker
 *
 * Return value: TRUE if an active blocker is running
 **/
gboolean
blx_adblock_manager_has_blocker (BlxAdBlockManager *self)
{
	return self->priv->blocker != NULL;
} 
