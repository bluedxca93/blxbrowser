/*
 *  Copyright © 2008 Gustavo Noronha Silva
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 *  $Id$
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_WEB_VIEW_H
#define BLX_WEB_VIEW_H

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <webkit/webkit.h>

#include "blx-embed-event.h"

G_BEGIN_DECLS

#define BLX_TYPE_WEB_VIEW         (blx_web_view_get_type ())
#define BLX_WEB_VIEW(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_WEB_VIEW, BlxWebView))
#define BLX_WEB_VIEW_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_WEB_VIEW, BlxWebViewClass))
#define BLX_IS_WEB_VIEW(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_WEB_VIEW))
#define BLX_IS_WEB_VIEW_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_WEB_VIEW))
#define BLX_WEB_VIEW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_WEB_VIEW, BlxWebViewClass))

typedef struct _BlxWebViewClass  BlxWebViewClass;
typedef struct _BlxWebView    BlxWebView;
typedef struct _BlxWebViewPrivate  BlxWebViewPrivate;

typedef enum
{
	BLX_WEB_VIEW_NAV_UP		= 1 << 0,
	BLX_WEB_VIEW_NAV_BACK		= 1 << 1,
	BLX_WEB_VIEW_NAV_FORWARD	= 1 << 2
} BlxWebViewNavigationFlags;

typedef enum
{
	BLX_WEB_VIEW_CHROME_MENUBAR		= 1 << 0,
	BLX_WEB_VIEW_CHROME_TOOLBAR		= 1 << 1,
	BLX_WEB_VIEW_CHROME_STATUSBAR		= 1 << 2,
	BLX_WEB_VIEW_CHROME_BOOKMARKSBAR	= 1 << 3
} BlxWebViewChrome;

#define BLX_WEB_VIEW_CHROME_ALL (BLX_WEB_VIEW_CHROME_MENUBAR |	\
			       BLX_WEB_VIEW_CHROME_TOOLBAR |	\
			       BLX_WEB_VIEW_CHROME_STATUSBAR |	\
			       BLX_WEB_VIEW_CHROME_BOOKMARKSBAR)

typedef enum
{
	BLX_WEB_VIEW_STATE_IS_UNKNOWN,
	BLX_WEB_VIEW_STATE_IS_INSECURE,
	BLX_WEB_VIEW_STATE_IS_BROKEN,
	BLX_WEB_VIEW_STATE_IS_SECURE_LOW,
	BLX_WEB_VIEW_STATE_IS_SECURE_MED,
	BLX_WEB_VIEW_STATE_IS_SECURE_HIGH
} BlxWebViewSecurityLevel;

typedef enum
{
	BLX_WEB_VIEW_DOCUMENT_HTML,
	BLX_WEB_VIEW_DOCUMENT_XML,
	BLX_WEB_VIEW_DOCUMENT_IMAGE,
	BLX_WEB_VIEW_DOCUMENT_OTHER
} BlxWebViewDocumentType;

typedef enum {
  BLX_WEB_VIEW_ERROR_PAGE_NETWORK_ERROR,
  BLX_WEB_VIEW_ERROR_PAGE_CRASH
} BlxWebViewErrorPage;

struct _BlxWebView
{
  WebKitWebView parent;

  /*< private >*/
  BlxWebViewPrivate *priv;
};

struct _BlxWebViewClass
{
  WebKitWebViewClass parent_class;

  /* Signals */
  void	 (* feed_link)		(BlxWebView *view,
                                 const char *type,
                                 const char *title,
                                 const char *address);
  void	 (* search_link)	(BlxWebView *view,
                                 const char *type,
                                 const char *title,
                                 const char *address);
  void	 (* popup_blocked)	(BlxWebView *view,
                                 const char *address,
                                 const char *target,
                                 const char *features);
  void	 (* content_blocked)	(BlxWebView *view,
                                 const char *uri);
  gboolean (* modal_alert)	(BlxWebView *view);
  void	 (* modal_alert_closed) (BlxWebView *view);
  void	 (* new_window)		(BlxWebView *view,
                                 BlxWebView *new_view);
  gboolean (* search_key_press)	(BlxWebView *view,
                                 GdkEventKey *event);
  gboolean (* close_request)	(BlxWebView *view);

  void	 (* new_document_now)	(BlxWebView *view,
                                 const char *uri);
};

GType                      blx_web_view_get_type                 (void);
GType                      blx_web_view_chrome_get_type          (void);
GType                      blx_web_view_security_level_get_type  (void);
GtkWidget *                blx_web_view_new                      (void);
void                       blx_web_view_load_request             (BlxWebView               *view,
                                                                   WebKitNetworkRequest      *request);
void                       blx_web_view_load_url                 (BlxWebView               *view,
                                                                   const char                *url);
void                       blx_web_view_copy_back_history        (BlxWebView               *source,
                                                                   BlxWebView               *dest);
void                       blx_web_view_clear_history            (BlxWebView               *view);
gboolean                   blx_web_view_is_loading               (BlxWebView               *view);
const char *               blx_web_view_get_loading_title        (BlxWebView               *view);
GdkPixbuf *                blx_web_view_get_icon                 (BlxWebView               *view);
BlxWebViewDocumentType    blx_web_view_get_document_type        (BlxWebView               *view);
BlxWebViewNavigationFlags blx_web_view_get_navigation_flags     (BlxWebView               *view);
const char *               blx_web_view_get_status_message       (BlxWebView               *view);
const char *               blx_web_view_get_link_message         (BlxWebView               *view);
gboolean                   blx_web_view_get_visibility           (BlxWebView               *view);
void                       blx_web_view_set_link_message         (BlxWebView               *view,
                                                                   char                      *link_message);
void                       blx_web_view_set_security_level       (BlxWebView               *view,
                                                                   BlxWebViewSecurityLevel   level);
void                       blx_web_view_set_visibility           (BlxWebView               *view,
                                                                   gboolean                   visibility);
const char *               blx_web_view_get_typed_address        (BlxWebView               *view);
void                       blx_web_view_set_typed_address        (BlxWebView               *view,
                                                                   const char                *address);
gboolean                   blx_web_view_get_is_blank             (BlxWebView               *view);
gboolean                   blx_web_view_has_modified_forms       (BlxWebView               *view);
char *                     blx_web_view_get_location             (BlxWebView               *view,
                                                                   gboolean                   toplevel);
void                       blx_web_view_go_up                    (BlxWebView               *view);
void                       blx_web_view_get_security_level       (BlxWebView               *view,
                                                                   BlxWebViewSecurityLevel  *level,
                                                                   char                     **description);
void                       blx_web_view_show_page_certificate    (BlxWebView               *view);
void                       blx_web_view_show_print_preview       (BlxWebView               *view);
void                       blx_web_view_print                    (BlxWebView               *view);
GSList *                   blx_web_view_get_go_up_list           (BlxWebView               *view);
void                       blx_web_view_set_title                (BlxWebView               *view,
                                                                   const char                *view_title);
const char *               blx_web_view_get_title                (BlxWebView               *view);
gboolean                   blx_web_view_can_go_up                (BlxWebView               *view);
const char *               blx_web_view_get_address              (BlxWebView               *view);
const char *               blx_web_view_get_title_composite      (BlxWebView               *view);

void                       blx_web_view_load_error_page          (BlxWebView		     *view,
                                                                   const char		     *uri,
                                                                   BlxWebViewErrorPage	      page,
                                                                   GError		     *error);


/* These should be private */
void                       blx_web_view_set_address              (BlxWebView               *view,
                                                                   const char                *address);
void                       blx_web_view_location_changed         (BlxWebView               *view,
                                                                   const char                *location);
void                       blx_web_view_set_loading_title        (BlxWebView               *view,
                                                                   const char                *title,
                                                                   gboolean                   is_address);
void                       blx_web_view_popups_manager_reset     (BlxWebView               *view);
void                       blx_web_view_save                     (BlxWebView               *view,
                                                                   const char                *uri);
void                       blx_web_view_load_homepage            (BlxWebView               *view);

char *
blx_web_view_create_web_application (BlxWebView *view, const char *title, GdkPixbuf *icon);

GdkPixbuf *
blx_web_view_get_snapshot (BlxWebView *view, int x, int y, int width, int height);

G_END_DECLS

#endif
