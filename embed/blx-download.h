/* vim: set sw=2 ts=2 sts=2 et: */
/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 * blx-download.h
 * This file is part of Blxbrowser
 *
 * Copyright © 2011 - Igalia S.L.
 *
 * Blxbrowser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Blxbrowser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blxbrowser; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef _BLX_DOWNLOAD_H
#define _BLX_DOWNLOAD_H

#include <glib-object.h>
#include <webkit/webkit.h>

#include "blx-embed.h"

G_BEGIN_DECLS

#define BLX_TYPE_DOWNLOAD              blx_download_get_type()
#define BLX_DOWNLOAD(obj)              (G_TYPE_CHECK_INSTANCE_CAST ((obj), BLX_TYPE_DOWNLOAD, BlxDownload))
#define BLX_DOWNLOAD_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_DOWNLOAD, BlxDownloadClass))
#define BLX_IS_DOWNLOAD(obj)           (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_DOWNLOAD))
#define BLX_IS_DOWNLOAD_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), BLX_TYPE_DOWNLOAD))
#define BLX_DOWNLOAD_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), BLX_TYPE_DOWNLOAD, BlxDownloadClass))

typedef struct _BlxDownload BlxDownload;
typedef struct _BlxDownloadClass BlxDownloadClass;
typedef struct _BlxDownloadPrivate BlxDownloadPrivate;

struct _BlxDownload
{
  GObject parent;

  BlxDownloadPrivate *priv;
};

struct _BlxDownloadClass
{
  GObjectClass parent_class;

  void (* completed)  (BlxDownload *download);
  void (* error)      (BlxDownload *download,
                       gint error_code,
                       gint error_detail,
                       char *reason);
};

typedef enum
{
  BLX_DOWNLOAD_ACTION_NONE,
  BLX_DOWNLOAD_ACTION_AUTO,
  BLX_DOWNLOAD_ACTION_BROWSE_TO,
  BLX_DOWNLOAD_ACTION_OPEN
} BlxDownloadActionType;

GType         blx_download_get_type              (void) G_GNUC_CONST;

BlxDownload *blx_download_new                   (void);
BlxDownload *blx_download_new_for_uri           (const char *uri);
BlxDownload *blx_download_new_for_download      (WebKitDownload *download);


void          blx_download_start                 (BlxDownload *download);
void          blx_download_pause                 (BlxDownload *download);
void          blx_download_cancel                (BlxDownload *download);

void          blx_download_set_auto_destination  (BlxDownload *download);
void          blx_download_set_destination_uri   (BlxDownload *download,
                                                   const char *destination);

WebKitDownload *blx_download_get_webkit_download (BlxDownload *download);

const char   *blx_download_get_destination_uri   (BlxDownload *download);
const char   *blx_download_get_source_uri        (BlxDownload *download);


guint32       blx_download_get_start_time        (BlxDownload *download);

GtkWidget    *blx_download_get_window            (BlxDownload *download);
void          blx_download_set_window            (BlxDownload *download,
                                                   GtkWidget *window);

BlxDownloadActionType blx_download_get_action   (BlxDownload *download);
void          blx_download_set_action            (BlxDownload *download,
                                                   BlxDownloadActionType action);
gboolean      blx_download_do_download_action    (BlxDownload *download,
                                                   BlxDownloadActionType action);

GtkWidget    *blx_download_get_widget            (BlxDownload *download);
void          blx_download_set_widget            (BlxDownload *download,
                                                   GtkWidget *widget);

G_END_DECLS

#endif /* _BLX_DOWNLOAD_H */
