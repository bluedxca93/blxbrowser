/*
 *  Copyright © 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_EMBED_DIALOG_H
#define BLX_EMBED_DIALOG_H

#include "blx-embed.h"
#include "blx-dialog.h"

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_EMBED_DIALOG		(blx_embed_dialog_get_type ())
#define BLX_EMBED_DIALOG(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_EMBED_DIALOG, BlxEmbedDialog))
#define BLX_EMBED_DIALOG_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_EMBED_DIALOG, BlxEmbedDialogClass))
#define BLX_IS_EMBED_DIALOG(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_EMBED_DIALOG))
#define BLX_IS_EMBED_DIALOG_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_EMBED_DIALOG))
#define BLX_EMBED_DIALOG_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_EMBED_DIALOG, BlxEmbedDialogClass))

typedef struct _BlxEmbedDialogClass	BlxEmbedDialogClass;
typedef struct _BlxEmbedDialog		BlxEmbedDialog;
typedef struct _BlxEmbedDialogPrivate	BlxEmbedDialogPrivate;

struct _BlxEmbedDialog
{
        BlxDialog parent;

	/*< private >*/
        BlxEmbedDialogPrivate *priv;
};

struct _BlxEmbedDialogClass
{
        BlxDialogClass parent_class;
};

GType			blx_embed_dialog_get_type		(void);

BlxEmbedDialog	       *blx_embed_dialog_new			(BlxEmbed *embed);

BlxEmbedDialog	       *blx_embed_dialog_new_with_parent	(GtkWidget *parent_window,
								BlxEmbed *embed);

void			blx_embed_dialog_set_embed		(BlxEmbedDialog *dialog,
								 BlxEmbed *embed);

BlxEmbed *		blx_embed_dialog_get_embed		(BlxEmbedDialog *dialog);

G_END_DECLS

#endif
