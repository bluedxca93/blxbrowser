/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2; -*- */
/*
 *  Copyright © 2000-2003 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#define LIBSOUP_I_HAVE_READ_BUG_594377_AND_KNOW_SOUP_PASSWORD_MANAGER_MIGHT_GO_AWAY
#define LIBSOUP_USE_UNSTABLE_REQUEST_API
#define NSPLUGINWRAPPER_SETUP "/usr/bin/mozilla-plugin-config"

#include "blx-embed-single.h"
#include "blx-embed-shell.h"
#include "blx-embed-prefs.h"
#include "blx-embed-type-builtins.h"
#include "blx-debug.h"
#include "blx-file-helpers.h"
#include "blx-signal-accumulator.h"
#include "blx-permission-manager.h"
#include "blx-profile-utils.h"
#include "blx-prefs.h"
#include "blx-settings.h"
#include "blx-request-about.h"

#include <webkit/webkit.h>
#include <glib/gi18n.h>
#include <libsoup/soup-gnome.h>
#include <libsoup/soup-cache.h>
#include <libsoup/soup-requester.h>
#include <gnome-keyring.h>

#define BLX_EMBED_SINGLE_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), BLX_TYPE_EMBED_SINGLE, BlxEmbedSinglePrivate))

struct _BlxEmbedSinglePrivate {
  guint online : 1;

  GHashTable *form_auth_data;
  SoupCache *cache;
};

enum {
  PROP_0,
  PROP_NETWORK_STATUS
};

static void blx_embed_single_init (BlxEmbedSingle *single);
static void blx_embed_single_class_init (BlxEmbedSingleClass *klass);
static void blx_permission_manager_iface_init (BlxPermissionManagerIface *iface);

static void
blx_embed_single_get_property (GObject *object,
                                guint prop_id,
                                GValue *value,
                                GParamSpec *pspec)
{
  BlxEmbedSingle *single = BLX_EMBED_SINGLE (object);

  switch (prop_id) {
  case PROP_NETWORK_STATUS:
    g_value_set_boolean (value, blx_embed_single_get_network_status (single));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}

static void
blx_embed_single_set_property (GObject *object,
                                guint prop_id,
                                const GValue *value,
                                GParamSpec *pspec)
{
  BlxEmbedSingle *single = BLX_EMBED_SINGLE (object);

  switch (prop_id) {
  case PROP_NETWORK_STATUS:
    blx_embed_single_set_network_status (single, g_value_get_boolean (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}

/* Some compilers (like gcc 2.95) don't support preprocessor directives inside macros,
   so we have to duplicate the whole thing */

G_DEFINE_TYPE_WITH_CODE (BlxEmbedSingle, blx_embed_single, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (BLX_TYPE_PERMISSION_MANAGER,
                                                blx_permission_manager_iface_init))

static void
form_auth_data_free (BlxEmbedSingleFormAuthData *data)
{
  g_free (data->form_username);
  g_free (data->form_password);
  g_free (data->username);

  g_slice_free (BlxEmbedSingleFormAuthData, data);
}

static BlxEmbedSingleFormAuthData*
form_auth_data_new (const char *form_username,
                    const char *form_password,
                    const char *username)
{
  BlxEmbedSingleFormAuthData *data;

  data = g_slice_new (BlxEmbedSingleFormAuthData);
  data->form_username = g_strdup (form_username);
  data->form_password = g_strdup (form_password);
  data->username = g_strdup (username);

  return data;
}

static void
get_attr_cb (GnomeKeyringResult result,
             GnomeKeyringAttributeList *attributes,
             BlxEmbedSingle *single)
{
  int i = 0;
  GnomeKeyringAttribute *attribute;
  char *server = NULL, *username = NULL;

  if (result != GNOME_KEYRING_RESULT_OK)
    return;

  attribute = (GnomeKeyringAttribute*)attributes->data;
  for (i = 0; i < attributes->len; i++) {
    if (server && username)
      break;

    if (attribute[i].type == GNOME_KEYRING_ATTRIBUTE_TYPE_STRING) {
      if (g_str_equal (attribute[i].name, "server")) {
        server = g_strdup (attribute[i].value.string);
      } else if (g_str_equal (attribute[i].name, "user")) {
        username = g_strdup (attribute[i].value.string);
      }
    }
  }

  if (server && username &&
      g_strstr_len (server, -1, "form%5Fusername") &&
      g_strstr_len (server, -1, "form%5Fpassword")) {
    /* This is a stored login/password from a form, cache the form
     * names locally so we don't need to hit the keyring daemon all
     * the time */
    const char *form_username, *form_password;
    GHashTable *t;
    SoupURI *uri = soup_uri_new (server);
    t = soup_form_decode (uri->query);
    form_username = g_hash_table_lookup (t, FORM_USERNAME_KEY);
    form_password = g_hash_table_lookup (t, FORM_PASSWORD_KEY);
    blx_embed_single_add_form_auth (single, uri->host, form_username, form_password, username);
    soup_uri_free (uri);
    g_hash_table_destroy (t);
  }

  g_free (server);
  g_free (username);
}

static void
store_form_data_cb (GnomeKeyringResult result, GList *l, BlxEmbedSingle *single)
{
  GList *p;

  if (result != GNOME_KEYRING_RESULT_OK)
    return;

  for (p = l; p; p = p->next) {
    guint key_id = GPOINTER_TO_UINT (p->data);
    gnome_keyring_item_get_attributes (GNOME_KEYRING_DEFAULT,
                                       key_id,
                                       (GnomeKeyringOperationGetAttributesCallback) get_attr_cb,
                                       single,
                                       NULL);
  }
}

static void
cache_keyring_form_data (BlxEmbedSingle *single)
{
  gnome_keyring_list_item_ids (GNOME_KEYRING_DEFAULT,
                               (GnomeKeyringOperationGetListCallback)store_form_data_cb,
                               single,
                               NULL);
}

static void
free_form_auth_data_list (gpointer data)
{
  GSList *p, *l = (GSList*)data;

  for (p = l; p; p = p->next)
    form_auth_data_free ((BlxEmbedSingleFormAuthData*)p->data);

  g_slist_free (l);
}

static void
remove_form_auth_data (gpointer key, gpointer value, gpointer user_data)
{
  if (value)
    free_form_auth_data_list ((GSList*)value);
}

static void
blx_embed_single_dispose (GObject *object)
{
  BlxEmbedSinglePrivate *priv = BLX_EMBED_SINGLE (object)->priv;

  if (priv->cache) {
    soup_cache_flush (priv->cache);
    soup_cache_dump (priv->cache);
    g_object_unref (priv->cache);
    priv->cache = NULL;
  }

  G_OBJECT_CLASS (blx_embed_single_parent_class)->dispose (object);
}

static void
blx_embed_single_finalize (GObject *object)
{
  BlxEmbedSinglePrivate *priv = BLX_EMBED_SINGLE (object)->priv;

  blx_embed_prefs_shutdown ();

  if (priv->form_auth_data) {
    g_hash_table_foreach (priv->form_auth_data,
                          (GHFunc)remove_form_auth_data,
                          NULL);
    g_hash_table_destroy (priv->form_auth_data);
  }

  G_OBJECT_CLASS (blx_embed_single_parent_class)->finalize (object);
}

static void
blx_embed_single_init (BlxEmbedSingle *single)
{
  BlxEmbedSinglePrivate *priv;

  single->priv = priv = BLX_EMBED_SINGLE_GET_PRIVATE (single);
  priv->online = TRUE;

  priv->form_auth_data = g_hash_table_new_full (g_str_hash,
                                                g_str_equal,
                                                g_free,
                                                NULL);
  cache_keyring_form_data (single);
}

static void
blx_embed_single_class_init (BlxEmbedSingleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = blx_embed_single_finalize;
  object_class->dispose = blx_embed_single_dispose;
  object_class->get_property = blx_embed_single_get_property;
  object_class->set_property = blx_embed_single_set_property;

  /**
   * BlxEmbedSingle::new-window:
   * @parent_embed: the #BlxEmbed requesting the new window, or %NULL
   * @mask: a #BlxEmbedChrome
   *
   * The ::new_window signal is emitted when a new window needs to be opened.
   * For example, when a JavaScript popup window was opened.
   *
   * Returns: (transfer none): a new #BlxEmbed.
   **/
  g_signal_new ("new-window",
                BLX_TYPE_EMBED_SINGLE,
                G_SIGNAL_RUN_FIRST | G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET (BlxEmbedSingleClass, new_window),
                blx_signal_accumulator_object, blx_embed_get_type,
                g_cclosure_marshal_generic,
                GTK_TYPE_WIDGET,
                2,
                GTK_TYPE_WIDGET,
                BLX_TYPE_WEB_VIEW_CHROME);

  /**
   * BlxEmbedSingle::handle_content:
   * @single:
   * @mime_type: the MIME type of the content
   * @address: the URL to the content
   *
   * The ::handle_content signal is emitted when encountering content of a mime
   * type Blxbrowser is unable to handle itself.
   *
   * If a connected callback returns %TRUE, the signal will stop propagating. For
   * example, this could be used by a download manager to prevent other
   * ::handle_content listeners from being called.
   **/
  g_signal_new ("handle_content",
                BLX_TYPE_EMBED_SINGLE,
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET (BlxEmbedSingleClass, handle_content),
                g_signal_accumulator_true_handled, NULL,
                g_cclosure_marshal_generic,
                G_TYPE_BOOLEAN,
                2,
                G_TYPE_STRING,
                G_TYPE_STRING);

  /**
   * BlxEmbedSingle::network-status:
   * 
   * Whether the network is on-line.
   */
  g_object_class_install_property
    (object_class,
     PROP_NETWORK_STATUS,
     g_param_spec_boolean ("network-status",
                           "network-status",
                           "network-status",
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

  g_type_class_add_private (object_class, sizeof (BlxEmbedSinglePrivate));
}

static void
impl_permission_manager_add (BlxPermissionManager *manager,
                             const char *host,
                             const char *type,
                             BlxPermission permission)
{
}

static void
impl_permission_manager_remove (BlxPermissionManager *manager,
                                const char *host,
                                const char *type)
{
}

static void
impl_permission_manager_clear (BlxPermissionManager *manager)
{
}

static BlxPermission
impl_permission_manager_test (BlxPermissionManager *manager,
                              const char *host,
                              const char *type)
{
  g_return_val_if_fail (type != NULL && type[0] != '\0', BLX_PERMISSION_DEFAULT);

  return (BlxPermission)0;
}

static GList *
impl_permission_manager_list (BlxPermissionManager *manager,
                              const char *type)
{
  GList *list = NULL;
  return list;
}

static void
blx_permission_manager_iface_init (BlxPermissionManagerIface *iface)
{
  iface->add = impl_permission_manager_add;
  iface->remove = impl_permission_manager_remove;
  iface->clear = impl_permission_manager_clear;
  iface->test = impl_permission_manager_test;
  iface->list = impl_permission_manager_list;
}

static void
cache_size_cb (GSettings *settings,
               char *key,
               BlxEmbedSingle *single)
{
  int new_cache_size = g_settings_get_int (settings, key);
  soup_cache_set_max_size (single->priv->cache, new_cache_size * 1024 * 1024 /* in bytes */);
}

/**
 * blx_embed_single_initialize:
 * @single: the #BlxEmbedSingle
 * 
 * Performs startup initialisations. Must be called before calling
 * any other methods.
 **/
gboolean
blx_embed_single_initialize (BlxEmbedSingle *single)
{
  SoupSession *session;
  SoupCookieJar *jar;
  char *filename;
  char *cookie_policy;
  char *cache_dir;
  BlxEmbedSinglePrivate *priv = single->priv;
  SoupSessionFeature *requester;

  /* Initialise nspluginwrapper's plugins if available */
  if (g_file_test (NSPLUGINWRAPPER_SETUP, G_FILE_TEST_EXISTS) != FALSE)
    g_spawn_command_line_sync (NSPLUGINWRAPPER_SETUP, NULL, NULL, NULL, NULL);

  blx_embed_prefs_init ();

  session = webkit_get_default_session ();

  /* Check SSL certificates */
  g_object_set (session,
                SOUP_SESSION_SSL_USE_SYSTEM_CA_FILE, TRUE,
                SOUP_SESSION_SSL_STRICT, FALSE,
                NULL);

  /* Store cookies in moz-compatible SQLite format */
  filename = g_build_filename (blx_dot_dir (), "cookies.sqlite", NULL);
  jar = soup_cookie_jar_sqlite_new (filename, FALSE);
  g_free (filename);
  cookie_policy = g_settings_get_string (BLX_SETTINGS_WEB,
                                         BLX_PREFS_WEB_COOKIES_POLICY);
  blx_embed_prefs_set_cookie_jar_policy (jar, cookie_policy);
  g_free (cookie_policy);

  soup_session_add_feature (session, SOUP_SESSION_FEATURE (jar));
  g_object_unref (jar);

  /* Use GNOME proxy settings through libproxy */
  soup_session_add_feature_by_type (session, SOUP_TYPE_PROXY_RESOLVER_GNOME);

  /* WebKitSoupCache */
  cache_dir = g_build_filename (g_get_user_cache_dir (), g_get_prgname (), NULL);
  priv->cache = soup_cache_new (cache_dir, SOUP_CACHE_SINGLE_USER);
  g_free (cache_dir);

  soup_session_add_feature (session, SOUP_SESSION_FEATURE (priv->cache));
  /* Cache size in Mb: 1024 * 1024 */
  soup_cache_set_max_size (priv->cache, g_settings_get_int (BLX_SETTINGS_WEB, BLX_PREFS_CACHE_SIZE) << 20);
  soup_cache_load (priv->cache);

  g_signal_connect (BLX_SETTINGS_WEB,
                    "changed::" BLX_PREFS_CACHE_SIZE,
                    G_CALLBACK (cache_size_cb),
                    single);

  /* about: URIs handler */
  requester = SOUP_SESSION_FEATURE (soup_requester_new());
  soup_session_add_feature (session, requester);
  soup_session_feature_add_feature (requester, BLX_TYPE_REQUEST_ABOUT);
  g_object_unref (requester);

#ifdef SOUP_TYPE_PASSWORD_MANAGER
  /* Use GNOME keyring to store passwords. Only add the manager if we
     are not using a private session, otherwise we want any new
     password to expire when we exit *and* we don't want to use any
     existing password in the keyring */
  if (blx_embed_shell_get_mode (blx_embed_shell_get_default ()) != BLX_EMBED_SHELL_MODE_PRIVATE)
    soup_session_add_feature_by_type (session, SOUP_TYPE_PASSWORD_MANAGER_GNOME);
#endif

  return TRUE;
}

/**
 * blx_embed_single_clear_cache:
 * @single: the #BlxEmbedSingle
 * 
 * Clears the HTTP cache (temporarily saved web pages).
 **/
void
blx_embed_single_clear_cache (BlxEmbedSingle *single)
{
  soup_cache_clear (single->priv->cache);
}

/**
 * blx_embed_single_clear_auth_cache:
 * @single: the #BlxEmbedSingle
 * 
 * Clears the HTTP authentication cache.
 *
 * This does not clear regular website passwords; it only clears the HTTP
 * authentication cache. Websites which use HTTP authentication require the
 * browser to send a password along with every HTTP request; the browser will
 * ask the user for the password once and then cache the password for subsequent
 * HTTP requests. This function will clear the HTTP authentication cache,
 * meaning the user will have to re-enter a username and password the next time
 * Blxbrowser requests a web page secured with HTTP authentication.
 **/
void
blx_embed_single_clear_auth_cache (BlxEmbedSingle *single)
{
}

/**
 * blx_embed_single_get_nework_status:
 * @single: the #BlxEmbedSingle
 * @offline: %TRUE if the network is on-line
 * 
 * Sets the state of the network connection.
 **/
void
blx_embed_single_set_network_status (BlxEmbedSingle *single,
                                      gboolean status)
{
  if (status != single->priv->online)
    single->priv->online = status;

  g_object_notify (G_OBJECT (single), "network-status");
}

/**
 * blx_embed_single_get_network_status:
 * @single: the #BlxEmbedSingle
 * 
 * Gets the state of the network connection.
 * 
 * Returns: %TRUE iff the network is on-line.
 **/
gboolean
blx_embed_single_get_network_status (BlxEmbedSingle *single)
{
  return single->priv->online;
}

/**
 * blx_embed_single_open_window:
 * @single: the #BlxEmbedSingle
 * @parent: the requested window's parent #BlxEmbed
 * @address: the URL to load
 * @name: a name for the window
 * @features: a Javascript features string
 *
 * Opens a new window, as if it were opened in @parent using the Javascript
 * method and arguments: <code>window.open(&quot;@address&quot;,
 * &quot;_blank&quot;, &quot;@features&quot;);</code>.
 * 
 * Returns: (transfer none): the new embed. This is either a #BlxEmbed, or,
 * when @features specified "chrome", a #GtkMozEmbed.
 *
 * NOTE: Use blx_shell_new_tab() unless this handling of the @features string
 * is required.
 */
GtkWidget *
blx_embed_single_open_window (BlxEmbedSingle *single,
                               BlxEmbed *parent,
                               const char *address,
                               const char *name,
                               const char *features)
{
  return NULL;
}

/**
 * blx_embed_single_get_form_auth:
 * @single: an #BlxEmbedSingle
 * @uri: the URI of a web page
 * 
 * Gets a #GSList of all stored login/passwords, in
 * #BlxEmbedSingleFormAuthData format, for any form in @uri, or %NULL
 * if we have none.
 * 
 * The #BlxEmbedSingleFormAuthData structs and the #GSList are owned
 * by @single and should not be freed by the user.
 * 
 * Returns: (transfer none): #GSList with the possible auto-fills for the forms
 * in @uri, or %NULL
 **/
GSList *
blx_embed_single_get_form_auth (BlxEmbedSingle *single,
                                 const char *uri)
{
  BlxEmbedSinglePrivate *priv;

  g_return_val_if_fail (BLX_IS_EMBED_SINGLE (single), NULL);
  g_return_val_if_fail (uri, NULL);

  priv = single->priv;

  return g_hash_table_lookup (priv->form_auth_data, uri);
}

/**
 * blx_embed_single_add_form_auth:
 * @single: an #BlxEmbedSingle
 * @uri: URI of the page
 * @form_username: name of the username input field
 * @form_password: name of the password input field
 * @username: username
 * 
 * Adds a new entry to the local cache of form auth data stored in
 * @single.
 * 
 **/
void
blx_embed_single_add_form_auth (BlxEmbedSingle *single,
                                 const char *uri,
                                 const char *form_username,
                                 const char *form_password,
                                 const char *username)
{
  BlxEmbedSingleFormAuthData *form_data;
  BlxEmbedSinglePrivate *priv;
  GSList *l;

  g_return_if_fail (BLX_IS_EMBED_SINGLE (single));
  g_return_if_fail (uri);
  g_return_if_fail (form_username);
  g_return_if_fail (form_password);
  g_return_if_fail (username);

  priv = single->priv;

  LOG ("Appending: name field: %s / pass field: %s / username: %s / uri: %s", form_username, form_password, username, uri);

  form_data = form_auth_data_new (form_username, form_password, username);
  l = g_hash_table_lookup (priv->form_auth_data,
                           uri);
  l = g_slist_append (l, form_data);
  g_hash_table_replace (priv->form_auth_data,
                        g_strdup (uri),
                        l);
}
