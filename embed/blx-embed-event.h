/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 *  Copyright © 2000-2003 Marco Pesenti Gritti
 *  Copyright © 2004 Christian Persch
 *  Copyright © 2009 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_EMBED_EVENT_H
#define BLX_EMBED_EVENT_H

#include <glib-object.h>
#include <glib.h>
#include <webkit/webkit.h>

G_BEGIN_DECLS

#define BLX_TYPE_EMBED_EVENT               (blx_embed_event_get_type ())
#define BLX_EMBED_EVENT(o)                 (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_EMBED_EVENT, BlxEmbedEvent))
#define BLX_EMBED_EVENT_CLASS(k)           (G_TYPE_CHECK_CLASS_CAST ((k), BLX_TYPE_EMBED_EVENT, BlxEmbedEventClass))
#define BLX_IS_EMBED_EVENT(o)              (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_EMBED_EVENT))
#define BLX_IS_EMBED_EVENT_CLASS(k)        (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_EMBED_EVENT))
#define BLX_EMBED_EVENT_GET_CLASS(o)       (G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_EMBED_EVENT, BlxEmbedEventClass))

typedef struct BlxEmbedEventClass BlxEmbedEventClass;
typedef struct BlxEmbedEvent BlxEmbedEvent;
typedef struct BlxEmbedEventPrivate BlxEmbedEventPrivate;

struct BlxEmbedEvent {
  GObject parent_instance;

  /*< private >*/
  BlxEmbedEventPrivate *priv;
};

struct BlxEmbedEventClass {
  GObjectClass parent_class;
};


GType           blx_embed_event_get_type     (void);
BlxEmbedEvent *blx_embed_event_new          (GdkEventButton      *event,
                                               WebKitHitTestResult *hit_test_result);
guint           blx_embed_event_get_context  (BlxEmbedEvent      *event);
guint           blx_embed_event_get_button   (BlxEmbedEvent      *event);
guint           blx_embed_event_get_modifier (BlxEmbedEvent      *event);
void            blx_embed_event_get_coords   (BlxEmbedEvent      *event,
                                               guint               *x,
                                               guint               *y);
void            blx_embed_event_get_property (BlxEmbedEvent      *event,
                                               const char          *name,
                                               GValue              *value);
gboolean        blx_embed_event_has_property (BlxEmbedEvent      *event,
                                               const char          *name);

G_END_DECLS

#endif
