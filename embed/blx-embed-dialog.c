/*
 *  Copyright © 2000, 2001, 2002 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include "blx-embed-dialog.h"

static void
blx_embed_dialog_class_init (BlxEmbedDialogClass *klass);
static void
blx_embed_dialog_init (BlxEmbedDialog *window);
static void
blx_embed_dialog_finalize (GObject *object);
static void
blx_embed_dialog_get_property (GObject *object,
				guint prop_id,
				GValue *value,
				GParamSpec *pspec);
static void
blx_embed_dialog_set_property (GObject *object,
				guint prop_id,
				const GValue *value,
				GParamSpec *pspec);

enum
{
	PROP_0,
	PROP_BLX_EMBED
};

#define BLX_EMBED_DIALOG_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), BLX_TYPE_EMBED_DIALOG, BlxEmbedDialogPrivate))

struct _BlxEmbedDialogPrivate
{
	BlxEmbed *embed;
};

G_DEFINE_TYPE (BlxEmbedDialog, blx_embed_dialog, BLX_TYPE_DIALOG)

static void
blx_embed_dialog_class_init (BlxEmbedDialogClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        object_class->finalize = blx_embed_dialog_finalize;
	object_class->set_property = blx_embed_dialog_set_property;
	object_class->get_property = blx_embed_dialog_get_property;

	g_object_class_install_property (object_class,
					 PROP_BLX_EMBED,
                                         g_param_spec_object ("embed",
                                                              "Embed",
                                                              "The dialog's embed",
                                                              G_TYPE_OBJECT,
                                                              G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

	g_type_class_add_private (object_class, sizeof(BlxEmbedDialogPrivate));
}

static void
blx_embed_dialog_init (BlxEmbedDialog *dialog)
{
        dialog->priv = BLX_EMBED_DIALOG_GET_PRIVATE (dialog);
}

static void
unset_embed (BlxEmbedDialog *dialog)
{
	if (dialog->priv->embed != NULL)
	{
		BlxEmbed **embedptr;
		embedptr = &dialog->priv->embed;
		g_object_remove_weak_pointer (G_OBJECT (dialog->priv->embed),
					      (gpointer *) embedptr);
	}
}

static void
blx_embed_dialog_finalize (GObject *object)
{
        BlxEmbedDialog *dialog = BLX_EMBED_DIALOG (object);

	unset_embed (dialog);

        G_OBJECT_CLASS (blx_embed_dialog_parent_class)->finalize (object);
}

static void
blx_embed_dialog_set_property (GObject *object,
				guint prop_id,
				const GValue *value,
				GParamSpec *pspec)
{
        BlxEmbedDialog *d = BLX_EMBED_DIALOG (object);

        switch (prop_id)
        {
                case PROP_BLX_EMBED:
                        blx_embed_dialog_set_embed (d, g_value_get_object (value));
                        break;
        }
}

static void
blx_embed_dialog_get_property (GObject *object,
				guint prop_id,
				GValue *value,
				GParamSpec *pspec)
{
        BlxEmbedDialog *d = BLX_EMBED_DIALOG (object);

        switch (prop_id)
        {
                case PROP_BLX_EMBED:
                        g_value_set_object (value, d->priv->embed);
                        break;
        }
}

BlxEmbedDialog *
blx_embed_dialog_new (BlxEmbed *embed)
{
	return BLX_EMBED_DIALOG (g_object_new (BLX_TYPE_EMBED_DIALOG,
						"embed", embed,
						NULL));
}

BlxEmbedDialog *
blx_embed_dialog_new_with_parent (GtkWidget *parent_window,
				     BlxEmbed *embed)
{
	return BLX_EMBED_DIALOG (g_object_new
				    (BLX_TYPE_EMBED_DIALOG,
				     "parent-window", parent_window,
				     "embed", embed,
				     NULL));
}

void
blx_embed_dialog_set_embed (BlxEmbedDialog *dialog,
			     BlxEmbed *embed)
{
	BlxEmbed **embedptr;

	unset_embed (dialog);
	dialog->priv->embed = embed;

	embedptr = &dialog->priv->embed;
	g_object_add_weak_pointer (G_OBJECT (dialog->priv->embed),
				   (gpointer *) embedptr);
	g_object_notify (G_OBJECT (dialog), "embed");
}

BlxEmbed *
blx_embed_dialog_get_embed (BlxEmbedDialog *dialog)
{
	return dialog->priv->embed;
}
