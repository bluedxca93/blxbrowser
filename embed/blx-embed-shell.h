/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Copyright © 2000-2003 Marco Pesenti Gritti
 *  Copyright © 2011 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_EMBED_SHELL_H
#define BLX_EMBED_SHELL_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#include "blx-download.h"

G_BEGIN_DECLS

#define BLX_TYPE_EMBED_SHELL		(blx_embed_shell_get_type ())
#define BLX_EMBED_SHELL(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_EMBED_SHELL, BlxEmbedShell))
#define BLX_EMBED_SHELL_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_EMBED_SHELL, BlxEmbedShellClass))
#define BLX_IS_EMBED_SHELL(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_EMBED_SHELL))
#define BLX_IS_EMBED_SHELL_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_EMBED_SHELL))
#define BLX_EMBED_SHELL_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_EMBED_SHELL, BlxEmbedShellClass))

typedef struct _BlxEmbedShellClass	BlxEmbedShellClass;
typedef struct _BlxEmbedShell		BlxEmbedShell;
typedef struct _BlxEmbedShellPrivate	BlxEmbedShellPrivate;

extern BlxEmbedShell *embed_shell;

typedef enum
{
	BLX_EMBED_SHELL_MODE_BROWSER,
	BLX_EMBED_SHELL_MODE_PRIVATE,
	BLX_EMBED_SHELL_MODE_APPLICATION
} BlxEmbedShellMode;

struct _BlxEmbedShell
{
	GtkApplication parent;

	/*< private >*/
	BlxEmbedShellPrivate *priv;
};

struct _BlxEmbedShellClass
{
	GtkApplicationClass parent_class;

	void	  (* download_added)	(BlxEmbedShell *shell, BlxDownload *download);
	void	  (* download_removed)	(BlxEmbedShell *shell, BlxDownload *download);

	void	  (* prepare_close)	(BlxEmbedShell *shell);

	/*< private >*/
	GObject * (* get_embed_single)  (BlxEmbedShell *shell);
};

GType		   blx_embed_shell_get_type		(void);

BlxEmbedShell	  *blx_embed_shell_get_default		(void);

GObject		  *blx_embed_shell_get_favicon_cache	(BlxEmbedShell *shell);

GObject		  *blx_embed_shell_get_global_history	(BlxEmbedShell *shell);

GObject		  *blx_embed_shell_get_encodings	(BlxEmbedShell *shell);

GObject		  *blx_embed_shell_get_embed_single	(BlxEmbedShell *shell);

GObject        	  *blx_embed_shell_get_adblock_manager	(BlxEmbedShell *shell);

void		   blx_embed_shell_prepare_close	(BlxEmbedShell *shell);

void		   blx_embed_shell_set_page_setup	(BlxEmbedShell *shell,
							 GtkPageSetup *page_setup);
		
GtkPageSetup	  *blx_embed_shell_get_page_setup	(BlxEmbedShell *shell);

void		   blx_embed_shell_set_print_settings	(BlxEmbedShell *shell,
							 GtkPrintSettings *settings);
		
GtkPrintSettings  *blx_embed_shell_get_print_settings	(BlxEmbedShell *shell);

GList		  *blx_embed_shell_get_downloads	(BlxEmbedShell *shell);

void		   blx_embed_shell_add_download	(BlxEmbedShell *shell,
							 BlxDownload *download);
void		   blx_embed_shell_remove_download	(BlxEmbedShell *shell,
							 BlxDownload *download);

BlxEmbedShellMode blx_embed_shell_get_mode            (BlxEmbedShell *shell);

G_END_DECLS

#endif /* !BLX_EMBED_SHELL_H */
