/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2; -*- */
/*
 *  Copyright © 2007 Xan Lopez
 *  Copyright © 2009 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#ifndef BLX_EMBED_H
#define BLX_EMBED_H

#include <glib-object.h>
#include <gtk/gtk.h>

#include "blx-web-view.h"

G_BEGIN_DECLS

#define BLX_TYPE_EMBED               (blx_embed_get_type ())
#define BLX_EMBED(o)                 (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_EMBED, BlxEmbed))
#define BLX_EMBED_CLASS(k)           (G_TYPE_CHECK_CLASS_CAST ((k), BLX_TYPE_EMBED, BlxEmbedClass))
#define BLX_IS_EMBED(o)              (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_EMBED))
#define BLX_IS_EMBED_CLASS(k)        (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_EMBED))
#define BLX_EMBED_GET_CLASS(o)       (G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_EMBED, BlxEmbedClass))

typedef struct _BlxEmbedClass BlxEmbedClass;
typedef struct _BlxEmbed BlxEmbed;
typedef struct _BlxEmbedPrivate BlxEmbedPrivate;

#define BLX_EMBED_STATUSBAR_TAB_MESSAGE_CONTEXT_DESCRIPTION "tab_message"
#define BLX_EMBED_STATUSBAR_HELP_MESSAGE_CONTEXT_DESCRIPTION "help_message"

struct _BlxEmbed {
  GtkBox parent_instance;

  /*< private >*/
  BlxEmbedPrivate *priv;
};

struct _BlxEmbedClass {
  GtkBoxClass parent_class;
};

GType        blx_embed_get_type                 (void);
BlxWebView* blx_embed_get_web_view             (BlxEmbed  *embed);
void         blx_embed_add_top_widget           (BlxEmbed  *embed,
                                                  GtkWidget  *widget,
                                                  gboolean    destroy_on_transition);
void         blx_embed_remove_top_widget        (BlxEmbed  *embed,
                                                  GtkWidget  *widget);
void         blx_embed_auto_download_url        (BlxEmbed  *embed,
                                                  const char *url);
void         _blx_embed_set_statusbar_label     (BlxEmbed  *embed,
                                                  const char *label);
void         blx_embed_statusbar_pop            (BlxEmbed  *embed,
                                                  guint       context_id);
guint        blx_embed_statusbar_push           (BlxEmbed  *embed,
                                                  guint       context_id,
                                                  const char *text);
guint        blx_embed_statusbar_get_context_id (BlxEmbed  *embed,
                                                  const char *context_description);

G_END_DECLS

#endif
