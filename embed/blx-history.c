/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Copyright © 2002, 2003 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include "blx-history.h"
#include "blx-file-helpers.h"
#include "blx-debug.h"
#include "blx-node-db.h"
#include "blx-node-common.h"
#include "blx-prefs.h"
#include "blx-request-about.h"
#include "blx-settings.h"
#include "blx-string.h"

#include <time.h>
#include <string.h>
#include <glib/gi18n.h>

#define BLX_HISTORY_XML_ROOT	 (const xmlChar *)"blx_history"
#define BLX_HISTORY_XML_VERSION (const xmlChar *)"1.0"

/* how often to save the history, in seconds */
#define HISTORY_SAVE_INTERVAL (5 * 60)

/* if you change this remember to change also the user interface description */
#define HISTORY_PAGE_OBSOLETE_DAYS 10

/* the number of seconds in a day */
#define SECS_PER_DAY (60*60*24)

#define BLX_HISTORY_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), BLX_TYPE_HISTORY, BlxHistoryPrivate))

struct _BlxHistoryPrivate
{
	char *xml_file;
	BlxNodeDb *db;
	BlxNode *hosts;
	BlxNode *pages;
	BlxNode *last_page;
	GHashTable *hosts_hash;
	GHashTable *pages_hash;
	guint autosave_timeout;
	guint update_hosts_idle;
	gboolean dirty;
	gboolean enabled;
};

enum
{
	REDIRECT_FLAG	= 1 << 0,
	TOPLEVEL_FLAG	= 1 << 1
};

enum
{
	PROP_0,
	PROP_ENABLED
};

enum
{
	ADD_PAGE,
	VISITED,
	CLEARED,
	REDIRECT,
	ICON_UPDATED,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL];

static void blx_history_class_init	(BlxHistoryClass *klass);
static void blx_history_init		(BlxHistory *history);
static void blx_history_finalize	(GObject *object);
static gboolean impl_add_page           (BlxHistory *, const char *, gboolean, gboolean);

G_DEFINE_TYPE (BlxHistory, blx_history, G_TYPE_OBJECT)

static void
blx_history_set_property (GObject *object,
			   guint prop_id,
			   const GValue *value,
			   GParamSpec *pspec)
{
	BlxHistory *history = BLX_HISTORY (object);

	switch (prop_id)
	{
		case PROP_ENABLED:
			blx_history_set_enabled (history, g_value_get_boolean (value));
			break;
	}
}

static void
blx_history_get_property (GObject *object,
			   guint prop_id,
			   GValue *value,
			   GParamSpec *pspec)
{
	BlxHistory *history = BLX_HISTORY (object);

	switch (prop_id)
	{
		case PROP_ENABLED:
			g_value_set_boolean (value, history->priv->enabled);
			break;
	}
}

static void
blx_history_class_init (BlxHistoryClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        object_class->finalize = blx_history_finalize;
	object_class->get_property = blx_history_get_property;
	object_class->set_property = blx_history_set_property;

	klass->add_page = impl_add_page;

	g_object_class_install_property (object_class,
					 PROP_ENABLED,
					 g_param_spec_boolean ("enabled",
							       "Enabled",
							       "Enabled",
							       TRUE,
							       G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

	signals[ADD_PAGE] =
		g_signal_new ("add_page",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (BlxHistoryClass, add_page),
			      g_signal_accumulator_true_handled, NULL,
			      g_cclosure_marshal_generic,
			      G_TYPE_BOOLEAN,
			      3,
			      G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE,
			      G_TYPE_BOOLEAN,
			      G_TYPE_BOOLEAN);

	signals[VISITED] =
                g_signal_new ("visited",
                              G_OBJECT_CLASS_TYPE (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (BlxHistoryClass, visited),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__STRING,
                              G_TYPE_NONE,
                              1,
			      G_TYPE_STRING);

	signals[CLEARED] =
                g_signal_new ("cleared",
                              G_OBJECT_CLASS_TYPE (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (BlxHistoryClass, cleared),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__VOID,
                              G_TYPE_NONE,
                              0);

	signals[REDIRECT] =
                g_signal_new ("redirect",
                              G_OBJECT_CLASS_TYPE (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (BlxHistoryClass, redirect),
                              NULL, NULL,
                              g_cclosure_marshal_generic,
                              G_TYPE_NONE,
                              2,
			      G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE,
			      G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE);

	signals[ICON_UPDATED] =
		g_signal_new ("icon_updated",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (BlxHistoryClass, icon_updated),
			      NULL, NULL,
			      g_cclosure_marshal_generic,
			      G_TYPE_NONE,
			      2,
			      G_TYPE_STRING, G_TYPE_STRING);

	g_type_class_add_private (object_class, sizeof (BlxHistoryPrivate));
}

static gboolean
page_is_obsolete (BlxNode *node, time_t now)
{
	int last_visit;

	last_visit = blx_node_get_property_int
		(node, BLX_NODE_PAGE_PROP_LAST_VISIT);
	return now - last_visit >= HISTORY_PAGE_OBSOLETE_DAYS*SECS_PER_DAY;
}

static void
remove_obsolete_pages (BlxHistory *eb)
{
	GPtrArray *children;
	int i;
	time_t now;

	now = time (NULL);

	children = blx_node_get_children (eb->priv->pages);
	for (i = (int) children->len - 1; i >= 0; i--)
	{
		BlxNode *kid;

		kid = g_ptr_array_index (children, i);

		if (page_is_obsolete (kid, now))
		{
			blx_node_unref (kid);
		}
	}
}

static gboolean
save_filter (BlxNode *node,
	     BlxNode *page_node)
{
	return node != page_node;
}

static void
blx_history_save (BlxHistory *eb)
{
	int ret;

	/* only save if there are changes */
	if (eb->priv->dirty == FALSE && eb->priv->enabled)
	{
		return;
	}

	LOG ("Saving history db");

	ret = blx_node_db_write_to_xml_safe
		(eb->priv->db, (const xmlChar *)eb->priv->xml_file,
		 BLX_HISTORY_XML_ROOT,
		 BLX_HISTORY_XML_VERSION,
		 NULL, /* comment */
		 eb->priv->hosts,
		 (BlxNodeFilterFunc) save_filter, eb->priv->pages,
		 eb->priv->pages, NULL, NULL,
		 NULL);

	if (ret >=0)
	{
		/* save was successful */
		eb->priv->dirty = FALSE;
	}
}

static void
hosts_added_cb (BlxNode *node,
	        BlxNode *child,
	        BlxHistory *eb)
{
	eb->priv->dirty = TRUE;

	g_hash_table_insert (eb->priv->hosts_hash,
			     (char *) blx_node_get_property_string (child, BLX_NODE_PAGE_PROP_LOCATION),
			     child);
}

static void
hosts_removed_cb (BlxNode *node,
		  BlxNode *child,
		  guint old_index,
		  BlxHistory *eb)
{
	eb->priv->dirty = TRUE;

	g_hash_table_remove (eb->priv->hosts_hash,
			     blx_node_get_property_string (child, BLX_NODE_PAGE_PROP_LOCATION));
}

static void
hosts_changed_cb (BlxNode *node,
		  BlxNode *child,
		  guint property_id,
		  BlxHistory *eb)
{
	eb->priv->dirty = TRUE;
}

static void
pages_added_cb (BlxNode *node,
	        BlxNode *child,
	        BlxHistory *eb)
{
	eb->priv->dirty = TRUE;

	g_hash_table_insert (eb->priv->pages_hash,
			     (char *) blx_node_get_property_string (child, BLX_NODE_PAGE_PROP_LOCATION),
			     child);
}

static void
pages_removed_cb (BlxNode *node,
		  BlxNode *child,
		  guint old_index,
		  BlxHistory *eb)
{
	eb->priv->dirty = TRUE;

	g_hash_table_remove (eb->priv->pages_hash,
			     blx_node_get_property_string (child, BLX_NODE_PAGE_PROP_LOCATION));
}

static void
pages_changed_cb (BlxNode *node,
		  BlxNode *child,
		  guint property_id,
		  BlxHistory *eb)
{
	eb->priv->dirty = TRUE;
}

static gboolean
periodic_save_cb (BlxHistory *eh)
{
	remove_obsolete_pages (eh);
	blx_history_save (eh);

	return TRUE;
}

static void
update_host_on_child_remove (BlxNode *node)
{
	GPtrArray *children;
	int i, host_last_visit, new_host_last_visit = 0;

	host_last_visit = blx_node_get_property_int
			(node, BLX_NODE_PAGE_PROP_LAST_VISIT);

	children = blx_node_get_children (node);
	for (i = 0; i < children->len; i++)
	{
		BlxNode *kid;
		int last_visit;

		kid = g_ptr_array_index (children, i);

		last_visit = blx_node_get_property_int
                        (kid, BLX_NODE_PAGE_PROP_LAST_VISIT);

		if (last_visit > new_host_last_visit)
		{
			new_host_last_visit = last_visit;
		}
	}

	if (host_last_visit != new_host_last_visit)
	{
		blx_node_set_property_int (node,
					    BLX_NODE_PAGE_PROP_LAST_VISIT,
					    new_host_last_visit);
	}
}

static gboolean
update_hosts (BlxHistory *eh)
{
	GPtrArray *children;
	int i;
	GList *empty = NULL;

	children = blx_node_get_children (eh->priv->hosts);
	for (i = 0; i < children->len; i++)
	{
		BlxNode *kid;

		kid = g_ptr_array_index (children, i);

		if (kid != eh->priv->pages)
		{
			if (blx_node_get_n_children (kid) > 0)
			{
				update_host_on_child_remove (kid);
			}
			else
			{
				empty = g_list_prepend (empty, kid);
			}
		}
	}

	g_list_foreach (empty, (GFunc)blx_node_unref, NULL);
	g_list_free (empty);

	eh->priv->update_hosts_idle = 0;

	return FALSE;
}

static void
page_removed_from_host_cb (BlxNode *node,
		           BlxNode *child,
		           guint old_index,
		           BlxHistory *eb)
{
	if (eb->priv->update_hosts_idle == 0)
	{
		eb->priv->update_hosts_idle = g_idle_add
			((GSourceFunc)update_hosts, eb);
	}
}

static void
remove_pages_from_host_cb (BlxNode *host,
			   BlxHistory *eh)
{
	GPtrArray *children;
	BlxNode *site;
	int i;

	children = blx_node_get_children (host);

	for (i = (int) children->len - 1; i >= 0; i--)
	{
		site = g_ptr_array_index (children, i);

		blx_node_unref (site);
	}
}

static void
connect_page_removed_from_host (char *url,
                                BlxNode *node,
                                BlxHistory *eb)
{
	if (node == eb->priv->pages) return;

	blx_node_signal_connect_object (node,
					 BLX_NODE_CHILD_REMOVED,
				         (BlxNodeCallback) page_removed_from_host_cb,
					 G_OBJECT (eb));
	blx_node_signal_connect_object (node,
					 BLX_NODE_DESTROY,
					 (BlxNodeCallback) remove_pages_from_host_cb,
					 G_OBJECT (eb));
}

static void
blx_history_init (BlxHistory *eb)
{
	BlxNodeDb *db;
	const char *all = _("All");

        eb->priv = BLX_HISTORY_GET_PRIVATE (eb);
	eb->priv->update_hosts_idle = 0;
	eb->priv->enabled = TRUE;

	db = blx_node_db_new (BLX_NODE_DB_HISTORY);
	eb->priv->db = db;

	eb->priv->xml_file = g_build_filename (blx_dot_dir (),
					       "blx-history.xml",
					       NULL);

	eb->priv->pages_hash = g_hash_table_new (g_str_hash,
			                          g_str_equal);
	eb->priv->hosts_hash = g_hash_table_new (g_str_hash,
			                         g_str_equal);

	/* Pages */
	eb->priv->pages = blx_node_new_with_id (db, PAGES_NODE_ID);

	blx_node_set_property_string (eb->priv->pages,
				       BLX_NODE_PAGE_PROP_LOCATION,
				       all);
	blx_node_set_property_string (eb->priv->pages,
				       BLX_NODE_PAGE_PROP_TITLE,
				       all);

	blx_node_set_property_int (eb->priv->pages,
				    BLX_NODE_PAGE_PROP_PRIORITY,
				    BLX_NODE_ALL_PRIORITY);
	
	blx_node_signal_connect_object (eb->priv->pages,
					 BLX_NODE_CHILD_ADDED,
				         (BlxNodeCallback) pages_added_cb,
					 G_OBJECT (eb));
	blx_node_signal_connect_object (eb->priv->pages,
					 BLX_NODE_CHILD_REMOVED,
				         (BlxNodeCallback) pages_removed_cb,
					 G_OBJECT (eb));
	blx_node_signal_connect_object (eb->priv->pages,
					 BLX_NODE_CHILD_CHANGED,
				         (BlxNodeCallback) pages_changed_cb,
					 G_OBJECT (eb));

	/* Hosts */
	eb->priv->hosts = blx_node_new_with_id (db, HOSTS_NODE_ID);
	blx_node_signal_connect_object (eb->priv->hosts,
					 BLX_NODE_CHILD_ADDED,
				         (BlxNodeCallback) hosts_added_cb,
					 G_OBJECT (eb));
	blx_node_signal_connect_object (eb->priv->hosts,
					 BLX_NODE_CHILD_REMOVED,
				         (BlxNodeCallback) hosts_removed_cb,
					 G_OBJECT (eb));
	blx_node_signal_connect_object (eb->priv->hosts,
					 BLX_NODE_CHILD_CHANGED,
				         (BlxNodeCallback) hosts_changed_cb,
					 G_OBJECT (eb));

	blx_node_add_child (eb->priv->hosts, eb->priv->pages);

	blx_node_db_load_from_file (eb->priv->db, eb->priv->xml_file,
				     BLX_HISTORY_XML_ROOT,
				     BLX_HISTORY_XML_VERSION);

	g_hash_table_foreach (eb->priv->hosts_hash,
			      (GHFunc) connect_page_removed_from_host,
			      eb);

	/* mark as clean */
	eb->priv->dirty = FALSE;

	/* setup the periodic history saving callback */
	eb->priv->autosave_timeout =
		g_timeout_add_seconds (HISTORY_SAVE_INTERVAL,
		       (GSourceFunc)periodic_save_cb,
		       eb);

	g_settings_bind (BLX_SETTINGS_LOCKDOWN,
			 BLX_PREFS_LOCKDOWN_HISTORY,
			 eb, "enabled",
			 G_SETTINGS_BIND_INVERT_BOOLEAN | G_SETTINGS_BIND_GET);
}

static void
blx_history_finalize (GObject *object)
{
        BlxHistory *eb = BLX_HISTORY (object);

	if (eb->priv->update_hosts_idle)
	{
		g_source_remove (eb->priv->update_hosts_idle);
	}

	blx_history_save (eb);

	blx_node_unref (eb->priv->pages);
	blx_node_unref (eb->priv->hosts);

	g_object_unref (eb->priv->db);

	g_hash_table_destroy (eb->priv->pages_hash);
	g_hash_table_destroy (eb->priv->hosts_hash);

	g_source_remove (eb->priv->autosave_timeout);

	g_free (eb->priv->xml_file);

	LOG ("Global history finalized");

	G_OBJECT_CLASS (blx_history_parent_class)->finalize (object);
}

BlxHistory *
blx_history_new (void)
{
	return BLX_HISTORY (g_object_new (BLX_TYPE_HISTORY, NULL));
}

static void
blx_history_host_visited (BlxHistory *eh,
			   BlxNode *host,
			   GTime now)
{
	int visits;

	LOG ("Host visited");

	visits = blx_node_get_property_int
		(host, BLX_NODE_PAGE_PROP_VISITS);
	if (visits < 0) visits = 0;
	visits++;

	blx_node_set_property_int (host, BLX_NODE_PAGE_PROP_VISITS, visits);
	blx_node_set_property_int (host, BLX_NODE_PAGE_PROP_LAST_VISIT,
				    now);
}

static BlxNode *
internal_get_host (BlxHistory *eh, const char *url, gboolean create)
{
	BlxNode *host = NULL;
	char *host_name = NULL;
	GList *host_locations = NULL, *l;
	char *scheme = NULL;
	GTime now;

	g_return_val_if_fail (url != NULL, NULL);

	if (eh->priv->enabled == FALSE)
	{
		return NULL;
	}

	now = time (NULL);

	if (url)
	{
		scheme = g_uri_parse_scheme (url);
		host_name = blx_string_get_host_name (url);
	}

	/* Build an host name */
	if (scheme == NULL || host_name == NULL)
	{
		host_name = g_strdup (_("Others"));
		host_locations = g_list_append (host_locations,
						g_strdup ("about:blank"));
	}
	else if (strcmp (scheme, "file") == 0)
	{
		host_name = g_strdup (_("Local files"));
		host_locations = g_list_append (host_locations,
						g_strdup ("file:///"));
	}
	else
	{
		char *location;
		char *tmp;
		
		if (g_str_equal (scheme, "https"))
		{
			/* If scheme is https, we still fake http. */
			location = g_strconcat ("http://", host_name, "/", NULL);
			host_locations = g_list_append (host_locations, location);
		}

		/* We append the real address */
		location = g_strconcat (scheme,
					"://", host_name, "/", NULL);
		host_locations = g_list_append (host_locations, location);

		/* and also a fake www-modified address if it's http or https. */
		if (g_str_has_prefix (scheme, "http"))
		{
			if (g_str_has_prefix (host_name, "www."))
			{
				tmp = g_strdup (host_name + 4);
			}
			else
			{
				tmp = g_strconcat ("www.", host_name, NULL);
			}
			location = g_strconcat ("http://", tmp, "/", NULL);
			g_free (tmp);
			host_locations = g_list_append (host_locations, location);
		}
	}

	g_return_val_if_fail (host_locations != NULL, NULL);

	for (l = host_locations; l != NULL; l = l->next)
	{
		host = g_hash_table_lookup (eh->priv->hosts_hash,
					    (char *)l->data);
		if (host) break;
	}

	if (!host && create)
	{
		host = blx_node_new (eh->priv->db);
		blx_node_signal_connect_object (host,
						 BLX_NODE_CHILD_REMOVED,
					         (BlxNodeCallback) page_removed_from_host_cb,
						 G_OBJECT (eh));
		blx_node_signal_connect_object (host,
						 BLX_NODE_DESTROY,
						 (BlxNodeCallback) remove_pages_from_host_cb,
						 G_OBJECT (eh));
		blx_node_set_property_string (host,
					       BLX_NODE_PAGE_PROP_TITLE,
					       host_name);
		blx_node_set_property_string (host,
					       BLX_NODE_PAGE_PROP_LOCATION,
					       (char *)host_locations->data);
		blx_node_set_property_int (host,
					    BLX_NODE_PAGE_PROP_FIRST_VISIT,
					    now);
		blx_node_add_child (eh->priv->hosts, host);
	}

	if (host)
	{
		blx_history_host_visited (eh, host, now);
	}

	g_free (scheme);
	g_free (host_name);

	g_list_foreach (host_locations, (GFunc)g_free, NULL);
	g_list_free (host_locations);

	return host;
}

/**
 * blx_history_get_host:
 *
 * Return value: (transfer none):
 **/
BlxNode *
blx_history_get_host (BlxHistory *eh, const char *url)
{
	return internal_get_host (eh, url, FALSE);
}

static BlxNode *
blx_history_add_host (BlxHistory *eh, BlxNode *page)
{
	const char *url;

	url = blx_node_get_property_string
		(page, BLX_NODE_PAGE_PROP_LOCATION);

	return internal_get_host (eh, url, TRUE);
}

static void
blx_history_visited (BlxHistory *eh, BlxNode *node)
{
	GTime now;
	int visits;
	const char *url;
	int host_id;

	now = time (NULL);

	g_assert (node != NULL);

	url = blx_node_get_property_string
		(node, BLX_NODE_PAGE_PROP_LOCATION);

	visits = blx_node_get_property_int
		(node, BLX_NODE_PAGE_PROP_VISITS);
	if (visits < 0) visits = 0;
	visits++;

	blx_node_set_property_int (node, BLX_NODE_PAGE_PROP_VISITS, visits);
	blx_node_set_property_int (node, BLX_NODE_PAGE_PROP_LAST_VISIT,
				    now);
	if (visits == 1)
	{
		blx_node_set_property_int
			(node, BLX_NODE_PAGE_PROP_FIRST_VISIT, now);
	}

	host_id = blx_node_get_property_int (node, BLX_NODE_PAGE_PROP_HOST_ID);
	if (host_id >= 0)
	{
		BlxNode *host;

		host = blx_node_db_get_node_from_id (eh->priv->db, host_id);
		blx_history_host_visited (eh, host, now);
	}

	eh->priv->last_page = node;

	g_signal_emit (G_OBJECT (eh), signals[VISITED], 0, url);
}

int
blx_history_get_page_visits (BlxHistory *gh,
			      const char *url)
{
	BlxNode *node;
	int visits = 0;

	node = blx_history_get_page (gh, url);
	if (node)
	{
		visits = blx_node_get_property_int
			(node, BLX_NODE_PAGE_PROP_VISITS);
		if (visits < 0) visits = 0;
	}

	return visits;
}

void
blx_history_add_page (BlxHistory *eh,
		       const char *url,
		       gboolean redirect,
		       gboolean toplevel)
{
	gboolean result = FALSE;

	g_signal_emit (eh, signals[ADD_PAGE], 0, url, redirect, toplevel, &result);
}

static gboolean
impl_add_page (BlxHistory *eb,
	       const char *orig_url,
	       gboolean redirect,
	       gboolean toplevel)
{
	BlxNode *bm, *node, *host;
	gulong flags = 0;
	char *url;

	if (eb->priv->enabled == FALSE)
	{
		return FALSE;
	}

	/* Do not show internal blx-about: protocol to users */
	if (g_str_has_prefix (orig_url, BLX_ABOUT_SCHEME))
		url = g_strdup_printf ("about:%s", orig_url + BLX_ABOUT_SCHEME_LEN + 1);
	else
		url = g_strdup (orig_url);

	node = blx_history_get_page (eb, url);
	if (node)
	{
		blx_history_visited (eb, node);
		g_free (url);
		return TRUE;
	}

	bm = blx_node_new (eb->priv->db);

	blx_node_set_property_string (bm, BLX_NODE_PAGE_PROP_LOCATION, url);
	blx_node_set_property_string (bm, BLX_NODE_PAGE_PROP_TITLE, url);
	g_free (url);

	if (redirect) flags |= REDIRECT_FLAG;
	if (toplevel) flags |= TOPLEVEL_FLAG;

	/* BlxNode SUCKS! */
	blx_node_set_property_long (bm, BLX_NODE_PAGE_PROP_EXTRA_FLAGS,
				     flags);

	host = blx_history_add_host (eb, bm);

	blx_node_set_property_int (bm, BLX_NODE_PAGE_PROP_HOST_ID,
				    blx_node_get_id (host));

	blx_history_visited (eb, bm);

	blx_node_add_child (host, bm);
	blx_node_add_child (eb->priv->pages, bm);

	return TRUE;
}

/**
 * blx_history_get_page:
 *
 * Return value: (transfer none):
 **/
BlxNode *
blx_history_get_page (BlxHistory *eb,
		       const char *url)
{
	BlxNode *node;

	node = g_hash_table_lookup (eb->priv->pages_hash, url);

	return node;
}

gboolean
blx_history_is_page_visited (BlxHistory *gh,
			      const char *url)
{
	return (blx_history_get_page (gh, url) != NULL);
}

void
blx_history_set_page_title (BlxHistory *gh,
			     const char *url,
			     const char *title)
{
	BlxNode *node;

	LOG ("Set page title");

	if (title == NULL || title[0] == '\0') return;
	if (url == NULL) return;

	node = blx_history_get_page (gh, url);
	if (node == NULL) return;

	blx_node_set_property_string (node, BLX_NODE_PAGE_PROP_TITLE,
				       title);
}

const char*
blx_history_get_icon (BlxHistory *gh,
		       const char *url)
{
	BlxNode *node, *host;
	int host_id;

	node = blx_history_get_page (gh, url);
	if (node == NULL) return NULL;
	
	host_id = blx_node_get_property_int (node, BLX_NODE_PAGE_PROP_HOST_ID);
	g_return_val_if_fail (host_id >= 0, NULL);

	host = blx_node_db_get_node_from_id (gh->priv->db, host_id);
	g_return_val_if_fail (host != NULL, NULL);

	return blx_node_get_property_string (host, BLX_NODE_PAGE_PROP_ICON);
}	
	
		       
void
blx_history_set_icon (BlxHistory *gh,
		       const char *url,
		       const char *icon)
{
	BlxNode *node, *host;
	int host_id;

	node = blx_history_get_page (gh, url);
	if (node == NULL) return;
	
	host_id = blx_node_get_property_int (node, BLX_NODE_PAGE_PROP_HOST_ID);
	g_return_if_fail (host_id >= 0);

	host = blx_node_db_get_node_from_id (gh->priv->db, host_id);
	if (host)
	{
		blx_node_set_property_string (host, BLX_NODE_PAGE_PROP_ICON,
					       icon);
	}

	g_signal_emit (gh, signals[ICON_UPDATED], 0, url, icon);
}

void
blx_history_clear (BlxHistory *gh)
{
	BlxNode *node;

	LOG ("clearing history");

	blx_node_db_set_immutable (gh->priv->db, FALSE);

	while ((node = blx_node_get_nth_child (gh->priv->pages, 0)) != NULL)
	{
		blx_node_unref (node);
	}
	blx_history_save (gh);

	blx_node_db_set_immutable (gh->priv->db, !gh->priv->enabled);

	g_signal_emit (gh, signals[CLEARED], 0);
}

/**
 * blx_history_get_hosts:
 *
 * Return value: (transfer none):
 **/
BlxNode *
blx_history_get_hosts (BlxHistory *eb)
{
	return eb->priv->hosts;
}

/**
 * blx_history_get_pages:
 *
 * Return value: (transfer none):
 **/
BlxNode *
blx_history_get_pages (BlxHistory *eb)
{
	return eb->priv->pages;
}

const char *
blx_history_get_last_page (BlxHistory *gh)
{
	if (gh->priv->last_page == NULL) return NULL;

	return blx_node_get_property_string
		(gh->priv->last_page, BLX_NODE_PAGE_PROP_LOCATION);
}

gboolean
blx_history_is_enabled (BlxHistory *history)
{
	g_return_val_if_fail (BLX_IS_HISTORY (history), FALSE);

	return history->priv->enabled;
}

void
blx_history_set_enabled (BlxHistory *history,
			  gboolean enabled)
{
	int ret;

	ret = 1;

	LOG ("blx_history_set_enabled %d", enabled);

	/* Write history only when disabling it, not when reenabling it */
	if (!enabled && history->priv->dirty)
	{
		ret = blx_node_db_write_to_xml_safe
			(history->priv->db, (const xmlChar *)history->priv->xml_file,
			 BLX_HISTORY_XML_ROOT,
			 BLX_HISTORY_XML_VERSION,
			 NULL, /* comment */
			 history->priv->hosts,
			 (BlxNodeFilterFunc) save_filter, history->priv->pages,
			 history->priv->pages, NULL, NULL,
			 NULL);
	}

	if (ret >=0)
	{
		/* save was successful */
		history->priv->dirty = FALSE;
	}

	history->priv->enabled = enabled;

	blx_node_db_set_immutable (history->priv->db, !enabled);
}
