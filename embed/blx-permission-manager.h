/*
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_PERMISSION_MANAGER_H
#define BLX_PERMISSION_MANAGER_H

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define BLX_TYPE_PERMISSION_MANAGER		(blx_permission_manager_get_type ())
#define BLX_PERMISSION_MANAGER(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_PERMISSION_MANAGER, BlxPermissionManager))
#define BLX_PERMISSION_MANAGER_IFACE(k)	(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_PERMISSION_MANAGER, BlxPermissionManagerIface))
#define BLX_IS_PERMISSION_MANAGER(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_PERMISSION_MANAGER))
#define BLX_IS_PERMISSION_MANAGER_IFACE(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_PERMISSION_MANAGER))
#define BLX_PERMISSION_MANAGER_GET_IFACE(inst)	(G_TYPE_INSTANCE_GET_INTERFACE ((inst), BLX_TYPE_PERMISSION_MANAGER, BlxPermissionManagerIface))

#define BLX_TYPE_PERMISSION_INFO		(blx_permission_info_get_type ())

#define BLX_PERMISSION_TYPE_COOKIE	"cookie"
#define BLX_PERMISSION_TYPE_IMAGE	"image"
#define BLX_PERMISSION_TYPE_POPUP	"popup"

typedef enum
{
	BLX_PERMISSION_ALLOWED,
	BLX_PERMISSION_DENIED,
	BLX_PERMISSION_DEFAULT
} BlxPermission;

typedef struct _BlxPermissionInfo		BlxPermissionInfo;

typedef struct _BlxPermissionManager		BlxPermissionManager;
typedef struct _BlxPermissionManagerIface	BlxPermissionManagerIface;

struct _BlxPermissionInfo
{
	char *host;
	GQuark qtype;
	BlxPermission permission;
};

struct _BlxPermissionManagerIface
{
	GTypeInterface base_iface;

	/* Signals */
	void	(* added)	(BlxPermissionManager *manager,
				 BlxPermissionInfo *info);
	void	(* changed)	(BlxPermissionManager *manager,
				 BlxPermissionInfo *info);
	void	(* deleted)	(BlxPermissionManager *manager,
				 BlxPermissionInfo *info);
	void	(* cleared)	(BlxPermissionManager *manager);

	/* Methods */
	void		(* add)		(BlxPermissionManager *manager,
					 const char *host,
					 const char *type,
					 BlxPermission permission);
	void		(* remove)	(BlxPermissionManager *manager,
					 const char *host,
					 const char *type);
	void		(* clear)	(BlxPermissionManager *manager);
	BlxPermission	(* test)	(BlxPermissionManager *manager,
					 const char *host,
					 const char *type);
	GList *		(* list)	(BlxPermissionManager *manager,
					 const char *type);
};

/* BlxPermissionInfo */

GType			blx_permission_get_type	(void);

GType			blx_permission_info_get_type	(void);

BlxPermissionInfo     *blx_permission_info_new	(const char *host,
							 const char *type,
							 BlxPermission permission);

BlxPermissionInfo     *blx_permission_info_copy	(const BlxPermissionInfo *info);

void			blx_permission_info_free	(BlxPermissionInfo *info);

/* BlxPermissionManager */

GType 		blx_permission_manager_get_type	(void);

void		blx_permission_manager_add_permission		(BlxPermissionManager *manager,
								 const char *host,
								 const char *type,
								 BlxPermission permission);

void		blx_permission_manager_remove_permission	(BlxPermissionManager *manager,
								 const char *host,
								 const char *type);

void		blx_permission_manager_clear_permissions	(BlxPermissionManager *manager);

BlxPermission	blx_permission_manager_test_permission		(BlxPermissionManager *manager,
								 const char *host,
								 const char *type);

GList *		blx_permission_manager_list_permissions	(BlxPermissionManager *manager,
								 const char *type);

G_END_DECLS

#endif
