/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Copyright © 2000-2003 Marco Pesenti Gritti
 *  Copyright © 2011 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include <config.h>

#include <glib.h>

#include <glib/gi18n.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>

#include "blx-adblock-manager.h"
#include "blx-debug.h"
#include "blx-download.h"
#include "blx-embed-shell.h"
#include "blx-embed-single.h"
#include "blx-embed-type-builtins.h"
#include "blx-encodings.h"
#include "blx-favicon-cache.h"
#include "blx-file-helpers.h"
#include "blx-history.h"

#include "blx-print-utils.h"

#define PAGE_SETUP_FILENAME	"page-setup-gtk.ini"
#define PRINT_SETTINGS_FILENAME	"print-settings.ini"

#define LEGACY_PAGE_SETUP_FILENAME	"page-setup.ini"

#define BLX_EMBED_SHELL_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), BLX_TYPE_EMBED_SHELL, BlxEmbedShellPrivate))

#define ENABLE_MIGRATION

struct _BlxEmbedShellPrivate
{
	BlxHistory *global_history;
	GList *downloads;
	BlxFaviconCache *favicon_cache;
	BlxEmbedSingle *embed_single;
	BlxEncodings *encodings;
	BlxAdBlockManager *adblock_manager;
	GtkPageSetup *page_setup;
	GtkPrintSettings *print_settings;
	BlxEmbedShellMode mode;
	guint single_initialised : 1;
};

enum
{
	DOWNLOAD_ADDED,
	DOWNLOAD_REMOVED,
	PREPARE_CLOSE,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL];

enum
{
	PROP_0,
	PROP_MODE,
	N_PROPERTIES
};

static GParamSpec *object_properties[N_PROPERTIES] = { NULL, };

static void blx_embed_shell_class_init	(BlxEmbedShellClass *klass);
static void blx_embed_shell_init	(BlxEmbedShell *shell);

BlxEmbedShell *embed_shell = NULL;

G_DEFINE_TYPE (BlxEmbedShell, blx_embed_shell, GTK_TYPE_APPLICATION)

static void
blx_embed_shell_dispose (GObject *object)
{
	BlxEmbedShell *shell = BLX_EMBED_SHELL (object);
	BlxEmbedShellPrivate *priv = shell->priv;

	if (priv->favicon_cache != NULL)
	{
		LOG ("Unref favicon cache");
		g_object_unref (priv->favicon_cache);
		priv->favicon_cache = NULL;
	}

	if (priv->encodings != NULL)
	{
		LOG ("Unref encodings");
		g_object_unref (priv->encodings);
		priv->encodings = NULL;
	}

	if (priv->page_setup != NULL)
	{
		g_object_unref (priv->page_setup);
		priv->page_setup = NULL;
	}

	if (priv->print_settings != NULL)
	{
		g_object_unref (priv->print_settings);
		priv->print_settings = NULL;
	}

	G_OBJECT_CLASS (blx_embed_shell_parent_class)->dispose (object);
}

static void
blx_embed_shell_finalize (GObject *object)
{
	BlxEmbedShell *shell = BLX_EMBED_SHELL (object);

	if (shell->priv->downloads != NULL)
	{
		LOG ("Destroying downloads list");
		g_list_foreach (shell->priv->downloads, (GFunc) g_object_unref, NULL);
		g_list_free (shell->priv->downloads);
		shell->priv->downloads = NULL;
	}

	if (shell->priv->global_history)
	{
		LOG ("Unref history");
		g_object_unref (shell->priv->global_history);
	}

	if (shell->priv->embed_single)
	{
		LOG ("Unref embed single");
		g_object_unref (G_OBJECT (shell->priv->embed_single));
	}

	if (shell->priv->adblock_manager != NULL)
	{
		LOG ("Unref adblock manager");
		g_object_unref (shell->priv->adblock_manager);
		shell->priv->adblock_manager = NULL;
	}

	G_OBJECT_CLASS (blx_embed_shell_parent_class)->finalize (object);
}

/**
 * blx_embed_shell_get_favicon_cache:
 * @shell: the #BlxEmbedShell
 *
 * Returns the favicons cache.
 *
 * Return value: (transfer none): the favicons cache
 **/
GObject *
blx_embed_shell_get_favicon_cache (BlxEmbedShell *shell)
{
	g_return_val_if_fail (BLX_IS_EMBED_SHELL (shell), NULL);

	if (shell->priv->favicon_cache == NULL)
	{
		shell->priv->favicon_cache = blx_favicon_cache_new ();
	}

	return G_OBJECT (shell->priv->favicon_cache);
}

/**
 * blx_embed_shell_get_global_history:
 * @shell: the #BlxEmbedShell
 *
 * Return value: (transfer none):
 **/
GObject *
blx_embed_shell_get_global_history (BlxEmbedShell *shell)
{
	g_return_val_if_fail (BLX_IS_EMBED_SHELL (shell), NULL);

	if (shell->priv->global_history == NULL)
	{
		shell->priv->global_history = blx_history_new ();
	}

	return G_OBJECT (shell->priv->global_history);
}

static GObject *
impl_get_embed_single (BlxEmbedShell *shell)
{
	BlxEmbedShellPrivate *priv;

	g_return_val_if_fail (BLX_IS_EMBED_SHELL (shell), NULL);

	priv = shell->priv;

	if (priv->embed_single != NULL &&
	    !priv->single_initialised)
	{
		g_warning ("blx_embed_shell_get_embed_single called while the single is being initialised!\n");
		return G_OBJECT (priv->embed_single);
	}

	if (priv->embed_single == NULL)
	{
		priv->embed_single = BLX_EMBED_SINGLE
                  (g_object_new (BLX_TYPE_EMBED_SINGLE, NULL));
		g_assert (priv->embed_single != NULL);

		if (!blx_embed_single_initialize (priv->embed_single))
		{
			GtkWidget *dialog;

			dialog = gtk_message_dialog_new
					(NULL,
					 GTK_DIALOG_MODAL,
					 GTK_MESSAGE_ERROR,
					 GTK_BUTTONS_CLOSE,
					 _("Blxbrowser can't be used now. "
					   "Initialization failed."));
			gtk_dialog_run (GTK_DIALOG (dialog));

			exit (0);
		}

		priv->single_initialised = TRUE;
	}

	return G_OBJECT (shell->priv->embed_single);
}

/**
 * blx_embed_shell_get_embed_single:
 * @shell: the #BlxEmbedShell
 *
 * Return value: (transfer none):
 **/
GObject *
blx_embed_shell_get_embed_single (BlxEmbedShell *shell)
{
	BlxEmbedShellClass *klass = BLX_EMBED_SHELL_GET_CLASS (shell);

	return klass->get_embed_single (shell);
}

/**
 * blx_embed_shell_get_encodings:
 * @shell: the #BlxEmbedShell
 *
 * Return value: (transfer none):
 **/
GObject *
blx_embed_shell_get_encodings (BlxEmbedShell *shell)
{
	g_return_val_if_fail (BLX_IS_EMBED_SHELL (shell), NULL);

	if (shell->priv->encodings == NULL)
	{
		shell->priv->encodings = blx_encodings_new ();
	}

	return G_OBJECT (shell->priv->encodings);
}

void
blx_embed_shell_prepare_close (BlxEmbedShell *shell)
{
	g_signal_emit (shell, signals[PREPARE_CLOSE], 0);
}

static void
blx_embed_shell_set_property (GObject *object,
			       guint prop_id,
			       const GValue *value,
			       GParamSpec *pspec)
{
  BlxEmbedShell *embed_shell = BLX_EMBED_SHELL (object);

  switch (prop_id)
  {
  case PROP_MODE:
	  embed_shell->priv->mode = g_value_get_enum (value);
	  break;
  default:
	  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
blx_embed_shell_get_property (GObject *object,
			       guint prop_id,
			       GValue *value,
			       GParamSpec *pspec)
{
  BlxEmbedShell *embed_shell = BLX_EMBED_SHELL (object);

  switch (prop_id)
  {
  case PROP_MODE:
	  g_value_set_enum (value, embed_shell->priv->mode);
	  break;
  default:
	  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
blx_embed_shell_init (BlxEmbedShell *shell)
{
	shell->priv = BLX_EMBED_SHELL_GET_PRIVATE (shell);

	/* globally accessible singleton */
	g_assert (embed_shell == NULL);
	embed_shell = shell;

	shell->priv->downloads = NULL;
}

static void
blx_embed_shell_class_init (BlxEmbedShellClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->dispose = blx_embed_shell_dispose;
	object_class->finalize = blx_embed_shell_finalize;
	object_class->set_property = blx_embed_shell_set_property;
	object_class->get_property = blx_embed_shell_get_property;

	klass->get_embed_single = impl_get_embed_single;

	object_properties[PROP_MODE] =
		g_param_spec_enum ("mode",
				   "Mode",
				   "The	 global mode for this instance of Blxbrowser .",
				   BLX_TYPE_EMBED_SHELL_MODE,
				   BLX_EMBED_SHELL_MODE_BROWSER,
				   G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |
				   G_PARAM_STATIC_BLURB | G_PARAM_CONSTRUCT_ONLY);
	
	g_object_class_install_properties (object_class,
					   N_PROPERTIES,
					   object_properties);

/**
 * BlxEmbed::download-added:
 * @shell: the #BlxEmbedShell
 * @download: the #BlxDownload added
 *
 * Emitted when a #BlxDownload has been added to the global watch list of
 * @shell, via blx_embed_shell_add_download.
 **/
	signals[DOWNLOAD_ADDED] =
		g_signal_new ("download-added",
			      BLX_TYPE_EMBED_SHELL,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (BlxEmbedShellClass, download_added),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, BLX_TYPE_DOWNLOAD);

/**
 * BlxEmbed::download-removed:
 * @shell: the #BlxEmbedShell
 * @download: the #BlxDownload being removed
 *
 * Emitted when a #BlxDownload has been removed from the global watch list of
 * @shell, via blx_embed_shell_remove_download.
 **/
	signals[DOWNLOAD_REMOVED] =
		g_signal_new ("download-removed",
			      BLX_TYPE_EMBED_SHELL,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (BlxEmbedShellClass, download_removed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, BLX_TYPE_DOWNLOAD);

/**
 * BlxEmbed::prepare-close:
 * @shell: the #BlxEmbedShell
 * 
 * The ::prepare-close signal is emitted when blxbrowser is preparing to
 * quit on command from the session manager. You can use it when you need
 * to do something special (shut down a service, for example).
 **/
	signals[PREPARE_CLOSE] =
		g_signal_new ("prepare-close",
			      BLX_TYPE_EMBED_SHELL,
			      G_SIGNAL_RUN_FIRST | G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (BlxEmbedShellClass, prepare_close),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	g_type_class_add_private (object_class, sizeof (BlxEmbedShellPrivate));
}

/**
 * blx_embed_shell_get_default:
 *
 * Retrieves the default #BlxEmbedShell object
 *
 * Return value: (transfer none): the default #BlxEmbedShell
 **/
BlxEmbedShell *
blx_embed_shell_get_default (void)
{
	return embed_shell;
}

/**
 * blx_embed_shell_get_adblock_manager:
 * @shell: the #BlxEmbedShell
 *
 * Returns the adblock manager.
 *
 * Return value: (transfer none): the adblock manager
 **/
GObject *
blx_embed_shell_get_adblock_manager (BlxEmbedShell *shell)
{
	g_return_val_if_fail (BLX_IS_EMBED_SHELL (shell), NULL);

	if (shell->priv->adblock_manager == NULL)
	{
		shell->priv->adblock_manager = g_object_new (BLX_TYPE_ADBLOCK_MANAGER, NULL);
	}

	return G_OBJECT (shell->priv->adblock_manager);
}

void
blx_embed_shell_set_page_setup	(BlxEmbedShell *shell,
				 GtkPageSetup *page_setup)
{
	BlxEmbedShellPrivate *priv;
	char *path;

	g_return_if_fail (BLX_IS_EMBED_SHELL (shell));
	priv = shell->priv;

	if (page_setup != NULL)
	{
		g_object_ref (page_setup);
	}
	else
	{
		page_setup = gtk_page_setup_new ();
	}

	if (priv->page_setup != NULL)
	{
		g_object_unref (priv->page_setup);
	}

	priv->page_setup = page_setup;

	path = g_build_filename (blx_dot_dir (), PAGE_SETUP_FILENAME, NULL);
	gtk_page_setup_to_file (page_setup, path, NULL);
	g_free (path);
}

/**
 * blx_embed_shell_get_page_setup:
 *
 * Return value: (transfer none):
 **/
GtkPageSetup *
blx_embed_shell_get_page_setup	(BlxEmbedShell *shell)
{
	BlxEmbedShellPrivate *priv;

	g_return_val_if_fail (BLX_IS_EMBED_SHELL (shell), NULL);
	priv = shell->priv;

	if (priv->page_setup == NULL)
	{
		GError *error = NULL;
		char *path;

		path = g_build_filename (blx_dot_dir (), PAGE_SETUP_FILENAME, NULL);
		priv->page_setup = gtk_page_setup_new_from_file (path, &error);
		g_free (path);

#ifdef ENABLE_MIGRATION
		/* If the file doesn't exist, try to fall back to the old format */
		if (error != NULL &&
		    error->domain == G_FILE_ERROR &&
		    error->code == G_FILE_ERROR_NOENT)
		{
			path = g_build_filename (blx_dot_dir (), LEGACY_PAGE_SETUP_FILENAME, NULL);
			priv->page_setup = blx_print_utils_page_setup_new_from_file (path, NULL);
			if (priv->page_setup != NULL)
			{
				/* Delete the old file, so we don't migrate again */
				g_unlink (path);
			}
			g_free (path);
		} else if (error != NULL)
			g_warning ("error: %s\n", error->message);
#endif /* ENABLE_MIGRATION */

		if (error)
		{
			g_error_free (error);
		}

		/* If that still didn't work, create a new, empty one */
		if (priv->page_setup == NULL)
		{
			priv->page_setup = gtk_page_setup_new ();
		}
	}

	return priv->page_setup;
}

/**
 * blx_embed_shell_set_print_gettings:
 * @shell: the #BlxEmbedShell
 * @settings: the new #GtkPrintSettings object
 *
 * Sets the global #GtkPrintSettings object.
 *
 **/
void
blx_embed_shell_set_print_settings (BlxEmbedShell *shell,
				     GtkPrintSettings *settings)
{
	BlxEmbedShellPrivate *priv;
	char *path;

	g_return_if_fail (BLX_IS_EMBED_SHELL (shell));
	priv = shell->priv;

	if (settings != NULL)
	{
		g_object_ref (settings);
	}

	if (priv->print_settings != NULL)
	{
		g_object_unref (priv->print_settings);
	}

	priv->print_settings = settings ? settings : gtk_print_settings_new ();

	path = g_build_filename (blx_dot_dir (), PRINT_SETTINGS_FILENAME, NULL);
	gtk_print_settings_to_file (settings, path, NULL);
	g_free (path);
}

/**
 * blx_embed_shell_get_print_settings:
 * @shell: the #BlxEmbedShell
 *
 * Gets the global #GtkPrintSettings object.
 *
 * Returns: (transfer none): a #GtkPrintSettings object
 **/
GtkPrintSettings *
blx_embed_shell_get_print_settings (BlxEmbedShell *shell)
{
	BlxEmbedShellPrivate *priv;

	g_return_val_if_fail (BLX_IS_EMBED_SHELL (shell), NULL);
	priv = shell->priv;

	if (priv->print_settings == NULL)
	{
		GError *error = NULL;
		char *path;

		path = g_build_filename (blx_dot_dir (), PRINT_SETTINGS_FILENAME, NULL);
		priv->print_settings = gtk_print_settings_new_from_file (path, &error);
		g_free (path);

		/* Note: the gtk print settings file format is the same as our
		 * legacy one, so no need to migrate here.
		 */

		if (priv->print_settings == NULL)
		{
			priv->print_settings = gtk_print_settings_new ();
		}
	}

	return priv->print_settings;
}

/**
 * blx_embed_shell_get_downloads:
 * @shell: the #BlxEmbedShell
 *
 * Gets the global #GList object listing active downloads.
 *
 * Returns: (transfer none): a #GList object
 **/
GList *
blx_embed_shell_get_downloads (BlxEmbedShell *shell)
{
	BlxEmbedShellPrivate *priv;

	g_return_val_if_fail (BLX_IS_EMBED_SHELL (shell), NULL);
	priv = shell->priv;

	return priv->downloads;
}

void
blx_embed_shell_add_download (BlxEmbedShell *shell, BlxDownload *download)
{
	BlxEmbedShellPrivate *priv;

	g_return_if_fail (BLX_IS_EMBED_SHELL (shell));

	priv = shell->priv;
	priv->downloads = g_list_prepend (priv->downloads, download);

	g_signal_emit_by_name (shell, "download-added", download, NULL);
}

void
blx_embed_shell_remove_download (BlxEmbedShell *shell, BlxDownload *download)
{
	BlxEmbedShellPrivate *priv;

	g_return_if_fail (BLX_IS_EMBED_SHELL (shell));

	priv = shell->priv;
	priv->downloads = g_list_remove (priv->downloads, download);

	g_signal_emit_by_name (shell, "download-removed", download, NULL);
}

/**
 * blx_embed_shell_get_mode:
 * @shell: an #BlxEmbedShell
 * 
 * Returns: the global mode of the @shell
 **/
BlxEmbedShellMode
blx_embed_shell_get_mode (BlxEmbedShell *shell)
{
	g_return_val_if_fail (BLX_IS_EMBED_SHELL (shell), BLX_EMBED_SHELL_MODE_BROWSER);
	
	return shell->priv->mode;
}
