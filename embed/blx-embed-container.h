/*
 *  Copyright © 2007 Xan Lopez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_EMBED_CONTAINER_H
#define BLX_EMBED_CONTAINER_H

#include "blx-embed.h"
#include "blx-web-view.h"

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define BLX_TYPE_EMBED_CONTAINER               (blx_embed_container_get_type ())
#define BLX_EMBED_CONTAINER(o)                 (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_EMBED_CONTAINER, BlxEmbedContainer))
#define BLX_EMBED_CONTAINER_IFACE(k)           (G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_EMBED_CONTAINER, BlxEmbedContainerIface))
#define BLX_IS_EMBED_CONTAINER(o)              (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_EMBED_CONTAINER))
#define BLX_IS_EMBED_CONTAINER_IFACE(k)        (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_EMBED_CONTAINER))
#define BLX_EMBED_CONTAINER_GET_IFACE(inst)    (G_TYPE_INSTANCE_GET_INTERFACE ((inst), BLX_TYPE_EMBED_CONTAINER, BlxEmbedContainerIface))

typedef struct _BlxEmbedContainer      BlxEmbedContainer;
typedef struct _BlxEmbedContainerIface BlxEmbedContainerIface;

struct _BlxEmbedContainerIface
{
  GTypeInterface parent_iface;

  gint (* add_child)               (BlxEmbedContainer *container,
                                    BlxEmbed *child,
                                    gint position,
                                    gboolean set_active);

  void (* set_active_child)        (BlxEmbedContainer *container,
                                    BlxEmbed *child);

  void (* remove_child)            (BlxEmbedContainer *container,
                                    BlxEmbed *child);

  BlxEmbed * (* get_active_child) (BlxEmbedContainer *container);

  GList * (* get_children)         (BlxEmbedContainer *container);

  gboolean (* get_is_popup)        (BlxEmbedContainer *container);

  BlxWebViewChrome (* get_chrome) (BlxEmbedContainer *container);
};

GType             blx_embed_container_get_type         (void);
gint              blx_embed_container_add_child        (BlxEmbedContainer *container,
                                                         BlxEmbed          *child,
                                                         gint                position,
                                                         gboolean            set_active);
void              blx_embed_container_set_active_child (BlxEmbedContainer *container,
                                                         BlxEmbed          *child);
void              blx_embed_container_remove_child     (BlxEmbedContainer *container,
                                                         BlxEmbed          *child);
BlxEmbed *       blx_embed_container_get_active_child (BlxEmbedContainer *container);
GList *           blx_embed_container_get_children     (BlxEmbedContainer *container);
gboolean          blx_embed_container_get_is_popup     (BlxEmbedContainer *container);
BlxWebViewChrome blx_embed_container_get_chrome       (BlxEmbedContainer *container);

G_END_DECLS

#endif
