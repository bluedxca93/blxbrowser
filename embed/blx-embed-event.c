/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 *  Copyright © 2000-2003 Marco Pesenti Gritti
 *  Copyright © 2009 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include "blx-embed-event.h"
#include "blx-embed-type-builtins.h"

#include <glib.h>
#include <gtk/gtk.h>

#define BLX_EMBED_EVENT_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), BLX_TYPE_EMBED_EVENT, BlxEmbedEventPrivate))

struct BlxEmbedEventPrivate
{
  guint button;
  guint modifier;
  guint x;
  guint y;
  WebKitHitTestResult *hit_test_result;
};

G_DEFINE_TYPE (BlxEmbedEvent, blx_embed_event, G_TYPE_OBJECT)

static void
dispose (GObject *object)
{
  BlxEmbedEventPrivate *priv = BLX_EMBED_EVENT (object)->priv;
  
  if (priv->hit_test_result) {
    g_object_unref (priv->hit_test_result);
    priv->hit_test_result = NULL;
  }

  G_OBJECT_CLASS (blx_embed_event_parent_class)->dispose (object);
}

static void
blx_embed_event_class_init (BlxEmbedEventClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)klass;

  object_class->dispose = dispose;

  g_type_class_add_private (G_OBJECT_CLASS (klass), sizeof(BlxEmbedEventPrivate));
}

static void
blx_embed_event_init (BlxEmbedEvent *embed_event)
{
  embed_event->priv = BLX_EMBED_EVENT_GET_PRIVATE (embed_event);
}

BlxEmbedEvent *
blx_embed_event_new (GdkEventButton *event, WebKitHitTestResult *hit_test_result)
{
  BlxEmbedEvent *embed_event;
  BlxEmbedEventPrivate *priv;

  embed_event = g_object_new (BLX_TYPE_EMBED_EVENT, NULL);
  priv = embed_event->priv;

  priv->hit_test_result = g_object_ref (hit_test_result);
  priv->button = event->button;
  priv->modifier = event->state;
  priv->x = event->x;
  priv->y = event->y;

  return embed_event;
}

guint
blx_embed_event_get_context (BlxEmbedEvent *event)
{
  BlxEmbedEventPrivate *priv;
  guint context;

  g_return_val_if_fail (BLX_IS_EMBED_EVENT (event), 0);

  priv = event->priv;
  g_object_get (priv->hit_test_result, "context", &context, NULL);
  return context;
}

guint
blx_embed_event_get_button (BlxEmbedEvent *event)
{
  BlxEmbedEventPrivate *priv;

  g_return_val_if_fail (BLX_IS_EMBED_EVENT (event), 0);

  priv = event->priv;

  return priv->button;
}

guint
blx_embed_event_get_modifier (BlxEmbedEvent *event)
{
  BlxEmbedEventPrivate *priv;

  g_return_val_if_fail (BLX_IS_EMBED_EVENT (event), 0);

  priv = event->priv;

  return priv->modifier;
}

void
blx_embed_event_get_coords (BlxEmbedEvent *event,
                             guint *x, guint *y)
{
  BlxEmbedEventPrivate *priv;

  g_return_if_fail (BLX_IS_EMBED_EVENT (event));

  priv = event->priv;

  if (x)
    *x = priv->x;

  if (y)
    *y = priv->y;
}

/**
 * blx_embed_event_get_property:
 *
 * @value: (out):
 */
void 
blx_embed_event_get_property   (BlxEmbedEvent *event,
                                 const char *name,
                                 GValue *value)
{
  BlxEmbedEventPrivate *priv;

  g_return_if_fail (BLX_IS_EMBED_EVENT (event));
  g_return_if_fail (name);

  priv = event->priv;

  /* FIXME: ugly hack! This only works for now because all properties
     we have are strings */
  g_value_init (value, G_TYPE_STRING);

  g_object_get_property (G_OBJECT (priv->hit_test_result), name, value);
}

gboolean
blx_embed_event_has_property   (BlxEmbedEvent *event,
                                 const char *name)
{
  BlxEmbedEventPrivate *priv;

  g_return_val_if_fail (BLX_IS_EMBED_EVENT (event), FALSE);
  g_return_val_if_fail (name, FALSE);

  priv = event->priv;

  return g_object_class_find_property (G_OBJECT_GET_CLASS (priv->hit_test_result),
                                       name) != NULL;
                                                           
}
