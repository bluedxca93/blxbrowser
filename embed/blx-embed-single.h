/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2; -*- */
/*
 *  Copyright © 2000-2003 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_EMBED_SINGLE_H
#define BLX_EMBED_SINGLE_H

#include "blx-embed.h"
#include "blx-web-view.h"

G_BEGIN_DECLS

#define BLX_TYPE_EMBED_SINGLE    (blx_embed_single_get_type ())
#define BLX_EMBED_SINGLE(o)    (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_EMBED_SINGLE, BlxEmbedSingle))
#define BLX_EMBED_SINGLE_IFACE(k)  (G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_EMBED_SINGLE, BlxEmbedSingleIface))
#define BLX_IS_EMBED_SINGLE(o)   (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_EMBED_SINGLE))
#define BLX_IS_EMBED_SINGLE_IFACE(k) (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_EMBED_SINGLE))
#define BLX_EMBED_SINGLE_GET_IFACE(i)  (G_TYPE_INSTANCE_GET_INTERFACE ((i), BLX_TYPE_EMBED_SINGLE, BlxEmbedSingleIface))

typedef struct _BlxEmbedSingle   BlxEmbedSingle;
typedef struct _BlxEmbedSingleClass    BlxEmbedSingleClass;
typedef struct _BlxEmbedSinglePrivate  BlxEmbedSinglePrivate;

typedef struct {
  char *form_username;
  char *form_password;
  char *username;
} BlxEmbedSingleFormAuthData;

struct _BlxEmbedSingle {
  GObject parent;

  /*< private >*/
  BlxEmbedSinglePrivate *priv;
};

struct _BlxEmbedSingleClass
{
  GObjectClass parent_class;

  /* Signals */

  BlxEmbed * (* new_window)  (BlxEmbedSingle *single,
                               BlxEmbed *parent_embed,
                               BlxWebViewChrome chromemask);

  gboolean (* handle_content) (BlxEmbedSingle *shell,
                               char *mime_type,
                               char *uri);
};

GType           blx_embed_single_get_type           (void);

gboolean        blx_embed_single_initialize         (BlxEmbedSingle *single);

GtkWidget      *blx_embed_single_open_window        (BlxEmbedSingle *single,
                                                      BlxEmbed       *parent,
                                                      const char      *address,
                                                      const char      *name,
                                                      const char      *features);

void            blx_embed_single_clear_cache        (BlxEmbedSingle *single);

void            blx_embed_single_clear_auth_cache   (BlxEmbedSingle *single);

void            blx_embed_single_set_network_status (BlxEmbedSingle *single,
                                                      gboolean         online);

gboolean        blx_embed_single_get_network_status (BlxEmbedSingle *single);

GSList *        blx_embed_single_get_form_auth      (BlxEmbedSingle *single,
                                                      const char *uri);

void            blx_embed_single_add_form_auth      (BlxEmbedSingle *single,
                                                      const char *uri,
                                                      const char *form_username,
                                                      const char *form_password,
                                                      const char *username);

G_END_DECLS

#endif
