/*
 *  Copyright © 2011 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_WEB_APP_UTILS_H
#define BLX_WEB_APP_UTILS_H

#include <glib.h>
#include <gtk/gtk.h>

#include "blx-web-view.h"

G_BEGIN_DECLS

typedef struct {
    char *name;
    char *icon_url;
    char *url;
    char install_date[128];
} BlxWebApplication;

#define BLX_WEB_APP_PREFIX "app-"

char    *blx_web_application_create (BlxWebView *view, const char *title, GdkPixbuf *icon);

gboolean blx_web_application_delete (const char *name);

char    *blx_web_application_get_profile_directory (const char *app_name);

GList   *blx_web_application_get_application_list (void);

void     blx_web_application_free_application_list (GList *list);

G_END_DECLS

#endif
