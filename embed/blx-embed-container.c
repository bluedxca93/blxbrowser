/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2; -*- */
/*
 *  Copyright © 2007 Xan Lopez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include "blx-embed-container.h"
#include "blx-embed-type-builtins.h"

static void
blx_embed_container_base_init (gpointer g_class)
{
  static gboolean initialized = FALSE;

  if (!initialized) {
    initialized = TRUE;

    g_object_interface_install_property (g_class,
                                         g_param_spec_flags ("chrome", NULL, NULL,
                                                             BLX_TYPE_WEB_VIEW_CHROME,
                                                             BLX_WEB_VIEW_CHROME_ALL,
                                                             G_PARAM_CONSTRUCT_ONLY |
                                                             G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_object_interface_install_property (g_class,
                                         g_param_spec_boolean ("is-popup", NULL, NULL,
                                                               FALSE,
                                                               G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB |
                                                               G_PARAM_CONSTRUCT_ONLY));
    g_object_interface_install_property (g_class,
                                         g_param_spec_object ("active-child", NULL, NULL,
                                                              GTK_TYPE_WIDGET /* Can't use an interface type here */,
                                                              G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
  }
}

GType
blx_embed_container_get_type (void)
{
  static GType type = 0;

  if (G_UNLIKELY (type == 0)) {
    const GTypeInfo our_info =
      {
        sizeof (BlxEmbedContainerIface),
        blx_embed_container_base_init,
        NULL,
      };

    type = g_type_register_static (G_TYPE_INTERFACE,
                                   "BlxEmbedContainer",
                                   &our_info, (GTypeFlags)0);
  }

  return type;
}

/**
 * blx_embed_container_add_child:
 * @container: an #BlxEmbedContainer
 * @child: an #BlxEmbed
 * @position: the position in @container's
 * @set_active: whether to set @embed as the active child of @container
 * after insertion
 *
 * Inserts @child into @container.
 *
 * Return value: @child's new position inside @container.
 **/
gint
blx_embed_container_add_child (BlxEmbedContainer *container,
                                BlxEmbed *child,
                                gint position,
                                gboolean set_active)
{
  BlxEmbedContainerIface *iface;

  g_return_val_if_fail (BLX_IS_EMBED_CONTAINER (container), -1);
  g_return_val_if_fail (BLX_IS_EMBED (child), -1);

  iface = BLX_EMBED_CONTAINER_GET_IFACE (container);
  return iface->add_child (container, child, position, set_active);
}

/**
 * blx_embed_container_set_active_child:
 * @container: an #BlxEmbedContainer
 * @child: an #BlxEmbed inside @container
 *
 * Sets @child as @container's active child.
 **/
void
blx_embed_container_set_active_child (BlxEmbedContainer *container,
                                       BlxEmbed *child)
{
  BlxEmbedContainerIface *iface;

  g_return_if_fail (BLX_IS_EMBED_CONTAINER (container));
  g_return_if_fail (BLX_IS_EMBED (child));

  iface = BLX_EMBED_CONTAINER_GET_IFACE (container);

  iface->set_active_child (container, child);
}

/**
 * blx_embed_container_remove_child:
 * @container: an #BlxEmbedContainer
 * @child: an #BlxEmbed
 *
 * Removes @child from @container.
 **/
void
blx_embed_container_remove_child (BlxEmbedContainer *container,
                                   BlxEmbed *child)
{
  BlxEmbedContainerIface *iface;

  g_return_if_fail (BLX_IS_EMBED_CONTAINER (container));
  g_return_if_fail (BLX_IS_EMBED (child));

  iface = BLX_EMBED_CONTAINER_GET_IFACE (container);

  iface->remove_child (container, child);
}

/**
 * blx_embed_container_get_active_child:
 * @container: an #BlxEmbedContainer
 *
 * Returns @container's active #BlxEmbed.
 *
 * Return value: (transfer none): @container's active child
 **/
BlxEmbed *
blx_embed_container_get_active_child (BlxEmbedContainer *container)
{
  BlxEmbedContainerIface *iface;

  g_return_val_if_fail (BLX_IS_EMBED_CONTAINER (container), NULL);

  iface = BLX_EMBED_CONTAINER_GET_IFACE (container);
  return iface->get_active_child (container);
}

/**
 * blx_embed_container_get_children:
 * @container: a #BlxEmbedContainer
 *
 * Returns the list of #BlxEmbed:s in the container.
 *
 * Return value: (element-type BlxEmbed) (transfer container):
 *               a newly-allocated list of #BlxEmbed:s
 */
GList *
blx_embed_container_get_children (BlxEmbedContainer *container)
{
  BlxEmbedContainerIface *iface;

  g_return_val_if_fail (BLX_IS_EMBED_CONTAINER (container), NULL);

  iface = BLX_EMBED_CONTAINER_GET_IFACE (container);
  return iface->get_children (container);
}

/**
 * blx_embed_container_get_is_popup:
 * @container: an #BlxEmbedContainer
 *
 * Returns whether this embed container is a popup.
 *
 * Return value: %TRUE if it is a popup
 **/
gboolean
blx_embed_container_get_is_popup (BlxEmbedContainer *container)
{
  BlxEmbedContainerIface *iface;

  g_return_val_if_fail (BLX_IS_EMBED_CONTAINER (container), FALSE);

  iface = BLX_EMBED_CONTAINER_GET_IFACE (container);
  return iface->get_is_popup (container);
}

/**
 * blx_embed_container_get_chrome:
 * @container: an #BlxEmbedContainer
 *
 * Returns the #BlxWebViewChrome flags indicating the visibility of several parts
 * of the UI.
 *
 * Return value: #BlxWebViewChrome flags.
 **/
BlxWebViewChrome
blx_embed_container_get_chrome (BlxEmbedContainer *container)
{
  BlxEmbedContainerIface *iface;

  g_return_val_if_fail (BLX_IS_EMBED_CONTAINER (container), 0);

  iface = BLX_EMBED_CONTAINER_GET_IFACE (container);
  return iface->get_chrome (container);
}
