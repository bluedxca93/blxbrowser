/*
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_EXTENSION_H
#define BLX_EXTENSION_H

#include "blx-window.h"

#include <glib-object.h>

G_BEGIN_DECLS

#define BLX_TYPE_EXTENSION		(blx_extension_get_type ())
#define BLX_EXTENSION(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), BLX_TYPE_EXTENSION, BlxExtension))
#define BLX_EXTENSION_IFACE(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_EXTENSION, BlxExtensionIface))
#define BLX_IS_EXTENSION(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_EXTENSION))
#define BLX_IS_EXTENSION_IFACE(class)	(G_TYPE_CHECK_CLASS_TYPE ((class), BLX_TYPE_EXTENSION))
#define BLX_EXTENSION_GET_IFACE(inst)	(G_TYPE_INSTANCE_GET_INTERFACE ((inst), BLX_TYPE_EXTENSION, BlxExtensionIface))

typedef struct _BlxExtension		BlxExtension;
typedef struct _BlxExtensionIface	BlxExtensionIface;
	
struct _BlxExtensionIface
{
	GTypeInterface base_iface;

	void	(* attach_window)	(BlxExtension *extension,
					 BlxWindow *window);
	void	(* detach_window)	(BlxExtension *extension,
					 BlxWindow *window);
	void	(* attach_tab)		(BlxExtension *extension,
					 BlxWindow *window,
					 BlxEmbed *embed);
	void	(* detach_tab)		(BlxExtension *extension,
					 BlxWindow *window,
					 BlxEmbed *embed);
};

GType	blx_extension_get_type		(void);

void	blx_extension_attach_window	(BlxExtension *extension,
					 BlxWindow *window);

void	blx_extension_detach_window	(BlxExtension *extension,
					 BlxWindow *window);

void	blx_extension_attach_tab	(BlxExtension *extension,
					 BlxWindow *window,
					 BlxEmbed *embed);

void	blx_extension_detach_tab	(BlxExtension *extension,
					 BlxWindow *window,
					 BlxEmbed *embed);

G_END_DECLS

#endif
