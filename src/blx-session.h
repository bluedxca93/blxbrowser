/*
 *  Copyright © 2002 Jorn Baayen
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_SESSION_H
#define BLX_SESSION_H

#include "blx-window.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_SESSION		(blx_session_get_type ())
#define BLX_SESSION(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_SESSION, BlxSession))
#define BLX_SESSION_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_SESSION, BlxSessionClass))
#define BLX_IS_SESSION(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_SESSION))
#define BLX_IS_SESSION_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_SESSION))
#define BLX_SESSION_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_SESSION, BlxSessionClass))

typedef struct _BlxSession		BlxSession;
typedef struct _BlxSessionPrivate	BlxSessionPrivate;
typedef struct _BlxSessionClass	BlxSessionClass;

typedef enum
{
	BLX_SESSION_CMD_RESUME_SESSION,
	BLX_SESSION_CMD_LOAD_SESSION,
	BLX_SESSION_CMD_OPEN_BOOKMARKS_EDITOR,
	BLX_SESSION_CMD_OPEN_URIS,
	BLX_SESSION_CMD_MAYBE_OPEN_WINDOW,
	BLX_SESSION_CMD_MAYBE_OPEN_WINDOW_RESTORE,
	BLX_SESSION_CMD_LAST
	
} BlxSessionCommand;

struct _BlxSession
{
        GObject parent;

	/*< private >*/
        BlxSessionPrivate *priv;
};

struct _BlxSessionClass
{
        GObjectClass parent_class;
};

GType		 blx_session_get_type		(void);

BlxWindow	*blx_session_get_active_window	(BlxSession *session);

gboolean	 blx_session_save		(BlxSession *session,
						 const char *filename);

gboolean	 blx_session_load		(BlxSession *session,
						 const char *filename,
						 guint32 user_time);

void		 blx_session_close		(BlxSession *session);

GList		*blx_session_get_windows	(BlxSession *session);

void		 blx_session_add_window	(BlxSession *session,
						 GtkWindow *window);

void		 blx_session_remove_window	(BlxSession *session,
						 GtkWindow *window);

void		 blx_session_queue_command	(BlxSession *session,
						 BlxSessionCommand op,
						 const char *arg,
						 const char **args,
						 guint32 user_time,
						 gboolean priority);

G_END_DECLS

#endif
