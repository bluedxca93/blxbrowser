/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include <string.h>

#include "blx-completion-model.h"
#include "blx-favicon-cache.h"
#include "blx-node.h"
#include "blx-shell.h"
#include "blx-history.h"

static void blx_completion_model_class_init (BlxCompletionModelClass *klass);
static void blx_completion_model_init (BlxCompletionModel *model);
static void blx_completion_model_tree_model_init (GtkTreeModelIface *iface);

#define BLX_COMPLETION_MODEL_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), BLX_TYPE_COMPLETION_MODEL, BlxCompletionModelPrivate))

struct _BlxCompletionModelPrivate
{
	BlxHistory *history_service;
	BlxBookmarks *bookmarks_service;
	BlxFaviconCache *favicon_cache;

	BlxNode *history;
	BlxNode *bookmarks;
	int stamp;
};

enum
{
	HISTORY_GROUP,
	BOOKMARKS_GROUP
};

G_DEFINE_TYPE_WITH_CODE (BlxCompletionModel, blx_completion_model, G_TYPE_OBJECT,
			 G_IMPLEMENT_INTERFACE (GTK_TYPE_TREE_MODEL,
						blx_completion_model_tree_model_init))

static void
blx_completion_model_class_init (BlxCompletionModelClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (object_class, sizeof (BlxCompletionModelPrivate));
}

static GtkTreePath *
get_path_real (BlxCompletionModel *model,
	       BlxNode *root,
	       BlxNode *child)
{
	GtkTreePath *retval;
	int index;

	retval = gtk_tree_path_new ();
	index = blx_node_get_child_index (root, child);

	if (root == model->priv->bookmarks)
	{
		index += blx_node_get_n_children (model->priv->history);
	}

	gtk_tree_path_append_index (retval, index);

	return retval;
}

static void
node_iter_from_node (BlxCompletionModel *model,
		     BlxNode *root,
		     BlxNode *child,
		     GtkTreeIter *iter)
{
	iter->stamp = model->priv->stamp;
	iter->user_data = child;
	iter->user_data2 = root;
}

static BlxNode *
get_index_root (BlxCompletionModel *model, int *index)
{
	int children;

	children = blx_node_get_n_children (model->priv->history);

	if (*index >= children)
	{
		*index = *index - children;

		if (*index < blx_node_get_n_children (model->priv->bookmarks))
		{
			return model->priv->bookmarks;
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		return model->priv->history;
	}
}

static void
root_child_removed_cb (BlxNode *node,
		       BlxNode *child,
		       guint old_index,
		       BlxCompletionModel *model)
{
	GtkTreePath *path;
	guint index;

	path = gtk_tree_path_new ();

	index = old_index;
	if (node == model->priv->bookmarks)
	{
		index += blx_node_get_n_children (model->priv->history);
	}
	gtk_tree_path_append_index (path, index);

	gtk_tree_model_row_deleted (GTK_TREE_MODEL (model), path);
	gtk_tree_path_free (path);
}

static void
root_child_added_cb (BlxNode *node,
		     BlxNode *child,
		     BlxCompletionModel *model)
{
	GtkTreePath *path;
	GtkTreeIter iter;

	node_iter_from_node (model, node, child, &iter);

	path = get_path_real (model, node, child);
	gtk_tree_model_row_inserted (GTK_TREE_MODEL (model), path, &iter);
	gtk_tree_path_free (path);
}

static void
root_child_changed_cb (BlxNode *node,
		       BlxNode *child,
		       guint property_id,
		       BlxCompletionModel *model)
{
	GtkTreePath *path;
	GtkTreeIter iter;

	node_iter_from_node (model, node, child, &iter);

	path = get_path_real (model, node, child);
	gtk_tree_model_row_changed (GTK_TREE_MODEL (model), path, &iter);
	gtk_tree_path_free (path);
}

static void
connect_signals (BlxCompletionModel *model, BlxNode *root)
{
	blx_node_signal_connect_object (root,
			                 BLX_NODE_CHILD_ADDED,
			                 (BlxNodeCallback)root_child_added_cb,
			                 G_OBJECT (model));
	blx_node_signal_connect_object (root,
			                 BLX_NODE_CHILD_REMOVED,
			                 (BlxNodeCallback)root_child_removed_cb,
			                 G_OBJECT (model));
	blx_node_signal_connect_object (root,
			                 BLX_NODE_CHILD_CHANGED,
			                 (BlxNodeCallback)root_child_changed_cb,
			                 G_OBJECT (model));
}

static void
blx_completion_model_init (BlxCompletionModel *model)
{
	model->priv = BLX_COMPLETION_MODEL_GET_PRIVATE (model);
	model->priv->stamp = g_random_int ();

	model->priv->history_service = BLX_HISTORY (
					blx_embed_shell_get_global_history (
					embed_shell));
	model->priv->history = blx_history_get_pages (
				model->priv->history_service);
	connect_signals (model, model->priv->history);

	model->priv->bookmarks_service = blx_shell_get_bookmarks (blx_shell);
	model->priv->bookmarks = blx_bookmarks_get_bookmarks (
				  model->priv->bookmarks_service);
	connect_signals (model, model->priv->bookmarks);

	model->priv->favicon_cache = BLX_FAVICON_CACHE (
					blx_embed_shell_get_favicon_cache (
					BLX_EMBED_SHELL (blx_shell)));
}

BlxCompletionModel *
blx_completion_model_new (void)
{
	BlxCompletionModel *model;

	model = BLX_COMPLETION_MODEL (g_object_new (BLX_TYPE_COMPLETION_MODEL,
						    NULL));

	g_return_val_if_fail (model->priv != NULL, NULL);

	return model;
}

static int
blx_completion_model_get_n_columns (GtkTreeModel *tree_model)
{
	return N_COL;
}

static GType
blx_completion_model_get_column_type (GtkTreeModel *tree_model,
			               int index)
{
	GType type = 0;

	switch (index)
	{
		case BLX_COMPLETION_TEXT_COL:
		case BLX_COMPLETION_ACTION_COL:
		case BLX_COMPLETION_KEYWORDS_COL:
			type =  G_TYPE_STRING;
			break;
		case BLX_COMPLETION_EXTRA_COL:
			type = G_TYPE_BOOLEAN;
			break;
		case BLX_COMPLETION_FAVICON_COL:
			type = GDK_TYPE_PIXBUF;
			break;
		case BLX_COMPLETION_RELEVANCE_COL:
			type = G_TYPE_INT;
			break;
	}

	return type;
}

static void
init_text_col (GValue *value, BlxNode *node, int group)
{
	const char *text;

	switch (group)
	{
		case BOOKMARKS_GROUP:
		case HISTORY_GROUP:
			text = blx_node_get_property_string
				(node, BLX_NODE_PAGE_PROP_TITLE);
			break;

		default:
			text = "";
	}
	
	g_value_set_string (value, text);
}

static void
init_action_col (GValue *value, BlxNode *node)
{
	const char *text;

	text = blx_node_get_property_string
		(node, BLX_NODE_BMK_PROP_LOCATION);
	
	g_value_set_string (value, text);
}

static void
init_keywords_col (GValue *value, BlxNode *node, int group)
{
	const char *text = NULL;

	switch (group)
	{
		case BOOKMARKS_GROUP:
			text = blx_node_get_property_string
				(node, BLX_NODE_BMK_PROP_KEYWORDS);
			break;
	}

	if (text == NULL)
	{
		text = "";
	}
	
	g_value_set_string (value, text);
}
static void
init_favicon_col (BlxCompletionModel *model, GValue *value, 
		  BlxNode *node, int group)
{
	const char *icon_location;
	GdkPixbuf *pixbuf = NULL;
	const char *url;

	switch (group)
	{
		case BOOKMARKS_GROUP:
			icon_location = blx_node_get_property_string
				(node, BLX_NODE_BMK_PROP_ICON);
			break;
		case HISTORY_GROUP:
			url = blx_node_get_property_string
				(node, BLX_NODE_PAGE_PROP_LOCATION);
			icon_location = blx_history_get_icon (
					model->priv->history_service, url);
			break;
		default:
			icon_location = NULL;
	}
	
	if (icon_location)
	{
		pixbuf = blx_favicon_cache_get (
				model->priv->favicon_cache, icon_location);
	}

	g_value_take_object (value, pixbuf);
}

static gboolean
is_base_address (const char *address)
{
        if (address == NULL)
                return FALSE;

        /* a base address is <scheme>://<host>/
         * Neither scheme nor host contain a slash, so we can use slashes
         * figure out if it's a base address.
         *
         * Note: previous code was using a GRegExp to do the same thing. 
         * While regexps are much nicer to read, they're also a lot
         * slower.
         */
        address = strchr (address, '/');
        if (address == NULL ||
            address[1] != '/')
                return FALSE;

        address += 2;
        address = strchr (address, '/');
        if (address == NULL ||
            address[1] != 0)
                return FALSE;

        return TRUE;
}

static void
init_relevance_col (GValue *value, BlxNode *node, int group)
{
	int relevance = 0;

	/* We have three ordered groups: history's base
	   addresses, bookmarks, deep history addresses */

	if (group == BOOKMARKS_GROUP)
	{
		relevance = 1 << 5;
	}
	else if (group == HISTORY_GROUP)
	{
		const char *address;
		int visits;
	
		visits = blx_node_get_property_int
			(node, BLX_NODE_PAGE_PROP_VISITS);
		address = blx_node_get_property_string
			(node, BLX_NODE_PAGE_PROP_LOCATION);

		visits = MIN (visits, (1 << 5) - 1);

		if (is_base_address (address))
		{
			relevance = visits << 10;
		}
		else
		{
			relevance = visits;
		}
	}
	
	g_value_set_int (value, relevance);
}

static void
init_url_col (GValue *value, BlxNode *node)
{
        const char *url = NULL;

	url = blx_node_get_property_string
	  (node, BLX_NODE_PAGE_PROP_LOCATION);
	
	g_value_set_string (value, url);
}

static void
blx_completion_model_get_value (GtkTreeModel *tree_model,
			         GtkTreeIter *iter,
			         int column,
			         GValue *value)
{
	int group;
	BlxCompletionModel *model = BLX_COMPLETION_MODEL (tree_model);
	BlxNode *node;

	g_return_if_fail (BLX_IS_COMPLETION_MODEL (tree_model));
	g_return_if_fail (iter != NULL);
	g_return_if_fail (iter->stamp == model->priv->stamp);

	node = iter->user_data;
	group = (iter->user_data2 == model->priv->history) ?
		HISTORY_GROUP : BOOKMARKS_GROUP;

	switch (column)
	{
		case BLX_COMPLETION_EXTRA_COL:
			g_value_init (value, G_TYPE_BOOLEAN);
			g_value_set_boolean (value, (group == BOOKMARKS_GROUP));
			break;
		case BLX_COMPLETION_TEXT_COL:
			g_value_init (value, G_TYPE_STRING);
			init_text_col (value, node, group);
			break;
		case BLX_COMPLETION_FAVICON_COL:
			g_value_init (value, GDK_TYPE_PIXBUF);
			init_favicon_col (model, value, node, group);
 			break;
		case BLX_COMPLETION_ACTION_COL:
			g_value_init (value, G_TYPE_STRING);
			init_action_col (value, node);
			break;
		case BLX_COMPLETION_KEYWORDS_COL:
			g_value_init (value, G_TYPE_STRING);
			init_keywords_col (value, node, group);
			break;
		case BLX_COMPLETION_RELEVANCE_COL:
			g_value_init (value, G_TYPE_INT);
			init_relevance_col (value, node, group);
			break;
                case BLX_COMPLETION_URL_COL:
                        g_value_init (value, G_TYPE_STRING);
                        init_url_col (value, node);
                        break;
	}
}

static GtkTreeModelFlags
blx_completion_model_get_flags (GtkTreeModel *tree_model)
{
	return GTK_TREE_MODEL_ITERS_PERSIST | GTK_TREE_MODEL_LIST_ONLY;
}

static gboolean
blx_completion_model_get_iter (GtkTreeModel *tree_model,
			        GtkTreeIter *iter,
			        GtkTreePath *path)
{
	BlxCompletionModel *model = BLX_COMPLETION_MODEL (tree_model);
	BlxNode *root, *child;
	int i;

	g_return_val_if_fail (BLX_IS_COMPLETION_MODEL (model), FALSE);
	g_return_val_if_fail (gtk_tree_path_get_depth (path) > 0, FALSE);

	i = gtk_tree_path_get_indices (path)[0];

	root = get_index_root (model, &i);
	if (root == NULL) return FALSE;

	child = blx_node_get_nth_child (root, i);
	g_return_val_if_fail (child != NULL, FALSE);

	node_iter_from_node (model, root, child, iter);

	return TRUE;
}

static GtkTreePath *
blx_completion_model_get_path (GtkTreeModel *tree_model,
			        GtkTreeIter *iter)
{
	BlxCompletionModel *model = BLX_COMPLETION_MODEL (tree_model);

	g_return_val_if_fail (iter != NULL, NULL);
	g_return_val_if_fail (iter->user_data != NULL, NULL);
	g_return_val_if_fail (iter->user_data2 != NULL, NULL);
	g_return_val_if_fail (iter->stamp == model->priv->stamp, NULL);

	return get_path_real (model, iter->user_data2, iter->user_data);
}

static gboolean
blx_completion_model_iter_next (GtkTreeModel *tree_model,
			         GtkTreeIter *iter)
{
	BlxCompletionModel *model = BLX_COMPLETION_MODEL (tree_model);
	BlxNode *node, *next, *root;

	g_return_val_if_fail (iter != NULL, FALSE);
	g_return_val_if_fail (iter->user_data != NULL, FALSE);
	g_return_val_if_fail (iter->user_data2 != NULL, FALSE);
	g_return_val_if_fail (iter->stamp == model->priv->stamp, FALSE);

	node = iter->user_data;
	root = iter->user_data2;

	next = blx_node_get_next_child (root, node);

	if (next == NULL && root == model->priv->history)
	{
		root = model->priv->bookmarks;
		next = blx_node_get_nth_child (model->priv->bookmarks, 0);
	}

	if (next == NULL) return FALSE;

	node_iter_from_node (model, root, next, iter);
	
	return TRUE;
}

static gboolean
blx_completion_model_iter_children (GtkTreeModel *tree_model,
			             GtkTreeIter *iter,
			             GtkTreeIter *parent)
{
	BlxCompletionModel *model = BLX_COMPLETION_MODEL (tree_model);
	BlxNode *root, *first_node;

	if (parent != NULL)
	{
		return FALSE;
	}

	root = model->priv->history;
	first_node = blx_node_get_nth_child (root, 0);

	if (first_node == NULL)
	{
		root = model->priv->bookmarks;
		first_node = blx_node_get_nth_child (root, 0);
	}

	if (first_node == NULL)
	{
		return FALSE;
	}

	node_iter_from_node (model, root, first_node, iter);

	return TRUE;
}

static gboolean
blx_completion_model_iter_has_child (GtkTreeModel *tree_model,
			             GtkTreeIter *iter)
{
	return FALSE;
}

static int
blx_completion_model_iter_n_children (GtkTreeModel *tree_model,
			               GtkTreeIter *iter)
{
	BlxCompletionModel *model = BLX_COMPLETION_MODEL (tree_model);

	g_return_val_if_fail (BLX_IS_COMPLETION_MODEL (tree_model), -1);

	if (iter == NULL)
	{
		return blx_node_get_n_children (model->priv->history) +
		       blx_node_get_n_children (model->priv->bookmarks);
	}

	g_return_val_if_fail (model->priv->stamp == iter->stamp, -1);

	return 0;
}

static gboolean
blx_completion_model_iter_nth_child (GtkTreeModel *tree_model,
			              GtkTreeIter *iter,
			              GtkTreeIter *parent,
			              int n)
{
	BlxCompletionModel *model = BLX_COMPLETION_MODEL (tree_model);
	BlxNode *node, *root;

	g_return_val_if_fail (BLX_IS_COMPLETION_MODEL (tree_model), FALSE);

	if (parent != NULL)
	{
		return FALSE;
	}

	root = get_index_root (model, &n);
	node = blx_node_get_nth_child (root, n);

	if (node == NULL) return FALSE;

	node_iter_from_node (model, root, node, iter);

	return TRUE;
}

static gboolean
blx_completion_model_iter_parent (GtkTreeModel *tree_model,
			           GtkTreeIter *iter,
			           GtkTreeIter *child)
{
	return FALSE;
}

static void
blx_completion_model_tree_model_init (GtkTreeModelIface *iface)
{
	iface->get_flags       = blx_completion_model_get_flags;
	iface->get_iter        = blx_completion_model_get_iter;
	iface->get_path        = blx_completion_model_get_path;
	iface->iter_next       = blx_completion_model_iter_next;
	iface->iter_children   = blx_completion_model_iter_children;
	iface->iter_has_child  = blx_completion_model_iter_has_child;
	iface->iter_n_children = blx_completion_model_iter_n_children;
	iface->iter_nth_child  = blx_completion_model_iter_nth_child;
	iface->iter_parent     = blx_completion_model_iter_parent;
	iface->get_n_columns   = blx_completion_model_get_n_columns;
	iface->get_column_type = blx_completion_model_get_column_type;
	iface->get_value       = blx_completion_model_get_value;
}
