/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set sw=2 ts=2 sts=2 et: */
/*
 *  Copyright © 2011 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#ifndef _BLX_COMBINED_STOP_RELOAD_ACTION_H
#define _BLX_COMBINED_STOP_RELOAD_ACTION_H

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_COMBINED_STOP_RELOAD_ACTION            (blx_combined_stop_reload_action_get_type())
#define BLX_COMBINED_STOP_RELOAD_ACTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), BLX_TYPE_COMBINED_STOP_RELOAD_ACTION, BlxCombinedStopReloadAction))
#define BLX_COMBINED_STOP_RELOAD_ACTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_COMBINED_STOP_RELOAD_ACTION, BlxCombinedStopReloadActionClass))
#define BLX_IS_COMBINED_STOP_RELOAD_ACTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_COMBINED_STOP_RELOAD_ACTION))
#define BLX_IS_COMBINED_STOP_RELOAD_ACTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), BLX_TYPE_COMBINED_STOP_RELOAD_ACTION))
#define BLX_COMBINED_STOP_RELOAD_ACTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), BLX_TYPE_COMBINED_STOP_RELOAD_ACTION, BlxCombinedStopReloadActionClass))

typedef struct _BlxCombinedStopReloadAction BlxCombinedStopReloadAction;
typedef struct _BlxCombinedStopReloadActionClass BlxCombinedStopReloadActionClass;
typedef struct _BlxCombinedStopReloadActionPrivate BlxCombinedStopReloadActionPrivate;

struct _BlxCombinedStopReloadAction
{
  GtkAction parent;

  /*< private >*/
  BlxCombinedStopReloadActionPrivate *priv;
};

struct _BlxCombinedStopReloadActionClass
{
  GtkActionClass parent_class;
};

GType blx_combined_stop_reload_action_get_type (void) G_GNUC_CONST;

void blx_combined_stop_reload_action_set_loading (BlxCombinedStopReloadAction *action,
				       gboolean loading);

G_END_DECLS

#endif /* _BLX_COMBINED_STOP_RELOAD_ACTION_H */
