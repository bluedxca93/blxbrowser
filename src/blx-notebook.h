/*
 *  Copyright © 2002 Christophe Fergeau
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_NOTEBOOK_H
#define BLX_NOTEBOOK_H

#include <glib.h>
#include <gtk/gtk.h>

#include "blx-embed.h"

G_BEGIN_DECLS

#define BLX_TYPE_NOTEBOOK		(blx_notebook_get_type ())
#define BLX_NOTEBOOK(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_NOTEBOOK, BlxNotebook))
#define BLX_NOTEBOOK_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_NOTEBOOK, BlxNotebookClass))
#define BLX_IS_NOTEBOOK(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_NOTEBOOK))
#define BLX_IS_NOTEBOOK_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_NOTEBOOK))
#define BLX_NOTEBOOK_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_NOTEBOOK, BlxNotebookClass))

typedef struct _BlxNotebookClass	BlxNotebookClass;
typedef struct _BlxNotebook		BlxNotebook;
typedef struct _BlxNotebookPrivate	BlxNotebookPrivate;

struct _BlxNotebook
{
	GtkNotebook parent;

	/*< private >*/
        BlxNotebookPrivate *priv;
};

struct _BlxNotebookClass
{
        GtkNotebookClass parent_class;

	/* Signals */
	void	 (* tab_close_req)  (BlxNotebook *notebook,
				     BlxEmbed *embed);
};

GType		blx_notebook_get_type		(void);

int		blx_notebook_add_tab		(BlxNotebook *nb,
						 BlxEmbed *embed,
						 int position,
						 gboolean jump_to);
	
void		blx_notebook_set_show_tabs	(BlxNotebook *nb,
						 gboolean show_tabs);

void		blx_notebook_set_dnd_enabled	(BlxNotebook *nb,
						 gboolean enabled);

GList *         blx_notebook_get_focused_pages (BlxNotebook *nb);

G_END_DECLS

#endif /* BLX_NOTEBOOK_H */
