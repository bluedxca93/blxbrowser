/*
 *  Copyright © 2000-2003 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_WINDOW_H
#define BLX_WINDOW_H

#include "blx-embed.h"

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_WINDOW	(blx_window_get_type ())
#define BLX_WINDOW(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_WINDOW, BlxWindow))
#define BLX_WINDOW_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_WINDOW, BlxWindowClass))
#define BLX_IS_WINDOW(o)	(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_WINDOW))
#define BLX_IS_WINDOW_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_WINDOW))
#define BLX_WINDOW_GET_CLASS(o)(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_WINDOW, BlxWindowClass))

typedef struct _BlxWindowClass		BlxWindowClass;
typedef struct _BlxWindow		BlxWindow;
typedef struct _BlxWindowPrivate	BlxWindowPrivate;

struct _BlxWindow
{
	GtkWindow parent;

	/*< private >*/
	BlxWindowPrivate *priv;
};

struct _BlxWindowClass
{
	GtkWindowClass parent_class;
};

GType		  blx_window_get_type		  (void);

BlxWindow	 *blx_window_new		  (void);

BlxWindow	 *blx_window_new_with_chrome	  (BlxWebViewChrome chrome,
						   gboolean is_popup);

GObject		 *blx_window_get_ui_manager	  (BlxWindow *window);

GtkWidget	 *blx_window_get_notebook	  (BlxWindow *window);

GtkWidget        *blx_window_get_find_toolbar    (BlxWindow *window);

void		  blx_window_load_url		  (BlxWindow *window,
						   const char *url);

void		  blx_window_set_zoom		  (BlxWindow *window,
						   float zoom);

void		  blx_window_activate_location	  (BlxWindow *window);
const char       *blx_window_get_location        (BlxWindow *window);
void              blx_window_set_location        (BlxWindow *window,
                                                   const char *address);

BlxEmbedEvent	 *blx_window_get_context_event	  (BlxWindow *window);

void		  blx_window_set_downloads_box_visibility (BlxWindow *window,
							    gboolean show);

G_END_DECLS

#endif
