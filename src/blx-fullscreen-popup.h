/*
 *  Copyright © 2000-2004 Marco Pesenti Gritti
 *  Copyright © 2003-2005 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_FULLSCREEN_POPUP_H
#define BLX_FULLSCREEN_POPUP_H

#include <gtk/gtk.h>
#include "blx-window.h"

G_BEGIN_DECLS

#define BLX_TYPE_FULLSCREEN_POPUP		(blx_fullscreen_popup_get_type ())
#define BLX_FULLSCREEN_POPUP(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_FULLSCREEN_POPUP, BlxFullscreenPopup))
#define BLX_FULLSCREEN_POPUP_CLASS(k)  	(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_FULLSCREEN_POPUP, BlxFullscreenPopupClass))
#define BLX_IS_FULLSCREEN_POPUP(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_FULLSCREEN_POPUP))
#define BLX_IS_FULLSCREEN_POPUP_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_FULLSCREEN_POPUP))
#define BLX_FULLSCREEN_POPUP_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_FULLSCREEN_POPUP, BlxFullscreenPopupClass))

typedef struct _BlxFullscreenPopup		BlxFullscreenPopup;
typedef struct _BlxFullscreenPopupPrivate	BlxFullscreenPopupPrivate;
typedef struct _BlxFullscreenPopupClass	BlxFullscreenPopupClass;

struct _BlxFullscreenPopup
{
	GtkWindow parent_instance;

	/*< private >*/
	BlxFullscreenPopupPrivate *priv;
};

struct _BlxFullscreenPopupClass
{
	GtkWindowClass parent_class;

	void (* exit_clicked)	(BlxFullscreenPopup *popup);
	void (* lock_clicked)	(BlxFullscreenPopup *popup);
};

GType	   blx_fullscreen_popup_get_type	    (void);

GtkWidget *blx_fullscreen_popup_new		    (BlxWindow *window);

void	   blx_fullscreen_popup_set_show_leave	    (BlxFullscreenPopup *popup,
						     gboolean show_button);

void	   blx_fullscreen_popup_set_security_state (BlxFullscreenPopup *popup,
						     gboolean show_lock,
						     const char *stock,
						     const char *tooltip);

G_END_DECLS

#endif /* !BLX_FULLSCREEN_POPUP_H */
