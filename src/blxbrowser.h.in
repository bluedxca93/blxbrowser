/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2; -*- */
/*  Copyright © 2008 Xan Lopez <xan@gnome.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#ifndef __BLXBROWSER_H__
#define __BLXBROWSER_H__

#define __BLX_BLXBROWSER_H_INSIDE__

/* From GTK_CHECK_VERSION */
#define BLX_MAJOR_VERSION (@BLXBROWSER_MAJOR_VERSION@)
#define BLX_MINOR_VERSION (@BLXBROWSER_MINOR_VERSION@)
#define BLX_MICRO_VERSION (@BLXBROWSER_MICRO_VERSION@)

#define BLX_CHECK_VERSION(major, minor, micro)\
  (BLX_MAJOR_VERSION > (major) || \
   (BLX_MAJOR_VERSION == (major) && BLX_MINOR_VERSION > (minor)) ||  \
    (BLX_MAJOR_VERSION == (major) && BLX_MINOR_VERSION == (minor) && \
     BLX_MICRO_VERSION >= (micro)))

#include <blxbrowser/blx-adblock.h>
#include <blxbrowser/blx-adblock-manager.h>
#include <blxbrowser/blx-bookmarks.h>
#include <blxbrowser/blx-bookmarks-type-builtins.h>
#include <blxbrowser/blx-dialog.h>
#include <blxbrowser/blx-download.h>
#include <blxbrowser/blx-embed-container.h>
#include <blxbrowser/blx-embed-event.h>
#include <blxbrowser/blx-embed.h>
#include <blxbrowser/blx-embed-prefs.h>
#include <blxbrowser/blx-embed-shell.h>
#include <blxbrowser/blx-embed-single.h>
#include <blxbrowser/blx-embed-type-builtins.h>
#include <blxbrowser/blx-embed-utils.h>
#include <blxbrowser/blx-extension.h>
#include <blxbrowser/blx-extensions-manager.h>
#include <blxbrowser/blx-history.h>
#include <blxbrowser/blx-lib-type-builtins.h>
#include <blxbrowser/blx-link.h>
#include <blxbrowser/blx-loader.h>
#include <blxbrowser/blx-node-db.h>
#include <blxbrowser/blx-node.h>
#include <blxbrowser/blx-notebook.h>
#include <blxbrowser/blx-permission-manager.h>
#include <blxbrowser/blx-session.h>
#include <blxbrowser/blx-shell.h>
#include <blxbrowser/blx-state.h>
#include <blxbrowser/blx-type-builtins.h>
#include <blxbrowser/blx-window.h>

#undef __BLX_BLXBROWSER_H_INSIDE__

#endif /* !__BLXBROWSER_H__ */
