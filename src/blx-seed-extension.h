/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2; -*- */
/*
 *  Copyright © 2009, Robert Carr <carrr@rpi.edu>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_SEED_EXTENSION_H
#define BLX_SEED_EXTENSION_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define BLX_TYPE_SEED_EXTENSION		(blx_seed_extension_get_type ())
#define BLX_SEED_EXTENSION(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_SEED_EXTENSION, BlxSeedExtension))
#define BLX_SEED_EXTENSION_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_SEED_EXTENSION, BlxSeedExtensionClass))
#define BLX_IS_SEED_EXTENSION(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_SEED_EXTENSION))
#define BLX_IS_SEED_EXTENSION_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_SEED_EXTENSION))
#define BLX_SEED_EXTENSION_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_SEED_EXTENSION, BlxSeedExtensionClass))

typedef struct _BlxSeedExtension		BlxSeedExtension;
typedef struct _BlxSeedExtensionClass	BlxSeedExtensionClass;
typedef struct _BlxSeedExtensionPrivate	BlxSeedExtensionPrivate;

struct _BlxSeedExtensionClass
{
  GObjectClass parent_class;
};

struct _BlxSeedExtension
{
  GObject parent_instance;

  /*< private >*/
  BlxSeedExtensionPrivate *priv;
};

GType	blx_seed_extension_get_type		(void);

G_END_DECLS

#endif
