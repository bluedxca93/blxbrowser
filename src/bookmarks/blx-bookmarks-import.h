/*
 *  Copyright © 2003, 2004 Marco Pesenti Gritti
 *  Copyright © 2003, 2004, 2005 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_BOOKMARKS_IMPORT_H
#define BLX_BOOKMARKS_IMPORT_H

#include "blx-bookmarks.h"

G_BEGIN_DECLS

#define MOZILLA_BOOKMARKS_DIR	".mozilla"
#define FIREFOX_BOOKMARKS_DIR_0	".phoenix"
#define FIREFOX_BOOKMARKS_DIR_1	".firefox"
#define FIREFOX_BOOKMARKS_DIR_2	".mozilla/firefox"
#define GALEON_BOOKMARKS_DIR	".galeon"
#define KDE_BOOKMARKS_DIR	".kde/share/apps/konqueror"

gboolean blx_bookmarks_import         (BlxBookmarks *bookmarks,
					const char *filename);

gboolean blx_bookmarks_import_mozilla (BlxBookmarks *bookmarks,
					const char *filename);

gboolean blx_bookmarks_import_xbel    (BlxBookmarks *bookmarks,
					const char *filename);

gboolean blx_bookmarks_import_rdf     (BlxBookmarks *bookmarks,
					const char *filename);

G_END_DECLS

#endif
