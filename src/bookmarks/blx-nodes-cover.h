/*
 *  Copyright © 2004 Peter Harvey <pah06@uow.edu.au>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_NODES_COVER_H
#define BLX_NODES_COVER_H

#include "blx-bookmarks.h"

G_BEGIN_DECLS

gint blx_nodes_remove_covered (BlxNode *parent, GPtrArray *children);

gint blx_nodes_remove_not_covered (BlxNode *parent, GPtrArray *children);

gint blx_nodes_count_covered (BlxNode *parent, const GPtrArray *children);
    
gboolean blx_nodes_covered (BlxNode *parent, const GPtrArray *children);

GPtrArray * blx_nodes_get_covered (BlxNode *parent, const GPtrArray *children, GPtrArray *_covered);

GPtrArray * blx_nodes_get_covering (const GPtrArray *parents, const GPtrArray *children,
				     GPtrArray *_covering, GPtrArray *_uncovered, GArray *_sizes);

G_END_DECLS

#endif /* BLX_NODES_COVER_H */
