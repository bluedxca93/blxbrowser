/*
 *  Copyright © 2005 Peter Harvey
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_BOOKMARKS_UI_H
#define BLX_BOOKMARKS_UI_H

#include "blx-window.h"
#include "blx-node.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_BOOKMARKS_UI_ACTION_NAME_BUFFER_SIZE	32

#define BLX_BOOKMARK_ACTION_NAME_BUFFER_SIZE		BLX_BOOKMARKS_UI_ACTION_NAME_BUFFER_SIZE
#define BLX_BOOKMARK_ACTION_NAME_FORMAT		"Bmk%u"
#define BLX_BOOKMARK_ACTION_NAME_FORMAT_ARG(node)	(blx_node_get_id (node))
#define BLX_BOOKMARK_ACTION_NAME_PRINTF(buffer,node)	(g_snprintf (buffer, sizeof (buffer), BLX_BOOKMARK_ACTION_NAME_FORMAT, BLX_BOOKMARK_ACTION_NAME_FORMAT_ARG (node)))
#define BLX_BOOKMARK_ACTION_NAME_STRDUP_PRINTF(node)	(g_strdup_printf (BLX_BOOKMARK_ACTION_NAME_FORMAT, BLX_BOOKMARK_ACTION_NAME_FORMAT_ARG (node)))

#define BLX_TOPIC_ACTION_NAME_BUFFER_SIZE		BLX_BOOKMARKS_UI_ACTION_NAME_BUFFER_SIZE
#define BLX_TOPIC_ACTION_NAME_FORMAT			"Tp%u"
#define BLX_TOPIC_ACTION_NAME_FORMAT_ARG(node)		(blx_node_get_id (node))
#define BLX_TOPIC_ACTION_NAME_PRINTF(buffer,node)	(g_snprintf (buffer, sizeof (buffer), BLX_TOPIC_ACTION_NAME_FORMAT, BLX_TOPIC_ACTION_NAME_FORMAT_ARG (node)))
#define BLX_TOPIC_ACTION_NAME_STRDUP_PRINTF(node)	(g_strdup_printf (BLX_TOPIC_ACTION_NAME_FORMAT, BLX_TOPIC_ACTION_NAME_FORMAT_ARG (node)))

#define BLX_OPEN_TABS_ACTION_NAME_BUFFER_SIZE		BLX_BOOKMARKS_UI_ACTION_NAME_BUFFER_SIZE
#define BLX_OPEN_TABS_ACTION_NAME_FORMAT		"OpTb%u"
#define BLX_OPEN_TABS_ACTION_NAME_FORMAT_ARG(node)	(blx_node_get_id (node))
#define BLX_OPEN_TABS_ACTION_NAME_PRINTF(buffer,node)	(g_snprintf (buffer, sizeof (buffer), BLX_OPEN_TABS_ACTION_NAME_FORMAT, BLX_OPEN_TABS_ACTION_NAME_FORMAT_ARG (node)))
#define BLX_OPEN_TABS_ACTION_NAME_STRDUP_PRINTF(node)	(g_strdup_printf (BLX_OPEN_TABS_ACTION_NAME_FORMAT, BLX_OPEN_TABS_ACTION_NAME_FORMAT_ARG (node)))

void	blx_bookmarks_ui_attach_window		(BlxWindow *window);

void	blx_bookmarks_ui_detach_window		(BlxWindow *window);

void	blx_bookmarks_ui_add_bookmark		(GtkWindow *parent,
						 const char *location,
						 const char *title);

void	blx_bookmarks_ui_show_bookmark		(BlxNode *bookmark);

G_END_DECLS

#endif
