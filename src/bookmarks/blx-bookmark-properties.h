/*
 *  Copyright © 2002 Marco Pesenti Gritti <mpeseng@tin.it>
 *  Copyright © 2005, 2006 Peter A. Harvey
 *  Copyright © 2006 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_BOOKMARK_PROPERTIES_H
#define BLX_BOOKMARK_PROPERTIES_H

#include "blx-bookmarks.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_BOOKMARK_PROPERTIES		(blx_bookmark_properties_get_type ())
#define BLX_BOOKMARK_PROPERTIES(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_BOOKMARK_PROPERTIES, BlxBookmarkProperties))
#define BLX_BOOKMARK_PROPERTIES_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_BOOKMARK_PROPERTIES, BlxBookmarkPropertiesClass))
#define BLX_IS_BOOKMARK_PROPERTIES(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_BOOKMARK_PROPERTIES))
#define BLX_IS_BOOKMARK_PROPERTIES_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_BOOKMARK_PROPERTIES))
#define BLX_BOOKMARK_PROPERTIES_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_BOOKMARK_PROPERTIES, BlxBookmarkPropertiesClass))

typedef struct _BlxBookmarkProperties		BlxBookmarkProperties;
typedef struct _BlxBookmarkPropertiesPrivate	BlxBookmarkPropertiesPrivate;
typedef struct _BlxBookmarkPropertiesClass	BlxBookmarkPropertiesClass;

struct _BlxBookmarkProperties
{
	GtkDialog parent_instance;

	/*< private >*/
	BlxBookmarkPropertiesPrivate *priv;
};

struct _BlxBookmarkPropertiesClass
{
	GtkDialogClass parent_class;
};

GType		 blx_bookmark_properties_get_type	(void);

GtkWidget	*blx_bookmark_properties_new		(BlxBookmarks *bookmarks,
							 BlxNode *bookmark,
							 gboolean creating);

BlxNode	*blx_bookmark_properties_get_node	(BlxBookmarkProperties *properties);

G_END_DECLS

#endif /* BLX_BOOKMARK_PROPERTIES_H */
