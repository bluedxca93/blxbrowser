/*
 *  Copyright © 2003, 2004 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_TOPIC_ACTION_H
#define BLX_TOPIC_ACTION_H

#include "blx-link-action.h"
#include "blx-node.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_TOPIC_ACTION			(blx_topic_action_get_type ())
#define BLX_TOPIC_ACTION(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), BLX_TYPE_TOPIC_ACTION, BlxTopicAction))
#define BLX_TOPIC_ACTION_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_TOPIC_ACTION, BlxTopicActionClass))
#define BLX_IS_TOPIC_ACTION(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_TOPIC_ACTION))
#define BLX_IS_TOPIC_ACTION_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), BLX_TYPE_TOPIC_ACTION))
#define BLX_TOPIC_ACTION_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), BLX_TYPE_TOPIC_ACTION, BlxTopicActionClass))

typedef struct _BlxTopicAction			BlxTopicAction;
typedef struct _BlxTopicActionPrivate		BlxTopicActionPrivate;
typedef struct _BlxTopicActionClass		BlxTopicActionClass;

struct _BlxTopicAction
{
	GtkAction parent_instance;

	/*< private >*/
	BlxTopicActionPrivate *priv;
};

struct _BlxTopicActionClass
{
	BlxLinkActionClass parent_class;
};


GType		blx_topic_action_get_type	(void);

GtkAction      *blx_topic_action_new		(BlxNode *node,
						 GtkUIManager *manager,
						 const char *name);

void		blx_topic_action_set_topic	(BlxTopicAction *action,
						 BlxNode *node);

BlxNode       *blx_topic_action_get_topic	(BlxTopicAction *action);

void		blx_topic_action_updated	(BlxTopicAction *action);

G_END_DECLS

#endif
