/*
 *  Copyright © 2003, 2004 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *  Copyright © 2005 Peter Harvey
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"
#include "blx-bookmark-action.h"

#include "blx-bookmarks-ui.h"
#include "blx-bookmarks.h"
#include "blx-debug.h"
#include "blx-dnd.h"
#include "blx-favicon-cache.h"
#include "blx-gui.h"
#include "blx-shell.h"
#include "blx-stock-icons.h"
#include "blx-string.h"

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <string.h>

/* FIXME tweak this, or make it configurable? (bug 148093) */
#define LABEL_WIDTH_CHARS       32

#define BLX_BOOKMARK_ACTION_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), BLX_TYPE_BOOKMARK_ACTION, BlxBookmarkActionPrivate))

struct _BlxBookmarkActionPrivate
{
	BlxNode *node;
	gboolean smart_url;
	guint cache_handler;
};

enum
{
	PROP_0,
	PROP_BOOKMARK,
	PROP_TOOLTIP,
	PROP_LOCATION,
	PROP_SMART_URL,
	PROP_ICON
};

typedef struct
{
	GObject *weak_ptr;
	GtkWidget *entry;
	BlxLinkFlags flags;
} ClipboardCtx;

G_DEFINE_TYPE (BlxBookmarkAction, blx_bookmark_action, BLX_TYPE_LINK_ACTION)

static void
favicon_cache_changed_cb (BlxFaviconCache *cache,
			  const char *icon_address,
			  BlxBookmarkAction *action)
{
	const char *icon;

	g_return_if_fail (action->priv->node != NULL);

	icon = blx_node_get_property_string (action->priv->node,
					      BLX_NODE_BMK_PROP_ICON);
	
	if (icon != NULL && strcmp (icon, icon_address) == 0)
	{
		g_signal_handler_disconnect (cache, action->priv->cache_handler);
		action->priv->cache_handler = 0;

		g_object_notify (G_OBJECT (action), "icon");
	}
}

static void
blx_bookmark_action_sync_icon (GtkAction *action,
				GParamSpec *pspec,
				GtkWidget *proxy)
{
	BlxBookmarkAction *bma = BLX_BOOKMARK_ACTION (action);
	const char *icon_location;
	BlxFaviconCache *cache;
	GdkPixbuf *pixbuf = NULL;

	g_return_if_fail (bma->priv->node != NULL);

	icon_location = blx_node_get_property_string (bma->priv->node,
						       BLX_NODE_BMK_PROP_ICON);

	cache = BLX_FAVICON_CACHE (blx_embed_shell_get_favicon_cache
		(blx_embed_shell_get_default ()));

	if (icon_location && *icon_location)
	{
		pixbuf = blx_favicon_cache_get (cache, icon_location);

		if (pixbuf == NULL && bma->priv->cache_handler == 0)
		{
			bma->priv->cache_handler =
				g_signal_connect_object
					(cache, "changed",
					 G_CALLBACK (favicon_cache_changed_cb),
					 action, 0);
		}
	}

	if (GTK_IS_MENU_ITEM (proxy) && pixbuf)
	{
		GtkWidget *image;

		image = gtk_image_new_from_pixbuf (pixbuf);
		gtk_widget_show (image);

		gtk_image_menu_item_set_image
			(GTK_IMAGE_MENU_ITEM (proxy), image);
		gtk_image_menu_item_set_always_show_image (GTK_IMAGE_MENU_ITEM (proxy),
							   TRUE);
	}

	if (pixbuf)
	{
		g_object_unref (pixbuf);
	}
}

void
blx_bookmark_action_activate (BlxBookmarkAction *action,
			       GtkWidget *widget,
			       BlxLinkFlags flags)
{
	BlxBookmarkActionPrivate *priv = action->priv;
	BlxBookmarks *bookmarks;
	const char *location;
	char *address = NULL, *text = NULL;

	g_return_if_fail (priv->node != NULL);

	location = blx_node_get_property_string
			(priv->node, BLX_NODE_BMK_PROP_LOCATION);
	g_return_if_fail (location != NULL);
	
	bookmarks = blx_shell_get_bookmarks (blx_shell_get_default ());
	
	if (GTK_IS_EDITABLE (widget))
	{
		text = gtk_editable_get_chars (GTK_EDITABLE (widget), 0, -1);
	}
	
	/* The entered search term is empty, and we have a smart bookmark */
	if ((text == NULL || text[0] == '\0') && strstr (location, "%s") != NULL)
	{
		char *scheme;
		char *host_name;

		scheme = g_uri_parse_scheme (location);
		host_name = blx_string_get_host_name (location);
		address = g_strconcat (scheme,
				       "://",
				       host_name,
				       NULL);
		g_free (scheme);
		g_free (host_name);
	}

	if (address == NULL)
	{
		address = blx_bookmarks_resolve_address (bookmarks, location, text);
	}
	g_return_if_fail (address != NULL);

	blx_link_open (BLX_LINK (action), address, NULL, flags);

	g_free (address);
	g_free (text);
}

static void
activate_cb (GtkWidget *widget,
	     BlxBookmarkAction *action)
{
       gboolean control = FALSE;
       GdkEvent *event;

       event = gtk_get_current_event ();
       if (event)
       {
	       if (event->type == GDK_KEY_PRESS ||
		   event->type == GDK_KEY_RELEASE)
	       {
		       control = (event->key.state & gtk_accelerator_get_default_mod_mask ()) == GDK_CONTROL_MASK;
	       }

	       gdk_event_free (event);
       }

       blx_bookmark_action_activate
	 (action, widget, (control || blx_gui_is_middle_click ()) ? BLX_LINK_NEW_TAB : 0);
}

static void
connect_proxy (GtkAction *action,
	       GtkWidget *proxy)
{
	LOG ("Connecting action %p to proxy %p", action, proxy);

	GTK_ACTION_CLASS (blx_bookmark_action_parent_class)->connect_proxy (action, proxy);

	blx_bookmark_action_sync_icon (action, NULL, proxy);
	g_signal_connect_object (action, "notify::icon",
				 G_CALLBACK (blx_bookmark_action_sync_icon), proxy, 0);

	if (GTK_IS_MENU_ITEM (proxy))
	{
		GtkLabel *label;

		label = GTK_LABEL (gtk_bin_get_child (GTK_BIN (proxy)));

		gtk_label_set_use_underline (label, FALSE);
		gtk_label_set_ellipsize (label, PANGO_ELLIPSIZE_END);
		gtk_label_set_max_width_chars (label, LABEL_WIDTH_CHARS);

		g_signal_connect (proxy, "activate", G_CALLBACK (activate_cb), action);
	}
}

void
blx_bookmark_action_updated (BlxBookmarkAction *action)
{
	GValue value = { 0, };
	BlxBookmarks *bookmarks = blx_shell_get_bookmarks (blx_shell);
	BlxNode *smart = blx_bookmarks_get_smart_bookmarks (bookmarks);
	BlxNode *node = action->priv->node;
	const char *title;
	
	g_return_if_fail (action != NULL);
	g_return_if_fail (node != NULL);
	
	g_object_freeze_notify (G_OBJECT (action));

	/* Set smart_url */
	action->priv->smart_url = blx_node_has_child (smart, node);
	g_object_notify (G_OBJECT (action), "smarturl");
	
	/* Set title */
	title = blx_node_get_property_string (node, BLX_NODE_BMK_PROP_TITLE);
	g_value_init (&value, G_TYPE_STRING);
	g_value_set_static_string (&value, title);
	g_object_set_property (G_OBJECT (action), "label", &value);
	g_value_unset (&value);
	
	/* Notify all other properties */
	g_object_notify (G_OBJECT (action), "location");
	g_object_notify (G_OBJECT (action), "icon");
	
	g_object_thaw_notify (G_OBJECT (action));

	/* We could force a tooltip re-query with gtk_tooltip_trigger_tooltip_query
	 * here, but it's not really worth it. Just show the updated tip next time
	 * the tip is queried.
	 */
}

BlxNode *
blx_bookmark_action_get_bookmark (BlxBookmarkAction *action)
{
	return action->priv->node;
}

void
blx_bookmark_action_set_bookmark (BlxBookmarkAction *action,
				   BlxNode *node)
{
	BlxBookmarkActionPrivate *priv = action->priv;
	GObject *object = G_OBJECT (action);

	g_return_if_fail (node != NULL);

	priv->node = node;

	g_object_freeze_notify (object);

	g_object_notify (object, "bookmark");
	blx_bookmark_action_updated (action);

	g_object_thaw_notify (object);
}

static void
blx_bookmark_action_set_property (GObject *object,
				   guint prop_id,
				   const GValue *value,
				   GParamSpec *pspec)
{
	BlxBookmarkAction *action = BLX_BOOKMARK_ACTION (object);

	switch (prop_id)
	{
		case PROP_BOOKMARK:
			blx_bookmark_action_set_bookmark (action, g_value_get_pointer (value));
			break;
		case PROP_TOOLTIP:
		case PROP_LOCATION:
		case PROP_SMART_URL:
		case PROP_ICON:
			/* not writable */
			break;
	}
}

static void
blx_bookmark_action_get_property (GObject *object,
				   guint prop_id,
				   GValue *value,
				   GParamSpec *pspec)
{
	BlxBookmarkAction *action = BLX_BOOKMARK_ACTION (object);
	BlxBookmarkActionPrivate *priv = action->priv;

	g_return_if_fail (priv->node != NULL);

	switch (prop_id)
	{
		case PROP_BOOKMARK:
			g_value_set_pointer (value, priv->node);
			break;
		case PROP_TOOLTIP:
		case PROP_LOCATION:
			g_value_set_string (value,
				blx_node_get_property_string (priv->node,
					BLX_NODE_BMK_PROP_LOCATION));
			break;
		case PROP_SMART_URL:
			g_value_set_boolean (value, priv->smart_url);
			break;
		case PROP_ICON:
			g_value_set_string (value,
				blx_node_get_property_string (priv->node,
					BLX_NODE_BMK_PROP_ICON));
			break;
	}
}

static void
blx_bookmark_action_init (BlxBookmarkAction *action)
{
	action->priv = BLX_BOOKMARK_ACTION_GET_PRIVATE (action);
	
	action->priv->cache_handler = 0;
}

static void
blx_bookmark_action_dispose (GObject *object)
{
	BlxBookmarkAction *action = (BlxBookmarkAction *) object;
	BlxBookmarkActionPrivate *priv = action->priv;
	GObject *cache;

	if (priv->cache_handler != 0)
	{
		cache = blx_embed_shell_get_favicon_cache
				(blx_embed_shell_get_default ());

		g_signal_handler_disconnect (cache, priv->cache_handler);
		priv->cache_handler = 0;
	}

	G_OBJECT_CLASS (blx_bookmark_action_parent_class)->dispose (object);
}

static void
blx_bookmark_action_class_init (BlxBookmarkActionClass *class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkActionClass *action_class = GTK_ACTION_CLASS (class);

	action_class->menu_item_type = GTK_TYPE_IMAGE_MENU_ITEM;
	action_class->connect_proxy = connect_proxy;

	object_class->dispose = blx_bookmark_action_dispose;
	object_class->set_property = blx_bookmark_action_set_property;
	object_class->get_property = blx_bookmark_action_get_property;

	g_object_class_install_property (object_class,
					 PROP_BOOKMARK,
					 g_param_spec_pointer ("bookmark", NULL, NULL,
							       G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB |
							       G_PARAM_CONSTRUCT_ONLY));

	/* overwrite GtkActionClass::tooltip, so we can use the url as tooltip */
	g_object_class_install_property (object_class,
					 PROP_TOOLTIP,
					 g_param_spec_string  ("tooltip", NULL, NULL,
							       NULL,
							       G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	
	g_object_class_install_property (object_class,
					 PROP_LOCATION,
					 g_param_spec_string  ("location", NULL, NULL,
							       NULL,
							       G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	g_object_class_install_property (object_class,
					 PROP_SMART_URL,
					 g_param_spec_boolean  ("smarturl", NULL, NULL,
								FALSE,
								G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	g_object_class_install_property (object_class,
					 PROP_ICON,
					 g_param_spec_string  ("icon", NULL, NULL,
							       NULL,
							       G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

	g_type_class_add_private (object_class, sizeof(BlxBookmarkActionPrivate));
}

GtkAction *
blx_bookmark_action_new (BlxNode *node,
			  const char *name)
{
	g_assert (name != NULL);

	return  GTK_ACTION (g_object_new (BLX_TYPE_BOOKMARK_ACTION,
					  "name", name,
					  "bookmark", node,
					  NULL));
}
