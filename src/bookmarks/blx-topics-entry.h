/*
 *  Copyright © 2002 Marco Pesenti Gritti <mpeseng@tin.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_TOPICS_ENTRY_H
#define BLX_TOPICS_ENTRY_H

#include "blx-bookmarks.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_TOPICS_ENTRY	 	(blx_topics_entry_get_type ())
#define BLX_TOPICS_ENTRY(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_TOPICS_ENTRY, BlxTopicsEntry))
#define BLX_TOPICS_ENTRY_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_TOPICS_ENTRY, BlxTopicsEntryClass))
#define BLX_IS_TOPICS_ENTRY(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_TOPICS_ENTRY))
#define BLX_IS_TOPICS_ENTRY_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_TOPICS_ENTRY))
#define BLX_TOPICS_ENTRY_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_TOPICS_ENTRY, BlxTopicsEntryClass))

typedef struct _BlxTopicsEntryPrivate BlxTopicsEntryPrivate;

typedef struct
{
	GtkEntry parent;

	/*< private >*/
	BlxTopicsEntryPrivate *priv;
} BlxTopicsEntry;

typedef struct
{
	GtkEntryClass parent;
} BlxTopicsEntryClass;

GType		     blx_topics_entry_get_type        (void);

GtkWidget	    *blx_topics_entry_new             (BlxBookmarks *bookmarks,
							BlxNode *bookmark);

void                 blx_topics_entry_insert_topic    (BlxTopicsEntry *entry, 
							const char *title);

G_END_DECLS

#endif /* BLX_TOPICS_ENTRY_H */
