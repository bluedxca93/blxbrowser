/*
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_BOOKMARKS_EDITOR_H
#define BLX_BOOKMARKS_EDITOR_H

#include <gtk/gtk.h>

#include "blx-node-view.h"
#include "blx-bookmarks.h"

G_BEGIN_DECLS

#define BLX_TYPE_BOOKMARKS_EDITOR		(blx_bookmarks_editor_get_type ())
#define BLX_BOOKMARKS_EDITOR(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_BOOKMARKS_EDITOR, BlxBookmarksEditor))
#define BLX_BOOKMARKS_EDITOR_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_BOOKMARKS_EDITOR, BlxBookmarksEditorClass))
#define BLX_IS_BOOKMARKS_EDITOR(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_BOOKMARKS_EDITOR))
#define BLX_IS_BOOKMARKS_EDITOR_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_BOOKMARKS_EDITOR))
#define BLX_BOOKMARKS_EDITOR_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_BOOKMARKS_EDITOR, BlxBookmarksEditorClass))

typedef struct _BlxBookmarksEditorPrivate BlxBookmarksEditorPrivate;

typedef struct
{
	GtkWindow parent;

	/*< private >*/
	BlxBookmarksEditorPrivate *priv;
} BlxBookmarksEditor;

typedef struct
{
	GtkDialogClass parent;
} BlxBookmarksEditorClass;

GType		     blx_bookmarks_editor_get_type (void);

GtkWidget	    *blx_bookmarks_editor_new        (BlxBookmarks *bookmarks);

void		     blx_bookmarks_editor_set_parent (BlxBookmarksEditor *ebe,
						       GtkWidget *window);

G_END_DECLS

#endif /* BLX_BOOKMARKS_EDITOR_H */
