/*
 *  Copyright © 2005 Peter Harvey
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include "blx-shell.h"
#include "blx-bookmark-action-group.h"
#include "blx-bookmark-action.h"
#include "blx-bookmarks.h"
#include "blx-bookmarks-ui.h"
#include "blx-link.h"
#include "blx-node.h"
#include "blx-node-common.h"
#include "blx-debug.h"

#include <gtk/gtk.h>
#include <string.h>

static void
smart_added_cb (BlxNode *parent, 
		BlxNode *child,
		GtkActionGroup *action_group)
{
	GtkAction *action;
	char name[BLX_BOOKMARK_ACTION_NAME_BUFFER_SIZE];

	BLX_BOOKMARK_ACTION_NAME_PRINTF (name, child);

	action = gtk_action_group_get_action (action_group, name);
	
	if (action != NULL)
	{
		blx_bookmark_action_updated ((BlxBookmarkAction *) action);
	}
}

static void
smart_removed_cb (BlxNode *parent,
		  BlxNode *child,
		  guint index, 
		  GtkActionGroup *action_group)
{
	GtkAction *action;
	char name[BLX_BOOKMARK_ACTION_NAME_BUFFER_SIZE];

	BLX_BOOKMARK_ACTION_NAME_PRINTF (name, child);

	action = gtk_action_group_get_action (action_group, name);
	
	if (action != NULL)
	{
		blx_bookmark_action_updated ((BlxBookmarkAction *) action);
	}
}

static void
node_changed_cb (BlxNode *parent,
		 BlxNode *child,
		 guint property_id,
		 GtkActionGroup *action_group)
{
	GtkAction *action;
	char name[BLX_BOOKMARK_ACTION_NAME_BUFFER_SIZE];

	BLX_BOOKMARK_ACTION_NAME_PRINTF (name, child);

	action = gtk_action_group_get_action (action_group, name);
	
	if (action != NULL)
	{
		blx_bookmark_action_updated ((BlxBookmarkAction *) action);
	}
}

static void
node_added_cb (BlxNode *parent,
	       BlxNode *child,
	       GtkActionGroup *action_group)
{
	GtkAction *action;
	char name[BLX_BOOKMARK_ACTION_NAME_BUFFER_SIZE];
	char accel[256];

	BLX_BOOKMARK_ACTION_NAME_PRINTF (name, child);

	action = blx_bookmark_action_new (child, name);

	g_signal_connect_swapped (action, "open-link",
				  G_CALLBACK (blx_link_open), action_group);

	g_snprintf (accel, sizeof (accel), "<Actions>/%s/%s",
		    gtk_action_group_get_name (action_group),
		    name);
	gtk_action_set_accel_path (action, accel);
	gtk_action_group_add_action (action_group, action);
	g_object_unref (action);
}

static void
node_removed_cb (BlxNode *parent,
		 BlxNode *child,
		 guint index,
		 GtkActionGroup *action_group)
{
	GtkAction *action;
	char name[BLX_BOOKMARK_ACTION_NAME_BUFFER_SIZE];

	BLX_BOOKMARK_ACTION_NAME_PRINTF (name, child);

	action = gtk_action_group_get_action (action_group, name);
	
	if (action != NULL)
	{
		gtk_action_group_remove_action (action_group, action);
	}
}

GtkActionGroup *
blx_bookmark_group_new (BlxNode *node)
{
	BlxBookmarks *bookmarks;
	BlxNode *smart;
	GPtrArray *children;
	GtkActionGroup *action_group;
	guint i;
	
	bookmarks = blx_shell_get_bookmarks (blx_shell);
	smart = blx_bookmarks_get_smart_bookmarks (bookmarks);

	action_group = (GtkActionGroup *) blx_link_action_group_new ("BA");

	children = blx_node_get_children (node);
	for (i = 0; i < children->len; i++)
	{
		node_added_cb (node, g_ptr_array_index (children, i),
			       action_group);
	}
	
	blx_node_signal_connect_object (node, BLX_NODE_CHILD_ADDED,
					 (BlxNodeCallback) node_added_cb,
					 (GObject *) action_group);
	blx_node_signal_connect_object (node, BLX_NODE_CHILD_REMOVED,
					 (BlxNodeCallback) node_removed_cb,
					 (GObject *) action_group);
	blx_node_signal_connect_object (node, BLX_NODE_CHILD_CHANGED,
					 (BlxNodeCallback) node_changed_cb,
					 (GObject *) action_group);
	
	blx_node_signal_connect_object (smart, BLX_NODE_CHILD_ADDED,
					 (BlxNodeCallback) smart_added_cb,
					 (GObject *) action_group);
	blx_node_signal_connect_object (smart, BLX_NODE_CHILD_REMOVED,
					 (BlxNodeCallback) smart_removed_cb,
					 (GObject *) action_group);
	
	return action_group;
}
