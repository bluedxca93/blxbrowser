/*
 *  Copyright © 2000-2004 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_BOOKMARKS_H
#define BLX_BOOKMARKS_H

#include <glib-object.h>
#include <gtk/gtk.h>

#include "blx-node.h"

G_BEGIN_DECLS

#define BLX_TYPE_BOOKMARKS		(blx_bookmarks_get_type ())
#define BLX_BOOKMARKS(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_BOOKMARKS, BlxBookmarks))
#define BLX_BOOKMARKS_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_BOOKMARKS, BlxBookmarksClass))
#define BLX_IS_BOOKMARKS(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_BOOKMARKS))
#define BLX_IS_BOOKMARKS_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_BOOKMARKS))
#define BLX_BOOKMARKS_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_BOOKMARKS, BlxBookmarksClass))

typedef struct _BlxBookmarksClass	BlxBookmarksClass;
typedef struct _BlxBookmarks		BlxBookmarks;
typedef struct _BlxBookmarksPrivate	BlxBookmarksPrivate;

typedef enum
{
	BLX_NODE_BMK_PROP_TITLE	= 2,
	BLX_NODE_BMK_PROP_LOCATION	= 3,
	BLX_NODE_BMK_PROP_KEYWORDS	= 4,
	BLX_NODE_KEYWORD_PROP_NAME	= 5,
	BLX_NODE_BMK_PROP_USERICON	= 6,
	BLX_NODE_BMK_PROP_ICON		= 7,
	BLX_NODE_KEYWORD_PROP_PRIORITY	= 8,
	BLX_NODE_BMK_PROP_SERVICE_ID	= 14,
	BLX_NODE_BMK_PROP_IMMUTABLE	= 15
} BlxBookmarkProperty;

struct _BlxBookmarks
{
	GObject parent;

	/*< private >*/
	BlxBookmarksPrivate *priv;
};

struct _BlxBookmarksClass
{
	GObjectClass parent_class;

	void	(* tree_changed)	(BlxBookmarks *eb);
	char *	(* resolve_address)	(BlxBookmarks *eb,
					 const char *address,
					 const char *argument);
};

GType		  blx_bookmarks_get_type		(void);

BlxBookmarks    *blx_bookmarks_new			(void);

BlxNode	 *blx_bookmarks_get_from_id		(BlxBookmarks *eb,
							 long id);

/* Bookmarks */

BlxNode	 *blx_bookmarks_add			(BlxBookmarks *eb,
							 const char *title,
							 const char *url);

BlxNode*	  blx_bookmarks_find_bookmark		(BlxBookmarks *eb,
							 const char *url);

gint              blx_bookmarks_get_similar		(BlxBookmarks *eb,
							 BlxNode *bookmark,
							 GPtrArray *identical,
							 GPtrArray *similar);
    
void		  blx_bookmarks_set_icon		(BlxBookmarks *eb,
							 const char *url,
							 const char *icon);

void		  blx_bookmarks_set_usericon		(BlxBookmarks *eb,
							 const char *url,
							 const char *icon);

void		  blx_bookmarks_set_address    	(BlxBookmarks *eb,
							 BlxNode *bookmark,
							 const char *address);

char		 *blx_bookmarks_resolve_address	(BlxBookmarks *eb,
							 const char *address,
							 const char *parameter);

guint		blx_bookmarks_get_smart_bookmark_width (BlxNode *bookmark);


/* Keywords */

BlxNode	 *blx_bookmarks_add_keyword		(BlxBookmarks *eb,
							 const char *name);

BlxNode	 *blx_bookmarks_find_keyword		(BlxBookmarks *eb,
							 const char *name,
							 gboolean partial_match);

void		  blx_bookmarks_remove_keyword		(BlxBookmarks *eb,
							 BlxNode *keyword);

gboolean	  blx_bookmarks_has_keyword		(BlxBookmarks *eb,
							 BlxNode *keyword,
							 BlxNode *bookmark);

void		  blx_bookmarks_set_keyword		(BlxBookmarks *eb,
							 BlxNode *keyword,
							 BlxNode *bookmark);

void		  blx_bookmarks_unset_keyword		(BlxBookmarks *eb,
							 BlxNode *keyword,
							 BlxNode *bookmark);

char		 *blx_bookmarks_get_topic_uri		(BlxBookmarks *eb,
							 BlxNode *node);

/* Favorites */

BlxNode	 *blx_bookmarks_get_favorites		(BlxBookmarks *eb);

/* Root */

BlxNode	 *blx_bookmarks_get_keywords		(BlxBookmarks *eb);

BlxNode	 *blx_bookmarks_get_bookmarks		(BlxBookmarks *eb);

BlxNode	 *blx_bookmarks_get_not_categorized	(BlxBookmarks *eb);

BlxNode	 *blx_bookmarks_get_smart_bookmarks	(BlxBookmarks *eb);

BlxNode	 *blx_bookmarks_get_local		(BlxBookmarks *eb);

/* Comparison functions, useful for sorting lists and arrays. */
int            blx_bookmarks_compare_topics            (gconstpointer a, gconstpointer b);
int            blx_bookmarks_compare_topic_pointers    (gconstpointer a, gconstpointer b);
int            blx_bookmarks_compare_bookmarks         (gconstpointer a, gconstpointer b);
int            blx_bookmarks_compare_bookmark_pointers (gconstpointer a, gconstpointer b);

G_END_DECLS

#endif
