/*
 *  Copyright © 2002  Ricardo Fernández Pascual
 *  Copyright © 2003  Marco Pesenti Gritti
 *  Copyright © 2003  Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_ENCODING_MENU_H
#define BLX_ENCODING_MENU_H

#include "blx-window.h"

G_BEGIN_DECLS

#define BLX_TYPE_ENCODING_MENU			(blx_encoding_menu_get_type())
#define BLX_ENCODING_MENU(object)		(G_TYPE_CHECK_INSTANCE_CAST((object), BLX_TYPE_ENCODING_MENU, BlxEncodingMenu))
#define BLX_ENCODING_MENU_CLASS(klass) 	(G_TYPE_CHECK_CLASS_CAST((klass), BLX_TYPE_ENCODING_MENU, BlxEncodingMenuClass))
#define BLX_IS_ENCODING_MENU(object)		(G_TYPE_CHECK_INSTANCE_TYPE((object), BLX_TYPE_ENCODING_MENU))
#define BLX_IS_ENCODING_MENU_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), BLX_TYPE_ENCODING_MENU))
#define BLX_ENCODING_MENU_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), BLX_TYPE_ENCODING_MENU, BlxEncodingMenuClass))

typedef struct _BlxEncodingMenu BlxEncodingMenu;
typedef struct _BlxEncodingMenuClass BlxEncodingMenuClass;
typedef struct _BlxEncodingMenuPrivate BlxEncodingMenuPrivate;

struct _BlxEncodingMenuClass
{
	GObjectClass parent_class;
};

struct _BlxEncodingMenu
{
	GObject parent_object;

	/*< private >*/
	BlxEncodingMenuPrivate *priv;
};

GType			blx_encoding_menu_get_type	(void);

BlxEncodingMenu       *blx_encoding_menu_new		(BlxWindow *window);

G_END_DECLS

#endif
