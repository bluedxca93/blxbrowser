/*
 *  Copyright © 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_LINK_H
#define BLX_LINK_H

#include <glib-object.h>

#include "blx-embed.h"
#include "blx-window.h"

G_BEGIN_DECLS

#define BLX_TYPE_LINK			(blx_link_get_type ())
#define BLX_LINK(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_LINK, BlxLink))
#define BLX_LINK_IFACE(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_LINK, BlxLinkIface))
#define BLX_IS_LINK(o)			(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_LINK))
#define BLX_IS_LINK_IFACE(k)		(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_LINK))
#define BLX_LINK_GET_IFACE(inst)	(G_TYPE_INSTANCE_GET_INTERFACE ((inst), BLX_TYPE_LINK, BlxLinkIface))

typedef struct _BlxLink	BlxLink;
typedef struct _BlxLinkIface	BlxLinkIface;

typedef enum
{
	BLX_LINK_NEW_WINDOW	       = 1 << 0,
	BLX_LINK_NEW_TAB	       = 1 << 1,
	BLX_LINK_JUMP_TO	       = 1 << 2,
	BLX_LINK_NEW_TAB_APPEND_AFTER = 1 << 3
} BlxLinkFlags;

struct _BlxLinkIface
{
	GTypeInterface base_iface;

	/* Signals */
	BlxEmbed * (* open_link) (BlxLink *link,
				   const char *address,
				   BlxEmbed *embed,
				   BlxLinkFlags flags);
};

GType	 blx_link_flags_get_type	(void);

GType	 blx_link_get_type		(void);

BlxEmbed *blx_link_open		(BlxLink *link,
					 const char *address,
					 BlxEmbed *embed,
					 BlxLinkFlags flags);

BlxLinkFlags blx_link_flags_from_current_event (void);

G_END_DECLS

#endif /* BLX_LINK_H */
