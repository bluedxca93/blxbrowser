/*
 *  Copyright © 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_LINK_ACTION_H
#define BLX_LINK_ACTION_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_LINK_ACTION			(blx_link_action_get_type ())
#define BLX_LINK_ACTION(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_LINK_ACTION, BlxLinkAction))
#define BLX_LINK_ACTION_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_LINK_ACTION, BlxLinkActionClass))
#define BLX_IS_LINK_ACTION(o)			(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_LINK_ACTION))
#define BLX_IS_LINK_ACTION_CLASS(k)		(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_LINK_ACTION))
#define BLX_LINK_ACTION_GET_CLASS(o)		(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_LINK_ACTION, BlxLinkActionClass))

#define BLX_TYPE_LINK_ACTION_GROUP		(blx_link_action_group_get_type ())
#define BLX_LINK_ACTION_GROUP(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_LINK_ACTION_GROUP, BlxLinkActionGroup))
#define BLX_LINK_ACTION_GROUP_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_LINK_ACTION_GROUP, BlxLinkActionGroupClass))
#define BLX_IS_LINK_ACTION_GROUP(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_LINK_ACTION_GROUP))
#define BLX_IS_LINK_ACTION_GROUP_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_LINK_ACTION_GROUP))
#define BLX_LINK_ACTION_GROUP_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_LINK_ACTION_GROUP, BlxLinkActionGroupClass))

typedef struct _BlxLinkAction			BlxLinkAction;
typedef struct _BlxLinkActionClass		BlxLinkActionClass;
typedef struct _BlxLinkActionPrivate		BlxLinkActionPrivate;

typedef struct _BlxLinkActionGroup		BlxLinkActionGroup;
typedef struct _BlxLinkActionGroupClass	BlxLinkActionGroupClass;

struct _BlxLinkAction
{
	GtkAction parent_instance;

	BlxLinkActionPrivate *priv;
};

struct _BlxLinkActionClass
{
	GtkActionClass parent_class;
};

struct _BlxLinkActionGroup
{
	GtkActionGroup parent_instance;
};

struct _BlxLinkActionGroupClass
{
	GtkActionGroupClass parent_class;
};

GType blx_link_action_get_type	  (void);
guint blx_link_action_get_button (BlxLinkAction *action);

GType blx_link_action_group_get_type (void);

BlxLinkActionGroup * blx_link_action_group_new (const char *name);

G_END_DECLS

#endif
