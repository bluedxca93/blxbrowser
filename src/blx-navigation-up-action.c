/*
 *  Copyright © 2003, 2004 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *  Copyright © 2008 Jan Alonzo
 *  Copyright © 2009 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"
#include "blx-navigation-up-action.h"

#include "blx-debug.h"
#include "blx-embed-container.h"
#include "blx-embed-shell.h"
#include "blx-embed-utils.h"
#include "blx-gui.h"
#include "blx-history.h"
#include "blx-link.h"
#include "blx-shell.h"
#include "blx-type-builtins.h"
#include "blx-window.h"

#include <gtk/gtk.h>
#include <webkit/webkit.h>

#define URL_DATA_KEY "GoURL"

static void blx_navigation_up_action_init       (BlxNavigationUpAction *action);
static void blx_navigation_up_action_class_init (BlxNavigationUpActionClass *klass);

G_DEFINE_TYPE (BlxNavigationUpAction, blx_navigation_up_action, BLX_TYPE_NAVIGATION_ACTION)

static void
action_activate (GtkAction *action)
{
  BlxWindow *window;
  BlxEmbed *embed;
  GSList *up_list;

  window = _blx_navigation_action_get_window (BLX_NAVIGATION_ACTION (action));
  embed = blx_embed_container_get_active_child (BLX_EMBED_CONTAINER (window));
  g_return_if_fail (embed != NULL);

  up_list = blx_web_view_get_go_up_list (blx_embed_get_web_view (embed));
  blx_link_open (BLX_LINK (action),
		  up_list->data,
		  NULL,
		  blx_gui_is_middle_click () ? BLX_LINK_NEW_TAB : 0);
  g_slist_foreach (up_list, (GFunc) g_free, NULL);
  g_slist_free (up_list);
}

static void
blx_navigation_up_action_init (BlxNavigationUpAction *action)
{
}

static void
blx_navigation_up_action_class_init (BlxNavigationUpActionClass *klass)
{
  GtkActionClass *action_class = GTK_ACTION_CLASS (klass);

  action_class->activate = action_activate;
}
