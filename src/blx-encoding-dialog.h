/*
 *  Copyright © 2000, 2001, 2002, 2003 Marco Pesenti Gritti
 *  Copyright © 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_ENCODING_DIALOG_H
#define BLX_ENCODING_DIALOG_H

#include "blx-embed-dialog.h"
#include "blx-window.h"

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define BLX_TYPE_ENCODING_DIALOG		(blx_encoding_dialog_get_type ())
#define BLX_ENCODING_DIALOG(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_ENCODING_DIALOG, BlxEncodingDialog))
#define BLX_ENCODING_DIALOG_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_ENCODING_DIALOG, BlxEncodingDialogClass))
#define BLX_IS_ENCODING_DIALOG(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_ENCODING_DIALOG))
#define BLX_IS_ENCODING_DIALOG_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_ENCODING_DIALOG))
#define BLX_ENCODING_DIALOG_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_ENCODING_DIALOG, BlxEncodingDialogClass))

typedef struct _BlxEncodingDialog		BlxEncodingDialog;
typedef struct _BlxEncodingDialogClass		BlxEncodingDialogClass;
typedef struct _BlxEncodingDialogPrivate	BlxEncodingDialogPrivate;

struct _BlxEncodingDialog
{
	BlxEmbedDialog parent;

	/*< private >*/
	BlxEncodingDialogPrivate *priv;
};

struct _BlxEncodingDialogClass
{
	BlxEmbedDialogClass parent_class;
};

GType			 blx_encoding_dialog_get_type	(void);

BlxEncodingDialog	*blx_encoding_dialog_new	(BlxWindow *window);

G_END_DECLS

#endif
