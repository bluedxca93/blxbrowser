/*
 *  Copyright © 2003 Marco Pesenti Gritti <mpeseng@tin.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_HISTORY_WINDOW_H
#define BLX_HISTORY_WINDOW_H

#include <gtk/gtk.h>

#include "blx-node-view.h"
#include "blx-history.h"

G_BEGIN_DECLS

#define BLX_TYPE_HISTORY_WINDOW     (blx_history_window_get_type ())
#define BLX_HISTORY_WINDOW(o)       (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_HISTORY_WINDOW, BlxHistoryWindow))
#define BLX_HISTORY_WINDOW_CLASS(k) (G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_HISTORY_WINDOW, BlxHistoryWindowClass))
#define BLX_IS_HISTORY_WINDOW(o)    (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_HISTORY_WINDOW))
#define BLX_IS_HISTORY_WINDOW_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_HISTORY_WINDOW))
#define BLX_HISTORY_WINDOW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_HISTORY_WINDOW, BlxHistoryWindowClass))

typedef struct _BlxHistoryWindowPrivate BlxHistoryWindowPrivate;

typedef struct
{
	GtkWindow parent;

	/*< private >*/
	BlxHistoryWindowPrivate *priv;
} BlxHistoryWindow;

typedef struct
{
	GtkWindowClass parent;
} BlxHistoryWindowClass;

GType		     blx_history_window_get_type (void);

GtkWidget	    *blx_history_window_new        (BlxHistory *history);

void		     blx_history_window_set_parent (BlxHistoryWindow *ehw,
						     GtkWidget *window);

G_END_DECLS

#endif /* BLX_HISTORY_WINDOW_H */
