/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Copyright © 2000, 2001, 2002, 2003, 2004 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *  Copyright © 2011 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"
#include "blx-window.h"

#include "blx-action-helper.h"
#include "blx-bookmarks-ui.h"
#include "blx-combined-stop-reload-action.h"
#include "blx-debug.h"
#include "blx-download-widget.h"
#include "blx-download.h"
#include "blx-embed-container.h"
#include "blx-embed-prefs.h"
#include "blx-embed-shell.h"
#include "blx-embed-single.h"
#include "blx-embed-type-builtins.h"
#include "blx-embed-utils.h"
#include "blx-encoding-menu.h"
#include "blx-extension.h"
#include "blx-file-helpers.h"
#include "blx-find-toolbar.h"
#include "blx-fullscreen-popup.h"
#include "blx-go-action.h"
#include "blx-gui.h"
#include "blx-home-action.h"
#include "blx-link.h"
#include "blx-location-action.h"
#include "blx-location-entry.h"
#include "blx-navigation-action.h"
#include "blx-navigation-history-action.h"
#include "blx-navigation-up-action.h"
#include "blx-notebook.h"
#include "blx-prefs.h"
#include "blx-settings.h"
#include "blx-shell.h"
#include "blx-state.h"
#include "blx-stock-icons.h"
#include "blx-topic-action.h"
#include "blx-type-builtins.h"
#include "blx-web-view.h"
#include "blx-zoom-action.h"
#include "blx-zoom.h"
#include "popup-commands.h"
#include "window-commands.h"

#include <gdk/gdkkeysyms.h>
#include <gio/gio.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <string.h>
#include <webkit/webkit.h>

#ifdef HAVE_X11_XF86KEYSYM_H
#include <X11/XF86keysym.h>
#endif

/**
 * SECTION:blx-window
 * @short_description: Blxbrowser's main #GtkWindow widget
 *
 * #BlxWindow is Blxbrowser's main widget.
 */

static void blx_window_show			(GtkWidget *widget);
static BlxEmbed *blx_window_open_link		(BlxLink *link,
						 const char *address,
						 BlxEmbed *embed,
						 BlxLinkFlags flags);
static void notebook_switch_page_cb		(GtkNotebook *notebook,
						 GtkWidget *page,
						 guint page_num,
						 BlxWindow *window);
static void blx_window_view_toolbar_cb         (GtkAction *action,
						 BlxWindow *window);
static void blx_window_view_menubar_cb         (GtkAction *action,
						 BlxWindow *window);
static void blx_window_view_popup_windows_cb	(GtkAction *action,
						 BlxWindow *window);
static void sync_tab_load_status		(BlxWebView *view,
						 GParamSpec *pspec,
						 BlxWindow *window);
static void sync_tab_security			(BlxWebView  *view,
						 GParamSpec *pspec,
						 BlxWindow *window);
static void sync_tab_zoom			(WebKitWebView *web_view,
						 GParamSpec *pspec,
						 BlxWindow *window);

static const GtkActionEntry blx_menu_entries [] = {

	/* Toplevel */

	{ "File", NULL, N_("_File") },
	{ "Edit", NULL, N_("_Edit") },
	{ "View", NULL, N_("_View") },
	{ "Bookmarks", NULL, N_("_Bookmarks") },
	{ "Tools", NULL, N_("T_ools") },
	{ "Help", NULL, N_("_Help") },
	{ "Toolbar", NULL, N_("_Toolbars") },
	{ "PopupAction", NULL, "" },
	{ "NotebookPopupAction", NULL, "" },

	/* File menu */

	{ "FileOpen", GTK_STOCK_OPEN, N_("_Open…"), "<control>O",
	  N_("Open a file"),
	  G_CALLBACK (window_cmd_file_open) },
	{ "FileSaveAs", GTK_STOCK_SAVE_AS, N_("Save _As…"), "<shift><control>S",
	  N_("Save the current page"),
	  G_CALLBACK (window_cmd_file_save_as) },
	{ "FileSaveAsApplication", GTK_STOCK_SAVE_AS, N_("Save As _Web Application…"), "<shift><control>A",
	  N_("Save the current page as a Web Application"),
	  G_CALLBACK (window_cmd_file_save_as_application) },
	{ "FilePrintSetup", STOCK_PRINT_SETUP, N_("Page Set_up"), NULL,
	  N_("Setup the page settings for printing"),
	  G_CALLBACK (window_cmd_file_print_setup) },
	{ "FilePrintPreview", GTK_STOCK_PRINT_PREVIEW, N_("Print Pre_view"),"<control><shift>P",
	  N_("Print preview"),
	  G_CALLBACK (window_cmd_file_print_preview) },
	{ "FilePrint", GTK_STOCK_PRINT, N_("_Print…"), "<control>P",
	  N_("Print the current page"),
	  G_CALLBACK (window_cmd_file_print) },
	{ "FileSendTo", STOCK_SEND_MAIL, N_("S_end Link by Email…"), NULL,
	  N_("Send a link of the current page"),
	  G_CALLBACK (window_cmd_file_send_to) },
	{ "FileCloseTab", GTK_STOCK_CLOSE, N_("_Close"), "<control>W",
	  N_("Close this tab"),
	  G_CALLBACK (window_cmd_file_close_window) },

	/* Edit menu */

	{ "EditUndo", GTK_STOCK_UNDO, N_("_Undo"), "<control>Z",
	  N_("Undo the last action"),
	  G_CALLBACK (window_cmd_edit_undo) },
	{ "EditRedo", GTK_STOCK_REDO, N_("Re_do"), "<shift><control>Z",
	  N_("Redo the last undone action"),
	  G_CALLBACK (window_cmd_edit_redo) },
	{ "EditCut", GTK_STOCK_CUT, N_("Cu_t"), "<control>X",
	  N_("Cut the selection"),
	  G_CALLBACK (window_cmd_edit_cut) },
	{ "EditCopy", GTK_STOCK_COPY, N_("_Copy"), "<control>C",
	  N_("Copy the selection"),
	  G_CALLBACK (window_cmd_edit_copy) },
	{ "EditPaste", GTK_STOCK_PASTE, N_("_Paste"), "<control>V",
	  N_("Paste clipboard"),
	  G_CALLBACK (window_cmd_edit_paste) },
	{ "EditDelete", GTK_STOCK_DELETE, NULL, NULL,
	  N_("Delete text"),
	  G_CALLBACK (window_cmd_edit_delete) },
	{ "EditSelectAll", GTK_STOCK_SELECT_ALL, N_("Select _All"), "<control>A",
	  N_("Select the entire page"),
	  G_CALLBACK (window_cmd_edit_select_all) },
	{ "EditFind", GTK_STOCK_FIND, N_("_Find…"), "<control>F",
	  N_("Find a word or phrase in the page"),
	  G_CALLBACK (window_cmd_edit_find) },
	{ "EditFindNext", NULL, N_("Find Ne_xt"), "<control>G",
	  N_("Find next occurrence of the word or phrase"),
	  G_CALLBACK (window_cmd_edit_find_next) },
	{ "EditFindPrev", NULL, N_("Find Pre_vious"), "<shift><control>G",
	  N_("Find previous occurrence of the word or phrase"),
	  G_CALLBACK (window_cmd_edit_find_prev) },
	{ "EditPersonalData", NULL, N_("P_ersonal Data"), NULL,
	  N_("View and remove cookies and passwords"),
	  G_CALLBACK (window_cmd_edit_personal_data) },
	{ "EditPrefs", GTK_STOCK_PREFERENCES, N_("P_references"), NULL,
	  N_("Configure the web browser"),
	  G_CALLBACK (window_cmd_edit_prefs) },

	/* View menu */

	{ "ViewStop", GTK_STOCK_STOP, N_("_Stop"), "Escape",
	  N_("Stop current data transfer"),
	  G_CALLBACK (window_cmd_view_stop) },
	{ "ViewAlwaysStop", GTK_STOCK_STOP, N_("_Stop"), "Escape",
	  NULL, G_CALLBACK (window_cmd_view_stop) },
	{ "ViewReload", GTK_STOCK_REFRESH, N_("_Reload"), "<control>R",
	  N_("Display the latest content of the current page"),
	  G_CALLBACK (window_cmd_view_reload) },
	{ "ViewZoomIn", GTK_STOCK_ZOOM_IN, N_("_Larger Text"), "<control>plus",
	  N_("Increase the text size"),
	  G_CALLBACK (window_cmd_view_zoom_in) },
	{ "ViewZoomOut", GTK_STOCK_ZOOM_OUT, N_("S_maller Text"), "<control>minus",
	  N_("Decrease the text size"),
	  G_CALLBACK (window_cmd_view_zoom_out) },
	{ "ViewZoomNormal", GTK_STOCK_ZOOM_100, N_("_Normal Size"), "<control>0",
	  N_("Use the normal text size"),
	  G_CALLBACK (window_cmd_view_zoom_normal) },
	{ "ViewEncoding", NULL, N_("Text _Encoding"), NULL,
	  N_("Change the text encoding"),
	  NULL },
	{ "ViewPageSource", NULL, N_("_Page Source"), "<control>U",
	  N_("View the source code of the page"),
	  G_CALLBACK (window_cmd_view_page_source) },
        { "ViewPageSecurityInfo", NULL, N_("Page _Security Information"), NULL,
          N_("Display security information for the web page"),
          G_CALLBACK (window_cmd_view_page_security_info) },

	/* Bookmarks menu */

	{ "FileBookmarkPage", STOCK_ADD_BOOKMARK, N_("_Add Bookmark…"), "<control>D",
	  N_("Add a bookmark for the current page"),
	  G_CALLBACK (window_cmd_file_bookmark_page) },
	{ "GoBookmarks", BLX_STOCK_BOOKMARKS, N_("_Edit Bookmarks"), "<control>B",
	  N_("Open the bookmarks window"),
	  G_CALLBACK (window_cmd_go_bookmarks) },

	/* Go menu */

	{ "GoLocation", NULL, N_("_Location…"), "<control>L",
	  N_("Go to a specified location"),
	  G_CALLBACK (window_cmd_go_location) },
	{ "GoHistory", BLX_STOCK_HISTORY, N_("Hi_story"), "<control>H",
	  N_("Open the history window"),
	  G_CALLBACK (window_cmd_go_history) },

	/* Tabs menu */

	{ "TabsPrevious", NULL, N_("_Previous Tab"), "<control>Page_Up",
	  N_("Activate previous tab"),
	  G_CALLBACK (window_cmd_tabs_previous) },
	{ "TabsNext", NULL, N_("_Next Tab"), "<control>Page_Down",
	  N_("Activate next tab"),
	  G_CALLBACK (window_cmd_tabs_next) },
	{ "TabsMoveLeft", NULL, N_("Move Tab _Left"), "<shift><control>Page_Up",
	  N_("Move current tab to left"),
	  G_CALLBACK (window_cmd_tabs_move_left) },
	{ "TabsMoveRight", NULL, N_("Move Tab _Right"), "<shift><control>Page_Down",
	  N_("Move current tab to right"),
	  G_CALLBACK (window_cmd_tabs_move_right) },
        { "TabsDetach", NULL, N_("_Detach Tab"), NULL,
          N_("Detach current tab"),
          G_CALLBACK (window_cmd_tabs_detach) },

	/* Help menu */

	{"HelpContents", GTK_STOCK_HELP, N_("_Contents"), "F1",
	 N_("Display web browser help"),
	 G_CALLBACK (window_cmd_help_contents) },
	{ "HelpAbout", GTK_STOCK_ABOUT, N_("_About"), NULL,
	  N_("Display credits for the web browser creators"),
	  G_CALLBACK (window_cmd_help_about) },
};

static const GtkToggleActionEntry blx_menu_toggle_entries [] =
{
	/* File Menu */

	{ "FileWorkOffline", NULL, N_("_Work Offline"), NULL,
	  N_("Switch to offline mode"),
	  G_CALLBACK (window_cmd_file_work_offline), FALSE },

	/* View Menu */

	{ "ViewToolbar", NULL, N_("_Hide Toolbars"), NULL,
	  N_("Show or hide toolbar"),
	  G_CALLBACK (blx_window_view_toolbar_cb), FALSE },
	{ "ViewDownloadsBar", NULL, N_("_Downloads Bar"), NULL,
	  N_("Show the active downloads for this window"),
	  NULL, FALSE },

	{ "ViewMenuBar", NULL, N_("Men_ubar"), NULL,
	  NULL, G_CALLBACK (blx_window_view_menubar_cb), TRUE },
	{ "ViewFullscreen", GTK_STOCK_FULLSCREEN, N_("_Fullscreen"), "F11",
	  N_("Browse at full screen"),
	  G_CALLBACK (window_cmd_view_fullscreen), FALSE },
	{ "ViewPopupWindows", BLX_STOCK_POPUPS, N_("Popup _Windows"), NULL,
	  N_("Show or hide unrequested popup windows from this site"),
	  G_CALLBACK (blx_window_view_popup_windows_cb), FALSE },
	{ "BrowseWithCaret", NULL, N_("Selection Caret"), "F7",
	  "",
	  G_CALLBACK (window_cmd_browse_with_caret), FALSE }
};

static const GtkActionEntry blx_popups_entries [] = {
        /* Document */

	{ "ContextBookmarkPage", STOCK_ADD_BOOKMARK, N_("Add Boo_kmark…"), "<control>D",
	  N_("Add a bookmark for the current page"),
	  G_CALLBACK (window_cmd_file_bookmark_page) },
	
	/* Framed document */

	{ "OpenFrame", NULL, N_("Show Only _This Frame"), NULL,
	  N_("Show only this frame in this window"),
	  G_CALLBACK (popup_cmd_open_frame) },

	/* Links */

	{ "OpenLink", GTK_STOCK_JUMP_TO, N_("_Open Link"), NULL,
	  N_("Open link in this window"),
	  G_CALLBACK (popup_cmd_open_link) },
	{ "OpenLinkInNewWindow", NULL, N_("Open Link in New _Window"), NULL,
	  N_("Open link in a new window"),
	  G_CALLBACK (popup_cmd_link_in_new_window) },
	{ "OpenLinkInNewTab", NULL, N_("Open Link in New _Tab"), NULL,
	  N_("Open link in a new tab"),
	  G_CALLBACK (popup_cmd_link_in_new_tab) },
	{ "DownloadLink", NULL, N_("_Download Link"), NULL,
	  NULL, G_CALLBACK (popup_cmd_download_link) },
	{ "DownloadLinkAs", GTK_STOCK_SAVE_AS, N_("_Save Link As…"), NULL,
	  N_("Save link with a different name"),
	  G_CALLBACK (popup_cmd_download_link_as) },
	{ "BookmarkLink", STOCK_ADD_BOOKMARK, N_("_Bookmark Link…"),
	  NULL, NULL, G_CALLBACK (popup_cmd_bookmark_link) },
	{ "CopyLinkAddress", NULL, N_("_Copy Link Address"), NULL,
	  NULL, G_CALLBACK (popup_cmd_copy_link_address) },

	/* Email links */

	/* This is on the context menu on a mailto: link and opens the mail program */
	{ "SendEmail", STOCK_NEW_MAIL, N_("_Send Email…"),
	  NULL, NULL, G_CALLBACK (popup_cmd_open_link) },
	{ "CopyEmailAddress", NULL, N_("_Copy Email Address"), NULL,
	  NULL, G_CALLBACK (popup_cmd_copy_link_address) },

	/* Images */

	{ "OpenImage", NULL, N_("Open _Image"), NULL,
	  NULL, G_CALLBACK (popup_cmd_open_image) },
	{ "SaveImageAs", NULL, N_("_Save Image As…"), NULL,
	  NULL, G_CALLBACK (popup_cmd_save_image_as) },
	{ "SetImageAsBackground", NULL, N_("_Use Image As Background"), NULL,
	  NULL, G_CALLBACK (popup_cmd_set_image_as_background) },
	{ "CopyImageLocation", NULL, N_("Copy I_mage Address"), NULL,
	  NULL, G_CALLBACK (popup_cmd_copy_image_location) },
	{ "StartImageAnimation", NULL, N_("St_art Animation"), NULL,
	  NULL, NULL },
	{ "StopImageAnimation", NULL, N_("St_op Animation"), NULL,
	  NULL, NULL },

	/* Spelling */

	{ "ReplaceWithSpellingSuggestion0", NULL, NULL, NULL,
	  NULL, G_CALLBACK (popup_replace_spelling), },
	{ "ReplaceWithSpellingSuggestion1", NULL, NULL, NULL,
	  NULL, G_CALLBACK (popup_replace_spelling), },
	{ "ReplaceWithSpellingSuggestion2", NULL, NULL, NULL,
	  NULL, G_CALLBACK (popup_replace_spelling), },
	{ "ReplaceWithSpellingSuggestion3", NULL, NULL, NULL,
	  NULL, G_CALLBACK (popup_replace_spelling), },


	/* Inspector */
	{ "InspectElement", NULL, N_("Inspect _Element"), NULL,
	  NULL, G_CALLBACK (popup_cmd_inspect_element) },
};

static const struct
{
	guint keyval;
	GdkModifierType modifier;
	const gchar *action;
	gboolean fromToolbar;
} extra_keybindings [] = {
	{ GDK_KEY_s,		GDK_CONTROL_MASK,	"FileSaveAs",		 FALSE },
	{ GDK_KEY_R,		GDK_CONTROL_MASK |
				GDK_SHIFT_MASK,		"ViewReload",		 FALSE },
	/* Tab navigation */
	{ GDK_KEY_Page_Up,      GDK_CONTROL_MASK,       "TabsPrevious",          FALSE },
	{ GDK_KEY_Page_Down,    GDK_CONTROL_MASK,       "TabsNext",              FALSE },
	{ GDK_KEY_Page_Up,      GDK_CONTROL_MASK |
	                        GDK_SHIFT_MASK,         "TabsMoveLeft",          FALSE },
	{ GDK_KEY_Page_Down,    GDK_CONTROL_MASK |
	                        GDK_SHIFT_MASK,         "TabsMoveRight",         FALSE },
	/* Go */
	{ GDK_KEY_l,            GDK_CONTROL_MASK,       "GoLocation",            FALSE },
	{ GDK_KEY_h,            GDK_CONTROL_MASK,       "GoHistory",             FALSE },
	/* Support all the MSIE tricks as well ;) */
	{ GDK_KEY_F5,		0,			"ViewReload",		 FALSE },
	{ GDK_KEY_F5,		GDK_CONTROL_MASK,	"ViewReload",		 FALSE },
	{ GDK_KEY_F5,		GDK_SHIFT_MASK,		"ViewReload",		 FALSE },
	{ GDK_KEY_F5,		GDK_CONTROL_MASK |
				GDK_SHIFT_MASK,		"ViewReload",		 FALSE },
	{ GDK_KEY_KP_Add,	GDK_CONTROL_MASK,	"ViewZoomIn",		 FALSE },
	{ GDK_KEY_KP_Subtract,	GDK_CONTROL_MASK,	"ViewZoomOut",		 FALSE },
	{ GDK_KEY_equal,	GDK_CONTROL_MASK,	"ViewZoomIn",		 FALSE },
	{ GDK_KEY_KP_0,		GDK_CONTROL_MASK,	"ViewZoomNormal",	 FALSE },
	/* These keys are a bit strange: when pressed with no modifiers, they emit
	 * KP_PageUp/Down Control; when pressed with Control+Shift they are KP_9/3,
	 * when NumLock is on they are KP_9/3 and with NumLock and Control+Shift
	 * They're KP_PageUp/Down again!
	 */
	{ GDK_KEY_KP_Left,	GDK_MOD1_MASK /*Alt*/,	"NavigationBack",	TRUE },
	{ GDK_KEY_KP_4,		GDK_MOD1_MASK /*Alt*/,	"NavigationBack",	TRUE },
	{ GDK_KEY_KP_Right,	GDK_MOD1_MASK /*Alt*/,	"NavigationForward",	TRUE },
	{ GDK_KEY_KP_6,		GDK_MOD1_MASK /*Alt*/,	"NavigationForward",	TRUE },
	{ GDK_KEY_KP_Up,	GDK_MOD1_MASK /*Alt*/,	"NavigationUp",		TRUE },
	{ GDK_KEY_KP_8,		GDK_MOD1_MASK /*Alt*/,	"NavigationUp",		TRUE },
	{ GDK_KEY_KP_Page_Up,	GDK_CONTROL_MASK,	"TabsPrevious",		FALSE },
	{ GDK_KEY_KP_9,		GDK_CONTROL_MASK,	"TabsPrevious",		FALSE },
	{ GDK_KEY_KP_Page_Down,	GDK_CONTROL_MASK,	"TabsNext",		FALSE },
	{ GDK_KEY_KP_3,		GDK_CONTROL_MASK,	"TabsNext",		FALSE },
	{ GDK_KEY_KP_Page_Up,	GDK_SHIFT_MASK | GDK_CONTROL_MASK,	"TabsMoveLeft",		FALSE },
	{ GDK_KEY_KP_9,		GDK_SHIFT_MASK | GDK_CONTROL_MASK,	"TabsMoveLeft",		FALSE },
	{ GDK_KEY_KP_Page_Down,	GDK_SHIFT_MASK | GDK_CONTROL_MASK,	"TabsMoveRight",	FALSE },
	{ GDK_KEY_KP_3,		GDK_SHIFT_MASK | GDK_CONTROL_MASK,	"TabsMoveRight",	FALSE },
#ifdef HAVE_X11_XF86KEYSYM_H
	{ XF86XK_Back,		0,			"NavigationBack",	TRUE  },
	{ XF86XK_Favorites,	0,			"GoBookmarks",		FALSE },
	{ XF86XK_Forward,	0,			"NavigationForward",	TRUE  },
	{ XF86XK_Go,	 	0,			"GoLocation",		FALSE },
	{ XF86XK_History, 	0,			"GoHistory",		FALSE },
	{ XF86XK_HomePage,	0,			"GoHome",		TRUE  },
	{ XF86XK_OpenURL, 	0,			"GoLocation",		FALSE },
	{ XF86XK_AddFavorite, 	0,			"FileBookmarkPage",	FALSE },
	{ XF86XK_Refresh, 	0,			"ViewReload",		FALSE },
	{ XF86XK_Reload,	0, 			"ViewReload",		FALSE },
	{ XF86XK_Search,	0,			"EditFind",		FALSE },
	{ XF86XK_Send,	 	0,			"FileSendTo",		FALSE },
	{ XF86XK_Start,		0,			"GoHome",		TRUE  },
	{ XF86XK_Stop,		0,			"ViewStop",		FALSE },
	{ XF86XK_ZoomIn,	0, 			"ViewZoomIn",		FALSE },
	{ XF86XK_ZoomOut,	0, 			"ViewZoomOut",		FALSE }
	/* FIXME: what about ScrollUp, ScrollDown, Menu*, Option, LogOff, Save,.. any others? */
#endif /* HAVE_X11_XF86KEYSYM_H */
};

#define BOOKMARKS_MENU_PATH "/menubar/BookmarksMenu"

#define SETTINGS_CONNECTION_DATA_KEY	"BlxWindowSettings"

#define BLX_WINDOW_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), BLX_TYPE_WINDOW, BlxWindowPrivate))

struct _BlxWindowPrivate
{
	GtkWidget *main_vbox;
	GtkWidget *menu_dock;
	GtkWidget *fullscreen_popup;
	GtkWidget *toolbar;
	GtkUIManager *manager;
	GtkActionGroup *action_group;
	GtkActionGroup *popups_action_group;
	GtkActionGroup *toolbar_action_group;
	BlxEncodingMenu *enc_menu;
	GtkNotebook *notebook;
	BlxEmbed *active_embed;
	BlxFindToolbar *find_toolbar;
	guint num_tabs;
	guint tab_message_cid;
	guint help_message_cid;
	BlxWebViewChrome chrome;
	GHashTable *tabs_to_remove;
	BlxEmbedEvent *context_event;
	guint idle_worker;
	GtkWidget *entry;
	GtkWidget *downloads_box;

	guint clear_progress_timeout_id;
	gulong set_focus_handler;
	guint menubar_accel_keyval;
	guint menubar_accel_modifier;

	guint closing : 1;
	guint has_size : 1;
	guint fullscreen_mode : 1;
	guint should_save_chrome : 1;
	guint is_popup : 1;
	guint present_on_insert : 1;
	guint key_theme_is_emacs : 1;
	guint updating_address : 1;
	guint show_lock : 1;
};

enum
{
	PROP_0,
	PROP_ACTIVE_CHILD,
	PROP_CHROME,
	PROP_SINGLE_TAB_MODE
};

/* Make sure not to overlap with those in blx-lockdown.c */
enum
{
	SENS_FLAG_CHROME	= 1 << 0,
	SENS_FLAG_CONTEXT	= 1 << 1,
	SENS_FLAG_DOCUMENT	= 1 << 2,
	SENS_FLAG_LOADING	= 1 << 3,
	SENS_FLAG_NAVIGATION	= 1 << 4
};

static gint
impl_add_child (BlxEmbedContainer *container,
		BlxEmbed *child,
		gint position,
		gboolean jump_to)
{
	BlxWindow *window = BLX_WINDOW (container);

	g_return_val_if_fail (!window->priv->is_popup ||
			      gtk_notebook_get_n_pages (GTK_NOTEBOOK (window->priv->notebook)) < 1, -1);

	window->priv->tab_message_cid = blx_embed_statusbar_get_context_id
		(child, BLX_EMBED_STATUSBAR_TAB_MESSAGE_CONTEXT_DESCRIPTION);
	window->priv->help_message_cid = blx_embed_statusbar_get_context_id
		(child, BLX_EMBED_STATUSBAR_HELP_MESSAGE_CONTEXT_DESCRIPTION);

	return blx_notebook_add_tab (BLX_NOTEBOOK (window->priv->notebook),
				      child, position, jump_to);
}

static void
impl_set_active_child (BlxEmbedContainer *container,
		       BlxEmbed *child)
{
	int page;
	BlxWindow *window;

	window = BLX_WINDOW (container);

	page = gtk_notebook_page_num
		(window->priv->notebook, GTK_WIDGET (child));
	gtk_notebook_set_current_page
		(window->priv->notebook, page);
}

static GtkWidget *
construct_confirm_close_dialog (BlxWindow *window,
				const char *title,
				const char *info,
				const char *action)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new (GTK_WINDOW (window),
					 GTK_DIALOG_MODAL,
					 GTK_MESSAGE_WARNING,
					 GTK_BUTTONS_CANCEL,
					 "%s", title);

	gtk_message_dialog_format_secondary_text
		(GTK_MESSAGE_DIALOG (dialog), "%s", info);
	
	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       action, GTK_RESPONSE_ACCEPT);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CANCEL);

	/* FIXME gtk_window_set_title (GTK_WINDOW (dialog), _("Close Document?")); */
	gtk_window_set_icon_name (GTK_WINDOW (dialog), BLX_STOCK_BLX);

	gtk_window_group_add_window (gtk_window_get_group (GTK_WINDOW (window)),
				     GTK_WINDOW (dialog));

	return dialog;
}

static gboolean
confirm_close_with_modified_forms (BlxWindow *window)
{
	if (g_settings_get_boolean (BLX_SETTINGS_MAIN,
				    BLX_PREFS_WARN_ON_CLOSE_UNSUBMITTED_DATA))
	{
		GtkWidget *dialog;
		int response;

		dialog = construct_confirm_close_dialog (window,
				_("There are unsubmitted changes to form elements"),
				_("If you close the document anyway, "
				  "you will lose that information."),
				_("Close _Document"));
		response = gtk_dialog_run (GTK_DIALOG (dialog));

		gtk_widget_destroy (dialog);

		return response == GTK_RESPONSE_ACCEPT;
	}
	
	return TRUE;
}

static gboolean
confirm_close_with_downloads (BlxWindow *window)
{
	GtkWidget *dialog;
	int response;

	dialog = construct_confirm_close_dialog (window,
			_("There are ongoing downloads in this window"),
			_("If you close this window, the downloads will be cancelled"),
			_("Close window and cancel downloads"));
	response = gtk_dialog_run (GTK_DIALOG (dialog));

	gtk_widget_destroy (dialog);

	return response == GTK_RESPONSE_ACCEPT;
}

static void
impl_remove_child (BlxEmbedContainer *container,
		   BlxEmbed *child)
{
	BlxWindow *window;
	BlxWindowPrivate *priv;
	GtkNotebook *notebook;
	gboolean modified;
	int position;

	window = BLX_WINDOW (container);
	priv = window->priv;

	modified = blx_web_view_has_modified_forms (blx_embed_get_web_view (child));
	if (modified && confirm_close_with_modified_forms (window) == FALSE)
	{
		/* don't close the tab */
		return;
	}

	notebook = GTK_NOTEBOOK (priv->notebook);
	position = gtk_notebook_page_num (notebook, GTK_WIDGET (child));
	gtk_notebook_remove_page (notebook, position);
}

static BlxEmbed *
impl_get_active_child (BlxEmbedContainer *container)
{
	return BLX_WINDOW (container)->priv->active_embed;
}

static GList *
impl_get_children (BlxEmbedContainer *container)
{
	BlxWindow *window = BLX_WINDOW (container);

	return gtk_container_get_children (GTK_CONTAINER (window->priv->notebook));
}

static gboolean
impl_get_is_popup (BlxEmbedContainer *container)
{
	return BLX_WINDOW (container)->priv->is_popup;
}

static BlxWebViewChrome
impl_get_chrome (BlxEmbedContainer *container)
{
	return BLX_WINDOW (container)->priv->chrome;
}

static void
blx_window_embed_container_iface_init (BlxEmbedContainerIface *iface)
{
	iface->add_child = impl_add_child;
	iface->set_active_child = impl_set_active_child;
	iface->remove_child = impl_remove_child;
	iface->get_active_child = impl_get_active_child;
	iface->get_children = impl_get_children;
	iface->get_is_popup = impl_get_is_popup;
	iface->get_chrome = impl_get_chrome;
}

static void
blx_window_link_iface_init (BlxLinkIface *iface)
{
	iface->open_link = blx_window_open_link;
}

G_DEFINE_TYPE_WITH_CODE (BlxWindow, blx_window, GTK_TYPE_WINDOW,
			 G_IMPLEMENT_INTERFACE (BLX_TYPE_LINK,
						blx_window_link_iface_init)
			 G_IMPLEMENT_INTERFACE (BLX_TYPE_EMBED_CONTAINER,
						blx_window_embed_container_iface_init))

/* FIXME: fix this! */
static void
blx_tab_get_size (BlxEmbed *embed,
                   int *width,
                   int *height)
{
        *width = -1;
        *height = -1;
}

static void
settings_change_notify (GtkSettings *settings,
			BlxWindow  *window)
{
	BlxWindowPrivate *priv = window->priv;
	char *key_theme_name, *menubar_accel_accel;

	g_object_get (settings,
		      "gtk-key-theme-name", &key_theme_name,
		      "gtk-menu-bar-accel", &menubar_accel_accel,
		      NULL);

	g_return_if_fail (menubar_accel_accel != NULL);

	if (menubar_accel_accel != NULL && menubar_accel_accel[0] != '\0')
	{
		gtk_accelerator_parse (menubar_accel_accel,
				       &priv->menubar_accel_keyval,
				       &priv->menubar_accel_modifier);
		if (priv->menubar_accel_keyval == 0)
		{
			g_warning ("Failed to parse menu bar accelerator '%s'\n",
				   menubar_accel_accel);
		}
	}
	else
	{
		priv->menubar_accel_keyval = 0;
		priv->menubar_accel_modifier = 0;
	}

	priv->key_theme_is_emacs =
		key_theme_name &&
		g_ascii_strcasecmp (key_theme_name, "Emacs") == 0;

	g_free (key_theme_name);
	g_free (menubar_accel_accel);
}

static void
settings_changed_cb (GtkSettings *settings)
{
	GList *list, *l;

	/* FIXME: multi-head */
	list = gtk_window_list_toplevels ();

	for (l = list; l != NULL; l = l->next)
	{
		if (BLX_IS_WINDOW (l->data))
		{
			settings_change_notify (settings, l->data);
		}
	}

	g_list_free (list);
}

static void
destroy_fullscreen_popup (BlxWindow *window)
{
	if (window->priv->fullscreen_popup != NULL)
	{
		gtk_widget_destroy (window->priv->fullscreen_popup);
		window->priv->fullscreen_popup = NULL;
	}
}

static void
add_widget (GtkUIManager *manager,
	    GtkWidget *widget,
	    BlxWindow *window)
{
	gtk_box_pack_start (GTK_BOX (window->priv->menu_dock),
			    widget, FALSE, FALSE, 0);
}

static void
exit_fullscreen_clicked_cb (BlxWindow *window)
{
	GtkAction *action;

	action = gtk_action_group_get_action (window->priv->action_group, "ViewFullscreen");
	g_return_if_fail (action != NULL);

	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), FALSE);
}

static gboolean
get_toolbar_visibility (BlxWindow *window)
{
	return ((window->priv->chrome & BLX_WEB_VIEW_CHROME_TOOLBAR) != 0);
}			

static void
get_chromes_visibility (BlxWindow *window,
			gboolean *show_menubar,
			gboolean *show_toolbar,
			gboolean *show_tabsbar)
{
	BlxWindowPrivate *priv = window->priv;
	BlxWebViewChrome flags = priv->chrome;

	if (window->priv->fullscreen_mode)
	{
		*show_toolbar = (flags & BLX_WEB_VIEW_CHROME_TOOLBAR) != 0;
		*show_menubar = FALSE;
		*show_tabsbar = !priv->is_popup;
	}
	else
	{
		*show_menubar = (flags & BLX_WEB_VIEW_CHROME_MENUBAR) != 0;
		*show_toolbar = (flags & BLX_WEB_VIEW_CHROME_TOOLBAR) != 0;
		*show_tabsbar = !priv->is_popup;
	}

	if (blx_embed_shell_get_mode (embed_shell) == BLX_EMBED_SHELL_MODE_APPLICATION)
		*show_menubar = FALSE;
}

static void
sync_chromes_visibility (BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	GtkWidget *menubar;
	gboolean show_menubar, show_toolbar, show_tabsbar;

	if (priv->closing) return;

	get_chromes_visibility (window, &show_menubar,
				&show_toolbar,
				&show_tabsbar);

	menubar = gtk_ui_manager_get_widget (window->priv->manager, "/menubar");
	g_assert (menubar != NULL);

	g_object_set (menubar, "visible", show_menubar, NULL);
	g_object_set (priv->toolbar, "visible", show_toolbar, NULL);

	blx_notebook_set_show_tabs (BLX_NOTEBOOK (priv->notebook), show_tabsbar);
}

static void
ensure_location_entry (BlxWindow *window)
{
	GtkActionGroup *toolbar_action_group;
	GtkAction *action;
	GSList *proxies;
	GtkWidget *proxy;
	BlxWindowPrivate *priv = window->priv;

	toolbar_action_group = priv->toolbar_action_group;
	action = gtk_action_group_get_action (toolbar_action_group,
					      "Location");
	proxies = gtk_action_get_proxies (action);
	if (proxies)
	{
		proxy = GTK_WIDGET (proxies->data);
		priv->entry = blx_location_entry_get_entry (BLX_LOCATION_ENTRY (proxy));
	}
}

static void
blx_window_fullscreen (BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	GtkWidget *popup;
	BlxEmbed *embed;
	GtkAction *action;
	gboolean lockdown_fs;

	priv->fullscreen_mode = TRUE;

	lockdown_fs = g_settings_get_boolean
				(BLX_SETTINGS_LOCKDOWN,
				 BLX_PREFS_LOCKDOWN_FULLSCREEN);

	popup = blx_fullscreen_popup_new (window);
	blx_fullscreen_popup_set_show_leave
		(BLX_FULLSCREEN_POPUP (popup), !lockdown_fs);
	priv->fullscreen_popup = popup;
	g_signal_connect_swapped (popup, "exit-clicked",
				  G_CALLBACK (exit_fullscreen_clicked_cb), window);

	action = gtk_action_group_get_action (priv->action_group, "ViewPageSecurityInfo");
	g_signal_connect_swapped (popup, "lock-clicked",
				  G_CALLBACK (gtk_action_activate), action);

	/* sync status */
	embed = window->priv->active_embed;
	sync_tab_load_status (blx_embed_get_web_view (embed), NULL, window);
	sync_tab_security (blx_embed_get_web_view (embed), NULL, window);

	ensure_location_entry (window);

	sync_chromes_visibility (window);
}

static void
blx_window_unfullscreen (BlxWindow *window)
{
	window->priv->fullscreen_mode = FALSE;

	destroy_fullscreen_popup (window);

	ensure_location_entry (window);

	sync_chromes_visibility (window);
}

static void
menubar_deactivate_cb (GtkWidget *menubar,
		       BlxWindow *window)
{
	g_signal_handlers_disconnect_by_func
		(menubar, G_CALLBACK (menubar_deactivate_cb), window);

	gtk_menu_shell_deselect (GTK_MENU_SHELL (menubar));

	sync_chromes_visibility (window);
}

static gboolean 
scroll_event_cb (GtkWidget *widget,
		 GdkEventScroll *event,
		 BlxWindow *window)
{
	guint modifier = event->state & gtk_accelerator_get_default_mod_mask ();

	if (modifier != GDK_CONTROL_MASK)
		return FALSE;

	if (event->direction == GDK_SCROLL_UP)
		blx_window_set_zoom (window, ZOOM_IN);
	else if (event->direction == GDK_SCROLL_DOWN)
		blx_window_set_zoom (window, ZOOM_OUT);

	return TRUE;
}

static gboolean 
blx_window_key_press_event (GtkWidget *widget,
			     GdkEventKey *event)
{
	BlxWindow *window = BLX_WINDOW (widget);
	BlxWindowPrivate *priv = window->priv;
	GtkWidget *menubar, *focus_widget;
	gboolean shortcircuit = FALSE, force_chain = FALSE, handled = FALSE;
	guint modifier = event->state & gtk_accelerator_get_default_mod_mask ();
	guint i;

	/* In an attempt to get the mozembed playing nice with things like emacs keybindings
	 * we are passing important events to the focused child widget before letting the window's
	 * base handler see them. This is *completely against* stated gtk2 policy but the 
	 * 'correct' behaviour is exceptionally useless. We need to keep an eye out for 
	 * unexpected consequences of this decision. IME's should be a high concern, but 
	 * considering that the IME folks complained about the upside-down event propagation
	 * rules, we might be doing them a favour.
	 *
	 * We achieve this by first evaluating the event to see if it's important, and if
	 * so, we get the focus widget and attempt to get the widget to handle that event.
	 * If the widget does handle it, we're done (unless force_chain is true, in which
	 * case the event is handled as normal in addition to being sent to the focus
	 * widget), otherwise the event follows the normal handling path.
	 */

	if (event->keyval == GDK_KEY_Escape && modifier == 0)
	{
		/* Always pass Escape to both the widget, and the parent */
		shortcircuit = TRUE;
		force_chain = TRUE;
	}
	else if (priv->key_theme_is_emacs && 
		 (modifier == GDK_CONTROL_MASK) &&
		 event->length > 0 &&
		 /* But don't pass Ctrl+Enter twice */
		 event->keyval != GDK_KEY_Return &&
		 event->keyval != GDK_KEY_KP_Enter &&
		 event->keyval != GDK_KEY_ISO_Enter)
	{
		/* Pass CTRL+letter characters to the widget */
		shortcircuit = TRUE;
	}

	if (shortcircuit)
	{
		focus_widget = gtk_window_get_focus (GTK_WINDOW (window));

		if (GTK_IS_WIDGET (focus_widget))
		{
			handled = gtk_widget_event (focus_widget,
						    (GdkEvent*) event);
		}

		if (handled && !force_chain)
		{
			return handled;
		}
	}

	/* Handle accelerators that we want bound, but aren't associated with
	 * an action */
	for (i = 0; i < G_N_ELEMENTS (extra_keybindings); i++)
	{
		if (event->keyval == extra_keybindings[i].keyval &&
		    modifier == extra_keybindings[i].modifier)
		{
			GtkAction * action = gtk_action_group_get_action
				(extra_keybindings[i].fromToolbar ? 
					priv->toolbar_action_group :
					priv->action_group,
				extra_keybindings[i].action);
			if (gtk_action_is_sensitive (action))
			{
				gtk_action_activate (action);
				return TRUE;
			}
			break;
		}
	}

	/* Don't activate menubar in lockdown mode */
	if (g_settings_get_boolean (BLX_SETTINGS_LOCKDOWN,
				    BLX_PREFS_LOCKDOWN_MENUBAR))
	{
		return GTK_WIDGET_CLASS (blx_window_parent_class)->key_press_event (widget, event);
	}

	/* Show and activate the menubar, if it isn't visible */
	if (priv->menubar_accel_keyval != 0 &&
	    event->keyval == priv->menubar_accel_keyval &&
            modifier == priv->menubar_accel_modifier)
	{
		menubar = gtk_ui_manager_get_widget (window->priv->manager, "/menubar");
		g_return_val_if_fail (menubar != NULL , FALSE);

		if (!gtk_widget_get_visible (menubar))
		{
			g_signal_connect (menubar, "deactivate",
					  G_CALLBACK (menubar_deactivate_cb), window);

			gtk_widget_show (menubar);
			gtk_menu_shell_select_first (GTK_MENU_SHELL (menubar), FALSE);

			return TRUE;
		}
	}

	return GTK_WIDGET_CLASS (blx_window_parent_class)->key_press_event (widget, event);
}

static gboolean
window_has_ongoing_downloads (BlxWindow *window)
{
	GList *l, *downloads;
	gboolean downloading = FALSE;

	downloads = gtk_container_get_children (GTK_CONTAINER (window->priv->downloads_box));

	for (l = downloads; l != NULL; l = l->next)
	{
		BlxDownload *download;
		WebKitDownloadStatus status;

		if (BLX_IS_DOWNLOAD_WIDGET (l->data) != TRUE)
			continue;

		download = blx_download_widget_get_download (BLX_DOWNLOAD_WIDGET (l->data));
		status = webkit_download_get_status (blx_download_get_webkit_download (download));

		if (status == WEBKIT_DOWNLOAD_STATUS_STARTED)
		{
			downloading = TRUE;
			break;
		}
	}
	g_list_free (downloads);

	return downloading;
}

static gboolean
blx_window_delete_event (GtkWidget *widget,
			  GdkEventAny *event)
{
	BlxWindow *window = BLX_WINDOW (widget);
	BlxEmbed *modified_embed = NULL;
	GList *tabs, *l;
	gboolean modified = FALSE;

	/* We ignore the delete_event if the disable_quit lockdown has been set
	 */
	if (g_settings_get_boolean (BLX_SETTINGS_LOCKDOWN,
				    BLX_PREFS_LOCKDOWN_QUIT)) return TRUE;

	tabs = impl_get_children (BLX_EMBED_CONTAINER (window));
	for (l = tabs; l != NULL; l = l->next)
	{
		BlxEmbed *embed = (BlxEmbed *) l->data;

		g_return_val_if_fail (BLX_IS_EMBED (embed), FALSE);

		if (blx_web_view_has_modified_forms (blx_embed_get_web_view (embed)))
		{
			modified = TRUE;
			modified_embed = embed;
			break;
		}
	}
	g_list_free (tabs);

	if (modified)
	{
		/* jump to the first tab with modified forms */
		impl_set_active_child (BLX_EMBED_CONTAINER (window),
				       modified_embed);

		if (confirm_close_with_modified_forms (window) == FALSE)
		{
			/* stop window close */
			return TRUE;
		}
	}


	if (window_has_ongoing_downloads (window) && confirm_close_with_downloads (window) == FALSE)
	{
		/* stop window close */
		return TRUE;
	}

	/* See bug #114689 */
	gtk_widget_hide (widget);

	/* proceed with window close */
	if (GTK_WIDGET_CLASS (blx_window_parent_class)->delete_event)
	{
		return GTK_WIDGET_CLASS (blx_window_parent_class)->delete_event (widget, event);
	}

	return FALSE;
}

#define MAX_SPELL_CHECK_GUESSES 4

static void
update_popup_actions_visibility (BlxWindow *window,
				 WebKitWebView *view,
				 guint context,
				 gboolean is_frame)
{
	GtkAction *action;
	GtkActionGroup *action_group;
	gboolean is_image = context & WEBKIT_HIT_TEST_RESULT_CONTEXT_IMAGE;
	gboolean is_editable = context & WEBKIT_HIT_TEST_RESULT_CONTEXT_EDITABLE;
	GtkWidget *separator;
	char **guesses = NULL;
	int i;

	action_group = window->priv->popups_action_group;

	if (blx_embed_shell_get_mode (embed_shell) == BLX_EMBED_SHELL_MODE_APPLICATION)
	{
		action = gtk_action_group_get_action (action_group, "OpenLinkInNewTab");
		gtk_action_set_visible (action, FALSE);
		action = gtk_action_group_get_action (action_group, "OpenLinkInNewWindow");
		gtk_action_set_visible (action, FALSE);
		action = gtk_action_group_get_action (action_group, "ContextBookmarkPage");
		gtk_action_set_visible (action, FALSE);
		action = gtk_action_group_get_action (action_group, "BookmarkLink");
		gtk_action_set_visible (action, FALSE);
	}

	action = gtk_action_group_get_action (action_group, "OpenImage");
	gtk_action_set_visible (action, is_image);
	action = gtk_action_group_get_action (action_group, "SaveImageAs");
	gtk_action_set_visible (action, is_image);
	action = gtk_action_group_get_action (action_group, "SetImageAsBackground");
	gtk_action_set_visible (action, is_image);
	action = gtk_action_group_get_action (action_group, "CopyImageLocation");
	gtk_action_set_visible (action, is_image);

	action = gtk_action_group_get_action (action_group, "OpenFrame");
	gtk_action_set_visible (action, is_frame);

	if (is_editable)
	{
		char *text = NULL;
		WebKitWebFrame *frame;
		WebKitDOMRange *range;

		frame = webkit_web_view_get_focused_frame (view);
		range = webkit_web_frame_get_range_for_word_around_caret (frame);
		text = webkit_dom_range_get_text (range);

		if (text && *text != '\0')
		{
			int location, length;
			WebKitSpellChecker *checker = (WebKitSpellChecker*)webkit_get_text_checker();
			webkit_spell_checker_check_spelling_of_string (checker, text, &location, &length);
			if (length)
				guesses = webkit_spell_checker_get_guesses_for_word (checker, text, NULL);
			
		}

		g_free (text);
	}

	for (i = 0; i < MAX_SPELL_CHECK_GUESSES; i++)
	{
		char *action_name;

		action_name = g_strdup_printf("ReplaceWithSpellingSuggestion%d", i);
		action = gtk_action_group_get_action (action_group, action_name);

		if (guesses && i <= g_strv_length (guesses)) {
			gtk_action_set_visible (action, TRUE);
			gtk_action_set_label (action, guesses[i]);
		} else
			gtk_action_set_visible (action, FALSE);

		g_free (action_name);
	}

	/* The separator! There must be a better way to do this? */
	separator = gtk_ui_manager_get_widget (window->priv->manager,
					       "/BlxInputPopup/SpellingSeparator");
	if (guesses)
		gtk_widget_show (separator);
	else
		gtk_widget_hide (separator);

	if (guesses)
		g_strfreev (guesses);
}

static void
update_edit_actions_sensitivity (BlxWindow *window, gboolean hide)
{
	GtkWidget *widget = gtk_window_get_focus (GTK_WINDOW (window));
	GtkActionGroup *action_group;
	GtkAction *action;
	gboolean can_copy, can_cut, can_undo, can_redo, can_paste;

	if (GTK_IS_EDITABLE (widget))
	{
		gboolean has_selection;
		GtkActionGroup *action_group;
		GtkAction *location_action;
		GSList *proxies;
		GtkWidget *proxy;
		
		action_group = window->priv->toolbar_action_group;
		location_action = gtk_action_group_get_action (action_group,
							       "Location");
		proxies = gtk_action_get_proxies (location_action);
		proxy = GTK_WIDGET (proxies->data);
		
		has_selection = gtk_editable_get_selection_bounds
			(GTK_EDITABLE (widget), NULL, NULL);

		can_copy = has_selection;
		can_cut = has_selection;
		can_paste = TRUE;
		if (proxy != NULL &&
		    BLX_IS_LOCATION_ENTRY (proxy) &&
		    widget == blx_location_entry_get_entry (BLX_LOCATION_ENTRY (proxy)))
		{
			can_undo = blx_location_entry_get_can_undo (BLX_LOCATION_ENTRY (proxy));
			can_redo = blx_location_entry_get_can_redo (BLX_LOCATION_ENTRY (proxy));
		}
		else
		{
			can_undo = FALSE;
			can_redo = FALSE;
		}
	}
	else
	{
		BlxEmbed *embed;
		WebKitWebView *view;

		embed = window->priv->active_embed;
		g_return_if_fail (embed != NULL);

		view = BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed);

		can_copy = webkit_web_view_can_copy_clipboard (view);
		can_cut = webkit_web_view_can_cut_clipboard (view);
		can_paste = webkit_web_view_can_paste_clipboard (view);
		can_undo = webkit_web_view_can_undo (view);
		can_redo = webkit_web_view_can_redo (view);
	}

	action_group = window->priv->action_group;

	action = gtk_action_group_get_action (action_group, "EditCopy");
	gtk_action_set_sensitive (action, can_copy);
	gtk_action_set_visible (action, !hide || can_copy);
	action = gtk_action_group_get_action (action_group, "EditCut");
	gtk_action_set_sensitive (action, can_cut);
	gtk_action_set_visible (action, !hide || can_cut);
	action = gtk_action_group_get_action (action_group, "EditPaste");
	gtk_action_set_sensitive (action, can_paste);
	gtk_action_set_visible (action,  !hide || can_paste);
	action = gtk_action_group_get_action (action_group, "EditUndo");
	gtk_action_set_sensitive (action, can_undo);
	gtk_action_set_visible (action,  !hide || can_undo);
	action = gtk_action_group_get_action (action_group, "EditRedo");
	gtk_action_set_sensitive (action, can_redo);
	gtk_action_set_visible (action, !hide || can_redo);
}

static void
enable_edit_actions_sensitivity (BlxWindow *window)
{
	GtkActionGroup *action_group;
	GtkAction *action;

	action_group = window->priv->action_group;

	action = gtk_action_group_get_action (action_group, "EditCopy");
	gtk_action_set_sensitive (action, TRUE);
	gtk_action_set_visible (action, TRUE);
	action = gtk_action_group_get_action (action_group, "EditCut");
	gtk_action_set_sensitive (action, TRUE);
	gtk_action_set_visible (action, TRUE);
	action = gtk_action_group_get_action (action_group, "EditPaste");
	gtk_action_set_sensitive (action, TRUE);
	gtk_action_set_visible (action, TRUE);
	action = gtk_action_group_get_action (action_group, "EditUndo");
	gtk_action_set_sensitive (action, TRUE);
	gtk_action_set_visible (action, TRUE);
	action = gtk_action_group_get_action (action_group, "EditRedo");
	gtk_action_set_sensitive (action, TRUE);
	gtk_action_set_visible (action, TRUE);
}

static void
edit_menu_show_cb (GtkWidget *menu,
		   BlxWindow *window)
{
	update_edit_actions_sensitivity (window, FALSE);
}

static void
edit_menu_hide_cb (GtkWidget *menu,
		   BlxWindow *window)
{
	enable_edit_actions_sensitivity (window);
}

static void
init_menu_updaters (BlxWindow *window)
{
	GtkWidget *edit_menu_item, *edit_menu;

	edit_menu_item = gtk_ui_manager_get_widget
		(window->priv->manager, "/menubar/EditMenu");
	edit_menu = gtk_menu_item_get_submenu (GTK_MENU_ITEM (edit_menu_item));

	g_signal_connect (edit_menu, "show",
			  G_CALLBACK (edit_menu_show_cb), window);
	g_signal_connect (edit_menu, "hide",
			  G_CALLBACK (edit_menu_hide_cb), window);
}

static BlxWebView*
blx_window_get_active_web_view (BlxWindow *window)
{
	BlxEmbed *active_embed = window->priv->active_embed;
	return blx_embed_get_web_view (active_embed);
}

static void
menu_item_select_cb (GtkMenuItem *proxy,
		     BlxWindow *window)
{
	GtkAction *action;
	char *message;

	action = gtk_activatable_get_related_action (GTK_ACTIVATABLE (proxy));
	g_return_if_fail (action != NULL);
	
	g_object_get (action, "tooltip", &message, NULL);
	if (message)
	{
		BlxWebView *view = blx_window_get_active_web_view (window);
		blx_embed_statusbar_push (BLX_GET_EMBED_FROM_BLX_WEB_VIEW (view),
					   window->priv->help_message_cid, message);
		g_free (message);
	}
}

static void
menu_item_deselect_cb (GtkMenuItem *proxy,
		       BlxWindow *window)
{
	BlxWebView *view = blx_window_get_active_web_view (window);
	blx_embed_statusbar_pop (BLX_GET_EMBED_FROM_BLX_WEB_VIEW (view),
				  window->priv->help_message_cid);
}

static gboolean
tool_item_enter_cb (GtkWidget *proxy,
		    GdkEventCrossing *event,
		    BlxWindow *window)
{
	gboolean repeated;

	repeated = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (proxy), "blx-window-enter-event"));
	
	if (event->mode == GDK_CROSSING_NORMAL && repeated == FALSE)
	{
		GtkToolItem *item;
		GtkAction *action;
		char *message;
    
		item = GTK_TOOL_ITEM (gtk_widget_get_ancestor (proxy, GTK_TYPE_TOOL_ITEM));
		
		action = gtk_activatable_get_related_action (GTK_ACTIVATABLE (item));
		g_return_val_if_fail (action != NULL, FALSE);
		
		g_object_get (action, "tooltip", &message, NULL);
		if (message)
		{
			BlxWebView *view = blx_window_get_active_web_view (window);
			blx_embed_statusbar_push (BLX_GET_EMBED_FROM_BLX_WEB_VIEW (view),
						   window->priv->help_message_cid, message);
			g_object_set_data (G_OBJECT (proxy), "blx-window-enter-event", GINT_TO_POINTER (TRUE));
			g_free (message);
		}
	}
	
	return FALSE;
}

static gboolean
tool_item_leave_cb (GtkWidget *proxy,
		    GdkEventCrossing *event,
		    BlxWindow *window)
{
	if (event->mode == GDK_CROSSING_NORMAL)
	{
		BlxWebView *view = blx_window_get_active_web_view (window);
		blx_embed_statusbar_pop (BLX_GET_EMBED_FROM_BLX_WEB_VIEW (view),
					  window->priv->help_message_cid);
		g_object_set_data (G_OBJECT (proxy), "blx-window-enter-event", GINT_TO_POINTER (FALSE));
	}
	
	return FALSE;
}

static void
tool_item_drag_begin_cb (GtkWidget          *widget,
			 GdkDragContext     *context,
			 BlxWindow         *window)
{
	BlxWebView *view = blx_window_get_active_web_view (window);
	blx_embed_statusbar_pop (BLX_GET_EMBED_FROM_BLX_WEB_VIEW (view),
				  window->priv->help_message_cid);
}


static void
connect_tool_item (GtkWidget *proxy, BlxWindow *window)
{
	if (GTK_IS_CONTAINER (proxy))
	{
		gtk_container_foreach (GTK_CONTAINER (proxy),
				       (GtkCallback) connect_tool_item,
				       (gpointer) window);
	}

	g_signal_connect (proxy, "drag_begin",
			  G_CALLBACK (tool_item_drag_begin_cb), window);
	g_signal_connect (proxy, "enter-notify-event",
			  G_CALLBACK (tool_item_enter_cb), window);
	g_signal_connect (proxy, "leave-notify-event",
			  G_CALLBACK (tool_item_leave_cb), window);
}

static void
disconnect_tool_item (GtkWidget *proxy, BlxWindow *window)
{
	if (GTK_IS_CONTAINER (proxy))
	{
		gtk_container_foreach (GTK_CONTAINER (proxy),
				       (GtkCallback) disconnect_tool_item,
				       (gpointer) window);
	}

	g_signal_handlers_disconnect_by_func
	  (proxy, G_CALLBACK (tool_item_enter_cb), window);
	g_signal_handlers_disconnect_by_func
	  (proxy, G_CALLBACK (tool_item_leave_cb), window);
}

static void
disconnect_proxy_cb (GtkUIManager *manager,
		     GtkAction *action,
		     GtkWidget *proxy,
		     BlxWindow *window)
{
	if (GTK_IS_MENU_ITEM (proxy))
	{
		g_signal_handlers_disconnect_by_func
			(proxy, G_CALLBACK (menu_item_select_cb), window);
		g_signal_handlers_disconnect_by_func
			(proxy, G_CALLBACK (menu_item_deselect_cb), window);
	}
	else if (GTK_IS_TOOL_ITEM (proxy))
	{
		disconnect_tool_item (proxy, window);
	}
}

static void
connect_proxy_cb (GtkUIManager *manager,
		  GtkAction *action,
		  GtkWidget *proxy,
		  BlxWindow *window)
{
	if (GTK_IS_MENU_ITEM (proxy))
	{
		g_signal_connect (proxy, "select",
				  G_CALLBACK (menu_item_select_cb), window);
		g_signal_connect (proxy, "deselect",
				  G_CALLBACK (menu_item_deselect_cb), window);
	}
	else if (GTK_IS_TOOL_ITEM (proxy))
	{
		connect_tool_item (proxy, window);
	}
}

static void
update_chromes_actions (BlxWindow *window)
{
	GtkActionGroup *action_group = window->priv->action_group;
	GtkAction *action;
	gboolean show_menubar, show_toolbar, show_tabsbar;

	get_chromes_visibility (window,
				&show_menubar,
				&show_toolbar,
				&show_tabsbar);

	action = gtk_action_group_get_action (action_group, "ViewToolbar");
	g_signal_handlers_block_by_func (G_OBJECT (action),
		 			 G_CALLBACK (blx_window_view_toolbar_cb),
		 			 window);
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), !show_toolbar);
	g_signal_handlers_unblock_by_func (G_OBJECT (action),
		 			   G_CALLBACK (blx_window_view_toolbar_cb),
		 			   window);
	action = gtk_action_group_get_action (action_group, "ViewMenuBar");
	g_signal_handlers_block_by_func (G_OBJECT (action),
		 			 G_CALLBACK (blx_window_view_menubar_cb),
		 			 window);
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), show_menubar);
	g_signal_handlers_unblock_by_func (G_OBJECT (action),
		 			   G_CALLBACK (blx_window_view_menubar_cb),
		 			   window);
}

static void
setup_ui_manager (BlxWindow *window)
{
	GtkActionGroup *action_group;
	GtkAction *action;
	GtkUIManager *manager;

	window->priv->main_vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_show (window->priv->main_vbox);
	gtk_container_add (GTK_CONTAINER (window),
			   window->priv->main_vbox);

	window->priv->menu_dock = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_show (window->priv->menu_dock);
	gtk_box_pack_start (GTK_BOX (window->priv->main_vbox),
			    GTK_WIDGET (window->priv->menu_dock),
			    FALSE, TRUE, 0);

	manager = gtk_ui_manager_new ();

	g_signal_connect (manager, "connect_proxy",
			  G_CALLBACK (connect_proxy_cb), window);
	g_signal_connect (manager, "disconnect_proxy",
			  G_CALLBACK (disconnect_proxy_cb), window);

	action_group = gtk_action_group_new ("WindowActions");
	gtk_action_group_set_translation_domain (action_group, NULL);
	gtk_action_group_add_actions (action_group, blx_menu_entries,
				      G_N_ELEMENTS (blx_menu_entries), window);
	gtk_action_group_add_toggle_actions (action_group,
					     blx_menu_toggle_entries,
					     G_N_ELEMENTS (blx_menu_toggle_entries),
					     window);
	gtk_ui_manager_insert_action_group (manager, action_group, 0);
	window->priv->action_group = action_group;
	g_object_unref (action_group);

	action = gtk_action_group_get_action (action_group, "FileOpen");
	g_object_set (action, "short_label", _("Open"), NULL);
	action = gtk_action_group_get_action (action_group, "FileSaveAs");
	g_object_set (action, "short_label", _("Save As"), NULL);
	action = gtk_action_group_get_action (action_group, "FileSaveAsApplication");
	g_object_set (action, "short_label", _("Save As Application"), NULL);
	action = gtk_action_group_get_action (action_group, "FilePrint");
	g_object_set (action, "short_label", _("Print"), NULL);
	action = gtk_action_group_get_action (action_group, "FileBookmarkPage");
	g_object_set (action, "short_label", _("Bookmark"), NULL);
	action = gtk_action_group_get_action (action_group, "EditFind");
	g_object_set (action, "short_label", _("Find"), NULL);
	action = gtk_action_group_get_action (action_group, "GoBookmarks");
	g_object_set (action, "short_label", _("Bookmarks"), NULL);

	action = gtk_action_group_get_action (action_group, "EditFind");
	g_object_set (action, "is_important", TRUE, NULL);
	action = gtk_action_group_get_action (action_group, "GoBookmarks");
	g_object_set (action, "is_important", TRUE, NULL);

	action = gtk_action_group_get_action (action_group, "ViewEncoding");
	g_object_set (action, "hide_if_empty", FALSE, NULL);
	action = gtk_action_group_get_action (action_group, "ViewZoomIn");
	/* Translators: This refers to text size */
	g_object_set (action, "short-label", _("Larger"), NULL);
	action = gtk_action_group_get_action (action_group, "ViewZoomOut");
	/* Translators: This refers to text size */
	g_object_set (action, "short-label", _("Smaller"), NULL);

	action_group = gtk_action_group_new ("PopupsActions");
	gtk_action_group_set_translation_domain (action_group, NULL);
	gtk_action_group_add_actions (action_group, blx_popups_entries,
				      G_N_ELEMENTS (blx_popups_entries), window);
	gtk_ui_manager_insert_action_group (manager, action_group, 0);
	window->priv->popups_action_group = action_group;
	g_object_unref (action_group);

	action_group = gtk_action_group_new ("SpecialToolbarActions");
	action =
		g_object_new (BLX_TYPE_NAVIGATION_HISTORY_ACTION,
			      "name", "NavigationBack",
			      "label", _("_Back"),
			      "stock_id", GTK_STOCK_GO_BACK,
			      "tooltip", _("Go to the previous visited page"),
			      "window", window,
			      "direction", BLX_NAVIGATION_HISTORY_DIRECTION_BACK,
			      "is_important", TRUE,
			      NULL);
	gtk_action_group_add_action_with_accel (action_group, action,
						"<alt>Left");
	g_object_unref (action);

	action =
		g_object_new (BLX_TYPE_NAVIGATION_HISTORY_ACTION,
			      "name", "NavigationForward",
			      "label", _("_Forward"),
			      "stock_id", GTK_STOCK_GO_FORWARD,
			      "tooltip", _("Go to the next visited page"),
			      "window", window,
			      "direction", BLX_NAVIGATION_HISTORY_DIRECTION_FORWARD,
			      NULL);
	gtk_action_group_add_action_with_accel (action_group, action,
						"<alt>Right");
	g_object_unref (action);

	action =
		g_object_new (BLX_TYPE_NAVIGATION_UP_ACTION,
			      "name", "NavigationUp",
			      "label", _("_Up"),
			      "stock_id", GTK_STOCK_GO_UP,
			      "tooltip", _("Go up one level"),
			      "window", window,
			      NULL);
	gtk_action_group_add_action_with_accel (action_group, action,
						"<alt>Up");
	g_object_unref (action);

	/* FIXME: I'm still waiting for the exact term to 
	 * user here from the docs team.
	 */
	action =
		g_object_new (BLX_TYPE_LOCATION_ACTION,
			      "name", "Location",
			      "label", _("Address Entry"),
			      "stock_id", BLX_STOCK_ENTRY,
			      "tooltip", _("Enter a web address to open, or a phrase to search for"),
			      "visible-overflown", FALSE,
			      "window", window,
			      NULL);
	gtk_action_group_add_action (action_group, action);
	g_object_unref (action);


	action = g_object_new (BLX_TYPE_HOME_ACTION,
			       "name", "GoHome",
			       "label", _("_Home"),
			       "stock_id", GTK_STOCK_HOME,
			       "tooltip", _("Go to the home page"),
			       "is_important", TRUE,
			       NULL);
	gtk_action_group_add_action_with_accel (action_group, action, "<alt>Home");
	g_object_unref (action);

	action =
		g_object_new (BLX_TYPE_ZOOM_ACTION,
			      "name", "Zoom",
			      "label", _("Zoom"),
			      "stock_id", GTK_STOCK_ZOOM_IN,
			      "tooltip", _("Adjust the text size"),
			      "zoom", 1.0,
			      NULL);
	gtk_action_group_add_action (action_group, action);
	g_object_unref (action);

	action = g_object_new (BLX_TYPE_HOME_ACTION,
			       "name", "FileNewTab",
			       "label", _("New _Tab"),
			       "stock_id", STOCK_NEW_TAB,
			       "tooltip", _("Open a new tab"),
			       NULL);
	gtk_action_group_add_action_with_accel (action_group, action, "<control>T");

	g_object_unref (action);

	action = g_object_new (BLX_TYPE_HOME_ACTION,
			       "name", "FileNewWindow",
			       "label", _("_New Window"),
			       "stock_id", STOCK_NEW_WINDOW,
			       "tooltip", _("Open a new window"),
			       NULL);
	gtk_action_group_add_action_with_accel (action_group, action, "<control>N");
	g_object_unref (action);

	action = g_object_new (BLX_TYPE_COMBINED_STOP_RELOAD_ACTION,
			       "name", "ViewCombinedStopReload",
			       "loading", FALSE,
			       "window", window,
			       NULL);
	gtk_action_group_add_action (action_group, action);
	g_object_unref (action);

	gtk_ui_manager_insert_action_group (manager, action_group, 0);
	window->priv->toolbar_action_group = action_group;
	g_object_unref (action_group);

	window->priv->manager = manager;
	g_signal_connect (manager, "add_widget", G_CALLBACK (add_widget), window);
	gtk_window_add_accel_group (GTK_WINDOW (window),
				    gtk_ui_manager_get_accel_group (manager));
}



static void
_blx_window_set_location (BlxWindow *window,
			   const char *address)
{
	BlxWindowPrivate *priv = window->priv;
	BlxLocationAction *action;

	if (priv->updating_address) return;

	action = BLX_LOCATION_ACTION (gtk_action_group_get_action (priv->toolbar_action_group,
								    "Location"));
	priv->updating_address = TRUE;
	blx_location_action_set_address (action, address);
	priv->updating_address = FALSE;
}

static void
sync_tab_address (BlxWebView *view,
	          GParamSpec *pspec,
		  BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	const char *address;
	const char *typed_address;

	if (priv->closing) return;

	address = blx_web_view_get_address (view);
	typed_address = blx_web_view_get_typed_address (view);

	_blx_window_set_location (window, typed_address ? typed_address : address);
	blx_find_toolbar_request_close (priv->find_toolbar);
}

static void
sync_tab_document_type (BlxWebView *view,
			GParamSpec *pspec,
			BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	GtkActionGroup *action_group = priv->action_group;
	GtkAction *action;
	BlxWebViewDocumentType type;
	gboolean can_find, disable, is_image;

	if (priv->closing) return;

	/* update zoom actions */
	sync_tab_zoom (WEBKIT_WEB_VIEW (view), NULL, window);
	
	type = blx_web_view_get_document_type (view);
	can_find = (type != BLX_WEB_VIEW_DOCUMENT_IMAGE);
	is_image = type == BLX_WEB_VIEW_DOCUMENT_IMAGE;
	disable = (type != BLX_WEB_VIEW_DOCUMENT_HTML);

	action = gtk_action_group_get_action (action_group, "ViewEncoding");
	blx_action_change_sensitivity_flags (action, SENS_FLAG_DOCUMENT, disable);
	action = gtk_action_group_get_action (action_group, "ViewPageSource");
	blx_action_change_sensitivity_flags (action, SENS_FLAG_DOCUMENT, is_image);
	action = gtk_action_group_get_action (action_group, "EditFind");
	blx_action_change_sensitivity_flags (action, SENS_FLAG_DOCUMENT, !can_find);
	action = gtk_action_group_get_action (action_group, "EditFindNext");
	blx_action_change_sensitivity_flags (action, SENS_FLAG_DOCUMENT, !can_find);
	action = gtk_action_group_get_action (action_group, "EditFindPrev");
	blx_action_change_sensitivity_flags (action, SENS_FLAG_DOCUMENT, !can_find);

	if (!can_find)
	{
		blx_find_toolbar_request_close (priv->find_toolbar);
	}
}

static void
_blx_window_action_set_favicon (BlxWindow *window,
				 GdkPixbuf *icon)
{
	BlxWindowPrivate *priv = window->priv;
	GtkAction *action;

	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "Location");
	g_object_set (action, "icon", icon, NULL);
}

static void
sync_tab_icon (BlxWebView *view,
	       GParamSpec *pspec,
	       BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	GdkPixbuf *icon;

	if (priv->closing) return;

	icon = blx_web_view_get_icon (view);

	_blx_window_action_set_favicon (window, icon);
}

static gboolean
clear_progress_cb (BlxWindow *window)
{
	gtk_entry_set_progress_fraction (GTK_ENTRY (window->priv->entry), 0.0);
	window->priv->clear_progress_timeout_id = 0;

	return FALSE;
}

static void
sync_tab_load_progress (BlxWebView *view, GParamSpec *pspec, BlxWindow *window)
{
	gdouble progress;
	const char *uri;
	gboolean loading;
	gboolean switching_tab = pspec == NULL;

	if (window->priv->closing) return;
	if (!window->priv->entry) return;

	if (window->priv->clear_progress_timeout_id)
	{
		g_source_remove (window->priv->clear_progress_timeout_id);
		window->priv->clear_progress_timeout_id = 0;
	}

	/* If we are loading about:blank do not show progress, as the
	   blink it causes is annoying. */
	/* FIXME: for some reason webkit_web_view_get_uri returns NULL
	   for about:blank until the load is finished, so assume NULL
	   here means we are loading about:blank. This might not be
	   rigt :) */
	/* All the weird checks for progress == 1.0 and !switching_tab
	 * are because we receive first the LOAD_FINISHED status than
	 * the 100% progress report, so for progress == 1.0 there's no
	 * sane way of knowing whether we are still loading or
	 * not. See https://bugs.webkit.org/show_bug.cgi?id=28851 */
	uri = webkit_web_view_get_uri (WEBKIT_WEB_VIEW (view));
	if (!switching_tab && (!uri || strcmp (uri, "about:blank") == 0))
		return;

	progress = webkit_web_view_get_progress (WEBKIT_WEB_VIEW (view));
	loading = blx_web_view_is_loading (view);

	if (progress == 1.0 && !switching_tab)
	{
		window->priv->clear_progress_timeout_id =
			g_timeout_add (500,
				       (GSourceFunc)clear_progress_cb,
				       window);
	}

	/* Do not set progress in the entry if the load is already
	   finished */
	gtk_entry_set_progress_fraction (GTK_ENTRY (window->priv->entry),
					 loading || (progress == 1.0 && !switching_tab) ? progress : 0.0);
}


static void
_blx_window_set_navigation_actions (BlxWindow *window,
				     gboolean back,
				     gboolean forward,
				     gboolean up)
{
	BlxWindowPrivate *priv = window->priv;
	GtkAction *action;

	action = gtk_action_group_get_action (priv->toolbar_action_group, "NavigationBack");
	blx_action_change_sensitivity_flags (action, SENS_FLAG, !back);
	action = gtk_action_group_get_action (priv->toolbar_action_group, "NavigationForward");
	blx_action_change_sensitivity_flags (action, SENS_FLAG, !forward);
	action = gtk_action_group_get_action (priv->toolbar_action_group, "NavigationUp");
	blx_action_change_sensitivity_flags (action, SENS_FLAG, !up);
}

static void
_blx_window_set_navigation_tooltips (BlxWindow *window,
				      const char *back_title,
				      const char *forward_title)
{
	BlxWindowPrivate *priv = window->priv;
	GtkAction *action;
	GValue value = { 0 };

	g_value_init (&value, G_TYPE_STRING);

	g_value_set_static_string (&value, back_title);
	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "NavigationBack");
	g_object_set_property (G_OBJECT (action),
			       "tooltip", &value);

	g_value_set_static_string (&value, forward_title);
	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "NavigationForward");
	g_object_set_property (G_OBJECT (action),
			       "tooltip", &value);
	g_value_unset (&value);
}

static void
sync_tab_navigation (BlxWebView *view,
		     GParamSpec *pspec,
		     BlxWindow *window)
{
	BlxWebViewNavigationFlags flags;
	WebKitWebHistoryItem *item;
	WebKitWebView *web_view;
	WebKitWebBackForwardList *web_back_forward_list;
	gboolean up = FALSE, back = FALSE, forward = FALSE;
	const char *back_title = NULL, *forward_title = NULL;

	if (window->priv->closing) return;

	flags = blx_web_view_get_navigation_flags (view);

	if (flags & BLX_WEB_VIEW_NAV_UP)
	{
		up = TRUE;
	}
	if (flags & BLX_WEB_VIEW_NAV_BACK)
	{
		back = TRUE;
	}
	if (flags & BLX_WEB_VIEW_NAV_FORWARD)
	{
		forward = TRUE;
	}

	_blx_window_set_navigation_actions (window, back, forward, up);

	web_view = WEBKIT_WEB_VIEW (view);
	web_back_forward_list = webkit_web_view_get_back_forward_list (web_view);

	item = webkit_web_back_forward_list_get_back_item (web_back_forward_list);

	if (item)
	{
		back_title = webkit_web_history_item_get_title (item);
	}

	item = webkit_web_back_forward_list_get_forward_item (web_back_forward_list);

	if (item)
	{
		forward_title = webkit_web_history_item_get_title (item);
	}

	_blx_window_set_navigation_tooltips (window,
					      back_title,
					      forward_title);
}

static void
_blx_window_set_security_state (BlxWindow *window,
				 gboolean show_lock,
				 const char *stock_id,
				 const char *tooltip)
{
	BlxWindowPrivate *priv = window->priv;
	GtkAction *action;

	priv->show_lock = show_lock != FALSE;

	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "Location");
	g_object_set (action,
		      "lock-stock-id", stock_id,
		      "lock-tooltip", tooltip,
		      "show-lock", priv->show_lock,
		      NULL);
}

static void
sync_tab_security (BlxWebView *view,
		   GParamSpec *pspec,
		   BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	BlxWebViewSecurityLevel level;
	char *description = NULL;
	char *state = NULL;
	char *tooltip;
	const char *stock_id = STOCK_LOCK_INSECURE;
	gboolean show_lock = FALSE, is_secure = FALSE;
	GtkAction *action;

	if (priv->closing) return;

	blx_web_view_get_security_level (view, &level, &description);

	switch (level)
	{
		case BLX_WEB_VIEW_STATE_IS_UNKNOWN:
			state = _("Unknown");
			break;
		case BLX_WEB_VIEW_STATE_IS_INSECURE:
			state = _("Insecure");
			g_free (description);
			description = NULL;
			break;
		case BLX_WEB_VIEW_STATE_IS_BROKEN:
			state = _("Broken");
			stock_id = STOCK_LOCK_BROKEN;
                        show_lock = TRUE;
                        g_free (description);
                        description = NULL;
                        break;
		case BLX_WEB_VIEW_STATE_IS_SECURE_LOW:
		case BLX_WEB_VIEW_STATE_IS_SECURE_MED:
			state = _("Low");
			/* We deliberately don't show the 'secure' icon
			 * for low & medium secure sites; see bug #151709.
			 */
			stock_id = STOCK_LOCK_INSECURE;
			break;
		case BLX_WEB_VIEW_STATE_IS_SECURE_HIGH:
			state = _("High");
			stock_id = STOCK_LOCK_SECURE;
			show_lock = TRUE;
			is_secure = TRUE;
			break;
		default:
			g_assert_not_reached ();
			break;
	}

	tooltip = g_strdup_printf (_("Security level: %s"), state);
	if (description != NULL)
	{
		char *tmp = tooltip;

		tooltip = g_strconcat (tmp, "\n", description, NULL);
		g_free (description);
		g_free (tmp);
	}

	_blx_window_set_security_state (window, show_lock, stock_id, tooltip);

	if (priv->fullscreen_popup != NULL)
	{
		blx_fullscreen_popup_set_security_state
			(BLX_FULLSCREEN_POPUP (priv->fullscreen_popup),
			 show_lock, stock_id, tooltip);
	}

	action = gtk_action_group_get_action (priv->action_group, "ViewPageSecurityInfo");
	gtk_action_set_sensitive (action, is_secure);

	g_free (tooltip);
}

static void
sync_tab_popup_windows (BlxWebView *view,
			GParamSpec *pspec,
			BlxWindow *window)
{
	/* FIXME: show popup count somehow */
}

static void
sync_tab_popups_allowed (BlxWebView *view,
			 GParamSpec *pspec,
			 BlxWindow *window)
{
	GtkAction *action;
	gboolean allow;

	g_return_if_fail (BLX_IS_WEB_VIEW (view));
	g_return_if_fail (BLX_IS_WINDOW (window));

	action = gtk_action_group_get_action (window->priv->action_group,
					      "ViewPopupWindows");
	g_return_if_fail (GTK_IS_ACTION (action));

	g_object_get (view, "popups-allowed", &allow, NULL);

	g_signal_handlers_block_by_func
		(G_OBJECT (action),
		 G_CALLBACK (blx_window_view_popup_windows_cb),
		 window);

	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), allow);

	g_signal_handlers_unblock_by_func
		(G_OBJECT (action),
		 G_CALLBACK (blx_window_view_popup_windows_cb),
		 window);
}

static void
sync_tab_load_status (BlxWebView *view,
		      GParamSpec *pspec,
		      BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	GtkActionGroup *action_group = priv->action_group;
	GtkAction *action;
	gboolean loading;

	if (window->priv->closing) return;

	loading = blx_web_view_is_loading (view);

	action = gtk_action_group_get_action (action_group, "ViewStop");
	gtk_action_set_sensitive (action, loading);

	/* disable print while loading, see bug #116344 */
	action = gtk_action_group_get_action (action_group, "FilePrintPreview");
	blx_action_change_sensitivity_flags (action, SENS_FLAG_LOADING, loading);
	action = gtk_action_group_get_action (action_group, "FilePrint");
	blx_action_change_sensitivity_flags (action, SENS_FLAG_LOADING, loading);

	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "ViewCombinedStopReload");
	blx_combined_stop_reload_action_set_loading (BLX_COMBINED_STOP_RELOAD_ACTION (action),
						      loading);
}

static void
sync_tab_title (BlxWebView *view,
		GParamSpec *pspec,
		BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;

	if (priv->closing) return;

	gtk_window_set_title (GTK_WINDOW(window),
			      blx_web_view_get_title_composite (view));
}

static void
_blx_window_action_set_zoom (BlxWindow *window,
			      gboolean can_zoom,
			      float zoom)
{
	BlxWindowPrivate *priv = window->priv;
	GtkAction *action = gtk_action_group_get_action (priv->toolbar_action_group,
							 "Zoom");

	gtk_action_set_sensitive (action, can_zoom);
	g_object_set (action, "zoom", can_zoom ? zoom : 1.0, NULL);
}

static void
sync_tab_zoom (WebKitWebView *web_view, GParamSpec *pspec, BlxWindow *window)
{
	GtkActionGroup *action_group;
	GtkAction *action;
	BlxWebViewDocumentType type;
	gboolean can_zoom_in = TRUE, can_zoom_out = TRUE, can_zoom_normal = FALSE, can_zoom;
	float zoom;
	BlxEmbed *embed = window->priv->active_embed;

	if (window->priv->closing) return;

	g_object_get (web_view,
		      "zoom-level", &zoom,
		      NULL);

	type = blx_web_view_get_document_type (blx_embed_get_web_view (embed));
	can_zoom = (type != BLX_WEB_VIEW_DOCUMENT_IMAGE);

	if (zoom >= ZOOM_MAXIMAL)
	{
		can_zoom_in = FALSE;
	}
	if (zoom <= ZOOM_MINIMAL)
	{
		can_zoom_out = FALSE;
	}
	if (zoom != 1.0)
	{
		can_zoom_normal = TRUE;
	}

	_blx_window_action_set_zoom (window, can_zoom, zoom);

	action_group = window->priv->action_group;
	action = gtk_action_group_get_action (action_group, "ViewZoomIn");
	gtk_action_set_sensitive (action, can_zoom_in && can_zoom);
	action = gtk_action_group_get_action (action_group, "ViewZoomOut");
	gtk_action_set_sensitive (action, can_zoom_out && can_zoom);
	action = gtk_action_group_get_action (action_group, "ViewZoomNormal");
	gtk_action_set_sensitive (action, can_zoom_normal && can_zoom);
}

static void
sync_network_status (BlxEmbedSingle *single,
		     GParamSpec *pspec,
		     BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	GtkAction *action;
	gboolean is_online;

	is_online = blx_embed_single_get_network_status (single);

	action = gtk_action_group_get_action (priv->action_group,
					      "FileWorkOffline");
	g_signal_handlers_block_by_func
		(action, G_CALLBACK (window_cmd_file_work_offline), window);
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), !is_online);
	g_signal_handlers_unblock_by_func
		(action, G_CALLBACK (window_cmd_file_work_offline), window);	
}

static void
popup_menu_at_coords (GtkMenu *menu, gint *x, gint *y, gboolean *push_in,
		      gpointer user_data)
{
	BlxWindow *window = BLX_WINDOW (user_data);
	BlxWindowPrivate *priv = window->priv;
	guint ux, uy;

	g_return_if_fail (priv->context_event != NULL);

	blx_embed_event_get_coords (priv->context_event, &ux, &uy);
	*x = ux; *y = uy;

	/* FIXME: better position the popup within the window bounds? */
	blx_gui_sanitise_popup_position (menu, GTK_WIDGET (window), x, y);

	*push_in = TRUE;
}

static gboolean
idle_unref_context_event (BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;

	LOG ("Idle unreffing context event %p", priv->context_event);

	if (priv->context_event != NULL)
	{
		g_object_unref (priv->context_event);
		priv->context_event = NULL;
	}

	priv->idle_worker = 0;
	return FALSE;
}

static void
_blx_window_set_context_event (BlxWindow *window,
				BlxEmbedEvent *event)
{
	BlxWindowPrivate *priv = window->priv;

	if (priv->idle_worker != 0)
	{
		g_source_remove (priv->idle_worker);
		priv->idle_worker = 0;
	}

	if (priv->context_event != NULL)
	{
		g_object_unref (priv->context_event);
	}

	priv->context_event = event != NULL ? g_object_ref (event) : NULL;
}

static void
_blx_window_unset_context_event (BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;

	/* Unref the event from idle since we still need it
	 * from the action callbacks which will run before idle.
	 */
	if (priv->idle_worker == 0 && priv->context_event != NULL)
	{
		priv->idle_worker =
			g_idle_add ((GSourceFunc) idle_unref_context_event, window);
	}
}

static void
embed_popup_deactivate_cb (GtkWidget *popup,
			   BlxWindow *window)
{
	LOG ("Deactivating popup menu");

	enable_edit_actions_sensitivity (window);

	g_signal_handlers_disconnect_by_func
		(popup, G_CALLBACK (embed_popup_deactivate_cb), window);

	_blx_window_unset_context_event (window);
}

static char *
get_name_from_address_value (const char *path)
{
	char *name;

	name = g_path_get_basename (path);

	return name != NULL ? name : g_strdup ("");
}

static void
update_popups_tooltips (BlxWindow *window, GdkEventButton *event, WebKitHitTestResult *hit_test_result)
{
	guint context;
	GtkActionGroup *group = window->priv->popups_action_group;
	GtkAction *action;
	char *tooltip, *name;

	 g_object_get (hit_test_result, "context", &context, NULL);

	if (context & WEBKIT_HIT_TEST_RESULT_CONTEXT_IMAGE)
	{
		char *uri;
		g_object_get (hit_test_result, "image-uri", &uri, NULL);
		name = get_name_from_address_value (uri);

		action = gtk_action_group_get_action (group, "OpenImage");
		tooltip = g_strdup_printf (_("Open image “%s”"), name);
		g_object_set (action, "tooltip", tooltip, NULL);
		g_free (tooltip);

		action = gtk_action_group_get_action (group, "SetImageAsBackground");
		tooltip = g_strdup_printf (_("Use as desktop background “%s”"), name);
		g_object_set (action, "tooltip", tooltip, NULL);
		g_free (tooltip);

		action = gtk_action_group_get_action (group, "SaveImageAs");
		tooltip = g_strdup_printf (_("Save image “%s”"), name);
		g_object_set (action, "tooltip", tooltip, NULL);
		g_free (tooltip);

		action = gtk_action_group_get_action (group, "CopyImageLocation");
		tooltip = g_strdup_printf (_("Copy image address “%s”"), uri);
		g_object_set (action, "tooltip", tooltip, NULL);
		g_free (tooltip);		

		g_free (uri);
		g_free (name);
	}

#if 0
	if (context & BLX_EMBED_CONTEXT_EMAIL_LINK)
	{
		value = blx_embed_event_get_property (event, "link");

		action = gtk_action_group_get_action (group, "SendEmail");
		tooltip = g_strdup_printf (_("Send email to address “%s”"),
					   g_value_get_string (value));
		g_object_set (action, "tooltip", tooltip, NULL);
		g_free (tooltip);

		action = gtk_action_group_get_action (group, "CopyEmailAddress");
		tooltip = g_strdup_printf (_("Copy email address “%s”"),
					   g_value_get_string (value));
		g_object_set (action, "tooltip", tooltip, NULL);
		g_free (tooltip);
	}
#endif

	if (context & WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK)
	{
		char *uri;
		g_object_get (hit_test_result, "link-uri", &uri, NULL);

		action = gtk_action_group_get_action (group, "DownloadLink");
		name = get_name_from_address_value (uri);
		tooltip = g_strdup_printf (_("Save link “%s”"), name);
		g_object_set (action, "tooltip", tooltip, NULL);
		g_free (name);
		g_free (tooltip);

		action = gtk_action_group_get_action (group, "BookmarkLink");
		tooltip = g_strdup_printf (_("Bookmark link “%s”"), uri);
		g_object_set (action, "tooltip", tooltip, NULL);
		g_free (tooltip);

		action = gtk_action_group_get_action (group, "CopyLinkAddress");
		tooltip = g_strdup_printf (_("Copy link's address “%s”"), uri);
		g_object_set (action, "tooltip", tooltip, NULL);
		g_free (tooltip);
		g_free (uri);
	}
}

static void
show_embed_popup (BlxWindow *window,
		  WebKitWebView *view,
		  GdkEventButton *event,
		  WebKitHitTestResult *hit_test_result)
{
	BlxWindowPrivate *priv = window->priv;
	GtkActionGroup *action_group;
	GtkAction *action;
	guint context;
	const char *popup;
	gboolean framed = FALSE, can_open_in_new;
	GtkWidget *widget;
	guint button;
	char *uri;
	BlxEmbedEvent *embed_event;

#if 0
	value = blx_embed_event_get_property (event, "framed_page");
	framed = g_value_get_int (value);
#endif

	g_object_get (hit_test_result, "link-uri", &uri, NULL);
	can_open_in_new = uri && blx_embed_utils_address_has_web_scheme (uri);
	g_free (uri);

	g_object_get (hit_test_result, "context", &context, NULL);

	LOG ("show_embed_popup context %x", context);

#if 0
	if (context & BLX_EMBED_CONTEXT_EMAIL_LINK)
	{
		popup = "/BlxEmailLinkPopup";
		update_edit_actions_sensitivity (window, TRUE);
	}
#endif
	if (context & WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK)
	{
		popup = "/BlxLinkPopup";
		update_edit_actions_sensitivity (window, TRUE);
	}
	else if (context & WEBKIT_HIT_TEST_RESULT_CONTEXT_EDITABLE)
	{
		popup = "/BlxInputPopup";
		update_edit_actions_sensitivity (window, FALSE);
	}
	else
	{
		popup = "/BlxDocumentPopup";
		update_edit_actions_sensitivity (window, TRUE);
	}

	update_popups_tooltips (window, event, hit_test_result);

	widget = gtk_ui_manager_get_widget (priv->manager, popup);
	g_return_if_fail (widget != NULL);

	action_group = window->priv->popups_action_group;

	action = gtk_action_group_get_action (action_group, "OpenLinkInNewWindow");
	gtk_action_set_sensitive (action, can_open_in_new);

	action = gtk_action_group_get_action (action_group, "OpenLinkInNewTab");
	blx_action_change_sensitivity_flags (action, SENS_FLAG_CONTEXT, !can_open_in_new);
	
	update_popup_actions_visibility (window,
					 view,
					 context,
					 framed);

	embed_event = blx_embed_event_new (event, hit_test_result);
	_blx_window_set_context_event (window, embed_event);
	g_object_unref (embed_event);

	g_signal_connect (widget, "deactivate",
			  G_CALLBACK (embed_popup_deactivate_cb), window);

	button = event->button;

	if (button == 0)
	{
		gtk_menu_popup (GTK_MENU (widget), NULL, NULL,
				popup_menu_at_coords, window, 0,
				gtk_get_current_event_time ());
		gtk_menu_shell_select_first (GTK_MENU_SHELL (widget), FALSE);
	}
	else
	{
		gtk_menu_popup (GTK_MENU (widget), NULL, NULL,
				NULL, NULL, button,
				gtk_get_current_event_time ());
	}
}

static gboolean
save_property_url (BlxEmbed *embed,
		   GdkEventButton *gdk_event,
		   WebKitHitTestResult *hit_test_result,
		   const char *property)
{
	const char *location;
	GValue value = { 0, };
	BlxEmbedEvent *event = blx_embed_event_new (gdk_event, hit_test_result);
	gboolean retval;

	blx_embed_event_get_property (event, property, &value);
	location = g_value_get_string (&value);

	LOG ("Location: %s", location);

	retval = blx_embed_utils_address_has_web_scheme (location);

	if (retval)
		blx_embed_auto_download_url (embed, location);

	g_value_unset (&value);
	g_object_unref (event);

	return retval;
}

typedef struct
{
	BlxWindow *window;
	BlxEmbed *embed;
} ClipboardTextCBData;

static void
clipboard_text_received_cb (GtkClipboard *clipboard,
			    const char *text,
			    ClipboardTextCBData *data)
{
	if (data->embed != NULL && text != NULL)
	{
		blx_link_open (BLX_LINK (data->window), text, data->embed, 0);
	}

	if (data->embed != NULL)
	{
		BlxEmbed **embed_ptr = &(data->embed);
		g_object_remove_weak_pointer (G_OBJECT (data->embed), (gpointer *) embed_ptr);
	}

	g_slice_free (ClipboardTextCBData, data);
}

static gboolean
blx_window_dom_mouse_click_cb (WebKitWebView *view,
				GdkEventButton *event,
				BlxWindow *window)
{
	guint button, modifier, context;
	gboolean handled = FALSE;
	gboolean with_control, with_shift;
	gboolean is_left_click, is_middle_click;
	gboolean is_link, is_image, is_middle_clickable;
	gboolean middle_click_opens;
	gboolean is_input;
	WebKitHitTestResult *hit_test_result;

	hit_test_result = webkit_web_view_get_hit_test_result (view, event);
	button = event->button;

	if (button == 3)
	{
		show_embed_popup (window, view, event, hit_test_result);
		g_object_unref (hit_test_result);
		return TRUE;
	}

	modifier = event->state;
	g_object_get (hit_test_result, "context", &context, NULL);

	LOG ("blx_window_dom_mouse_click_cb: button %d, context %d, modifier %d (%d:%d)",
	     button, context, modifier, (int)event->x, (int)event->y);

	with_control = (modifier & GDK_CONTROL_MASK) == GDK_CONTROL_MASK;
	with_shift = (modifier & GDK_SHIFT_MASK) == GDK_SHIFT_MASK;

	is_left_click = (button == 1);
	is_middle_click = (button == 2);

	middle_click_opens =
		g_settings_get_boolean (BLX_SETTINGS_MAIN,
					BLX_PREFS_MIDDLE_CLICK_OPENS_URL) &&
		!g_settings_get_boolean
				(BLX_SETTINGS_LOCKDOWN,
				 BLX_PREFS_LOCKDOWN_ARBITRARY_URL);

	is_link = (context & WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK) != 0;
	is_image = (context & WEBKIT_HIT_TEST_RESULT_CONTEXT_IMAGE) != 0;
	is_middle_clickable = !((context & WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK)
				|| (context & WEBKIT_HIT_TEST_RESULT_CONTEXT_EDITABLE));
	is_input = (context & WEBKIT_HIT_TEST_RESULT_CONTEXT_EDITABLE) != 0;

	if (is_left_click && with_shift && !with_control)
	{
		/* shift+click saves the link target */
		if (is_link)
		{
			handled = save_property_url (BLX_GET_EMBED_FROM_BLX_WEB_VIEW (view), event, hit_test_result, "link-uri");
		}

		/* Note: pressing enter to submit a form synthesizes a mouse
		 * click event
		 */
		/* shift+click saves the non-link image */
		else if (is_image && !is_input)
		{
			handled = save_property_url (BLX_GET_EMBED_FROM_BLX_WEB_VIEW (view), event, hit_test_result, "image-uri");
		}
	}

	/* middle click opens the selection url */
	if (is_middle_clickable && is_middle_click && middle_click_opens)
	{
		/* See bug #133633 for why we do it this way */

		/* We need to make sure we know if the embed is destroyed
		 * between requesting the clipboard contents, and receiving
		 * them.
		 */
		ClipboardTextCBData *cb_data;
		BlxEmbed *embed;
		BlxEmbed **embed_ptr;

		embed = BLX_GET_EMBED_FROM_BLX_WEB_VIEW (view);

		cb_data = g_slice_new0 (ClipboardTextCBData);
		cb_data->embed = embed;
		cb_data->window = window;
		embed_ptr = &cb_data->embed;
		
		g_object_add_weak_pointer (G_OBJECT (embed), (gpointer *) embed_ptr);

		gtk_clipboard_request_text
			(gtk_widget_get_clipboard (GTK_WIDGET (embed),
						   GDK_SELECTION_PRIMARY),
			 (GtkClipboardTextReceivedFunc) clipboard_text_received_cb,
			 cb_data);

		handled = TRUE;
	}

	g_object_unref (hit_test_result);
	return handled;
}

static void
blx_window_visibility_cb (BlxEmbed *embed, GParamSpec *pspec, BlxWindow *window)
{
	gboolean visibility;

	visibility = blx_web_view_get_visibility (blx_embed_get_web_view (embed));

	if (visibility)
		gtk_widget_show (GTK_WIDGET (window));
	else
		gtk_widget_hide (GTK_WIDGET (window));
}

static void
blx_window_set_is_popup (BlxWindow *window,
			  gboolean is_popup)
{
	BlxWindowPrivate *priv = window->priv;

	priv->is_popup = is_popup;

	g_object_notify (G_OBJECT (window), "is-popup");
}

static gboolean
web_view_ready_cb (WebKitWebView *web_view,
		   WebKitWebView *parent_web_view)
{
	BlxWindow *window, *parent_view_window;
	gboolean using_new_window;

	window = BLX_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (web_view)));
	parent_view_window = BLX_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (parent_web_view)));

	using_new_window = window != parent_view_window;

	if (using_new_window)
	{
		int width, height;
		gboolean toolbar_visible;
		gboolean menubar_visible;
		BlxWebViewChrome chrome_mask;
		WebKitWebWindowFeatures *features;

		toolbar_visible = menubar_visible = TRUE;
		features = webkit_web_view_get_window_features (web_view);

		chrome_mask = window->priv->chrome;

		g_object_get (features,
			      "width", &width,
			      "height", &height,
			      "toolbar-visible", &toolbar_visible,
			      "menubar-visible", &menubar_visible,
			      NULL);

		if (!toolbar_visible)
			chrome_mask &= ~BLX_WEB_VIEW_CHROME_TOOLBAR;

		if (!menubar_visible)
			chrome_mask &= ~BLX_WEB_VIEW_CHROME_MENUBAR;


		/* We will consider windows with different chrome settings popups. */
		if (chrome_mask != window->priv->chrome) {
			gtk_window_set_default_size (GTK_WINDOW (window), width, height);

			window->priv->is_popup = TRUE;
			window->priv->chrome = chrome_mask;

			update_chromes_actions (window);
			sync_chromes_visibility (window);
		}

		g_signal_emit_by_name (parent_web_view, "new-window", web_view);
	}

	gtk_widget_show (GTK_WIDGET (window));

	return TRUE;
}

static WebKitWebView*
create_web_view_cb (WebKitWebView *web_view,
		    WebKitWebFrame *frame,
		    BlxWindow *window)
{
	BlxEmbed *embed;
	WebKitWebView *new_web_view;
	BlxNewTabFlags flags;
	BlxWindow *parent_window;

	if (g_settings_get_boolean (BLX_SETTINGS_MAIN,
				    BLX_PREFS_NEW_WINDOWS_IN_TABS))
	{
		parent_window = window;
		flags = BLX_NEW_TAB_IN_EXISTING_WINDOW |
			BLX_NEW_TAB_JUMP |
			BLX_NEW_TAB_APPEND_AFTER;
	}
	else
	{
		parent_window = NULL;
		flags = BLX_NEW_TAB_IN_NEW_WINDOW |
			BLX_NEW_TAB_DONT_SHOW_WINDOW;
	}

	embed = blx_shell_new_tab_full (blx_shell_get_default (),
					 parent_window,
					 BLX_GET_EMBED_FROM_BLX_WEB_VIEW (web_view),
					 NULL,
					 flags,
					 BLX_WEB_VIEW_CHROME_ALL,
					 FALSE, /* is popup? */
					 0);

	new_web_view = BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed);
	g_signal_connect (new_web_view, "web-view-ready",
			  G_CALLBACK (web_view_ready_cb),
			  web_view);

	return new_web_view;
}

static gboolean
policy_decision_required_cb (WebKitWebView *web_view,
			     WebKitWebFrame *web_frame,
			     WebKitNetworkRequest *request,
			     WebKitWebNavigationAction *action,
			     WebKitWebPolicyDecision *decision,
			     BlxWindow *window)
{
	WebKitWebNavigationReason reason;
	gint button;
	gint state;
	const char *uri;

	reason = webkit_web_navigation_action_get_reason (action);
	button = webkit_web_navigation_action_get_button (action);
	state = webkit_web_navigation_action_get_modifier_state (action);
	uri = webkit_network_request_get_uri (request);

	if (g_str_has_prefix (uri, "mailto:")) {
		webkit_web_policy_decision_ignore (decision);
		gtk_show_uri (NULL, uri, GDK_CURRENT_TIME, NULL);
		return TRUE;
	}

	if (reason == WEBKIT_WEB_NAVIGATION_REASON_LINK_CLICKED &&
	    blx_embed_shell_get_mode (embed_shell) == BLX_EMBED_SHELL_MODE_APPLICATION)
	{
		/* The only thing we allow here is to either navigate
		 * in the same window and tab to the current domain,
		 * or launch a new (non app mode) blxbrowser instance
		 * for all the other cases. */
		gboolean return_value;
		SoupURI *soup_uri = soup_uri_new (uri);
		SoupURI *current_soup_uri = soup_uri_new (webkit_web_view_get_uri (web_view));

		if (g_str_equal (soup_uri->host, current_soup_uri->host))
		{
			return_value = FALSE;
		}
		else
		{
			char *command_line;
			GError *error = NULL;

			return_value = TRUE;
			/* A gross hack to be able to launch blxbrowser from within
			 * Blxbrowser. Might be a good idea to figure out a better
			 * solution... */
			g_unsetenv (BLX_UUID_ENVVAR);
			command_line = g_strdup_printf ("gvfs-open %s", uri);
			g_spawn_command_line_async (command_line, &error);

			if (error)
			{
				g_debug ("Error opening %s: %s", uri, error->message);
				g_error_free (error);
			}

			g_free (command_line);
		}

		soup_uri_free (soup_uri);
		soup_uri_free (current_soup_uri);

		return return_value;
	}

	if (reason == WEBKIT_WEB_NAVIGATION_REASON_LINK_CLICKED) {
		BlxEmbed *embed;
		BlxNewTabFlags flags;

		flags = BLX_NEW_TAB_OPEN_PAGE;

		/* New tab in new window for control+shift+click */
		if (button == 1 &&
		    state == (GDK_SHIFT_MASK | GDK_CONTROL_MASK))
		{
			flags |= BLX_NEW_TAB_IN_NEW_WINDOW;
		}
		/* New tab in existing window for middle click and
		 * control+click */
		else if (button == 2 ||
			 (button == 1 && state == GDK_CONTROL_MASK))
		{
			flags |= BLX_NEW_TAB_IN_EXISTING_WINDOW | BLX_NEW_TAB_APPEND_AFTER;
		}
		/* Because we connect to button-press-event *after*
		 * (G_CONNECT_AFTER) we need to prevent WebKit from browsing to
		 * a link when you shift+click it. Otherwise when you
		 * shift+click a link to download it you would also be taken to
		 * the link destination. */
		else if (button == 1 && state == GDK_SHIFT_MASK)
		{
			return TRUE;
		}
		/* Those were our special cases, we won't handle this */
		else
		{
			return FALSE;
		}

		embed = blx_embed_container_get_active_child
			(BLX_EMBED_CONTAINER (window));

		blx_shell_new_tab_full (blx_shell_get_default (),
					 window,
					 embed,
					 request,
					 flags,
					 BLX_WEB_VIEW_CHROME_ALL, FALSE, 0);

		return TRUE;
	}

	return FALSE;
}

static void
blx_window_set_active_tab (BlxWindow *window, BlxEmbed *new_embed)
{
	BlxEmbed *old_embed;
	BlxEmbed *embed;

	g_return_if_fail (BLX_IS_WINDOW (window));
	g_return_if_fail (gtk_widget_get_toplevel (GTK_WIDGET (new_embed)) == GTK_WIDGET (window));

	old_embed = window->priv->active_embed;

	if (old_embed == new_embed) return;

	if (old_embed != NULL)
	{
		WebKitWebView *web_view;
		BlxWebView *view;
		guint sid;

		embed = old_embed;
		web_view = BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed);
		view = BLX_WEB_VIEW (web_view);

		g_signal_handlers_disconnect_by_func (web_view,
						      G_CALLBACK (sync_tab_zoom),
						      window);
		g_signal_handlers_disconnect_by_func (web_view,
						      G_CALLBACK (scroll_event_cb),
						      window);
		g_signal_handlers_disconnect_by_func (web_view,
						      G_CALLBACK (create_web_view_cb),
						      window);
		sid = g_signal_lookup ("navigation-policy-decision-requested",
				       WEBKIT_TYPE_WEB_VIEW);
		g_signal_handlers_disconnect_matched (web_view,
						      G_SIGNAL_MATCH_ID |
						      G_SIGNAL_MATCH_FUNC,
						      sid,
						      0, NULL,
						      G_CALLBACK (policy_decision_required_cb),
						      NULL);
		sid = g_signal_lookup ("new-window-policy-decision-requested",
				       WEBKIT_TYPE_WEB_VIEW);
		g_signal_handlers_disconnect_matched (web_view,
						      G_SIGNAL_MATCH_ID |
						      G_SIGNAL_MATCH_FUNC,
						      sid,
						      0, NULL,
						      G_CALLBACK (policy_decision_required_cb),
						      NULL);

		g_signal_handlers_disconnect_by_func (view,
						      G_CALLBACK (sync_tab_popup_windows),
						      window);
		g_signal_handlers_disconnect_by_func (view,
						      G_CALLBACK (sync_tab_popups_allowed),
						      window);
		g_signal_handlers_disconnect_by_func (view,
						      G_CALLBACK (sync_tab_security),
						      window);
		g_signal_handlers_disconnect_by_func (view,
						      G_CALLBACK (sync_tab_document_type),
						      window);
		g_signal_handlers_disconnect_by_func (view,
						      G_CALLBACK (sync_tab_load_status),
						      window);
		g_signal_handlers_disconnect_by_func (view,
						      G_CALLBACK (sync_tab_navigation),
						      window);
		g_signal_handlers_disconnect_by_func (view,
						      G_CALLBACK (sync_tab_title),
						      window);
		g_signal_handlers_disconnect_by_func (view,
						      G_CALLBACK (sync_tab_address),
						      window);
		g_signal_handlers_disconnect_by_func (view,
						      G_CALLBACK (sync_tab_icon),
						      window);
		g_signal_handlers_disconnect_by_func (view,
						      G_CALLBACK (blx_window_visibility_cb),
						      window);

		g_signal_handlers_disconnect_by_func
			(view, G_CALLBACK (blx_window_dom_mouse_click_cb), window);

	}

	window->priv->active_embed = new_embed;

	if (new_embed != NULL)
	{
		WebKitWebView *web_view;
		BlxWebView *view;

		embed = new_embed;
		view = blx_embed_get_web_view (embed);

		sync_tab_security	(view, NULL, window);
		sync_tab_document_type	(view, NULL, window);
		sync_tab_load_progress	(view, NULL, window);
		sync_tab_load_status	(view, NULL, window);
		sync_tab_navigation	(view, NULL, window);
		sync_tab_title		(view, NULL, window);
		sync_tab_address	(view, NULL, window);
		sync_tab_icon		(view, NULL, window);
		sync_tab_popup_windows	(view, NULL, window);
		sync_tab_popups_allowed	(view, NULL, window);

		web_view = BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed);
		view = BLX_WEB_VIEW (web_view);

		sync_tab_zoom		(web_view, NULL, window);

		g_signal_connect_object (web_view, "notify::zoom-level",
					 G_CALLBACK (sync_tab_zoom),
					 window, 0);
		/* FIXME: we should set our own handler for
		   scroll-event, but right now it's pointless because
		   GtkScrolledWindow will eat all the events, even
		   those with modifier keys we want to catch to zoom
		   in and out. See bug #562630
		*/
		g_signal_connect_object (web_view, "scroll-event",
					 G_CALLBACK (scroll_event_cb),
					 window, 0);
		g_signal_connect_object (web_view, "create-web-view",
					 G_CALLBACK (create_web_view_cb),
					 window, 0);
		g_signal_connect_object (web_view, "navigation-policy-decision-requested",
					 G_CALLBACK (policy_decision_required_cb),
					 window, 0);
		g_signal_connect_object (web_view, "new-window-policy-decision-requested",
					 G_CALLBACK (policy_decision_required_cb),
					 window, 0);

		g_signal_connect_object (view, "notify::hidden-popup-count",
					 G_CALLBACK (sync_tab_popup_windows),
					 window, 0);
		g_signal_connect_object (view, "notify::popups-allowed",
					 G_CALLBACK (sync_tab_popups_allowed),
					 window, 0);
		g_signal_connect_object (view, "notify::embed-title",
					 G_CALLBACK (sync_tab_title),
					 window, 0);
		g_signal_connect_object (view, "notify::address",
					 G_CALLBACK (sync_tab_address),
					 window, 0);
		g_signal_connect_object (view, "notify::icon",
					 G_CALLBACK (sync_tab_icon),
					 window, 0);
		g_signal_connect_object (view, "notify::security-level",
					 G_CALLBACK (sync_tab_security),
					 window, 0);
		g_signal_connect_object (view, "notify::document-type",
					 G_CALLBACK (sync_tab_document_type),
					 window, 0);
		g_signal_connect_object (view, "notify::progress",
					 G_CALLBACK (sync_tab_load_progress),
					 window, 0);
		g_signal_connect_object (view, "notify::load-status",
					 G_CALLBACK (sync_tab_load_status),
					 window, 0);
		g_signal_connect_object (view, "notify::navigation",
					 G_CALLBACK (sync_tab_navigation),
					 window, 0);
		g_signal_connect_object (view, "notify::progress",
					 G_CALLBACK (sync_tab_load_progress),
					 window, 0);
		/* We run our button-press-event after the default
		 * handler to make sure pages have a chance to perform
		 * their own handling - for instance, have their own
		 * context menus, or provide specific functionality
		 * for the right mouse button */
		g_signal_connect_object (view, "button-press-event",
					 G_CALLBACK (blx_window_dom_mouse_click_cb),
					 window, G_CONNECT_AFTER);
		g_signal_connect_object (view, "notify::visibility",
					 G_CALLBACK (blx_window_visibility_cb),
					 window, 0);

		g_object_notify (G_OBJECT (window), "active-child");
	}
}

static gboolean
embed_modal_alert_cb (BlxEmbed *embed,
		      BlxWindow *window)
{
	const char *address;

	/* switch the window to the tab, and bring the window to the foreground
	 * (since the alert is modal, the user won't be able to do anything
	 * with his current window anyway :|)
	 */
	impl_set_active_child (BLX_EMBED_CONTAINER (window), embed);
	gtk_window_present (GTK_WINDOW (window));

	/* make sure the location entry shows the real URL of the tab's page */
	address = blx_web_view_get_address (blx_embed_get_web_view (embed));
	_blx_window_set_location (window, address);

	/* don't suppress alert */
	return FALSE;
}

static gboolean
idle_tab_remove_cb (GtkWidget *tab)
{
	GtkWidget *toplevel;
	BlxWindow *window;
	BlxWindowPrivate *priv;
	GtkNotebook *notebook;
	int position;

	toplevel = gtk_widget_get_toplevel (tab);
	if (!BLX_IS_WINDOW (toplevel)) return FALSE; /* FIXME should this ever occur? */

	window = BLX_WINDOW (toplevel);
	priv = window->priv;

	if (priv->closing) return FALSE;

	g_hash_table_remove (priv->tabs_to_remove, tab);

	notebook = GTK_NOTEBOOK (blx_window_get_notebook (window));

	position = gtk_notebook_page_num (notebook, tab);
	gtk_notebook_remove_page (notebook, position);

	/* don't run again */
	return FALSE;
}

static void
schedule_tab_close (BlxWindow *window,
		    BlxEmbed *embed)
{
	BlxWindowPrivate *priv = window->priv;
	guint id;

	LOG ("scheduling close of embed %p in window %p", embed, window);

	if (priv->closing) return;

	if (g_hash_table_lookup (priv->tabs_to_remove, embed) != NULL) return;

	/* do this on idle, because otherwise we'll crash in certain circumstances
	* (see galeon bug #116256)
	*/
	id = g_idle_add_full (G_PRIORITY_HIGH_IDLE,
			      (GSourceFunc) idle_tab_remove_cb,
			      embed, NULL);

	g_hash_table_insert (priv->tabs_to_remove, embed, GUINT_TO_POINTER (id));

	/* don't wait until idle to hide the window */
	if (g_hash_table_size (priv->tabs_to_remove) == priv->num_tabs)
	{
		gtk_widget_hide (GTK_WIDGET (window));
	}
}

static gboolean
embed_close_request_cb (BlxEmbed *embed,
			BlxWindow *window)
{
	LOG ("embed_close_request_cb embed %p window %p", embed, window);

	schedule_tab_close (window, embed);

	/* handled */
	return TRUE;
}

static gboolean
show_notebook_popup_menu (GtkNotebook *notebook,
			  BlxWindow *window,
			  GdkEventButton *event)
{
	GtkWidget *menu, *tab, *tab_label;
	GtkAction *action;

	menu = gtk_ui_manager_get_widget (window->priv->manager, "/BlxNotebookPopup");
	g_return_val_if_fail (menu != NULL, FALSE);

	/* allow extensions to sync when showing the popup */
	action = gtk_action_group_get_action (window->priv->action_group,
					      "NotebookPopupAction");
	g_return_val_if_fail (action != NULL, FALSE);
	gtk_action_activate (action);

	if (event != NULL)
	{
		gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
				NULL, NULL,
				event->button, event->time);
	}
	else
	{
		tab = GTK_WIDGET (window->priv->active_embed);
		tab_label = gtk_notebook_get_tab_label (notebook, tab);

		gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
				blx_gui_menu_position_under_widget, tab_label,
				0, gtk_get_current_event_time ());
		gtk_menu_shell_select_first (GTK_MENU_SHELL (menu), FALSE);
	}

	return TRUE;
}

static gboolean
notebook_button_press_cb (GtkNotebook *notebook,
			  GdkEventButton *event,
			  BlxWindow *window)
{
	if (GDK_BUTTON_PRESS == event->type && 3 == event->button)
	{
		return show_notebook_popup_menu (notebook, window, event);
	}

	return FALSE;
}

static gboolean
notebook_popup_menu_cb (GtkNotebook *notebook,
			BlxWindow *window)
{
	/* Only respond if the notebook is the actual focus */
	if (BLX_IS_NOTEBOOK (gtk_window_get_focus (GTK_WINDOW (window))))
	{
		return show_notebook_popup_menu (notebook, window, NULL);
	}

	return FALSE;
}

static gboolean
present_on_idle_cb (GtkWindow *window)
{
      gtk_window_present (window);
      return FALSE;
}

static void
notebook_page_added_cb (BlxNotebook *notebook,
			BlxEmbed *embed,
			guint position,
			BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	BlxExtension *manager;

	LOG ("page-added   notebook %p embed %p position %u\n", notebook, embed, position);

	g_return_if_fail (BLX_IS_EMBED (embed));

	priv->num_tabs++;

#if 0
	g_signal_connect_object (embed, "open-link",
				 G_CALLBACK (blx_link_open), window,
				 G_CONNECT_SWAPPED);
#endif

	g_signal_connect_object (blx_embed_get_web_view (embed), "close-request",
				 G_CALLBACK (embed_close_request_cb),
				 window, 0);
	g_signal_connect_object (blx_embed_get_web_view (embed), "ge-modal-alert",
				 G_CALLBACK (embed_modal_alert_cb), window, G_CONNECT_AFTER);

	/* Let the extensions attach themselves to the tab */
	manager = BLX_EXTENSION (blx_shell_get_extensions_manager (blx_shell));
	blx_extension_attach_tab (manager, window, embed);

        if (priv->present_on_insert)
        {
                priv->present_on_insert = FALSE;
                g_idle_add ((GSourceFunc) present_on_idle_cb, g_object_ref (window));
        }
}

static void
notebook_page_removed_cb (BlxNotebook *notebook,
			  BlxEmbed *embed,
			  guint position,
			  BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	BlxExtension *manager;

	LOG ("page-removed notebook %p embed %p position %u\n", notebook, embed, position);

	if (priv->closing) return;

	g_return_if_fail (BLX_IS_EMBED (embed));

	/* Let the extensions remove themselves from the tab */
	manager = BLX_EXTENSION (blx_shell_get_extensions_manager (blx_shell));
	blx_extension_detach_tab (manager, window, embed);

#if 0
	g_signal_handlers_disconnect_by_func (G_OBJECT (embed),
					      G_CALLBACK (blx_link_open),
					      window);	
#endif

	priv->num_tabs--;

	g_signal_handlers_disconnect_by_func
		(blx_embed_get_web_view (embed), G_CALLBACK (embed_modal_alert_cb), window);
	g_signal_handlers_disconnect_by_func
		(blx_embed_get_web_view (embed), G_CALLBACK (embed_close_request_cb), window);
}

static void
notebook_page_close_request_cb (BlxNotebook *notebook,
				BlxEmbed *embed,
				BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;

	if (gtk_notebook_get_n_pages (priv->notebook) == 1)
	{
		if (g_settings_get_boolean (BLX_SETTINGS_LOCKDOWN,
					    BLX_PREFS_LOCKDOWN_QUIT))
		{
			return;
		}
		if (window_has_ongoing_downloads (window) &&
		    !confirm_close_with_downloads (window))
		{
			return;
		}
	}

	if ((!blx_web_view_has_modified_forms (blx_embed_get_web_view (embed)) ||
	     confirm_close_with_modified_forms (window)))
	{
		gtk_widget_destroy (GTK_WIDGET (embed));
	}
}

static GtkWidget *
notebook_create_window_cb (GtkNotebook *notebook,
			   GtkWidget *page,
                           int x,
                           int y,
                           BlxWindow *window)
{
  BlxWindow *new_window;
  BlxWindowPrivate *new_priv;

  new_window = blx_window_new ();
  new_priv = new_window->priv;

  new_priv->present_on_insert = TRUE;

  return blx_window_get_notebook (new_window);
}

static GtkNotebook *
setup_notebook (BlxWindow *window)
{
	GtkNotebook *notebook;

	notebook = GTK_NOTEBOOK (g_object_new (BLX_TYPE_NOTEBOOK, NULL));

	g_signal_connect_after (notebook, "switch-page",
				G_CALLBACK (notebook_switch_page_cb),
				window);
        g_signal_connect (notebook, "create-window",
                          G_CALLBACK (notebook_create_window_cb),
                          window);

	g_signal_connect (notebook, "popup-menu",
			  G_CALLBACK (notebook_popup_menu_cb), window);
	g_signal_connect (notebook, "button-press-event",
			  G_CALLBACK (notebook_button_press_cb), window);

	g_signal_connect (notebook, "page-added",
			  G_CALLBACK (notebook_page_added_cb), window);
	g_signal_connect (notebook, "page-removed",
			  G_CALLBACK (notebook_page_removed_cb), window);
	g_signal_connect (notebook, "tab-close-request",
			  G_CALLBACK (notebook_page_close_request_cb), window);

	return notebook;
}

static void
blx_window_set_chrome (BlxWindow *window, BlxWebViewChrome mask)
{
	BlxWebViewChrome chrome_mask = mask;

	if (mask == BLX_WEB_VIEW_CHROME_ALL)
	{
		window->priv->should_save_chrome = TRUE;
	}

	if (!g_settings_get_boolean (BLX_SETTINGS_UI,
				     BLX_PREFS_UI_SHOW_TOOLBARS))
	{
		chrome_mask &= ~BLX_WEB_VIEW_CHROME_TOOLBAR;
	}

	if (g_settings_get_boolean (BLX_SETTINGS_LOCKDOWN,
				    BLX_PREFS_LOCKDOWN_MENUBAR))
	{
		chrome_mask &= ~BLX_WEB_VIEW_CHROME_MENUBAR;
	}

	window->priv->chrome = chrome_mask;
}

static void
download_added_cb (BlxEmbedShell *shell,
		   BlxDownload *download,
		   gpointer data)
{
	BlxWindow *window = BLX_WINDOW (data);
	GtkWidget *download_window;
	GtkWidget *widget;

	download_window = blx_download_get_window (download);
	widget = blx_download_get_widget (download);

	if (widget == NULL &&
	    (download_window == NULL || download_window == GTK_WIDGET (window)))
	{
		widget = blx_download_widget_new (download);
		gtk_box_pack_start (GTK_BOX (window->priv->downloads_box),
				    widget, FALSE, FALSE, 0);
		gtk_widget_show (widget);
		blx_window_set_downloads_box_visibility (window, TRUE);
	}
}

static void
downloads_removed_cb (GtkContainer *container,
		      GtkWidget *widget,
		      gpointer data)
{
	BlxWindow *window = BLX_WINDOW (data);
	GList *children = NULL;

	children = gtk_container_get_children (container);
	if (g_list_length (children) == 1)
		blx_window_set_downloads_box_visibility (window, FALSE);

	g_list_free (children);
}

static void
downloads_close_cb (GtkButton *button, BlxWindow *window)
{
	GList *l, *downloads;

	downloads = gtk_container_get_children (GTK_CONTAINER (window->priv->downloads_box));

	for (l = downloads; l != NULL; l = l->next)
	{
		BlxDownload *download;
		WebKitDownloadStatus status;

		if (BLX_IS_DOWNLOAD_WIDGET (l->data) != TRUE)
			continue;

		download = blx_download_widget_get_download (BLX_DOWNLOAD_WIDGET (l->data));
		status = webkit_download_get_status (blx_download_get_webkit_download (download));

		if (status == WEBKIT_DOWNLOAD_STATUS_FINISHED)
		{
			gtk_widget_destroy (GTK_WIDGET (l->data));
		}
	}
	g_list_free (downloads);

	blx_window_set_downloads_box_visibility (window, FALSE);
}

static GtkWidget *
setup_downloads_box (BlxWindow *window)
{
	GtkWidget *widget;
	GtkWidget *close_button;
	GtkWidget *image;

	widget = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
	close_button = gtk_button_new ();
	image = gtk_image_new_from_stock (GTK_STOCK_CLOSE, GTK_ICON_SIZE_BUTTON);

	gtk_button_set_relief (GTK_BUTTON (close_button), GTK_RELIEF_NONE);

	gtk_container_add (GTK_CONTAINER (close_button), image);
	gtk_box_pack_end (GTK_BOX (widget), close_button, FALSE, FALSE, 4);

	gtk_widget_set_margin_right (widget, 20);

	g_signal_connect (close_button, "clicked",
			  G_CALLBACK (downloads_close_cb), window);
	g_signal_connect (widget, "remove",
			  G_CALLBACK (downloads_removed_cb), window);

	gtk_widget_show_all (close_button);

	return widget;
}

void
blx_window_set_downloads_box_visibility (BlxWindow *window,
					  gboolean show)
{
	if (show)
		gtk_widget_show (window->priv->downloads_box);
	else
		gtk_widget_hide (window->priv->downloads_box);
}

static void
blx_window_dispose (GObject *object)
{
	BlxWindow *window = BLX_WINDOW (object);
	BlxWindowPrivate *priv = window->priv;
	GObject *single;
	GSList *popups;

	LOG ("BlxWindow dispose %p", window);

	/* Only do these once */
	if (window->priv->closing == FALSE)
	{
		BlxExtension *manager;

		window->priv->closing = TRUE;

		/* Let the extensions detach themselves from the window */
		manager = BLX_EXTENSION (blx_shell_get_extensions_manager (blx_shell));
		blx_extension_detach_window (manager, window);
		blx_bookmarks_ui_detach_window (window);

		g_signal_handlers_disconnect_by_func
			(embed_shell, download_added_cb, window);

		/* Deactivate menus */
		popups = gtk_ui_manager_get_toplevels (window->priv->manager, GTK_UI_MANAGER_POPUP);
		g_slist_foreach (popups, (GFunc) gtk_menu_shell_deactivate, NULL);
		g_slist_free (popups);
	
		single = blx_embed_shell_get_embed_single (embed_shell);
		g_signal_handlers_disconnect_by_func
			(single, G_CALLBACK (sync_network_status), window);
	
		g_hash_table_remove_all (priv->tabs_to_remove);

		g_object_unref (priv->enc_menu);
		priv->enc_menu = NULL;

		priv->action_group = NULL;
		priv->popups_action_group = NULL;

		g_object_unref (priv->manager);
		priv->manager = NULL;

		_blx_window_set_context_event (window, NULL);
	}

	destroy_fullscreen_popup (window);

	G_OBJECT_CLASS (blx_window_parent_class)->dispose (object);
}

static void
blx_window_set_property (GObject *object,
			  guint prop_id,
			  const GValue *value,
			  GParamSpec *pspec)
{
	BlxWindow *window = BLX_WINDOW (object);

	switch (prop_id)
	{
		case PROP_ACTIVE_CHILD:
			impl_set_active_child (BLX_EMBED_CONTAINER (window),
					       g_value_get_object (value));
			break;
		case PROP_CHROME:
			blx_window_set_chrome (window, g_value_get_flags (value));
			break;
		case PROP_SINGLE_TAB_MODE:
			blx_window_set_is_popup (window, g_value_get_boolean (value));
			break;
	        default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec); 
			break;
	}
}

static void
blx_window_get_property (GObject *object,
			  guint prop_id,
			  GValue *value,
			  GParamSpec *pspec)
{
	BlxWindow *window = BLX_WINDOW (object);

	switch (prop_id)
	{
		case PROP_ACTIVE_CHILD:
			g_value_set_object (value, window->priv->active_embed);
			break;
		case PROP_CHROME:
			g_value_set_flags (value, window->priv->chrome);
			break;
		case PROP_SINGLE_TAB_MODE:
			g_value_set_boolean (value, window->priv->is_popup);
			break;
	        default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec); 
			break;
	}
}

static gboolean
blx_window_focus_in_event (GtkWidget *widget,
			    GdkEventFocus *event)
{
	BlxWindow *window = BLX_WINDOW (widget);
	BlxWindowPrivate *priv = window->priv;

	if (priv->fullscreen_popup && !get_toolbar_visibility (window))
	{
		gtk_widget_show (priv->fullscreen_popup);
	}

	return GTK_WIDGET_CLASS (blx_window_parent_class)->focus_in_event (widget, event);
}

static gboolean
blx_window_focus_out_event (GtkWidget *widget,
			     GdkEventFocus *event)
{
	BlxWindow *window = BLX_WINDOW (widget);
	BlxWindowPrivate *priv = window->priv;

	if (priv->fullscreen_popup)
	{
		gtk_widget_hide (priv->fullscreen_popup);
	}

	return GTK_WIDGET_CLASS (blx_window_parent_class)->focus_out_event (widget, event);
}

static gboolean
blx_window_state_event (GtkWidget *widget,
			 GdkEventWindowState *event)
{
	BlxWindow *window = BLX_WINDOW (widget);
	BlxWindowPrivate *priv = window->priv;
	gboolean (* window_state_event) (GtkWidget *, GdkEventWindowState *);

	window_state_event = GTK_WIDGET_CLASS (blx_window_parent_class)->window_state_event;
	if (window_state_event)
	{
		window_state_event (widget, event);
	}

	if (event->changed_mask & GDK_WINDOW_STATE_FULLSCREEN)
	{
		GtkActionGroup *action_group;
		GtkAction *action;
		gboolean fullscreen;

		fullscreen = event->new_window_state & GDK_WINDOW_STATE_FULLSCREEN;

		if (fullscreen)
		{
			blx_window_fullscreen (window);
		}
		else
		{
			blx_window_unfullscreen (window);
		}

		action_group = priv->action_group;

		action = gtk_action_group_get_action (action_group, "ViewFullscreen");
		g_signal_handlers_block_by_func
			(action, G_CALLBACK (window_cmd_view_fullscreen), window);
		gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), fullscreen);
		g_signal_handlers_unblock_by_func
			(action, G_CALLBACK (window_cmd_view_fullscreen), window);
	}

	return FALSE;
}

static void
blx_window_finalize (GObject *object)
{
	BlxWindow *window = BLX_WINDOW (object);
	BlxWindowPrivate *priv = window->priv;

	g_hash_table_destroy (priv->tabs_to_remove);

	if (priv->set_focus_handler != 0)
		g_signal_handler_disconnect (window,
					     priv->set_focus_handler);

	if (priv->clear_progress_timeout_id)
		g_source_remove (priv->clear_progress_timeout_id);

	G_OBJECT_CLASS (blx_window_parent_class)->finalize (object);

	LOG ("BlxWindow finalised %p", object);
}

static void
cancel_handler (gpointer idptr)
{
	guint id = GPOINTER_TO_UINT (idptr);

	g_source_remove (id);
}

static void
find_toolbar_close_cb (BlxFindToolbar *toolbar,
		       BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	BlxEmbed *embed;

	if (priv->closing) return;

	blx_find_toolbar_close (priv->find_toolbar);

	embed = priv->active_embed;
	if (embed == NULL) return;

	gtk_widget_grab_focus (GTK_WIDGET (embed));
}

static void
allow_popups_notifier (GSettings *settings,
		       char *key,
		       BlxWindow *window)
{
	GList *tabs;
	BlxEmbed *embed;

	g_return_if_fail (BLX_IS_WINDOW (window));

	tabs = impl_get_children (BLX_EMBED_CONTAINER (window));

	for (; tabs; tabs = g_list_next (tabs))
	{
		embed = BLX_EMBED (tabs->data);
		g_return_if_fail (BLX_IS_EMBED (embed));

		g_object_notify (G_OBJECT (blx_embed_get_web_view (embed)), "popups-allowed");
	}
	g_list_free (tabs);
}

static void
sync_user_input_cb (BlxLocationAction *action,
		    GParamSpec *pspec,
		    BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	BlxEmbed *embed;
	const char *address;

	LOG ("sync_user_input_cb");

	if (priv->updating_address) return;

	embed = blx_embed_container_get_active_child (BLX_EMBED_CONTAINER (window));
	g_assert (BLX_IS_EMBED (embed));

	address = blx_location_action_get_address (action);

	priv->updating_address = TRUE;
	blx_web_view_set_typed_address (blx_embed_get_web_view (embed), address);
	priv->updating_address = FALSE;
}

static void
zoom_to_level_cb (GtkAction *action,
		  float zoom,
		  BlxWindow *window)
{
	blx_window_set_zoom (window, zoom);
}

static GtkWidget *
setup_toolbar (BlxWindow *window)
{
	GtkWidget *toolbar;
	GtkUIManager *manager;
	GtkAction *action;
	BlxWindowPrivate *priv = window->priv;

	manager = GTK_UI_MANAGER (blx_window_get_ui_manager (window));

	toolbar = gtk_ui_manager_get_widget (manager, "/DefaultToolbar");

	gtk_style_context_add_class (gtk_widget_get_style_context (toolbar),
				     GTK_STYLE_CLASS_PRIMARY_TOOLBAR);

	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "NavigationBack");
	g_signal_connect_swapped (action, "open-link",
				  G_CALLBACK (blx_link_open), window);

	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "NavigationForward");
	g_signal_connect_swapped (action, "open-link",
				  G_CALLBACK (blx_link_open), window);

	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "NavigationUp");
	g_signal_connect_swapped (action, "open-link",
				  G_CALLBACK (blx_link_open), window);

	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "Location");
	g_signal_connect (action, "notify::address",
			  G_CALLBACK (sync_user_input_cb), window);
	g_signal_connect_swapped (action, "open-link",
				  G_CALLBACK (blx_link_open), window);

	/* FIXME: No one seems to be using this atm. When we need it, the
	 * signal should be added to BlxWindow. */
#if 0
	g_signal_connect (action, "lock-clicked",
			  G_CALLBACK (lock_clicked_cb), toolbar);
#endif
	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "FileNewTab");
	g_signal_connect_swapped (action, "open-link",
				  G_CALLBACK (blx_link_open), window);
	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "FileNewWindow");
	g_signal_connect_swapped (action, "open-link",
				  G_CALLBACK (blx_link_open), window);

	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "GoHome");
	g_signal_connect_swapped (action, "open-link",
				  G_CALLBACK (blx_link_open), window);

	action = gtk_action_group_get_action (priv->toolbar_action_group,
					      "Zoom");
	g_signal_connect (action, "zoom-to-level",
			  G_CALLBACK (zoom_to_level_cb), window);

	return toolbar;
}

static const char* disabled_actions_for_app_mode[] = { "FileOpen",
                                                       "FileSaveAs",
                                                       "FileSaveAsApplication",
                                                       "ViewMenuBar",
                                                       "ViewEncoding",
                                                       "FileBookmarkPage",
                                                       "GoBookmarks" };

static GObject *
blx_window_constructor (GType type,
			 guint n_construct_properties,
			 GObjectConstructParam *construct_params)
{
	GObject *object;
	BlxWindow *window;
	BlxWindowPrivate *priv;
	BlxExtension *manager;
	BlxEmbedSingle *single;
	GtkSettings *settings;
	GtkAction *action;
	GtkActionGroup *toolbar_action_group;
	GError *error = NULL;
	guint settings_connection;
	GtkCssProvider *css_provider;
	GFile *css_file;
	int i;
	BlxEmbedShellMode mode;

	object = G_OBJECT_CLASS (blx_window_parent_class)->constructor
		(type, n_construct_properties, construct_params);

	window = BLX_WINDOW (object);

	priv = window->priv;

	priv->tabs_to_remove = g_hash_table_new_full (g_direct_hash, g_direct_equal,
						      NULL, cancel_handler);

	blx_gui_ensure_window_group (GTK_WINDOW (window));

	/* initialize the listener for the key theme
	 * FIXME: Need to handle multi-head and migration.
	 */
	settings = gtk_settings_get_default ();
	settings_connection = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (settings),
								   SETTINGS_CONNECTION_DATA_KEY));
	if (settings_connection == 0)
	{
		settings_connection =
			g_signal_connect (settings, "notify::gtk-key-theme-name",
					  G_CALLBACK (settings_changed_cb), NULL);
		g_object_set_data (G_OBJECT (settings), SETTINGS_CONNECTION_DATA_KEY,
				   GUINT_TO_POINTER (settings_connection));

	}

	settings_change_notify (settings, window);

	/* Setup the UI manager and connect verbs */
	setup_ui_manager (window);

	priv->notebook = setup_notebook (window);
	g_signal_connect_swapped (priv->notebook, "open-link",
				  G_CALLBACK (blx_link_open), window);
	gtk_box_pack_start (GTK_BOX (priv->main_vbox),
			    GTK_WIDGET (priv->notebook),
			    TRUE, TRUE, 0);
	gtk_widget_show (GTK_WIDGET (priv->notebook));
	blx_notebook_set_dnd_enabled (BLX_NOTEBOOK (priv->notebook), !priv->is_popup);

	priv->find_toolbar = blx_find_toolbar_new (window);
	g_signal_connect (priv->find_toolbar, "close",
			  G_CALLBACK (find_toolbar_close_cb), window);

	gtk_box_pack_start (GTK_BOX (priv->main_vbox),
			    GTK_WIDGET (priv->find_toolbar), FALSE, FALSE, 0);

	priv->downloads_box = setup_downloads_box (window);
	gtk_box_pack_start (GTK_BOX (priv->main_vbox),
			    GTK_WIDGET (priv->downloads_box), FALSE, FALSE, 0);
	action = gtk_action_group_get_action (window->priv->action_group,
					      "ViewDownloadsBar");

	g_object_bind_property (action, "active",
				priv->downloads_box, "visible",
				G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
	
	/* now load the UI definition */
	gtk_ui_manager_add_ui_from_file
		(priv->manager, blx_file ("blxbrowser-ui.xml"), &error);
	if (error != NULL)
	{
		g_warning ("Could not merge blxbrowser-ui.xml: %s", error->message);
		g_error_free (error);
	}

	/* Attach the CSS provider to the window */
	css_file = g_file_new_for_path (blx_file ("blxbrowser.css"));
	css_provider = gtk_css_provider_new ();
	gtk_css_provider_load_from_file (css_provider,
	                                 css_file,
	                                 &error);
	if (error == NULL)
	{
		gtk_style_context_add_provider_for_screen (gtk_widget_get_screen (GTK_WIDGET (window)),
		                                           GTK_STYLE_PROVIDER (css_provider),
		                                           GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	}
	else
	{
		g_warning ("Could not attach css style: %s", error->message);
		g_error_free (error);
	}

	g_object_unref (css_provider);
	g_object_unref (css_file);

	/* create the toolbars */
	priv->toolbar = setup_toolbar (window);

	/* Initialize the menus */
	priv->enc_menu = blx_encoding_menu_new (window);

	/* Once the window is sufficiently created let the extensions attach to it */
	manager = BLX_EXTENSION (blx_shell_get_extensions_manager (blx_shell));
	blx_extension_attach_window (manager, window);
	blx_bookmarks_ui_attach_window (window);

	/* other notifiers */
	action = gtk_action_group_get_action (window->priv->action_group,
					      "BrowseWithCaret");

	g_settings_bind (BLX_SETTINGS_MAIN,
			 BLX_PREFS_ENABLE_CARET_BROWSING,
			 action, "active",
			 G_SETTINGS_BIND_GET);

	g_signal_connect (BLX_SETTINGS_WEB,
			  "changed::" BLX_PREFS_WEB_ENABLE_POPUPS,
			  G_CALLBACK (allow_popups_notifier), window);

	/* network status */
	single = BLX_EMBED_SINGLE (blx_embed_shell_get_embed_single (embed_shell));
	sync_network_status (single, NULL, window);
	g_signal_connect (single, "notify::network-status",
			  G_CALLBACK (sync_network_status), window);

	/* Disable actions not needed for popup mode. */
	toolbar_action_group = priv->toolbar_action_group;
	action = gtk_action_group_get_action (toolbar_action_group, "FileNewTab");
	blx_action_change_sensitivity_flags (action, SENS_FLAG_CHROME,
					      priv->is_popup);

	action = gtk_action_group_get_action (priv->popups_action_group, "OpenLinkInNewTab");
	blx_action_change_sensitivity_flags (action, SENS_FLAG_CHROME,
					      priv->is_popup);

	/* Disabled actions not needed for application mode. */
	mode = blx_embed_shell_get_mode (embed_shell);
	if (mode == BLX_EMBED_SHELL_MODE_APPLICATION)
	{
		/* FileNewTab and FileNewWindow are sort of special. */
		action = gtk_action_group_get_action (toolbar_action_group, "FileNewTab");
		blx_action_change_sensitivity_flags (action, SENS_FLAG_CHROME,
						      TRUE);

		action = gtk_action_group_get_action (toolbar_action_group, "FileNewWindow");
		blx_action_change_sensitivity_flags (action, SENS_FLAG_CHROME,
						      TRUE);
		
		for (i = 0; i < G_N_ELEMENTS (disabled_actions_for_app_mode); i++)
		{
			action = gtk_action_group_get_action (priv->action_group,
							      disabled_actions_for_app_mode[i]);
			blx_action_change_sensitivity_flags (action, SENS_FLAG_CHROME, TRUE);
		}
	}
		
	/* Connect lock clicks */
	action = gtk_action_group_get_action (priv->action_group, "ViewPageSecurityInfo");
	/* FIXME: No one seems to be using this atm. When we need it, the
	 * signal should be added to BlxWindow. */
#if 0
	g_signal_connect_swapped (priv->toolbar, "lock-clicked",
				  G_CALLBACK (gtk_action_activate), action);
#endif

	/* ensure the UI is updated */
	gtk_ui_manager_ensure_update (priv->manager);

	init_menu_updaters (window);

	update_chromes_actions (window);

	sync_chromes_visibility (window);

	ensure_location_entry (window);

	return object;
}

static void
blx_window_class_init (BlxWindowClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	object_class->constructor = blx_window_constructor;
	object_class->dispose = blx_window_dispose;
	object_class->finalize = blx_window_finalize;
	object_class->get_property = blx_window_get_property;
	object_class->set_property = blx_window_set_property;

	widget_class->show = blx_window_show;
	widget_class->key_press_event = blx_window_key_press_event;
	widget_class->focus_in_event = blx_window_focus_in_event;
	widget_class->focus_out_event = blx_window_focus_out_event;
	widget_class->window_state_event = blx_window_state_event;
	widget_class->delete_event = blx_window_delete_event;

	g_object_class_override_property (object_class,
					  PROP_ACTIVE_CHILD,
					  "active-child");

	g_object_class_override_property (object_class,
					  PROP_SINGLE_TAB_MODE,
					  "is-popup");

	g_object_class_override_property (object_class,
					  PROP_CHROME,
					  "chrome");

	g_type_class_add_private (object_class, sizeof (BlxWindowPrivate));
}

static void 
maybe_finish_activation_cb (BlxWindow *window,
			    GtkWidget *widget,
			    GtkWidget *toolbar)
{
	while (widget != NULL && widget != toolbar)
	{
		widget = gtk_widget_get_parent (widget);
	}

	/* if widget == toolbar, the new focus widget is in the toolbar, so we
	 * don't deactivate.
	 */
	if (widget != toolbar)
	{
		g_signal_handler_disconnect (window, window->priv->set_focus_handler);
		window->priv->set_focus_handler = 0;
		sync_chromes_visibility (window);
	}
}

static void
_blx_window_activate_location (BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	GtkAction *action;
	GSList *proxies;
	GtkWidget *entry = NULL;
	gboolean visible;

	action = gtk_action_group_get_action (priv->toolbar_action_group, "Location");
	proxies = gtk_action_get_proxies (action);

	if (proxies != NULL && BLX_IS_LOCATION_ENTRY (proxies->data))
	{
		entry = GTK_WIDGET (proxies->data);
	}

	if (entry == NULL)
	{
		/* happens when the user has removed the location entry from
		 * the toolbars.
		 */
		return;
	}

	g_object_get (G_OBJECT (priv->toolbar), "visible", &visible, NULL);
	if (visible == FALSE)
	{
		gtk_widget_show (GTK_WIDGET (priv->toolbar));
		window->priv->set_focus_handler =
			g_signal_connect (window, "set-focus",
					  G_CALLBACK (maybe_finish_activation_cb),
					  priv->toolbar);
	}

	blx_location_entry_activate (BLX_LOCATION_ENTRY (entry));
}

static BlxEmbed *
blx_window_open_link (BlxLink *link,
		       const char *address,
		       BlxEmbed *embed,
		       BlxLinkFlags flags)
{
	BlxWindow *window = BLX_WINDOW (link);
	BlxWindowPrivate *priv = window->priv;
	BlxEmbed *new_embed;

	g_return_val_if_fail (address != NULL, NULL);

	if (embed == NULL)
	{
		embed = window->priv->active_embed;
	}

	if (flags  & (BLX_LINK_JUMP_TO | 
		      BLX_LINK_NEW_TAB | 
		      BLX_LINK_NEW_WINDOW))
	{
		BlxNewTabFlags ntflags = BLX_NEW_TAB_OPEN_PAGE;

		if (flags & BLX_LINK_JUMP_TO)
		{
			ntflags |= BLX_NEW_TAB_JUMP;
		}
		if (flags & BLX_LINK_NEW_WINDOW ||
		    (flags & BLX_LINK_NEW_TAB && priv->is_popup))
		{
			ntflags |= BLX_NEW_TAB_IN_NEW_WINDOW;
		}
		else
		{
			ntflags |= BLX_NEW_TAB_IN_EXISTING_WINDOW;
		}

		if (flags & BLX_LINK_NEW_TAB_APPEND_AFTER)
			ntflags |= BLX_NEW_TAB_APPEND_AFTER;

		new_embed = blx_shell_new_tab
				(blx_shell,
				 BLX_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (embed))),
				 embed, address, ntflags);
	}
	else
	{
		blx_web_view_load_url (blx_embed_get_web_view (embed), address);

		if (address == NULL || address[0] == '\0' || strcmp (address, "about:blank") == 0)
		{
			_blx_window_activate_location (window);
		}
		else
		{
			gtk_widget_grab_focus (GTK_WIDGET (embed));
		}

		new_embed = embed;
	}

	return new_embed;
}

static void
blx_window_init (BlxWindow *window)
{
	LOG ("BlxWindow initialising %p", window);

	gtk_application_add_window (GTK_APPLICATION (blx_shell), GTK_WINDOW (window));

	window->priv = BLX_WINDOW_GET_PRIVATE (window);

	g_signal_connect (embed_shell,
			 "download-added", G_CALLBACK (download_added_cb),
			 window);
}

/**
 * blx_window_new:
 *
 * Equivalent to g_object_new() but returns an #BlxWindow so you don't have
 * to cast it.
 *
 * Return value: a new #BlxWindow
 **/
BlxWindow *
blx_window_new (void)
{
	return BLX_WINDOW (g_object_new (BLX_TYPE_WINDOW, NULL));
}

/**
 * blx_window_new_with_chrome:
 * @chrome: an #BlxWebViewChrome
 * @is_popup: whether the new window is a popup window
 *
 * Identical to blx_window_new(), but allows you to specify a chrome.
 *
 * Return value: a new #BlxWindow
 **/
BlxWindow *
blx_window_new_with_chrome (BlxWebViewChrome chrome,
			     gboolean is_popup)
{
	return BLX_WINDOW (g_object_new (BLX_TYPE_WINDOW,
					  "chrome", chrome,
					  "is-popup", is_popup,
					  NULL));
}

/**
 * blx_window_get_ui_manager:
 * @window: an #BlxWindow
 *
 * Returns this window's UI manager.
 *
 * Return value: (transfer none): an #GtkUIManager
 **/
GObject *
blx_window_get_ui_manager (BlxWindow *window)
{
	g_return_val_if_fail (BLX_IS_WINDOW (window), NULL);

	return G_OBJECT (window->priv->manager);
}

/**
 * blx_window_get_notebook:
 * @window: an #BlxWindow
 *
 * Returns the #GtkNotebook used by this window.
 *
 * Return value: (transfer none): the @window's #GtkNotebook
 **/
GtkWidget *
blx_window_get_notebook (BlxWindow *window)
{
	g_return_val_if_fail (BLX_IS_WINDOW (window), NULL);

	return GTK_WIDGET (window->priv->notebook);
}

/**
 * blx_window_get_find_toolbar:
 * @window: an #BlxWindow
 *
 * Returns the #BlxFindToolbar used by this window.
 *
 * Return value: (transfer none): the @window's #BlxFindToolbar
 **/
GtkWidget *
blx_window_get_find_toolbar (BlxWindow *window)
{
       g_return_val_if_fail (BLX_IS_WINDOW (window), NULL);

       return GTK_WIDGET (window->priv->find_toolbar);
}

static BlxEmbed *
real_get_active_tab (BlxWindow *window, int page_num)
{
	GtkWidget *embed;

	if (page_num == -1)
	{
		page_num = gtk_notebook_get_current_page (window->priv->notebook);
	}
	embed = gtk_notebook_get_nth_page (window->priv->notebook, page_num);

	g_return_val_if_fail (BLX_IS_EMBED (embed), NULL);

	return BLX_EMBED (embed);
}

/**
 * blx_window_load_url:
 * @window: a #BlxWindow
 * @url: the url to load
 *
 * Loads a new url in the active tab of @window.
 * Unlike blx_web_view_load_url(), this function activates
 * the embed.
 *
 **/
void
blx_window_load_url (BlxWindow *window,
		      const char *url)
{
	g_return_if_fail (url != NULL);

	blx_link_open (BLX_LINK (window), url, NULL, 0);
}

/**
 * blx_window_activate_location:
 * @window: an #BlxWindow
 *
 * Activates the location entry on @window's toolbar.
 **/
void
blx_window_activate_location (BlxWindow *window)
{
	if (window->priv->fullscreen_popup)
	{
		gtk_widget_hide (window->priv->fullscreen_popup);
	}

	_blx_window_activate_location (window);
}

static void
blx_window_show (GtkWidget *widget)
{
	BlxWindow *window = BLX_WINDOW(widget);
	BlxWindowPrivate *priv = window->priv;

	if (!priv->has_size)
	{
		BlxEmbed *embed;
		int width, height;

		embed = priv->active_embed;
		g_return_if_fail (BLX_IS_EMBED (embed));

		blx_tab_get_size (embed, &width, &height);
		if (width == -1 && height == -1)
		{
			int flags = 0;
			if (!priv->is_popup)
				flags = BLX_STATE_WINDOW_SAVE_SIZE;

			blx_state_add_window (widget, "main_window", 600, 500,
					       TRUE, flags);
		}

		priv->has_size = TRUE;
	}

	GTK_WIDGET_CLASS (blx_window_parent_class)->show (widget);
}

static void
notebook_switch_page_cb (GtkNotebook *notebook,
			 GtkWidget *page,
			 guint page_num,
			 BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	BlxEmbed *embed;

	LOG ("switch-page notebook %p position %u\n", notebook, page_num);

	if (priv->closing) return;

	/* get the new tab */
	embed = real_get_active_tab (window, page_num);

	/* update new tab */
	blx_window_set_active_tab (window, embed);

	blx_find_toolbar_set_embed (priv->find_toolbar, embed);
}

/**
 * blx_window_set_zoom:
 * @window: an #BlxWindow
 * @zoom: the desired zoom level
 *
 * Sets the zoom on @window's active #BlxEmbed. A @zoom of 1.0 corresponds to
 * 100% zoom (normal size).
 **/
void
blx_window_set_zoom (BlxWindow *window,
		      float zoom)
{
	BlxEmbed *embed;
	float current_zoom = 1.0;
	WebKitWebView *web_view;

	g_return_if_fail (BLX_IS_WINDOW (window));

	embed = window->priv->active_embed;
	g_return_if_fail (embed != NULL);

	web_view = BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed);

	g_object_get (web_view, "zoom-level", &current_zoom, NULL);

	if (zoom == ZOOM_IN)
	{
		zoom = blx_zoom_get_changed_zoom_level (current_zoom, 1);
	}
	else if (zoom == ZOOM_OUT)
	{
		zoom = blx_zoom_get_changed_zoom_level (current_zoom, -1);
	}

	if (zoom != current_zoom)
	{
		g_object_set (G_OBJECT (web_view), "zoom-level", zoom, NULL);
	}
}

static void
sync_prefs_with_chrome (BlxWindow *window)
{
	BlxWebViewChrome flags = window->priv->chrome;

	if (window->priv->should_save_chrome)
	{
		g_settings_set_boolean (BLX_SETTINGS_UI,
					BLX_PREFS_UI_SHOW_TOOLBARS,
				        flags & BLX_WEB_VIEW_CHROME_TOOLBAR);

		g_settings_set_boolean (BLX_SETTINGS_LOCKDOWN,
					BLX_PREFS_LOCKDOWN_MENUBAR,
				        !(flags & BLX_WEB_VIEW_CHROME_MENUBAR));
	}
}

static void
sync_chrome_with_view_toggle (GtkAction *action,
			      BlxWindow *window,
			      BlxWebViewChrome chrome_flag,
			      gboolean invert)
{
	gboolean active;

	active = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action));
	window->priv->chrome = (active != invert) ?
	  				window->priv->chrome | chrome_flag :
	  				window->priv->chrome & (~chrome_flag);

	sync_chromes_visibility (window);
	sync_prefs_with_chrome (window);
}

static void
blx_window_view_toolbar_cb (GtkAction *action,
			     BlxWindow *window)
{
	sync_chrome_with_view_toggle (action, window,
				      BLX_WEB_VIEW_CHROME_TOOLBAR, TRUE);
}

static void
blx_window_view_menubar_cb (GtkAction *action,
			     BlxWindow *window)
{
	sync_chrome_with_view_toggle (action, window,
				      BLX_WEB_VIEW_CHROME_MENUBAR, FALSE);
}

static void
blx_window_view_popup_windows_cb (GtkAction *action,
				   BlxWindow *window)
{
	BlxEmbed *embed;
	gboolean allow;

	g_return_if_fail (BLX_IS_WINDOW (window));

	embed = window->priv->active_embed;
	g_return_if_fail (BLX_IS_EMBED (embed));

	if (gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)))
	{
		allow = TRUE;
	}
	else
	{
		allow = FALSE;
	}

	g_object_set (G_OBJECT (blx_embed_get_web_view (embed)), "popups-allowed", allow, NULL);
}

/**
 * blx_window_get_context_event:
 * @window: an #BlxWindow
 *
 * Returns the #BlxEmbedEvent for the current context menu.
 * Use this to get the event from the action callback.
 *
 * Return value: (transfer none): an #BlxEmbedEvent, or %NULL
 **/
BlxEmbedEvent *
blx_window_get_context_event (BlxWindow *window)
{
	g_return_val_if_fail (BLX_IS_WINDOW (window), NULL);

	return window->priv->context_event;
}

/**
 * blx_window_get_location:
 * @window: an #BlxWindow widget
 *
 * Gets the current address according to @window's #BlxLocationAction.
 *
 * Returns: current @window address
 **/
const char *
blx_window_get_location (BlxWindow *window)
{
	BlxWindowPrivate *priv = window->priv;
	GtkAction * action = gtk_action_group_get_action (priv->toolbar_action_group,
							  "Location");
	return blx_location_action_get_address (BLX_LOCATION_ACTION (action));
}

/**
 * blx_window_set_location:
 * @window: an #BlxWindow widget
 * @address: new address
 *
 * Sets the internal #BlxLocationAction address to @address.
 **/
void
blx_window_set_location (BlxWindow *window,
			  const char *address)
{
	BlxWindowPrivate *priv = window->priv;
	GtkAction *action = gtk_action_group_get_action (priv->toolbar_action_group,
							 "Location");

	if (priv->updating_address) return;

	priv->updating_address = TRUE;
	blx_location_action_set_address (BLX_LOCATION_ACTION (action), address);
	priv->updating_address = FALSE;
}
