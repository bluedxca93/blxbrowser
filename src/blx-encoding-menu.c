/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Copyright © 2002  Ricardo Fernández Pascual
 *  Copyright © 2003  Marco Pesenti Gritti
 *  Copyright © 2003  Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"

#include "blx-encoding-menu.h"
#include "blx-encoding-dialog.h"
#include "blx-encodings.h"
#include "blx-embed.h"
#include "blx-embed-container.h"
#include "blx-embed-shell.h"
#include "blx-embed-utils.h"
#include "blx-shell.h"
#include "blx-debug.h"

#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <string.h>
#include <webkit/webkit.h>

#define BLX_ENCODING_MENU_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), BLX_TYPE_ENCODING_MENU, BlxEncodingMenuPrivate))

struct _BlxEncodingMenuPrivate
{
	BlxEncodings *encodings;
	BlxWindow *window;
	GtkUIManager *manager;
	GtkActionGroup *action_group;
	gboolean update_tag;
	guint merge_id;
	GSList *encodings_radio_group;
	BlxEncodingDialog *dialog;
};

#define ENCODING_PLACEHOLDER_PATH	"/menubar/ViewMenu/ViewEncodingMenu/ViewEncodingPlaceholder"

static void	blx_encoding_menu_class_init	  (BlxEncodingMenuClass *klass);
static void	blx_encoding_menu_init		  (BlxEncodingMenu *menu);

enum
{
	PROP_0,
	PROP_WINDOW
};

G_DEFINE_TYPE (BlxEncodingMenu, blx_encoding_menu, G_TYPE_OBJECT)

static void
blx_encoding_menu_init (BlxEncodingMenu *menu)
{
	menu->priv = BLX_ENCODING_MENU_GET_PRIVATE (menu);

	menu->priv->encodings =
		BLX_ENCODINGS (blx_embed_shell_get_encodings
				(BLX_EMBED_SHELL (blx_shell)));
}

static int
sort_encodings (gconstpointer a, gconstpointer b)
{
	BlxNode *node1 = (BlxNode *) a;
	BlxNode *node2 = (BlxNode *) b;
	const char *key1, *key2;

	key1 = blx_node_get_property_string
			(node1, BLX_NODE_ENCODING_PROP_COLLATION_KEY);
	key2 = blx_node_get_property_string
			(node2, BLX_NODE_ENCODING_PROP_COLLATION_KEY);

	return strcmp (key1, key2);
}

static void
add_menu_item (BlxNode *node, BlxEncodingMenu *menu)
{
	const char *code;
	char action[128], name[128];

	code = blx_node_get_property_string
				(node, BLX_NODE_ENCODING_PROP_ENCODING);

	g_snprintf (action, sizeof (action), "Encoding%s", code);
	g_snprintf (name, sizeof (name), "%sItem", action);

	gtk_ui_manager_add_ui (menu->priv->manager, menu->priv->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       name, action,
			       GTK_UI_MANAGER_MENUITEM, FALSE);
}

static void
update_encoding_menu_cb (GtkAction *dummy, BlxEncodingMenu *menu)
{
	BlxEncodingMenuPrivate *p = menu->priv;
	BlxEmbed *embed;
	GtkAction *action;
	char name[128];
	const char *encoding;
	BlxNode *enc_node;
	GList *recent, *related = NULL, *l;
	BlxLanguageGroup groups;
	gboolean is_automatic = FALSE;
	WebKitWebView *view;

	START_PROFILER ("Rebuilding encoding menu")

	/* FIXME: block the "activate" signal on the actions instead; needs to 
	 * wait until g_signal_handlers_block_matched supports blocking
	 * by signal id alone.
	 */
	menu->priv->update_tag = TRUE;

	/* get most recently used encodings */
	recent = blx_encodings_get_recent (p->encodings);

	embed = blx_embed_container_get_active_child (BLX_EMBED_CONTAINER (p->window));
	view = BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed);
	encoding = webkit_web_view_get_custom_encoding (view);
	if (encoding == NULL)
	{
		encoding = webkit_web_view_get_encoding (view);
		if (encoding == NULL) goto build_menu;
		is_automatic = TRUE;
	}

	enc_node = blx_encodings_get_node (p->encodings, encoding, TRUE);
	g_assert (BLX_IS_NODE (enc_node));

	action = gtk_action_group_get_action (p->action_group,
					      "ViewEncodingAutomatic");
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), is_automatic);
	gtk_action_set_sensitive (action, !is_automatic);

	/* set the encodings group's active member */
	g_snprintf (name, sizeof (name), "Encoding%s", encoding);
	action = gtk_action_group_get_action (p->action_group, name);
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), TRUE);

	/* get encodings related to the current encoding */
	groups = blx_node_get_property_int
			(enc_node, BLX_NODE_ENCODING_PROP_LANGUAGE_GROUPS);

	related = blx_encodings_get_encodings (p->encodings, groups);
	related = g_list_sort (related, (GCompareFunc) sort_encodings);

	/* add the current encoding to the list of
	 * things to display, making sure we don't add it more than once
	 */
	if (g_list_find (related, enc_node) == NULL
	    && g_list_find (recent, enc_node) == NULL)
	{
		related = g_list_prepend (related, enc_node);
	}

	/* make sure related and recent are disjoint so we don't display twice */
	for (l = related; l != NULL; l = l->next)
	{
		recent = g_list_remove (recent, l->data);
	}

	recent = g_list_sort (recent, (GCompareFunc) sort_encodings);

build_menu:
	/* clear the menu */
	if (p->merge_id > 0)
	{
		gtk_ui_manager_remove_ui (p->manager, p->merge_id);
		gtk_ui_manager_ensure_update (p->manager);
	}

	/* build the new menu */
	p->merge_id = gtk_ui_manager_new_merge_id (p->manager);

	gtk_ui_manager_add_ui (p->manager, p->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       "ViewEncodingAutomaticItem",
			       "ViewEncodingAutomatic",
			       GTK_UI_MANAGER_MENUITEM, FALSE);

	gtk_ui_manager_add_ui (p->manager, p->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       "Sep1Item", "Sep1",
			       GTK_UI_MANAGER_SEPARATOR, FALSE);

	g_list_foreach (recent, (GFunc) add_menu_item, menu);

	gtk_ui_manager_add_ui (p->manager, p->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       "Sep2Item", "Sep2",
			       GTK_UI_MANAGER_SEPARATOR, FALSE);

	g_list_foreach (related, (GFunc) add_menu_item, menu);

	gtk_ui_manager_add_ui (p->manager, p->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       "Sep3Item", "Sep3",
			       GTK_UI_MANAGER_SEPARATOR, FALSE);

	gtk_ui_manager_add_ui (p->manager, p->merge_id,
			       ENCODING_PLACEHOLDER_PATH,
			       "ViewEncodingOtherItem",
			       "ViewEncodingOther",
			       GTK_UI_MANAGER_MENUITEM, FALSE);

	/* cleanup */
	g_list_free (related);
	g_list_free (recent);

	menu->priv->update_tag = FALSE;

	STOP_PROFILER ("Rebuilding encoding menu")
}

static void
encoding_activate_cb (GtkAction *action, BlxEncodingMenu *menu)
{
	BlxEmbed *embed;
	WebKitWebView *view;
	const char *name, *encoding;

	if (gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)) == FALSE
	    || menu->priv->update_tag)
	{
		return;
	}

	name = gtk_action_get_name (GTK_ACTION (action));
	encoding = name + strlen("Encoding");

	embed = blx_embed_container_get_active_child (BLX_EMBED_CONTAINER (menu->priv->window));

	view = BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed);
	webkit_web_view_set_custom_encoding (view, encoding);

	blx_encodings_add_recent (menu->priv->encodings, encoding);
}

static void
add_action (BlxNode *encodings, BlxNode *node, BlxEncodingMenu *menu)
{
	GtkAction *action;
	char name[128];
	const char *encoding, *title;

	encoding = blx_node_get_property_string
			(node, BLX_NODE_ENCODING_PROP_ENCODING);
	title = blx_node_get_property_string
			(node, BLX_NODE_ENCODING_PROP_TITLE);

	LOG ("add_action for encoding '%s'", encoding);

	g_snprintf (name, sizeof (name), "Encoding%s", encoding);

	action = g_object_new (GTK_TYPE_RADIO_ACTION,
			       "name", name,
			       "label", title,
			       NULL);

	gtk_radio_action_set_group (GTK_RADIO_ACTION (action),
				    menu->priv->encodings_radio_group);
	menu->priv->encodings_radio_group = gtk_radio_action_get_group
						(GTK_RADIO_ACTION (action));

	g_signal_connect (action, "activate",
			  G_CALLBACK (encoding_activate_cb),
			  menu);

	gtk_action_group_add_action_with_accel
		(menu->priv->action_group, action, NULL);
	g_object_unref (action);
}

static void
blx_encoding_menu_view_dialog_cb (GtkAction *action, BlxEncodingMenu *menu)
{
	if (menu->priv->dialog == NULL)
	{
		BlxEncodingDialog **dialog = &menu->priv->dialog;
		menu->priv->dialog = blx_encoding_dialog_new
					(menu->priv->window);

		g_object_add_weak_pointer(G_OBJECT (menu->priv->dialog),
					  (gpointer *)dialog);
	}

	blx_dialog_show (BLX_DIALOG (menu->priv->dialog));
}

static void
blx_encoding_menu_automatic_cb (GtkAction *action, BlxEncodingMenu *menu)
{
	BlxEmbed *embed;
	WebKitWebView *view;

	if (gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)) == FALSE
	    || menu->priv->update_tag)
	{
		return;
	}

	embed = blx_embed_container_get_active_child 
          (BLX_EMBED_CONTAINER (menu->priv->window));

	/* setting NULL will clear the forced encoding */
	view = BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed);
	webkit_web_view_set_custom_encoding (view, NULL);
}

static const GtkActionEntry menu_entries [] =
{
	{ "ViewEncodingOther", NULL, N_("_Other…"), NULL,
	  N_("Other encodings"),
	  G_CALLBACK (blx_encoding_menu_view_dialog_cb) }
};

static const GtkToggleActionEntry toggle_menu_entries [] =
{
	{ "ViewEncodingAutomatic", NULL, N_("_Automatic"), NULL,
	  N_("Use the encoding specified by the document"),
	  G_CALLBACK (blx_encoding_menu_automatic_cb), FALSE }
};

static void
blx_encoding_menu_set_window (BlxEncodingMenu *menu, BlxWindow *window)
{
	GtkActionGroup *action_group;
	GtkAction *action;
	BlxNode *encodings;
	GPtrArray *children;
	int i;

	g_return_if_fail (BLX_IS_WINDOW (window));

	menu->priv->window = window;
	menu->priv->manager = GTK_UI_MANAGER (blx_window_get_ui_manager (window));

	action_group = gtk_action_group_new ("EncodingActions");
	gtk_action_group_set_translation_domain (action_group, NULL);
	menu->priv->action_group = action_group;

	gtk_action_group_add_actions (action_group, menu_entries,
				      G_N_ELEMENTS (menu_entries), menu);
	gtk_action_group_add_toggle_actions (action_group, toggle_menu_entries,
                                    	     G_N_ELEMENTS (toggle_menu_entries), menu);

	/* add actions for the existing encodings */
	encodings = blx_encodings_get_all (menu->priv->encodings);
	children = blx_node_get_children (encodings);
	for (i = 0; i < children->len; i++)
	{
		BlxNode *encoding;

		encoding = (BlxNode *) g_ptr_array_index (children, i);
		add_action (encodings, encoding, menu);
	}

	/* when we encounter an unknown encoding, it is added to the database,
	 * so we need to listen to child_added on the encodings node to
	 * add an action for it
	 */
	blx_node_signal_connect_object (encodings,
					 BLX_NODE_CHILD_ADDED,
					 (BlxNodeCallback) add_action,
					 G_OBJECT (menu));

	gtk_ui_manager_insert_action_group (menu->priv->manager,
					    action_group, 0);
	g_object_unref (action_group);

	action = gtk_ui_manager_get_action (menu->priv->manager,
					    "/menubar/ViewMenu");
	g_signal_connect_object (action, "activate",
				 G_CALLBACK (update_encoding_menu_cb),
				 menu, 0);
}

static void
blx_encoding_menu_set_property (GObject *object,
				 guint prop_id,
				 const GValue *value,
				 GParamSpec *pspec)
{
	BlxEncodingMenu *menu = BLX_ENCODING_MENU (object);

	switch (prop_id)
	{
		case PROP_WINDOW:
			blx_encoding_menu_set_window (menu, g_value_get_object (value));
			break;
	}
}

static void
blx_encoding_menu_get_property (GObject *object,
				 guint prop_id,
				 GValue *value,
				 GParamSpec *pspec)
{
	BlxEncodingMenu *menu = BLX_ENCODING_MENU (object);

	switch (prop_id)
	{
		case PROP_WINDOW:
			g_value_set_object (value, menu->priv->window);
			break;
	}
}

static void
blx_encoding_menu_finalize (GObject *object)
{
	BlxEncodingMenu *menu = BLX_ENCODING_MENU (object); 

	if (menu->priv->dialog)
	{
		g_object_unref (menu->priv->dialog);
	}

	G_OBJECT_CLASS (blx_encoding_menu_parent_class)->finalize (object);
}

static void
blx_encoding_menu_class_init (BlxEncodingMenuClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = blx_encoding_menu_finalize;
	object_class->set_property = blx_encoding_menu_set_property;
	object_class->get_property = blx_encoding_menu_get_property;

	g_object_class_install_property (object_class,
					 PROP_WINDOW,
					 g_param_spec_object ("window",
							      "Window",
							      "Parent window",
							      BLX_TYPE_WINDOW,
							      G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB |
							      G_PARAM_CONSTRUCT_ONLY));

	g_type_class_add_private (object_class, sizeof(BlxEncodingMenuPrivate));
}

BlxEncodingMenu *
blx_encoding_menu_new (BlxWindow *window)
{
	return g_object_new (BLX_TYPE_ENCODING_MENU,
			     "window", window,
			     NULL);
}
