/*
 *  Copyright © 2003, 2004 Marco Pesenti Gritti
 *  Copyright © 2003, 2004, 2005 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_LOCKDOWN_H
#define BLX_LOCKDOWN_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define BLX_TYPE_LOCKDOWN		(blx_lockdown_get_type ())
#define BLX_LOCKDOWN(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_LOCKDOWN, BlxLockdown))
#define BLX_LOCKDOWN_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_LOCKDOWN, BlxLockdownClass))
#define BLX_IS_LOCKDOWN(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_LOCKDOWN))
#define BLX_IS_LOCKDOWN_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_LOCKDOWN))
#define BLX_LOCKDOWN_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_LOCKDOWN, BlxLockdownClass))

typedef struct _BlxLockdownClass	BlxLockdownClass;
typedef struct _BlxLockdown		BlxLockdown;
typedef struct _BlxLockdownPrivate	BlxLockdownPrivate;

struct _BlxLockdownClass
{
	GObjectClass parent_class;
};

struct _BlxLockdown
{
	GObject parent_instance;

	/*< private >*/
	BlxLockdownPrivate *priv;
};

GType	blx_lockdown_get_type		(void);

G_END_DECLS

#endif
