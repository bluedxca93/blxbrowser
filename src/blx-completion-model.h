/*
 *  Copyright © 2003 Marco Pesenti Gritti <marco@gnome.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_COMPLETION_MODEL_H
#define BLX_COMPLETION_MODEL_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLX_TYPE_COMPLETION_MODEL         (blx_completion_model_get_type ())
#define BLX_COMPLETION_MODEL(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_COMPLETION_MODEL, BlxCompletionModel))
#define BLX_COMPLETION_MODEL_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_COMPLETION_MODEL, BlxCompletionModelClass))
#define BLX_IS_COMPLETION_MODEL(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_COMPLETION_MODEL))
#define BLX_IS_COMPLETION_MODEL_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_COMPLETION_MODEL))
#define BLX_COMPLETION_MODEL_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_COMPLETION_MODEL, BlxCompletionModelClass))

typedef struct _BlxCompletionModelPrivate BlxCompletionModelPrivate;

typedef enum
{
	BLX_COMPLETION_TEXT_COL,
	BLX_COMPLETION_ACTION_COL,
	BLX_COMPLETION_KEYWORDS_COL,
	BLX_COMPLETION_RELEVANCE_COL,
	BLX_COMPLETION_URL_COL,
	BLX_COMPLETION_EXTRA_COL,
	BLX_COMPLETION_FAVICON_COL,
	N_COL
} BlxCompletionColumn;

typedef struct
{
	GObject parent;

	/*< private >*/
	BlxCompletionModelPrivate *priv;
} BlxCompletionModel;

typedef struct
{
	GObjectClass parent;
} BlxCompletionModelClass;

GType                blx_completion_model_get_type	(void);

BlxCompletionModel *blx_completion_model_new		(void);

G_END_DECLS

#endif /* BLX_COMPLETION_MODEL_H */
