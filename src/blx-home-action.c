/*
*  Copyright © 2004 Christian Persch
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2, or (at your option)
*  any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*
*/

#include "config.h"

#include "blx-home-action.h"
#include "blx-link.h"
#include "blx-prefs.h"
#include "blx-settings.h"
#include "blx-gui.h"
#include "blx-dnd.h"

#include <string.h>

#include <gtk/gtk.h>

#define INSANE_NUMBER_OF_URLS 20

typedef struct
{
	GObject *weak_ptr;
	BlxLinkFlags flags;
} ClipboardCtx;

G_DEFINE_TYPE (BlxHomeAction, blx_home_action, BLX_TYPE_LINK_ACTION)

static const GtkTargetEntry url_drag_types [] = 
{
	{ BLX_DND_URI_LIST_TYPE,   0, 0 },
	{ BLX_DND_URL_TYPE,        0, 1 }
};

static void
clipboard_text_received_cb (GtkClipboard *clipboard,
			    const char *text,
			    ClipboardCtx *ctx)
{
	if (ctx->weak_ptr != NULL && text != NULL)
	{
		blx_link_open (BLX_LINK (ctx->weak_ptr), text, NULL, ctx->flags);
	}

	if (ctx->weak_ptr != NULL)
	{
		GObject **object = &(ctx->weak_ptr);
		g_object_remove_weak_pointer (G_OBJECT (ctx->weak_ptr), 
					      (gpointer *)object);
	}

	g_free (ctx);
}

static void
blx_home_action_with_clipboard (GtkAction *action,
				 BlxLinkFlags flags)
{
	ClipboardCtx *ctx;
	GObject **object;

	ctx = g_new (ClipboardCtx, 1);
	ctx->flags = flags;

	/* We need to make sure we know if the action is destroyed between
	 * requesting the clipboard contents, and receiving them.
	 */
	ctx->weak_ptr = G_OBJECT (action);
	object = &(ctx->weak_ptr);
	g_object_add_weak_pointer (ctx->weak_ptr, (gpointer *)object);

	gtk_clipboard_request_text
		(gtk_clipboard_get_for_display (gdk_display_get_default(), 
					        GDK_SELECTION_PRIMARY),
		 (GtkClipboardTextReceivedFunc) clipboard_text_received_cb,
		 ctx);

}

static void
blx_home_action_open (GtkAction *action, 
		       const char *address, 
		       BlxLinkFlags flags)
{
	if (blx_gui_is_middle_click ())
	{
		blx_home_action_with_clipboard (action, flags);
	}
	else /* Left button */
	{
		blx_link_open (BLX_LINK (action),
				address != NULL && address[0] != '\0' ? address : "about:blank",
				NULL,
				flags);
	}
}

static void
action_name_association (GtkAction *action,
			 char *action_name,
			 char *address,
			 gboolean is_drag_action)
{
	if (strcmp (action_name, "FileNewTab") == 0)
	{
		if (is_drag_action)
		{
			blx_link_open (BLX_LINK (action),
					address, NULL,
					BLX_LINK_NEW_TAB | BLX_LINK_JUMP_TO);
		}
		else
		{
			blx_home_action_open (action, 
					       address, 
					       BLX_LINK_NEW_TAB | BLX_LINK_JUMP_TO);
		}
	}
	else if (strcmp (action_name, "FileNewWindow") == 0)
	{
		if (is_drag_action)
		{
			blx_link_open (BLX_LINK (action),
					address, NULL,
					BLX_LINK_NEW_WINDOW);
		}
		else
		{
			blx_home_action_open (action,
					       address,
					       BLX_LINK_NEW_WINDOW);
		}
	}
	else if (strcmp (action_name, "GoHome") == 0)
	{
		blx_link_open (BLX_LINK (action),
				address != NULL && address[0] != '\0' ? address : "about:blank",
				NULL,
				blx_link_flags_from_current_event ());
	}
}	

static void
blx_home_action_activate (GtkAction *action)
{
	char *action_name;

	g_object_get (G_OBJECT (action), "name", &action_name, NULL);
		
	action_name_association (action, action_name, "about:blank", FALSE);

	g_free (action_name);
}

static void
home_action_drag_data_received_cb (GtkWidget* widget,
				   GdkDragContext *context,
				   gint x,
				   gint y,
				   GtkSelectionData *selection_data,
				   guint info,
				   guint time,
				   BlxHomeAction *action)
{
	GdkAtom       target;
	gchar        *action_name;
	const guchar *data;
	gint          length;
			
	g_object_get (action, "name", &action_name, NULL);
	
	g_signal_stop_emission_by_name (widget, "drag_data_received");

	length = gtk_selection_data_get_length (selection_data);
	data = gtk_selection_data_get_data (selection_data);
	target = gtk_selection_data_get_target (selection_data);

	if (length <= 0 || data == NULL) return;

	if (target == gdk_atom_intern (BLX_DND_URL_TYPE, FALSE))
	{
		char **split;

		split = g_strsplit ((const gchar *) data, "\n", 2);
		if (split != NULL && split[0] != NULL && split[0][0] != '\0')
		{			
			action_name_association (GTK_ACTION (action), 
						 action_name, split[0], TRUE);
		}
		g_strfreev (split);
	}
	else if (target == gdk_atom_intern (BLX_DND_URI_LIST_TYPE, FALSE))
	{
		char **uris;
		int i;
	
		uris = gtk_selection_data_get_uris (selection_data);
		if (uris == NULL) return;

		for (i = 0; uris[i] != NULL && i < INSANE_NUMBER_OF_URLS; i++)
		{
			action_name_association (GTK_ACTION (action),
						 action_name, uris[i], TRUE);
		}

		g_strfreev (uris);
	}
	else
	{
		char *text;
	       
		text = (char *) gtk_selection_data_get_text (selection_data);
		if (text != NULL) 
		{			
			action_name_association (GTK_ACTION (action),
						 action_name, text, TRUE);
		}
	}
}

static void
connect_proxy (GtkAction *action,
	       GtkWidget *proxy)
{
	GtkWidget *child;
	const gchar *action_name;
	
	GTK_ACTION_CLASS (blx_home_action_parent_class)->connect_proxy (action, proxy);
	
	action_name = gtk_action_get_name (action);

	if (GTK_IS_TOOL_ITEM (proxy) && (strcmp (action_name, "GoHome") != 0))
	{
		child = gtk_bin_get_child (GTK_BIN (proxy));
		g_signal_connect (child, "drag-data-received",
				  G_CALLBACK (home_action_drag_data_received_cb), action);
		gtk_drag_dest_set (child,
				   GTK_DEST_DEFAULT_ALL,
				   url_drag_types, G_N_ELEMENTS (url_drag_types),
				   GDK_ACTION_MOVE | GDK_ACTION_COPY);
		gtk_drag_dest_add_text_targets (child);
	}
}

static void
disconnect_proxy (GtkAction *action,
		  GtkWidget *proxy)
{
	g_signal_handlers_disconnect_by_func
		(proxy, G_CALLBACK (gtk_action_activate), action);

	GTK_ACTION_CLASS (blx_home_action_parent_class)->disconnect_proxy (action, proxy);
}

static void
blx_home_action_class_init (BlxHomeActionClass *class)
{
	GtkActionClass *action_class = GTK_ACTION_CLASS (class);
	
	action_class->activate = blx_home_action_activate;
	action_class->connect_proxy = connect_proxy;
	action_class->disconnect_proxy = disconnect_proxy;
}

static void
blx_home_action_init (BlxHomeAction *action)
{
        /* Empty, needed for G_DEFINE_TYPE macro */
}
