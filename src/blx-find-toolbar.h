/*
 *  Copyright © 2004  Tommi Komulainen
 *  Copyright © 2004, 2005  Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_FIND_TOOLBAR_H
#define BLX_FIND_TOOLBAR_H

#include <gtk/gtk.h>

#include "blx-window.h"

G_BEGIN_DECLS

#define BLX_TYPE_FIND_TOOLBAR		(blx_find_toolbar_get_type ())
#define BLX_FIND_TOOLBAR(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_FIND_TOOLBAR, BlxFindToolbar))
#define BLX_FIND_TOOLBAR_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_FIND_TOOLBAR, BlxFindToolbarClass))
#define BLX_IS_FIND_TOOLBAR(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_FIND_TOOLBAR))
#define BLX_IS_FIND_TOOLBAR_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_FIND_TOOLBAR))
#define BLX_FIND_TOOLBAR_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_FIND_TOOLBAR, BlxFindToolbarClass))

typedef struct _BlxFindToolbar		BlxFindToolbar;
typedef struct _BlxFindToolbarPrivate	BlxFindToolbarPrivate;
typedef struct _BlxFindToolbarClass	BlxFindToolbarClass;

struct _BlxFindToolbar
{
	GtkToolbar parent;

	/*< private >*/
	BlxFindToolbarPrivate *priv;
};

struct _BlxFindToolbarClass
{
	GtkToolbarClass parent_class;

	/* Signals */
	void (* next)		(BlxFindToolbar *toolbar);
	void (* previous)	(BlxFindToolbar *toolbar);
	void (* close)		(BlxFindToolbar *toolbar);
};

GType		 blx_find_toolbar_get_type	 (void) G_GNUC_CONST;

BlxFindToolbar *blx_find_toolbar_new		 (BlxWindow *window);

const char	*blx_find_toolbar_get_text	 (BlxFindToolbar *toolbar);

void		 blx_find_toolbar_set_embed	 (BlxFindToolbar *toolbar,
						  BlxEmbed *embed);

void		 blx_find_toolbar_find_next	 (BlxFindToolbar *toolbar);

void		 blx_find_toolbar_find_previous (BlxFindToolbar *toolbar);

void		 blx_find_toolbar_open		 (BlxFindToolbar *toolbar,
						  gboolean links_only,
						  gboolean clear_search);

void		 blx_find_toolbar_close	 (BlxFindToolbar *toolbar);

void		 blx_find_toolbar_request_close (BlxFindToolbar *toolbar);

G_END_DECLS

#endif /* BLX_FIND_TOOLBAR_H */
