/*
 *  Copyright © 2003, 2004 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *  Copyright © 2008 Jan Alonzo
 *  Copyright © 2009 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "config.h"
#include "blx-navigation-history-action.h"

#include "blx-action-helper.h"
#include "blx-debug.h"
#include "blx-embed-container.h"
#include "blx-embed-shell.h"
#include "blx-embed-utils.h"
#include "blx-gui.h"
#include "blx-history.h"
#include "blx-link.h"
#include "blx-shell.h"
#include "blx-type-builtins.h"
#include "blx-window.h"

#include <gtk/gtk.h>
#include <webkit/webkit.h>

#define HISTORY_ITEM_DATA_KEY "HistoryItem"

#define BLX_NAVIGATION_HISTORY_ACTION_GET_PRIVATE(object)		\
  (G_TYPE_INSTANCE_GET_PRIVATE ((object),				\
				BLX_TYPE_NAVIGATION_HISTORY_ACTION,	\
				BlxNavigationHistoryActionPrivate))

typedef enum {
  WEBKIT_HISTORY_BACKWARD,
  WEBKIT_HISTORY_FORWARD
} WebKitHistoryType;

struct _BlxNavigationHistoryActionPrivate {
  BlxNavigationHistoryDirection direction;
  BlxHistory *history;
};

enum {
  PROP_0,
  PROP_DIRECTION
};

static void blx_navigation_history_action_init       (BlxNavigationHistoryAction *action);
static void blx_navigation_history_action_class_init (BlxNavigationHistoryActionClass *klass);

G_DEFINE_TYPE (BlxNavigationHistoryAction, blx_navigation_history_action, BLX_TYPE_NAVIGATION_ACTION)

static void
blx_history_cleared_cb (BlxHistory *history,
                         BlxNavigationHistoryAction *action)
{
  blx_action_change_sensitivity_flags (GTK_ACTION (action), SENS_FLAG, TRUE);
}

static void
action_activate (GtkAction *action)
{
  BlxNavigationHistoryAction *history_action;
  BlxWindow *window;
  BlxEmbed *embed;
  WebKitWebView *web_view;

  history_action = BLX_NAVIGATION_HISTORY_ACTION (action);
  window = _blx_navigation_action_get_window (BLX_NAVIGATION_ACTION (action));
  embed = blx_embed_container_get_active_child (BLX_EMBED_CONTAINER (window));
  g_return_if_fail (embed != NULL);

  web_view = BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed);

  /* We use blx_link_action_get_button on top of
   * blx_gui_is_middle_click because of the hacks we have to do to
   * fake middle clicks on tool buttons. Read the documentation of
   * blx_link_action_get_button for more details. */
  if (history_action->priv->direction == BLX_NAVIGATION_HISTORY_DIRECTION_BACK) {
    if (blx_gui_is_middle_click () ||
        blx_link_action_get_button (BLX_LINK_ACTION (history_action)) == 2) {
      embed = blx_shell_new_tab (blx_shell_get_default (),
                                  BLX_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (embed))),
                                  embed,
                                  NULL,
                                  BLX_NEW_TAB_IN_EXISTING_WINDOW);
      web_view = BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed);
    }
    webkit_web_view_go_back (web_view);
  } else if (history_action->priv->direction == BLX_NAVIGATION_HISTORY_DIRECTION_FORWARD) {
    if (blx_gui_is_middle_click () ||
        blx_link_action_get_button (BLX_LINK_ACTION (history_action)) == 2) {
      const char *forward_uri;
      WebKitWebHistoryItem *forward_item;
      WebKitWebBackForwardList *history;

      /* Forward history is not copied when opening
         a new tab, so get the forward URI manually
         and load it */
      history = webkit_web_view_get_back_forward_list (BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed));
      forward_item = webkit_web_back_forward_list_get_forward_item (history);
      forward_uri = webkit_web_history_item_get_original_uri (forward_item);

      embed = blx_shell_new_tab (blx_shell_get_default (),
                                  BLX_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (embed))),
                                  embed,
                                  NULL,
                                  BLX_NEW_TAB_IN_EXISTING_WINDOW);

      web_view = BLX_GET_WEBKIT_WEB_VIEW_FROM_EMBED (embed);
      webkit_web_view_load_uri (web_view, forward_uri);
    } else {
      webkit_web_view_go_forward (web_view);
    }
  }
}

static void
blx_navigation_history_action_init (BlxNavigationHistoryAction *action)
{
  BlxHistory *history;

  action->priv = BLX_NAVIGATION_HISTORY_ACTION_GET_PRIVATE (action);

  history = BLX_HISTORY (blx_embed_shell_get_global_history (embed_shell));
  action->priv->history = BLX_HISTORY (g_object_ref (history));

  g_signal_connect (action->priv->history,
                    "cleared", G_CALLBACK (blx_history_cleared_cb),
                    action);
}

static void
blx_navigation_history_action_finalize (GObject *object)
{
  BlxNavigationHistoryAction *action = BLX_NAVIGATION_HISTORY_ACTION (object);

  g_signal_handlers_disconnect_by_func (action->priv->history,
                                        blx_history_cleared_cb,
                                        action);
  g_object_unref (action->priv->history);
  action->priv->history = NULL;

  G_OBJECT_CLASS (blx_navigation_history_action_parent_class)->finalize (object);
}

static void
blx_navigation_history_action_set_property (GObject *object,
					     guint prop_id,
					     const GValue *value,
					     GParamSpec *pspec)
{
  BlxNavigationHistoryAction *nav = BLX_NAVIGATION_HISTORY_ACTION (object);

  switch (prop_id) {
  case PROP_DIRECTION:
    nav->priv->direction = g_value_get_int (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}

static void
blx_navigation_history_action_get_property (GObject *object,
					     guint prop_id,
					     GValue *value,
					     GParamSpec *pspec)
{
  BlxNavigationHistoryAction *nav = BLX_NAVIGATION_HISTORY_ACTION (object);

  switch (prop_id) {
  case PROP_DIRECTION:
    g_value_set_int (value, nav->priv->direction);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}

static void
blx_navigation_history_action_class_init (BlxNavigationHistoryActionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkActionClass *action_class = GTK_ACTION_CLASS (klass);

  object_class->finalize = blx_navigation_history_action_finalize;
  object_class->set_property = blx_navigation_history_action_set_property;
  object_class->get_property = blx_navigation_history_action_get_property;

  action_class->activate = action_activate;

  g_object_class_install_property (object_class,
				   PROP_DIRECTION,
				   g_param_spec_int ("direction", NULL, NULL,
						     0,
						     G_MAXINT,
						     0,
						     G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

  g_type_class_add_private (object_class, sizeof (BlxNavigationHistoryActionPrivate));
}
