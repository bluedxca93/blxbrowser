/*
 *  Copyright © 2003 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_GO_ACTION_H
#define BLX_GO_ACTION_H

#include "blx-link-action.h"

#define BLX_TYPE_GO_ACTION            (blx_go_action_get_type ())
#define BLX_GO_ACTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), BLX_TYPE_GO_ACTION, BlxGoAction))
#define BLX_GO_ACTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_GO_ACTION, BlxGoActionClass))
#define BLX_IS_GO_ACTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_GO_ACTION))
#define BLX_IS_GO_ACTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), BLX_TYPE_GO_ACTION))
#define BLX_GO_ACTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), BLX_TYPE_GO_ACTION, BlxGoActionClass))

typedef struct _BlxGoAction      BlxGoAction;
typedef struct _BlxGoActionClass BlxGoActionClass;

struct _BlxGoAction
{
	BlxLinkAction parent;
};

struct _BlxGoActionClass
{
	BlxLinkActionClass parent_class;
};

GType    blx_go_action_get_type   (void);

#endif
