/*
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_EXTENSIONS_MANAGER_H
#define BLX_EXTENSIONS_MANAGER_H

#include "blx-extension.h"
#include "blx-node.h"

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define BLX_TYPE_EXTENSIONS_MANAGER		(blx_extensions_manager_get_type ())
#define BLX_EXTENSIONS_MANAGER(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_EXTENSIONS_MANAGER, BlxExtensionsManager))
#define BLX_EXTENSIONS_MANAGER_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_EXTENSIONS_MANAGER, BlxExtensionsManagerClass))
#define BLX_IS_EXTENSIONS_MANAGER(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_EXTENSIONS_MANAGER))
#define BLX_IS_EXTENSIONS_MANAGER_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_EXTENSIONS_MANAGER))
#define BLX_EXTENSIONS_MANAGER_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_EXTENSIONS_MANAGER, BlxExtensionsManagerClass))

typedef struct _BlxExtensionsManager		BlxExtensionsManager;
typedef struct _BlxExtensionsManagerClass	BlxExtensionsManagerClass;
typedef struct _BlxExtensionsManagerPrivate	BlxExtensionsManagerPrivate;

typedef struct
{
	char *identifier;
	GKeyFile *keyfile;
	guint active  :1;
	guint enabled :1;
} BlxExtensionInfo;
	
struct _BlxExtensionsManagerClass
{
	GObjectClass parent_class;

	/* Signals */
	void	(* added)	(BlxExtensionsManager *manager,
				 BlxExtensionInfo *info);
	void	(* changed)	(BlxExtensionsManager *manager,
				 BlxExtensionInfo *info);
	void	(* removed)	(BlxExtensionsManager *manager,
				 BlxExtensionInfo *info);
};

struct _BlxExtensionsManager
{
	GObject parent_instance;

	/*< private >*/
	BlxExtensionsManagerPrivate *priv;
};

GType	  blx_extensions_manager_get_type	 (void);

void	  blx_extensions_manager_startup	 (BlxExtensionsManager *manager);

void	  blx_extensions_manager_load		 (BlxExtensionsManager *manager,
						  const char *identifier);

void	  blx_extensions_manager_unload	 (BlxExtensionsManager *manager,
						  const char *identifier);

void	  blx_extensions_manager_register	 (BlxExtensionsManager *manager,
						  GObject *object);

GList	 *blx_extensions_manager_get_extensions (BlxExtensionsManager *manager);

G_END_DECLS

#endif
