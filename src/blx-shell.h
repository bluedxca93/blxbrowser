/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 *  Copyright © 2000-2004 Marco Pesenti Gritti
 *  Copyright © 2003, 2004, 2006 Christian Persch
 *  Copyright © 2011 Igalia S.L.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_SHELL_H
#define BLX_SHELL_H

#include "blx-embed-shell.h"
#include "blx-bookmarks.h"
#include "blx-window.h"
#include "blx-embed.h"

#include <webkit/webkit.h>
#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define BLX_TYPE_SHELL         (blx_shell_get_type ())
#define BLX_SHELL(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), BLX_TYPE_SHELL, BlxShell))
#define BLX_SHELL_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), BLX_TYPE_SHELL, BlxShellClass))
#define BLX_IS_SHELL(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), BLX_TYPE_SHELL))
#define BLX_IS_SHELL_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BLX_TYPE_SHELL))
#define BLX_SHELL_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BLX_TYPE_SHELL, BlxShellClass))

typedef struct _BlxShell   BlxShell;
typedef struct _BlxShellClass    BlxShellClass;
typedef struct _BlxShellPrivate  BlxShellPrivate;

extern BlxShell *blx_shell;

typedef enum {
  /* Page types */
  BLX_NEW_TAB_HOME_PAGE    = 1 << 0,
  BLX_NEW_TAB_NEW_PAGE   = 1 << 1,
  BLX_NEW_TAB_OPEN_PAGE    = 1 << 2,

  /* Page mode */
  BLX_NEW_TAB_FULLSCREEN_MODE  = 1 << 4,
  BLX_NEW_TAB_DONT_SHOW_WINDOW = 1 << 5,

  /* Tabs */
  BLX_NEW_TAB_APPEND_LAST  = 1 << 7,
  BLX_NEW_TAB_APPEND_AFTER = 1 << 8,
  BLX_NEW_TAB_JUMP   = 1 << 9,
  BLX_NEW_TAB_IN_NEW_WINDOW  = 1 << 10,
  BLX_NEW_TAB_IN_EXISTING_WINDOW = 1 << 11,

  /* The way to load */
  BLX_NEW_TAB_FROM_EXTERNAL      = 1 << 12,
  BLX_NEW_TAB_DONT_COPY_HISTORY  = 1 << 13,
  
} BlxNewTabFlags;

typedef enum {
  BLX_STARTUP_NEW_TAB          = 1 << 0,
  BLX_STARTUP_NEW_WINDOW       = 1 << 1,
  BLX_STARTUP_BOOKMARKS_EDITOR = 1 << 2
} BlxStartupFlags;

typedef struct {
  BlxStartupFlags startup_flags;
  
  char *bookmarks_filename;
  char *session_filename;
  char *bookmark_url;
  
  char **arguments;
  
  guint32 user_time;
} BlxShellStartupContext;

struct _BlxShell {
  BlxEmbedShell parent;

  /*< private >*/
  BlxShellPrivate *priv;
};

struct _BlxShellClass {
  BlxEmbedShellClass parent_class;
};

GType           blx_new_tab_flags_get_type             (void) G_GNUC_CONST;

GType           blx_shell_get_type                     (void);

BlxShell      *blx_shell_get_default                  (void);

BlxEmbed      *blx_shell_new_tab                      (BlxShell *shell,
                                                         BlxWindow *parent_window,
                                                         BlxEmbed *previous_embed,
                                                         const char *url,
                                                         BlxNewTabFlags flags);

BlxEmbed      *blx_shell_new_tab_full                 (BlxShell *shell,
                                                         BlxWindow *parent_window,
                                                         BlxEmbed *previous_embed,
                                                         WebKitNetworkRequest *request,
                                                         BlxNewTabFlags flags,
                                                         BlxWebViewChrome chrome,
                                                         gboolean is_popup,
                                                         guint32 user_time);

GObject        *blx_shell_get_session                  (BlxShell *shell);

GObject        *blx_shell_get_net_monitor              (BlxShell *shell);

BlxBookmarks  *blx_shell_get_bookmarks                (BlxShell *shell);

GObject        *blx_shell_get_extensions_manager       (BlxShell *shell);

GtkWidget      *blx_shell_get_bookmarks_editor         (BlxShell *shell);

GtkWidget      *blx_shell_get_history_window           (BlxShell *shell);

GObject        *blx_shell_get_pdm_dialog               (BlxShell *shell);

GObject        *blx_shell_get_prefs_dialog             (BlxShell *shell);

void            blx_shell_set_startup_context          (BlxShell *shell,
                                                         BlxShellStartupContext  *ctx);

BlxShellStartupContext *blx_shell_startup_context_new (BlxStartupFlags startup_flags,
                                                         char            *bookmarks_filename,
                                                         char            *session_filename,
                                                         char            *bookmark_url,
                                                         char           **arguments,
                                                         guint32          user_time);

/* private API */
void           _blx_shell_create_instance              (BlxEmbedShellMode mode);

G_END_DECLS

#endif
