/*
 *  Copyright © 2003, 2004 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_NAVIGATION_ACTION_H
#define BLX_NAVIGATION_ACTION_H

#include "blx-link-action.h"
#include "blx-window.h"

G_BEGIN_DECLS

#define BLX_TYPE_NAVIGATION_ACTION            (blx_navigation_action_get_type ())
#define BLX_NAVIGATION_ACTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), BLX_TYPE_NAVIGATION_ACTION, BlxNavigationAction))
#define BLX_NAVIGATION_ACTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_NAVIGATION_ACTION, BlxNavigationActionClass))
#define BLX_IS_NAVIGATION_ACTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_NAVIGATION_ACTION))
#define BLX_IS_NAVIGATION_ACTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), BLX_TYPE_NAVIGATION_ACTION))
#define BLX_NAVIGATION_ACTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), BLX_TYPE_NAVIGATION_ACTION, BlxNavigationActionClass))

typedef struct _BlxNavigationAction		BlxNavigationAction;
typedef struct _BlxNavigationActionPrivate	BlxNavigationActionPrivate;
typedef struct _BlxNavigationActionClass	BlxNavigationActionClass;

struct _BlxNavigationAction
{
	BlxLinkAction parent;

	/*< private >*/
	BlxNavigationActionPrivate *priv;
};

struct _BlxNavigationActionClass
{
	BlxLinkActionClass parent_class;
};

GType blx_navigation_action_get_type (void);

/*< Protected >*/

BlxWindow     *_blx_navigation_action_get_window       (BlxNavigationAction *action);

G_END_DECLS

#endif
