/*
 *  Copyright © 2003 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#if !defined (__BLX_BLXBROWSER_H_INSIDE__) && !defined (BLXBROWSER_COMPILATION)
#error "Only <blxbrowser/blxbrowser.h> can be included directly."
#endif

#ifndef BLX_LOCATION_ACTION_H
#define BLX_LOCATION_ACTION_H

#include "blx-link-action.h"

G_BEGIN_DECLS

#define BLX_TYPE_LOCATION_ACTION            (blx_location_action_get_type ())
#define BLX_LOCATION_ACTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), BLX_TYPE_LOCATION_ACTION, BlxLocationAction))
#define BLX_LOCATION_ACTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), BLX_TYPE_LOCATION_ACTION, BlxLocationActionClass))
#define BLX_IS_LOCATION_ACTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLX_TYPE_LOCATION_ACTION))
#define BLX_IS_LOCATION_ACTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), BLX_TYPE_LOCATION_ACTION))
#define BLX_LOCATION_ACTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), BLX_TYPE_LOCATION_ACTION, BlxLocationActionClass))

typedef struct _BlxLocationAction		BlxLocationAction;
typedef struct _BlxLocationActionPrivate	BlxLocationActionPrivate;
typedef struct _BlxLocationActionClass		BlxLocationActionClass;

struct _BlxLocationAction
{
	BlxLinkAction parent;

	/*< private >*/
	BlxLocationActionPrivate *priv;
};

struct _BlxLocationActionClass
{
	BlxLinkActionClass parent_class;

	/* Signals */
	void (* lock_clicked)	(BlxLocationAction *action);
};

GType		blx_location_action_get_type		(void);

const char     *blx_location_action_get_address	(BlxLocationAction *action);

void		blx_location_action_set_address	(BlxLocationAction *action,
							 const char *address);

G_END_DECLS

#endif
