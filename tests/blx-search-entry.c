/* vim: set sw=2 ts=2 sts=2 et: */
/*
 * blx-search-entry.c
 * This file is part of Blxbrowser
 *
 * Copyright © 2008 - Diego Escalante Urrelo
 *
 * Blxbrowser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Blxbrowser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blxbrowser; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

#include "config.h"
#include "blx-search-entry.h"
#include <glib.h>
#include <gtk/gtk.h>

static void
test_entry_new (void)
{

  BlxSearchEntry *entry;
  entry = BLX_SEARCH_ENTRY (blx_search_entry_new ());

  g_assert (GTK_IS_WIDGET (entry));
  g_assert (GTK_IS_ENTRY (entry));
}

static void
test_entry_clear (void)
{
  const char *set = "test";
  const char *get = NULL;

  BlxSearchEntry *entry;
  entry = BLX_SEARCH_ENTRY (blx_search_entry_new ());

  gtk_entry_set_text (GTK_ENTRY (entry), set);
  get = gtk_entry_get_text (GTK_ENTRY (entry));

  g_assert_cmpstr (set, ==, get);

  /* At this point, the text in the entry is either 'vanilla' or the
   * contents of 'set' char*
   */
  blx_search_entry_clear (BLX_SEARCH_ENTRY (entry));
  get = gtk_entry_get_text (GTK_ENTRY (entry));

  g_assert_cmpstr ("", ==, get);
}

int
main (int argc, char *argv[])
{
  gtk_test_init (&argc, &argv);

  g_test_add_func ("/lib/widgets/blx-search-entry/new",
                   test_entry_new);
  g_test_add_func ("/lib/widgets/blx-search-entry/clear",
                   test_entry_clear);

  return g_test_run ();
}
